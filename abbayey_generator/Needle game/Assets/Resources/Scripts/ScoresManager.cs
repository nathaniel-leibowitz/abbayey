﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ScoresManager : MonoBehaviour {

    public InputField retries;
    public InputField time;
    public InputField performanceAssesment;


    private int m_numRetries = 0;
    private float elapsedTime = 0;
    private int currentCounter = 0;
    private int numChallenges = 0;
    private int numSuccess = 0;
    private bool gameOn = false;
    private int maxRetries = 0;
    private int maxSeconds = 0;
    private List<Vector2> hits = new List<Vector2>();
    private bool m_success;

    

    // Use this for initialization
    void Start()
    {
        Reset();
        retries.gameObject.SetActive(false);
        time.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update()
    {
        updateTime();
    }

    void updateTime()
    {
        if (gameOn)
            elapsedTime += Time.deltaTime;
        if ((int)elapsedTime > currentCounter)
        {
            currentCounter = (int)elapsedTime;
            time.text = currentCounter.ToString() + "/" + maxSeconds.ToString();
            retries.text = "Retries:" + m_numRetries + "/" + maxRetries.ToString();
        }
    }

    public void startTrials(int numberChallenges)
    {
        Reset();
        retries.gameObject.SetActive(true);
        time.gameObject.SetActive(true);
        gameOn = true;
        numChallenges = numberChallenges;

        maxRetries = (int)(0.15 * numberChallenges);
        maxSeconds = 10 * numberChallenges;
        performanceAssesment.text = "";
        performanceAssesment.gameObject.SetActive(false);
        hits = new List<Vector2>();
    }

    public void trial(bool success, float x, float y)
    {
        if (success)
        {
            numSuccess++;
            hits.Add(new Vector2(x, y));
            if (hits.Count > numChallenges)
            {
                Debug.LogError(hits.Count + " hits when only " + numChallenges + " challenges");
                Assert.IsTrue(hits.Count > numChallenges);
            }
            if (finishedAllChallenges())
            {
                gameOn = false;
                PerformanceAssesment();
            }
        }
        else
        {
            m_numRetries++;
            retries.text = "Retries:" + m_numRetries;
        }
    }

    public bool finishedAllChallenges()
    {
        return (numSuccess == numChallenges);
    }

    public bool completedSuccessfully()
    {
        return m_success;
    }

    private void Reset()
    {
        m_numRetries = 0;
        elapsedTime = 0;
        currentCounter = 0;
        numChallenges = 0;
        numSuccess = 0;
        gameOn = false;
        performanceAssesment.gameObject.SetActive(false);
        m_success = false;
    }

    public void Clear()
    {
        Reset();
        retries.gameObject.SetActive(false);
        time.gameObject.SetActive(false);
        performanceAssesment.gameObject.SetActive(false);
    }

    private void PerformanceAssesment()
    {
        Text text = performanceAssesment.transform.Find("Text").GetComponent<Text>();
        bool hitsAreOrdered = HitsAreOrdered();

        if ((m_numRetries <= maxRetries) && (elapsedTime <= maxSeconds) && hitsAreOrdered)
        {
            performanceAssesment.text = new string("!יישר כח! מדויק! מהיר! לפי הסדר!!".ToCharArray().Reverse().ToArray());
            text.color = Color.green;
            m_success = true;
        }
        else
        {
            text.color = Color.red;
            performanceAssesment.text = new string(" אמנם מדויק אך".ToCharArray().Reverse().ToArray());
            performanceAssesment.text = performanceAssesment.text + "\n";
            if (m_numRetries > maxRetries)
                performanceAssesment.text = performanceAssesment.text + new string(" יותר מידי תיקונים".ToCharArray().Reverse().ToArray());
            performanceAssesment.text = performanceAssesment.text + "\n";
            if (elapsedTime > maxSeconds)
                performanceAssesment.text = performanceAssesment.text + new string("לאט מידי".ToCharArray().Reverse().ToArray());
            if (!hitsAreOrdered)
                performanceAssesment.text = performanceAssesment.text + new string("לא לפי הסדר".ToCharArray().Reverse().ToArray());
        }

        performanceAssesment.gameObject.SetActive(true);
    }

    private bool HitsAreOrdered()
    {
        bool first = true;
        Vector2 prev = new Vector2(-1, -1);
        foreach (Vector2 hit in hits)
        {
            Debug.Log(hit);
        }
        foreach (Vector2 hit in hits)
        {
            if (first)
                first = false;
            else
            {
                if (hit.y > prev.y)
                    return false;
                else if ((hit.y == prev.y) && (hit.x > prev.x))
                    return false;
            }
            prev = hit;
        }
        return true;
    }
}
