﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlManager : MonoBehaviour {

    public InputField saveFileName;
    public InputField scaleInput;
    public InputField levelInputField;
    public InputField dbgResolutionInputField;
    public HotspotFactory factory;
    public DafFactory dafFactory;
    public Dropdown loadFileDropdown;
    public Dropdown loadDafDropdown;
    public Dropdown loadSugiyaDropdown;
    public Toggle toggleEdit;
    public Toggle toggleMeters;
    public ScoresManager scores;
    public Button saveButton;
    public Button nextButton;
    public Button prevButton;
    public Button teacherShortcutButton;
    public Button SolveButton;
    public Button clear;
    public Button scaleButton;
    public GameObject AddHotspots;
    public GameObject AddIndexes;
    public Button startButton;
    public Button copyrightAcceptButton;
    public GameObject copyrightNotice;
    public Toggle enableAnimation;

    private GameObject m_line1TopText;
    private GameObject m_line1BottomText;
    private GameObject m_line5TopText;
    private string m_sugiaFileName;
    private string m_dafFileName;
    private Level m_level = Level.Copyright;
    private bool m_editMode;
    private Resolution m_currentResolution;



    public enum Level { Copyright, Index, Translate, Syntax, Replace, Question };


    // Use this for initialization
    void Start ()
    {
        m_currentResolution = Screen.currentResolution;
        float widthRatio = (float)(m_currentResolution.width) / (float)1920;
        Debug.LogError("currentResolution=" + m_currentResolution + " currentRatio= " + (float)m_currentResolution.width / (float)m_currentResolution.height +
            " widthRatio=" + widthRatio + " heightRatio=" + (float)m_currentResolution.height / 1080);
        dbgResolutionInputField.text = widthRatio.ToString();

        m_line1TopText = GameObject.Find("line1_top_text");
        m_line1BottomText = GameObject.Find("line1_bottom_text");
        m_line5TopText = GameObject.Find("line5_top_text");
        if (m_line1TopText == null) throw new UnityException("failed finding line1_top_text");
        if (m_line1BottomText == null) throw new UnityException("failed finding line1_bottom_text");
        if (m_line5TopText == null) throw new UnityException("failed finding line5_top_text");

        m_line1TopText.SetActive(false);
        m_line1BottomText.SetActive(false);
        m_line5TopText.SetActive(false);
        //PopulateLoadFileOptions(loadFileDropdown, "sugiot", "*.cfg");
        PopulateLoadFileOptions(loadFileDropdown, "dapim//kidushin", "*.cfg");

        toggleEdit.isOn = toggleMeters.isOn = false;
        SetEditMode(false);
        loadSugiyaDropdown.gameObject.SetActive(false);
        ChangeLevel(Level.Copyright);
        nextButton.interactable = false;
        prevButton.interactable = false;
        SolveButton.interactable = false;
        m_editMode = false;

        AddIndexes.SetActive(true);
        AddHotspots.SetActive(false);
        prevButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(false);
        startButton.gameObject.SetActive(false);
        teacherShortcutButton.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update ()
    {
        if (m_level == Level.Translate && scores.completedSuccessfully())
            nextButton.interactable = true;

        if (m_level == Level.Syntax && scores.completedSuccessfully() && enableAnimation.isOn)
            HotspotFactory.m_animateMode = true;
        else
            HotspotFactory.m_animateMode = false;

        if (m_editMode)
        {
            nextButton.gameObject.SetActive(true);
            prevButton.gameObject.SetActive(true);
            nextButton.interactable = true;
            prevButton.interactable = true;
        }
        saveButton.interactable = (m_currentResolution.width == 1920 && m_currentResolution.height == 1080);
    }

    public void NextLevel()
    {
        if (m_level == Level.Index)
            ChangeLevel(Level.Translate);
        else if (m_level == Level.Translate)
            ChangeLevel(Level.Syntax);
    }

    public void PrevLevel()
    {
        if (m_level == Level.Syntax)
            ChangeLevel(Level.Translate);
        else if (m_level == Level.Translate)
            ChangeLevel(Level.Index);
    }

    public void SetSugiya(string sugiya)
    {
        m_sugiaFileName = sugiya;
    }

    public void ChangeLevel(Level to)
    {
        //from
        switch (m_level)
        {
            case Level.Index:
                dafFactory.Clear();
                break;
            case Level.Translate:
                factory.Clear();
                scores.Clear();
                teacherShortcutButton.gameObject.SetActive(false);
                break;
            case Level.Syntax:
                factory.Clear();
                scores.Clear();
                teacherShortcutButton.gameObject.SetActive(false);
                break;
        }


        m_level = to;
        switch (to)
        {
            case Level.Syntax:
                nextButton.interactable = false;
                prevButton.interactable = true;
                teacherShortcutButton.gameObject.SetActive(true);
                SolveButton.gameObject.SetActive(true);
                SolveButton.interactable = false;
                LoadSugiya();
                AddIndexes.SetActive(false);
                AddHotspots.SetActive(true);
                loadFileDropdown.gameObject.SetActive(false);
                startButton.gameObject.SetActive(true);
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
                PopulateLoadFileOptions(loadDafDropdown, "Assets\\Resources\\Sprites\\sugiya", "*.PNG");
                break;
            case Level.Translate:
                nextButton.interactable = false;
                prevButton.interactable = true;
                LoadSugiya();
                AddIndexes.SetActive(false);
                AddHotspots.SetActive(true);
                loadFileDropdown.gameObject.SetActive(false);
                startButton.gameObject.SetActive(true);
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
                teacherShortcutButton.gameObject.SetActive(true);
                SolveButton.gameObject.SetActive(false);
                PopulateLoadFileOptions(loadDafDropdown, "Assets\\Resources\\Sprites\\sugiya", "*.PNG");
                break;
            case Level.Copyright:
                nextButton.interactable = false;
                prevButton.interactable = false;
                //LoadDafConfigFile(m_dafFileName);
                //saveFileName.text = m_dafFileName;
                //AddIndexes.SetActive(true);
                AddHotspots.SetActive(false);
                loadFileDropdown.gameObject.SetActive(false);
                startButton.gameObject.SetActive(false);
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(false);
                SolveButton.gameObject.SetActive(false);
                copyrightAcceptButton.gameObject.SetActive(true);
                copyrightNotice.SetActive(true);
                break;
            case Level.Index:
                nextButton.interactable = false;
                prevButton.interactable = false;
                LoadDafConfigFile(m_dafFileName);
                saveFileName.text = m_dafFileName;
                AddIndexes.SetActive(true);
                AddHotspots.SetActive(false);
                loadFileDropdown.gameObject.SetActive(true);
                startButton.gameObject.SetActive(false);
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(false);
                copyrightAcceptButton.gameObject.SetActive(false);
                copyrightNotice.SetActive(false);
                SolveButton.gameObject.SetActive(false);
                PopulateLoadFileOptions(loadDafDropdown, "Assets\\Resources\\Sprites\\daf\\kidushin", "*.PNG");
                break;
        }
        levelInputField.text = m_level.ToString();
    }


    public void Quit()
    {
        Application.Quit();
    }

    public void Save()
    {
        /*factory.SaveConfig();
        string json = JsonUtility.ToJson(HotspotFactory.m_config);
        Debug.Log(json);
        System.IO.File.WriteAllText("sugiot//" + saveFileName.text + ".cfg", json);
        PopulateLoadFileOptions(loadFileDropdown, "sugiot", "*.cfg");*/
    }

    public void SaveDaf()
    {
        string json = null;
        switch (m_level)
        {
            case Level.Index:
                dafFactory.SaveDafConfig();
                json = JsonUtility.ToJson(DafFactory.m_dafConfig);
                System.IO.File.WriteAllText("dapim//kidushin//" + saveFileName.text + ".cfg", json);
                Debug.Log(json);
                break;
            case Level.Translate:
            case Level.Syntax:
                string abbayeyConfPrefix = (m_level == Level.Translate) ? "translate_prefix.js" : "syntax_prefix.js";
                string abbayeyConfSuffix = (m_level == Level.Translate) ? "translate_suffix.js" : "syntax_suffix.js";
                factory.SaveConfig(generateExerciseName());
                json = JsonUtility.ToJson(HotspotFactory.m_config);
                System.IO.File.WriteAllText("sugiot//" + saveFileName.text + "." + m_level.ToString(), json);
                json = JsonUtility.ToJson(HotspotFactory.m_config.hotspots);
                System.IO.File.WriteAllText("sugiot//" + saveFileName.text + "." + m_level.ToString() + "_json", json);
                System.IO.File.WriteAllText("sugiot//" + generateExerciseName() + ".js",
                    System.IO.File.ReadAllText("sugiot//" + abbayeyConfPrefix) +
                    abbayeyConfPrefix + 
                    HotspotFactory.m_config.createWebConfig() +
                    System.IO.File.ReadAllText("sugiot//" + abbayeyConfSuffix));
                Debug.Log(json);
                break;
        }
    }

    public string generateExerciseName()
    {
        return saveFileName.text + "_" + WebLevelNames(m_level);
    }

    public void SaveSugiya()
    {
        factory.SaveConfig(generateExerciseName());
        string json = JsonUtility.ToJson(HotspotFactory.m_config);
        Debug.Log(json);
        System.IO.File.WriteAllText("sugiot//" + saveFileName.text + "." + m_level.ToString(), json);
        //PopulateLoadFileOptions(loadFileDropdown, "dapim//kidushin", "*.cfg");
    }

    public void StartGame()
    {
        //if (loadFileDropdown.value == 0)
            //return;
        LoadSugiya();
        if (HotspotFactory.m_config.symbols.Count == 0)
            return;
        HotspotFactory.m_gameOn = true;
        scores.startTrials(HotspotFactory.m_config.symbols.Count);
    }

    public void LoadDafConfigFile(int optionNum)
    {
        m_dafFileName = loadFileDropdown.options[optionNum].text;
        ChangeLevel(Level.Index);
    }

    /*INDEX
    public void Load(int optionNum)
    {
        HotspotFactory.m_gameOn = false;
        scores.reset();
        //m_editMode.isOn = false;
        HotspotFactory.m_config.symbols.Clear();

        factory.LoadIndex("kidushin/32a");
    }*/

    public void LoadRawDaf(int optionNum)
    {
        Debug.Log(" LoadRawDaf option " + optionNum);
        switch (m_level)
        {
            case Level.Index:
                DafFactory.m_dafConfig.m_dafName = loadDafDropdown.options[optionNum].text;
                dafFactory.LoadRawDaf(loadDafDropdown.options[optionNum].text);
                saveFileName.text = DafFactory.m_dafConfig.m_dafName;
                break;
            case Level.Translate:
            case Level.Syntax:
                HotspotFactory.m_config.m_sugiya = loadDafDropdown.options[optionNum].text;
                factory.LoadRawSugiya(loadDafDropdown.options[optionNum].text);
                saveFileName.text = m_sugiaFileName; ;// HotspotFactory.m_config.m_sugiya;
                break;
        }
    }

    public void LoadRawSugiya(int optionNum)
    {
        HotspotFactory.m_config.m_sugiya = loadSugiyaDropdown.options[optionNum].text;
        factory.LoadRawSugiya(loadSugiyaDropdown.options[optionNum].text);

        saveFileName.text = m_sugiaFileName; ;// HotspotFactory.m_config.m_sugiya;
    }

    public void SaveSugiyaToConfig(int optionNum)
    {
        HotspotFactory.m_config.m_sugiya = loadDafDropdown.options[optionNum].text;
    }

    public void LoadSugiya()
    {
        string fullFilename = "sugiot//" + m_sugiaFileName + "." + m_level.ToString();
        saveFileName.text = m_sugiaFileName;
        HotspotFactory.m_gameOn = false;
        //scores.Reset();
        //m_editMode.isOn = false;
        HotspotFactory.m_config.symbols.Clear();
        HotspotFactory.m_config.hotspots.hotspots.Clear();
        Debug.LogError("load input field = " + fullFilename);
        if (System.IO.File.Exists(fullFilename))
        {
            string confstring = System.IO.File.ReadAllText(fullFilename);
            Debug.Log(confstring);
            HotspotFactory.m_config = JsonUtility.FromJson<AbbayeyConfig>(confstring);
            if (HotspotFactory.m_config.m_symbolAreaBL.x == 0) HotspotFactory.m_config.m_symbolAreaBL.x = 10;
            if (HotspotFactory.m_config.m_symbolAreaBL.y == 0) HotspotFactory.m_config.m_symbolAreaBL.y = 100;
            if (HotspotFactory.m_config.m_symbolPosInc.x == 0) HotspotFactory.m_config.m_symbolPosInc.x = 40;
            if (HotspotFactory.m_config.m_symbolPosInc.y == 0) HotspotFactory.m_config.m_symbolPosInc.y = 60;
            if (HotspotFactory.m_config.m_fontSize == 0) HotspotFactory.m_config.m_fontSize = 30;
        }

        //TBD: we temporarily override. Should be modifyable in GUI
        HotspotFactory.m_config.m_symbolPosInc.x = 30;
        HotspotFactory.m_config.m_symbolPosInc.y = 45;
        float widthratio = dbgResolutionInputField.text == "" ? 1.0f : float.Parse(dbgResolutionInputField.text);
        Debug.Log("using widthRatio " + widthratio);

        HotspotFactory.m_config.m_meter_line1_bottom_text *= widthratio;
        HotspotFactory.m_config.m_meter_line1_top_text *= widthratio;
        HotspotFactory.m_config.m_meter_line2_top_text *= widthratio;
        HotspotFactory.m_config.m_symbolAreaBL *= widthratio;
        //HotspotFactory.m_config.m_symbolPosInc *= widthratio;
        HotspotFactory.m_config.m_fontSize *= widthratio;
        foreach (SymbolConfig symbol in HotspotFactory.m_config.symbols)
        {
            symbol.m_arroPosition *= widthratio;
            symbol.height *= widthratio;
        }
        factory.LoadConfig();

        AbbayeyConfig cfg = HotspotFactory.m_config;
        Transform tr = m_line1TopText.GetComponent<RectTransform>().transform;
        tr.position = new Vector3(tr.position.x, cfg.m_meter_line1_top_text == 0 ? 200 : cfg.m_meter_line1_top_text, tr.position.z);

        tr = m_line1BottomText.GetComponent<RectTransform>().transform;
        tr.position = new Vector3(tr.position.x, cfg.m_meter_line1_bottom_text == 0 ? 180 : cfg.m_meter_line1_bottom_text, tr.position.z);

        tr = m_line5TopText.GetComponent<RectTransform>().transform;
        tr.position = new Vector3(tr.position.x, cfg.m_meter_line2_top_text == 0 ? 120 : cfg.m_meter_line2_top_text, tr.position.z);
    }

    public void LoadDafConfigFile(string filename)
    {
        string fullFileName = "dapim//kidushin//" + filename + ".cfg";
        m_dafFileName = filename;
        factory.Clear();
        DafFactory.m_dafConfig.indexes.Clear();
        Debug.Log("load daf " + fullFileName);
        if (System.IO.File.Exists(fullFileName))
        {
            string confstring = System.IO.File.ReadAllText(fullFileName);
            Debug.Log(confstring);
            DafFactory.m_dafConfig = JsonUtility.FromJson<DafConfig>(confstring);
        }

        float widthratio = dbgResolutionInputField.text == "" ? 1.0f : float.Parse(dbgResolutionInputField.text);
        Debug.Log("using widthRatio " + widthratio);
        foreach (IndexConfig index in DafFactory.m_dafConfig.indexes)
        {
            index.height *= widthratio;
            index.m_center *= widthratio;
            index.width *= widthratio;
        }
        dafFactory.LoadConfig();
    }

    public void SetEditMode(bool edit)
    {
        Debug.Log("setting edit mode to " + edit);
        factory.SetEditMode(edit);
        m_editMode = edit;
        dafFactory.SetEditMode(edit);
        toggleMeters.gameObject.SetActive(edit);
        EnableMeters(toggleMeters.isOn && edit);
        clear.gameObject.SetActive(edit);
        saveFileName.gameObject.SetActive(edit);
        scaleInput.gameObject.SetActive(edit);
        loadDafDropdown.gameObject.SetActive(edit);
        scaleButton.gameObject.SetActive(edit);
        saveButton.gameObject.SetActive(edit);
    }

    public void Solve()
    {
        factory.DisplaySyntax();
    }


    public void EnableMeters(bool enable)
    {
        m_line1TopText.SetActive(enable);
        m_line1BottomText.SetActive(enable);
        m_line5TopText.SetActive(enable);  
    }

    public void PopulateLoadFileOptions(Dropdown dropdown, string directory, string searchPattern)
    {
        //string folder = System.IO.Path.GetDirectoryName(GetEntryAssembly().Location);
        //string folder = System.IO.Directory.GetCurrentDirectory();
        //System.IO.Directory.SetCurrentDirectory(@"C:/Users/nathaniel/Dropbox/motar/abbayey/executables/webgl/");
        string folder = System.IO.Directory.GetCurrentDirectory();
        Debug.LogError("current directory is " + folder);
        string[] files = System.IO.Directory.GetFiles(directory, searchPattern);
        if (files.Length > 0)
            Debug.Log("populating dropdown with files from " + directory + " filtered by " + searchPattern + " with " + files.Length + " files such as " + files[0]);
        Dropdown.OptionData first = dropdown.options[0];
        dropdown.ClearOptions();
        dropdown.options.Add(first);
        foreach (string f in files)
            dropdown.options.Add(new Dropdown.OptionData(System.IO.Path.GetFileNameWithoutExtension(f)));
        dropdown.value = 0;
        dropdown.Select();
        dropdown.RefreshShownValue();
    }

    public void AcceptCopyright()
    {
        ChangeLevel(Level.Index);
    }

    public void TeacherShortcut()
    {
        nextButton.interactable = true;
        if (m_level == Level.Syntax)
            SolveButton.interactable = true;
    }

    public string WebLevelNames(Level l)
    {
        if (l == Level.Translate)
            return "words";
        if (l == Level.Syntax)
            return "syntax";
        return "unidentified";

    }
}
