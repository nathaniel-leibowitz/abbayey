﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Linq;

public class DafFactory : MonoBehaviour
{
    public RectTransform prefabIndex;
    public GameObject indexParentObject;
    public float indexCenter_a;
    public float indexCenter_b;
    public float indexWidth;
    public float indexHeight;

    public static DafConfig m_dafConfig = new DafConfig();
    public static bool m_editMode;
    private Image dafImage;
    public InputField sugiyaName;

    void Start()
    {
        m_editMode = false;
    }

    // Update is called once per frame
    void Update()
    {
    }


    void OnGUI()
    {
        if (!m_editMode)
            return;

        Event e = Event.current;
        if (!e.isMouse)
            return;

        if (e.button ==1 && (e.type == EventType.MouseUp))
        {
            float center = Math.Abs(Input.mousePosition.x - indexCenter_a) < Math.Abs(Input.mousePosition.x - indexCenter_b) ? indexCenter_a : indexCenter_b;
            Vector3 position = new Vector3(center, Input.mousePosition.y, Input.mousePosition.z);
            if (sugiyaName.text.Contains("babakama"))
                addNewIndex(sugiyaName.text, position, indexWidth, indexHeight);
            else
                addNewIndex(new string(sugiyaName.text.ToCharArray().Reverse().ToArray()), position, indexWidth, indexHeight);
        }
    }

    void addNewIndex(string sugiyaName, Vector3 position, float width, float height)
    {

        Transform[] ts = indexParentObject.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in ts)
        {
            if (child.tag.Equals(prefabIndex.tag))
            {
                RectTransform rt = (RectTransform)(child.transform);
                if (sugiyaName.Equals(child.GetComponentInChildren<Text>(true).text))
                {
                    Debug.Log("xxx Sorry, index " + sugiyaName + " already exists");
                    return;
                }
            }
        }


        RectTransform sugiya = Instantiate(prefabIndex);
        sugiya.GetComponentInChildren<Text>().text = sugiyaName;
        sugiya.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        sugiya.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);

        sugiya.parent = indexParentObject.transform;
        sugiya.position = position;


        //float colliderHeight =50f;
        //float colliderWidth = 1000f;
        //sugiya.GetComponent<BoxCollider2D>().size = new Vector2(colliderWidth, colliderHeight);
    }

    public void SaveDafConfig()
    {
        m_dafConfig.indexes.Clear();
        Transform[] ts = indexParentObject.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in ts)
        {
            if (child.tag.Equals(prefabIndex.tag))
            {
                RectTransform rt = (RectTransform)(child.transform);
                m_dafConfig.indexes.Add(new IndexConfig(child.GetComponentInChildren<Text>(true).text, child.position, rt.rect.height, rt.rect.width));
                Debug.Log("!!!! adding index at " + child.position);
            }
        }
    }

    public void enableIndex(bool enable)
    {
        GameObject index = GameObject.Find("index");
        //Destroy(index);
        //SetActiveAllChildren(index.transform, enable);
        index.SetActive(enable);
    }

    public void SetActiveAllChildren(Transform transform, bool value)
    {
        foreach (Transform child in transform)
        {
            SetActiveAllChildren(child, value);
            child.gameObject.SetActive(value);
        }
    }

    public void SetEditMode(bool edit)
    {
        m_editMode = edit;
        sugiyaName.gameObject.SetActive(edit);
    }

    public void Clear()
    {
        Transform[] ts = indexParentObject.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in ts)
        {
            if (child.tag.Equals(prefabIndex.tag))
            {
                child.gameObject.SetActive(false);//because the destroy is not immediate, lets mark it as not active.
                Destroy(child.gameObject);
            }
        }
    }

    public void LoadRawDaf(string dafName)
    {
        /*GameObject[] indexes = GameObject.FindGameObjectsWithTag("index");
        foreach (GameObject index in indexes)
            DestroyImmediate(index);*/

        Clear();
        


        GameObject background = GameObject.Find("background");
        dafImage = background.GetComponent<Image>();

        Sprite sprite = Resources.Load<Sprite>("Sprites/daf/kidushin/" + dafName);
        if (sprite == null)
            return;
        dafImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, sprite.rect.height);
        dafImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sprite.rect.width);
        dafImage.GetComponent<Image>().sprite = sprite;

        Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@BackgroundCenter=" + dafImage.GetComponent<RectTransform>().position + " width=" + sprite.rect.width + " height=" + sprite.rect.height);
    }

    public void LoadConfig()
    {
        LoadRawDaf(m_dafConfig.m_dafName);
        foreach (IndexConfig index in m_dafConfig.indexes)
            addNewIndex(index.m_name, index.m_center, index.width, index.height);
        DecorateIndexes();
    }

    private void DecorateIndexes()
    {
        //GameObject[] indexes = GameObject.FindGameObjectsWithTag("index");
        IEnumerable<GameObject> indexes = GameObject.FindGameObjectsWithTag("index").ToList().OrderBy(child => child.transform.position.y + 10 * child.transform.position.x);
        int i = 0;
       
        foreach (GameObject index in indexes)
        {
            if (index.activeSelf)
            {
                i++;
                if (i % 2 == 0)
                    index.GetComponent<Image>().color = new Color32(241, 152, 19, 55);
                else
                    index.GetComponent<Image>().color = new Color32(243, 243, 8, 55);
                Debug.Log("Decorating: " + index.transform.position);
            }
        }
    }
}


