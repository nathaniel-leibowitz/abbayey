﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class IndexMoveHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler
{

    private Vector3 origLocalScale;
    private Vector3 origSymbolLocalScale;
    private GameObject text;
    private Image image;
    private Color origColor;

    private Vector3 startPosition;
    public static GameObject index;

    public ControlManager control;

    private bool m_doDestroy;

    // Use this for initialization
    void Start ()
    {
        origLocalScale = transform.localScale;
        Debug.Log("orig scale of " + tag + " is " + origLocalScale);
        control = FindObjectOfType<ControlManager>();
        text = GetComponentInChildren<Text>(true).gameObject;
        image = GetComponentInChildren<Image>(true);
        text.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    /*void OnGUI()
    {
        Event e = Event.current;
        if (!e.isMouse)
            return;

        var mousePosition = Input.mousePosition;
        if (e.button == 0 && (e.clickCount == 2))
        {
            Debug.Log("detected dblclik on gameobject");
            //Destroy(gameObject);
        }
    }*/

    public void OnPointerExit(PointerEventData data)
    {
        //GetComponentInChildren<Text>().transform.localScale = origLocalScale;
        text.SetActive(false);
        image.color = origColor;
    }

    public void OnPointerEnter(PointerEventData data)
    {
        //GetComponentInChildren<Text>().transform.localScale = new Vector3(4 * transform.localScale.x, 4 * transform.localScale.y, 4 * transform.localScale.z);
        text.SetActive(true);
        origColor = image.color;
        //image.color = new Color(image.color.r, image.color.g, image.color.b, 0.75f);
        image.color = new Color(0, 0, 0, 1);
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!DafFactory.m_editMode)
        {
            gameObject.SetActive(false);
            if (GetComponentInChildren<Text>(true).text.Contains("babakama"))
                control.SetSugiya(GetComponentInChildren<Text>(true).text);
            else
                control.SetSugiya(new string(GetComponentInChildren<Text>(true).text.ToCharArray().Reverse().ToArray()));
            control.NextLevel();
        }
        else
        {
            transform.localScale = origLocalScale;
            //Debug.Log("detected down on gameobject numclickes=" + data.clickCount + " button=" + data.button);
            if ((data.button != PointerEventData.InputButton.Left))
                return;
            if (Input.GetKey("delete"))
            {
                Destroy(gameObject);
                return;
            }

            if (Input.GetKey("up") || Input.GetKey("down"))
            {
                int direction = Input.GetKey("up") ? 1 : -1;
                RectTransform rt = gameObject.GetComponent<RectTransform>();
                //Debug.Log("rect=" + rt.rect);
                Vector3 origPos = new Vector3(rt.transform.position.x, rt.transform.position.y, rt.transform.position.z);
                rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, rt.rect.height + direction * 6);
                rt.transform.SetPositionAndRotation(origPos, rt.transform.rotation);
                rt.ForceUpdateRectTransforms();
            }

            if (Input.GetKey("right") || Input.GetKey("left"))
            {
                int direction = Input.GetKey("right") ? 1 : -1;
                RectTransform rt = gameObject.GetComponent<RectTransform>();
                //Debug.Log("rect=" + rt.rect);
                rt.transform.SetPositionAndRotation(new Vector3(rt.transform.position.x, rt.transform.position.y + direction * 3, rt.transform.position.z), rt.transform.rotation);
            }
        }
    }

    public void OnMouseDown()
    {
        //Debug.Log("detected click on gameobject");
        //Destroy(gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //balloon = gameObject;
        //startPosition = transform.position;
    }



    public void OnDrag(PointerEventData eventData)
    {

        /*if (!HotspotFactory.m_editMode)
            return;
        transform.position = Input.mousePosition;*/
        }

    public void OnEndDrag(PointerEventData eventData)
    {
        //balloon = null;
    }
}
