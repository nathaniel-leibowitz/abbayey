﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class IndexConfig
{
    public IndexConfig(string name, Vector3 center, float l_height, float l_width)
    { m_name = name; m_center = center; height = l_height;  width = l_width; }
    public string m_tag;
    public string m_name;
    public Vector3 m_center;
    public float height;
    public float width;
}

[Serializable]
public class DafConfig
{
    public DafConfig()
    {
        m_dafName = null;
        indexes = new List<IndexConfig>();
    }
    public string m_dafName;
    public List<IndexConfig> indexes;
}

 


