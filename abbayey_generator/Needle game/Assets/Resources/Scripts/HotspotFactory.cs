﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Linq;
using UnityEngine.Assertions;

public class HotspotFactory : MonoBehaviour
{

    public float[] symbol2TextRatios;

    public RectTransform[] prefabSymbols;
    public RectTransform prefabSyntaxHotspot;
    public RectTransform prefabTranslationHotspot;
    public RectTransform dropDown;
    public float raiseHotspotDropDown;
    public ScoresManager scores;
    public InputField translationWord;


    public GameObject symbolParentObject;
    public GameObject hotspotParentObject;

    public static AbbayeyConfig m_config = new AbbayeyConfig();
    public static bool m_editMode;
    public static bool m_gameOn;
    public static bool m_animateMode = false;

    private Image sugiya;
    private Image indexImage;

    void Start()
    {
        m_editMode = false;
        m_gameOn = false;
        float xFactor = Screen.width / 1920f;
        float yFactor = Screen.height / 1080f;
        Rect newCameraRect = new Rect(0, 0, 1, xFactor / yFactor);
        Debug.Log("old camera rect=" + Camera.main.rect + " new=" + newCameraRect);
        Camera.main.rect = newCameraRect;

        dropDown = Instantiate(dropDown);
        dropDown.transform.SetParent(symbolParentObject.transform);
        dropDown.transform.position = new Vector3(0, 0, 0);

        List<string> options = new List<string> { "" };
        foreach (RectTransform symbol in prefabSymbols)
            options.Add(symbol.tag);



        Dropdown drop = dropDown.GetComponent<Dropdown>();
        drop.ClearOptions();
        drop.AddOptions(options); 
        drop.onValueChanged.AddListener(delegate {
            getSelection(drop);
        });
    }

    // Update is called once per frame
    void Update()
    {
    }


    void OnGUI()
    {
        if (!m_editMode)
            return;
        Event e = Event.current;
        if (!e.isMouse)
            return;
       
        var mousePosition = Input.mousePosition;
        if (e.button ==1 && (e.clickCount == 1))
        {
            Vector3 raiseToSolveDisappearanceProblem = new Vector3(0, raiseHotspotDropDown, 0);
            dropDown.transform.position = Input.mousePosition + raiseToSolveDisappearanceProblem;
        }
    }

    void getSelection(Dropdown drop)
    {
        int r = drop.value;
        if (r == 0)
            return;
        string tag = drop.options[r].text;
        string name = tag.Contains("translation") ? new string(translationWord.text.ToCharArray().Reverse().ToArray()) : "";
        Vector3 raiseToSolveDisappearanceProblem = new Vector3(0, raiseHotspotDropDown, 0);
        addHotspotAndSymbol(tag, name, dropDown.transform.position - raiseToSolveDisappearanceProblem, 0, 0);        
        dropDown.transform.position = new Vector3(0, 0, 0);
        drop.value = 0;

        /*Sprite sprite = Resources.Load<Sprite>("Sprites/sugiyot/" + m_config.m_sugiya);
        sugia.GetComponent<Image>().sprite = sprite;*/
    }

    void addHotspotAndSymbol(string tag, string name, Vector3 position, float width, float height)
    {
        Debug.Log("!!!!!Adding tag " + tag + " name=" + name);
        RectTransform prefabSymbol = findSymbol(tag);
        if (prefabSymbol == null)
            return;

        Debug.Log("-------- Instantiating tag " + tag + " name=" + name);
        RectTransform symbol = Instantiate(prefabSymbol);
        if ((name != null) && (name.Length > 0))
            symbol.name = name.Replace("hotspot:", string.Empty);
        if (symbol.tag.Contains("translation"))
        {
            symbol.GetComponent<Text>().fontSize = (int)m_config.m_fontSize;
            symbol.GetComponent<Text>().text = symbol.name;
        }

        symbol.GetComponent<neeldeHandler>().scores = scores;
        positionNewSymbol(symbol);

        RectTransform hotspot = Instantiate(symbol.tag.Contains("translation")? prefabTranslationHotspot : prefabSyntaxHotspot);
        hotspot.tag = tag;
        hotspot.name = "hotspot:" + symbol.name;
        positionNewArrow(hotspot, position, width, height);
        Debug.Log("new arrow name is " + hotspot.name);
    }

    RectTransform findSymbol(string tag)
    {
        foreach (RectTransform symbol in prefabSymbols)
            if (symbol.tag.Equals(tag))
                return symbol;
        return null;
    }

    int findSymbolIndex(string tag)
    {
        int index = -1;
        Debug.Log("searching index of tag:" + tag);
        foreach (RectTransform symbol in prefabSymbols)
        {
            index++;
            Debug.Log("       index:" + index + " foundTag:" + symbol.tag);
            if (symbol.tag.Equals(tag))
                return index;
        }
        return index;
    }

    void positionNewSymbol(RectTransform symbol)
    {
        symbol.parent = symbolParentObject.transform;
        int numSuchSymbols = 0;
        Transform[] ts = symbolParentObject.GetComponentsInChildren<Transform>();
        foreach (Transform child in ts)
            if (child.tag.Equals(symbol.tag))
                numSuchSymbols++;

        if (symbol.tag.Contains("translation"))
        {
            symbol.position = new Vector3(
            m_config.m_symbolAreaBL.x + 4 * m_config.m_symbolPosInc.x,
            m_config.m_symbolAreaBL.y + ((numSuchSymbols * m_config.m_symbolPosInc.y)/1.5f),
            0);
        }
        else
        {
            symbol.position = new Vector3(
            m_config.m_symbolAreaBL.x + (numSuchSymbols * m_config.m_symbolPosInc.x),
            m_config.m_symbolAreaBL.y + (findSymbolIndex(symbol.tag) * m_config.m_symbolPosInc.y),
            0);

            float symbol2TextRatio = symbol2TextRatios[findSymbolIndex(symbol.tag)];
            float origHeight = symbol.rect.height;
            float origWidth = symbol.rect.width;
            float textSize = Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line1_bottom_text);
            float scaleFactor = symbol2TextRatio / (origHeight / textSize);
            symbol.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scaleFactor * origHeight);
            symbol.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scaleFactor * origWidth);
        }
    }


    void positionNewArrow(RectTransform arrow, Vector3 position, float width, float height)
    {
        float baseLine = m_config.m_meter_line1_top_text;
        if (Math.Abs(position.y - m_config.m_meter_line2_top_text) < Math.Abs(position.y - m_config.m_meter_line1_top_text))
            baseLine = m_config.m_meter_line1_top_text;
        float distBetweenLines = Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line2_top_text)/5;
        arrow.parent = hotspotParentObject.transform;
        arrow.position = position;

        float sign = 1;
        float currentY = arrow.position.y;
        if (currentY < m_config.m_meter_line1_top_text)
            sign = -1;
        float delta = currentY - baseLine;
        delta *= sign;
        float lowerDelta = delta - delta % distBetweenLines;
        if ((delta - lowerDelta) > distBetweenLines / 2)
            lowerDelta += distBetweenLines;
        float textsize = Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line1_bottom_text);

        float hotspotHeight = baseLine + sign * lowerDelta;
        float colliderHeight = 1.5f * textsize / arrow.transform.lossyScale.y;
        float colliderWidth = 20f;
        float colliderOffset = -0.5f * textsize / arrow.transform.lossyScale.y;
        if (arrow.tag.Contains("translation"))
        {
            arrow.transform.localScale = new Vector3(1, 1, 1);
            RectTransform rt = arrow.GetComponent<RectTransform>();
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 1.4f*textsize);
            hotspotHeight -= (textsize - rt.rect.height/2f);
            hotspotHeight -= 0.2f * textsize;
            colliderOffset = 0;
            colliderWidth *= 4;
            if (width > 0)
                rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        }
        else
        {
            float symbol2TextRatio = 1.5f;
            float origHeight = arrow.rect.height;
            float origWidth = arrow.rect.width;
            float textSize = Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line1_bottom_text);
            float scaleFactor = symbol2TextRatio / (origHeight / textSize);
            arrow.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scaleFactor * origHeight);
            arrow.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scaleFactor * origWidth);
        }
        arrow.GetComponent<BoxCollider2D>().offset = new Vector2(0, colliderOffset);
        arrow.GetComponent<BoxCollider2D>().size = new Vector2(colliderWidth, colliderHeight);
        arrow.transform.position = new Vector3(arrow.transform.position.x, hotspotHeight, 0f);        
        
    }

    public void SaveConfig(string exerciseName)
    {
        int playerParkingXCenter = -700;
        int numTranslations = 0;
        Dictionary<string, int> numSymbols = new Dictionary<string, int>();
        m_config.symbols.Clear();
        m_config.hotspots.hotspots.Clear();
        Transform[] ts = hotspotParentObject.GetComponentsInChildren<Transform>();
        Transform[] players = symbolParentObject.GetComponentsInChildren<Transform>();
        m_config.hotspots.dafPortion = "storage/miniGames/gemara/page_sections/" + m_config.m_sugiya + ".png";
        m_config.hotspots.exerciseName = exerciseName;
        foreach (Transform child in ts)
        {
            if (!child.tag.Equals("Untagged"))
            {
                var player = findSymbol(players.ToList(), child.tag);
                Assert.IsNotNull(player);
                RectTransform rt = (RectTransform)(child.transform);
                m_config.symbols.Add(new SymbolConfig(child.tag, child.name, child.position, rt.rect.height, rt.rect.width));


                Vector3 anchor = new Vector3(m_config.m_sugiyaCenter.x - m_config.m_sugiyaSize.x, /*m_config.m_sugiyaSize.y - */(m_config.m_sugiyaCenter.y - m_config.m_sugiyaSize.y), 0);
                Vector3 positionRelativeToCenterSugiya = child.position - anchor;
                //invert the y axis
                positionRelativeToCenterSugiya.y = m_config.m_sugiyaSize.y - positionRelativeToCenterSugiya.y;
                positionRelativeToCenterSugiya.x -= m_config.m_sugiyaSize.x;

                string level = "";
                string id = child.name.Replace("hotspot:", string.Empty);
                string hsFn;
                string playerFn;
                int playerWidth = 0;
                int playerHeight = 0;
                int snapDeltaX = 0;
                int snapDeltaY = 0;
                string playerText = "";
                int playerParkingX = playerParkingXCenter;
                int playerParkingY = 0;

                if (child.tag.Contains("translation"))
                {
                    id = new string(id.ToCharArray().Reverse().ToArray());
                    level = "translation";
                    hsFn = "";
                    playerFn = id + ".text";
                    playerText = id;

                    
                    if (numTranslations % 2 == 0)
                        playerParkingY = 40 * numTranslations / 2;
                    else
                        playerParkingY = -40 * (numTranslations + 1) / 2;
                    numTranslations++;
                }
                else
                {
                    level = "syntax";
                    id = id.Replace("(Clone)", string.Empty);
                    hsFn = "storage/miniGames/gemara/syntax/arrow6.png";
                    if (child.tag.Contains("biblequote"))
                        playerFn = "storage/miniGames/gemara/syntax/" + id + ".gif";
                    else
                        playerFn = "storage/miniGames/gemara/syntax/" + id + ".png";
                    RectTransform playerRt = (RectTransform)(player.transform);
                    playerWidth = (int)Math.Round(playerRt.rect.width);
                    playerHeight = (int)Math.Round(playerRt.rect.height);
                    snapDeltaY = (int)Math.Round(offsetBelowLineAsFraction(player.tag) * Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line1_bottom_text));
                    if (!numSymbols.ContainsKey(child.tag))
                        numSymbols[child.tag] = 0;


                    if (numSymbols[child.tag] % 2 == 0)
                        playerParkingX = playerParkingXCenter + 30 * numSymbols[child.tag] / 2;
                    else
                        playerParkingX = playerParkingXCenter  + -30 * (numSymbols[child.tag] + 1) / 2;

                    int symbolIndex = findSymbolIndex(child.tag);
                    playerParkingY = 350 - symbolIndex * 50;
                    numSymbols[child.tag]++;
                }

                Debug.Log("adding to " + m_config.m_sugiya + " webSymbol" + id + " tag " + child.tag + " symbolTag " + player.tag);
                m_config.hotspots.hotspots.Add(new HotSpot(
                     level,
                     id,
                     (int)Math.Round(positionRelativeToCenterSugiya.x),
                     (int)Math.Round(positionRelativeToCenterSugiya.y),
                     (int)Math.Round(rt.rect.width),
                     (int)Math.Round(rt.rect.height),
                     snapDeltaX,
                     snapDeltaY,
                     hsFn,
                     playerFn,
                     playerWidth,
                     playerHeight,
                     getPlayerScale(child.tag),
                     getPlayerScale(child.tag),
                     playerParkingX,
                     playerParkingY,
                     playerText
                    ));

            }
        }
        foreach (var symbol in m_config.hotspots.hotspots)
            Debug.Log(symbol.id + "(" + symbol.hotspotCenterX + "," + symbol.hotspotCenterY + ")" + "(" + symbol.hotspotWidth + "," + symbol.hotspotHeight + ")");
    }

    public void LoadConfig()
    {
        LoadRawSugiya(m_config.m_sugiya);
        foreach (SymbolConfig symbol in m_config.symbols)
            addHotspotAndSymbol(symbol.m_tag, symbol.m_name, symbol.m_arroPosition, symbol.width, symbol.height);
        ScrambleTranslations();
    }


    public void LoadRawSugiya(string sugiyaName)
    {
        Clear();
        GameObject[] indexes = GameObject.FindGameObjectsWithTag("index");
        foreach (GameObject index in indexes)
            DestroyImmediate(index);
        GameObject background = GameObject.Find("background");
        sugiya = background.GetComponent<Image>();

        Sprite sprite = Resources.Load<Sprite>("Sprites/sugiya/" + sugiyaName);
        if (sprite == null)
            return;
        sugiya.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, sprite.rect.height);
        sugiya.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sprite.rect.width);
        sugiya.GetComponent<Image>().sprite = sprite;
        m_config.m_sugiyaCenter = sugiya.GetComponent<RectTransform>().position;
        m_config.m_sugiyaSize = new Vector3(sprite.rect.width, sprite.rect.height, 0);

        Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@BackgroundCenter=" + sugiya.GetComponent<RectTransform>().position + " width=" + sprite.rect.width + " height=" + sprite.rect.height);

    }

    public void Clear()
    {
        Transform[] ts = hotspotParentObject.GetComponentsInChildren<Transform>();
        foreach (Transform child in ts)
        {
            if (!child.tag.Equals("Untagged"))
                DestroyImmediate(child.gameObject);
        }

        ts = symbolParentObject.GetComponentsInChildren<Transform>();
        foreach (Transform child in ts)
        {
            if (!child.tag.Equals("Untagged"))
                DestroyImmediate(child.gameObject);
        }
    }

    public void SetEditMode(bool edit)
    {
        m_editMode = edit;
        translationWord.gameObject.SetActive(edit);
    }

    public void ScrambleTranslations()
    {
        Transform[] origTs = symbolParentObject.GetComponentsInChildren<Transform>();
        List<Transform> origTranslations = new List<Transform>();

        foreach (Transform translation in origTs)
        {
            if (translation.tag.Contains("translation"))
                origTranslations.Add(translation);
        }

        if (origTranslations.Count == 0)
            return;

        List<Vector3> origPositions = new List<Vector3>();
        foreach (Transform translation in origTranslations)
            origPositions.Add(new Vector3(translation.position.x, translation.position.y, translation.position.z));

        IEnumerable<Transform> orderedTs = origTranslations.OrderBy(child => child.GetComponent<Text>().text);

        int counter = 0;
        foreach (Transform orderedTranslation in orderedTs)
        {
            Vector3 newPos = origPositions[counter];
            orderedTranslation.transform.position = new Vector3(newPos.x, newPos.y, newPos.z);
            counter++;
        }
    }

    public void DisplaySyntax()
    {
        Debug.Log("Displaying syntax");
        Transform[] hotspots = hotspotParentObject.GetComponentsInChildren<Transform>();
        GameObject symbolsParent = GameObject.Find("answers/syntax");
        List<Transform> symbols = symbolsParent.GetComponentsInChildren<Transform>().ToList();
        Debug.Log("displaying " + hotspots.Length + " hotspots using " + symbols.Count + " symbols");


        foreach (Transform hotspot in hotspots)
        {
            string tag = hotspot.tag;
            //Debug.Log("Encountered tag " + tag);
            if (!tag.Equals("Untagged") && !tag.Equals("translation"))
            {
                //Debug.Log("Displaying tag " + tag);
                var symbol = findSymbol(symbols, tag);
                if (symbol != null)
                {
                    Vector3 snapPos = hotspot.position;
                    snapPos.y = hotspot.position.y - offsetBelowLineAsFraction(tag) * Math.Abs(m_config.m_meter_line1_top_text - m_config.m_meter_line1_bottom_text);
                    //transform.position = snapPos;
                    symbol.transform.position = snapPos;
                    Destroy(hotspot.gameObject);
                    symbols.Remove(symbol);
                }
                
            }
        }
    }

    Transform findSymbol(List<Transform> symbols, string tag)
    {
        foreach (var symbol in symbols)
        {
            //Debug.Log("Checking tag " + symbol.tag);
            if (symbol.tag.Equals(tag))
            {
                //Debug.Log("Found name " + symbol.name + "Found tag " + symbol.tag);
                return symbol;
            }
        }
        return null;
    }

    float offsetBelowLineAsFraction(string tag)
    {
        switch (tag)
        {
            case "assist" : return 0.25f; break;
            case "nonassit": return 0.75f; break;
            case "period": return 1.0f; break;
            case "quotebegin": return -0.25f; break;
            case "quoteend": return -0.25f; break;
            case "speakbegin": return -0.25f; break;
            case "speakend": return -0.25f; break;
            case "wrongly": return 0.1f; break;
            default: return 0.5f;
        }
    }

    float getPlayerScale(string tag)
    {
        switch (tag)
        {
            case "hyphen": return 0.4f; break;
            case "assist": return 0.25f; break;
            case "nonassist": return 0.2f; break;
            case "period": return 0.06f; break;
            case "quotebegin": return 1f; break;
            case "quoteend": return 1f; break;
            case "speakbegin": return 0.07f; break;
            case "speakend": return 0.07f; break;
            case "wrongly": return 0.2f; break;
            case "resolution": return 0.7f; break;
            case "kushia": return 0.7f; break;
            case "biblequote": return 0.18f; break;
            case "colon": return 0.7f; break;
            case "question": return 0.08f; break;

            default: return 1f;
        }
    }
}






