﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[Serializable]
public class SymbolConfig
{
    public SymbolConfig(string tagName, string name, Vector3 arrowPos, float l_height, float l_width)
    { m_tag = tagName; m_name = name; m_arroPosition = arrowPos; height = l_height;  width = l_width;
    }
    public string m_tag;
    public string m_name;
    public Vector3 m_arroPosition;
    public float height;
    public float width;
}


[Serializable]
public class HotSpot
{
    public HotSpot(string a_level, string a_id, int a_hotspotCenterX, int a_hotspotCenterY, int a_hotspotWidth, int a_hotspotHeight, 
        int a_hotspotSnapDeltaX, int a_hotspotSnapDeltaY, string a_hotspotFn, 
        string a_playerFn, int a_playerImageWidth, int a_playerImageHeight, float a_playerScaleX, float a_playerScaleY,
        int a_playerParkingX, int a_playerParkingY, 
        string a_playerText)
    {
        level = a_level; id = a_id; hotspotCenterX = a_hotspotCenterX; hotspotCenterY = a_hotspotCenterY; hotspotWidth = a_hotspotWidth; hotspotHeight = a_hotspotHeight;
        hotspotSnapDeltaX = a_hotspotSnapDeltaX; hotspotSnapDeltaY = a_hotspotSnapDeltaY;
        hotspotImageFileName = a_hotspotFn; playerImageFileName = a_playerFn;
        playerImageWidth = a_playerImageWidth; playerImageHeight = a_playerImageHeight;
        playerScaleX = a_playerScaleX; playerScaleY = a_playerScaleY;
        playerParkingX = a_playerParkingX; playerParkingY = a_playerParkingY;
        playerText = a_playerText;

    }
    public string level;
    public string id;
    public int hotspotCenterX;
    public int hotspotCenterY;
    public int hotspotWidth;
    public int hotspotHeight;
    public int hotspotSnapDeltaX;
    public int hotspotSnapDeltaY;
    public string hotspotImageFileName;
    public string playerImageFileName;
    public int playerImageWidth;
    public int playerImageHeight;
    public float playerScaleX;
    public float playerScaleY;
    public int playerParkingX;
    public int playerParkingY;
    public string playerText;
}

[Serializable]
public class HotSpots
{
    public HotSpots()
    {
        hotspots = new List<HotSpot>();
        dafPortion = "";
        exerciseName = "";
    }

    public List<HotSpot> hotspots;
    public string dafPortion;
    public string exerciseName;
}


[Serializable]
public class AbbayeyConfig
{
    public AbbayeyConfig()
    {
        m_sugiya = null;
        symbols = new List<SymbolConfig>();
        hotspots = new HotSpots();
        m_symbolAreaBL = new Vector3(0, 0, 0);
        m_symbolPosInc = new Vector3(0, 0, 0);
    }
    public string m_sugiya;//TBD ranme this as daf
    public Vector3 m_sugiyaCenter;
    public Vector3 m_sugiyaSize;
    public Vector3 m_symbolAreaBL;
    public Vector3 m_symbolPosInc;
    public float m_meter_line1_top_text;
    public float m_meter_line1_bottom_text;
    public float m_meter_line2_top_text;
    public List<SymbolConfig> symbols;
    public HotSpots hotspots;
    public float m_fontSize;



    public string createWebConfig()
    {
        string str = "";
        int i = 0;

        HashSet<string> uniqueIds = new HashSet<string>();

        str += ("\n\t\t\"backGround\": \"" + hotspots.dafPortion + "\",\n");

        foreach (HotSpot hs in hotspots.hotspots)
        {
            //if (hs.tag.Contains("translation"))
            {
                if (!uniqueIds.Contains(hs.id))
                {
                    uniqueIds.Add(hs.id);
                    /*str += ("\t\t\"" + translation + "\"");
                    str += (" :\"" + translation + "_" + i + "\",\n");*/
                    str += ("\t\t\"" + hs.id + "\":");
                    //str += (" \"" + hs.id + ".text\",\n");
                    str += (" \"" + hs.playerImageFileName + "\",\n");
                }
            }
        }
        str += ("\t},\n");

        str += ("\t\"parameters\": {\n");
        str += ("\t\t\"numOfItems\": " + hotspots.hotspots.Count + ",\n");
        str += ("\t\t\"placeholders\": " + "[\n");

        i = 0;
        foreach (HotSpot hs in hotspots.hotspots)
        {
            //if (symbol.m_tag.Contains("translation"))
            {
                i++;
                int x = hs.hotspotCenterX;
                int y = hs.hotspotCenterY;
                int w = hs.hotspotWidth;
                int h = hs.hotspotHeight;
                int offsetX = hs.hotspotSnapDeltaX;
                int offsetY = hs.hotspotSnapDeltaY;
                //str += ("\t\t\t{ \"id\": \"" + hs.id + "_" + i + "\", \"loc\": { \"x\": " + x + ", \"y\": " + y + " }, \"rect\": { \"width\": " + w + ", \"height\": " + h + " }, \"isReused\": false },\n");
                str += ("\t\t\t{ \"id\": \"" + hs.id + "_" + i + "\", \"loc\": { \"x\": " + x + ", \"y\": " + y + " }, \"rect\": { \"width\": " + w + ", \"height\": " + h + " }, \"isReused\": false, " + "\"offsetX\": " + offsetX + ", \"offsetY\": " + offsetY + ", \"url\": \"" + hs.hotspotImageFileName + "\" },\n");

            }
        }

        str += ("\t\t],\n");
        str += ("\t\t\"items\": " + "[\n");
        i = 0;
        foreach (HotSpot hs in hotspots.hotspots)
        {
            //if (symbol.m_tag.Contains("translation"))
            {
                i++;
                string placeHolderIds = findPlacehoderIds(hs.id);
                
                float scaleX = hs.playerScaleX;
                float scaleY = hs.playerScaleY;

                str += ("\t\t\t{\n");
                str += ("\t\t\t\t\"type\": " + "\"" + hs.id + "\", \n");
                str += ("\t\t\t\t\"score\": 1,\n");
                str += ("\t\t\t\t\"loc\": { \"x\": " + hs.playerParkingX + ", \"y\": " + hs.playerParkingY + " },\n");
                str += ("\t\t\t\t\"scale\": { \"x\": " + scaleX + ", \"y\": " + scaleY + " },\n");
                str += placeHolderIds;
                str += ("\t\t\t},\n");
            }
        }
        str += ("\t\t],\n");
        str += ("\t\t\"ex_name\": \"" + hotspots.exerciseName + "\",\n");

        return str;
    }

    public string findPlacehoderIds(string id)
    {
        string str = ("\t\t\t\t\"placeholderIds\": [ ");
        int i = 0;
        int numFound = 0;
        foreach (HotSpot hs in hotspots.hotspots)
        {
            i++;
            if (id.Equals(hs.id))
            {
                if (numFound > 0)
                    str += (", ");
                str += ("\"" + id + "_" + i + "\"");
                numFound++;
            }
        }
        str += ("],\n");
        return str;
    }

}



 


