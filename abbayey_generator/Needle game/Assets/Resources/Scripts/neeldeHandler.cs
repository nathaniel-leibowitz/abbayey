﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class neeldeHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler
{


    public static Vector3 startPostion;
    public static GameObject needle;
    public ScoresManager scores;
    private bool m_movebackEnabled;
    public float m_offsetBelowLineAsFraction;
    private bool m_enableDrag;
    private Vector3 origLocalScale;
    private Vector3 origEnemyScale;
    private bool m_doAnimate = false;
    private Vector3 origSnapPosition;
    private int animationDirection = 1;


    // Use this for initialization
    void Start()
    {
        m_enableDrag = true;
        m_movebackEnabled = true;
        isMove = false;
        origLocalScale = transform.localScale;
        origEnemyScale = new Vector3(1, 1, 1);
        Debug.Log("orig scale of " + tag + " is " + origLocalScale);
        //throw new UnityException("unhandled tag in determining offsetBelowLine" + tag);
    }


    // Update is called once per frame
    void Update()
    {
        if (HotspotFactory.m_animateMode && m_doAnimate)
        {
            if (Vector3.Distance(transform.position, origSnapPosition) > 10)
                animationDirection *= -1;

            //transform.Rotate(new Vector3(0, 2, 0));
            transform.Translate(new Vector3(0, 1f * animationDirection, 0));
        }
    }

    public void OnPointerExit(PointerEventData data)
    {
        transform.localScale = origLocalScale;
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (isMove)
            return;
        float scaleup = gameObject.tag.Contains("translation") ? 1.6f : 2f;
        transform.localScale = new Vector3(scaleup * transform.localScale.x, scaleup * transform.localScale.y, scaleup * transform.localScale.z);
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!CanDrag())
            return;
        transform.localScale = origLocalScale;
        needle = gameObject;
        startPostion = transform.position;
        isMove = true;
        needleCollider(true);
    }

    public static bool isMove;

    public void OnDrag(PointerEventData eventData)
    {
        if (!CanDrag())
            return;
        if (isMove)
        {

            transform.position = Input.mousePosition;

        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!CanDrag())
            return;
        isMove = false;
        needleCollider(false);
        moveBack();
    }

    public void moveBack()
    {
        if (m_movebackEnabled)
            transform.position = startPostion;
    }

    public void  needleCollider(bool isTrue)
    {
        GetComponent<Collider2D>().enabled = isTrue;
    }

    private void OnTriggerExit2D(Collider2D enemy)
    {
        transform.localScale = origLocalScale;
        enemy.gameObject.transform.localScale = origEnemyScale;
        if (isMove)
        {
            return;
        }
        if (!enemy.name.Contains("hotspot"))
            return;
        if (IsMatch(enemy))
        {
            m_movebackEnabled = false;
            Vector3 snapPos = transform.position;
            snapPos.x = enemy.transform.position.x;
            snapPos.y = enemy.transform.position.y - m_offsetBelowLineAsFraction * System.Math.Abs(HotspotFactory.m_config.m_meter_line1_top_text - HotspotFactory.m_config.m_meter_line1_bottom_text);
            transform.position = snapPos;
            Destroy(enemy.gameObject);
            //StartCoroutine(spritesAnimation.BaloonPopSprites(0.1f, enemy.gameObject.GetComponent<Image>(), "Expolsion/"));
            m_enableDrag = false;
            scores.trial(true, enemy.transform.position.x, enemy.transform.position.y);
            if (gameObject.tag.Contains("translation"))
                Destroy(gameObject);
            m_doAnimate = true;
            origSnapPosition = gameObject.transform.position;
        }
        else
        {
            //enemy.GetComponent<AudioSource>().PlayOneShot();
            scores.trial(false, enemy.transform.position.x, enemy.transform.position.y);
        }
    }

    private bool IsMatch(Collider2D enemy)
    {
        //Debug.Log("checking match between name " + gameObject.name + " and " + enemy.name);
        //TBD - use only name. To do that, set the names of all the symbols to the tagname, and also set the arrow name to its tag name
        //Use tage differently - the tags will be hotpsot, symbol, translation etc.
        if (!enemy.tag.Equals(gameObject.tag))
            return false;
        if (!gameObject.tag.Contains("translation"))
            return true;
        string formattedName = "hotspot:" + gameObject.name;
        return formattedName.Equals(enemy.name);
    }

    private void OnTriggerEnter2D(Collider2D enemy)
    {
        if (!isMove)
            return;
        if (!enemy.name.Contains("hotspot"))
            return;
        float scaleup = gameObject.tag.Contains("translation") ? 1.5f : 1.5f;
        transform.localScale = new Vector3(scaleup * transform.localScale.x, scaleup * transform.localScale.y, scaleup * transform.localScale.z);

        origEnemyScale = enemy.gameObject.transform.localScale;
        float enemyScaleup = gameObject.tag.Contains("translation") ? 1.5f : 2.5f;
        enemy.gameObject.transform.localScale = new Vector3(enemyScaleup * enemy.gameObject.transform.localScale.x, enemyScaleup * enemy.gameObject.transform.localScale.y, enemyScaleup * enemy.gameObject.transform.localScale.z);

    }

    private bool CanDrag()
    {
        return (m_enableDrag && HotspotFactory.m_gameOn);
    }




}
