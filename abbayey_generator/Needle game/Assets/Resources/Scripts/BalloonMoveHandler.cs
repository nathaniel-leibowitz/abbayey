﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class BalloonMoveHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler
{

    private Vector3 origLocalScale;
    private Vector3 origSymbolLocalScale;

    private Vector3 startPosition;
    public static GameObject balloon;

    public ControlManager control;

    // Use this for initialization
    void Start ()
    {
        origLocalScale = transform.localScale;
        Debug.Log("orig scale of " + tag + " is " + origLocalScale);
        control = FindObjectOfType<ControlManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    /*void OnGUI()
    {
        Event e = Event.current;
        if (!e.isMouse)
            return;

        var mousePosition = Input.mousePosition;
        if (e.button == 0 && (e.clickCount == 2))
        {
            Debug.Log("detected dblclik on gameobject");
            //Destroy(gameObject);
        }
    }*/

    public void OnPointerExit(PointerEventData data)
    {
        if (!HotspotFactory.m_editMode)
            return;
        transform.localScale = origLocalScale;
        GameObject symbols = GameObject.Find("answers/syntax");
        GameObject found = new List<GameObject>(GameObject.FindGameObjectsWithTag(tag)).Find(g => g.transform.IsChildOf(symbols.transform) && name.Contains(g.name));
        if (found != null)
            found.transform.localScale = origSymbolLocalScale;
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (!HotspotFactory.m_editMode)
            return;
        transform.localScale = new Vector3(2 * transform.localScale.x, 2 * transform.localScale.y, 2 * transform.localScale.z);

        GameObject symbols = GameObject.Find("answers/syntax");
        GameObject found = new List<GameObject>(GameObject.FindGameObjectsWithTag(tag)).Find(g => g.transform.IsChildOf(symbols.transform) && name.Contains(g.name));
        if (found != null)
        {
            origSymbolLocalScale = found.transform.localScale;
            found.transform.localScale = new Vector3(2 * found.transform.localScale.x, 2 * found.transform.localScale.y, 2 * found.transform.localScale.z);
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!HotspotFactory.m_editMode)
            return;
        transform.localScale = origLocalScale;
        //Debug.Log("detected down on gameobject numclickes=" + data.clickCount + " button=" + data.button);
        if ((data.button != PointerEventData.InputButton.Left))
            return;
        if (Input.GetKey("delete"))
        {
            GameObject symbols = GameObject.Find("answers/syntax");
            GameObject found = new List<GameObject>(GameObject.FindGameObjectsWithTag(tag)).Find(g => g.transform.IsChildOf(symbols.transform) && name.Contains(g.name));
            Destroy(found);
            Destroy(gameObject);
        }

        if (tag.Contains("translation") && (Input.GetKey("up") || Input.GetKey("down")))
        {
            int direction = Input.GetKey("up") ? 1 : -1;
            RectTransform rt = gameObject.GetComponent<RectTransform>();
            //Debug.Log("rect=" + rt.rect);
            Vector3 origPos = new Vector3(rt.transform.position.x, rt.transform.position.y, rt.transform.position.z);
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, rt.rect.width + direction*3);
            rt.transform.SetPositionAndRotation(origPos, rt.transform.rotation);
            rt.ForceUpdateRectTransforms();
        }

        if (Input.GetKey("right") || Input.GetKey("left"))
        {
            int direction = Input.GetKey("right") ? 1 : -1;
            RectTransform rt = gameObject.GetComponent<RectTransform>();
            //Debug.Log("rect=" + rt.rect);
            rt.transform.SetPositionAndRotation(new Vector3(rt.transform.position.x + direction, rt.transform.position.y, rt.transform.position.z), rt.transform.rotation);
        }


        /*if ((data.button == PointerEventData.InputButton.Left) && (data.clickCount == 1))
        {
            RectTransform rt = gameObject.GetComponent<RectTransform>();
            Debug.Log("width=" + rt.rect.width + " height=" + rt.rect.height + " rect=" + rt.rect);

            //rt.rect = new Rect(rt.rect.x, rt.rect.y, 60, rt.rect.height);        
        }*/
    }

    public void OnMouseDown()
    {
        Debug.Log("detected click on gameobject");
        //Destroy(gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        balloon = gameObject;
        startPosition = transform.position;
    }



    public void OnDrag(PointerEventData eventData)
    {

        if (!HotspotFactory.m_editMode)
            return;
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        balloon = null;
    }
}
