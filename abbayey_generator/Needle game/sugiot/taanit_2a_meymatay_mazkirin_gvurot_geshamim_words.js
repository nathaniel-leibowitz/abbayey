var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/taanit_2a_top.png",
		"סוכות": "סוכות.text",
		"משיב הרוח ומוריד הגשם": "משיב הרוח ומוריד הגשם.text",
		"מאיזה תאריך": "מאיזה תאריך.text",
	},
	"parameters": {
		"numOfItems": 4,
		"placeholders": [
			{ "id": "סוכות_1", "loc": { "x": -173, "y": -125 }, "rect": { "width": 26, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סוכות_2", "loc": { "x": -158, "y": -92 }, "rect": { "width": 26, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משיב הרוח ומוריד הגשם_3", "loc": { "x": -73, "y": -158 }, "rect": { "width": 164, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיזה תאריך_4", "loc": { "x": -129, "y": -289 }, "rect": { "width": 206, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "סוכות", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סוכות_1", "סוכות_2"],
			},
			{
				"type": "סוכות", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סוכות_1", "סוכות_2"],
			},
			{
				"type": "משיב הרוח ומוריד הגשם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משיב הרוח ומוריד הגשם_3"],
			},
			{
				"type": "מאיזה תאריך", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיזה תאריך_4"],
			},
		],
		"ex_name": "taanit_2a_meymatay_mazkirin_gvurot_geshamim_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}