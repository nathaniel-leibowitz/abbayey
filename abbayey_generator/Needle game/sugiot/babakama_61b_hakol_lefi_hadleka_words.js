var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_61b_top.png",
		"מאיזה מרחק מהשכן הוא נחשב אחראי": "מאיזה מרחק מהשכן הוא נחשב אחראי.text",
		"בתוך החצר שלו": "בתוך החצר שלו.text",
		"רבי אלעזר": "רבי אלעזר.text",
		"רשות הרבים": "רשות הרבים.text",
		"רבי שמעון": "רבי שמעון.text",
		"שש-עשרה": "שש-עשרה.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "מאיזה מרחק מהשכן הוא נחשב אחראי_1", "loc": { "x": 161, "y": -222 }, "rect": { "width": 299, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתוך החצר שלו_2", "loc": { "x": 341, "y": -222 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רבי אלעזר_3", "loc": { "x": 243, "y": -150 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רשות הרבים_4", "loc": { "x": -79, "y": -150 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רבי שמעון_5", "loc": { "x": 18, "y": -113 }, "rect": { "width": 53, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שש-עשרה_6", "loc": { "x": 124, "y": -150 }, "rect": { "width": 44, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאיזה מרחק מהשכן הוא נחשב אחראי", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיזה מרחק מהשכן הוא נחשב אחראי_1"],
			},
			{
				"type": "בתוך החצר שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתוך החצר שלו_2"],
			},
			{
				"type": "רבי אלעזר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי אלעזר_3"],
			},
			{
				"type": "רשות הרבים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רשות הרבים_4"],
			},
			{
				"type": "רבי שמעון", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי שמעון_5"],
			},
			{
				"type": "שש-עשרה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שש-עשרה_6"],
			},
		],
		"ex_name": "babakama_61b_hakol_lefi_hadleka_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}