var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_27b_top.png",
		"למה דיבר דווקא על": "למה דיבר דווקא על.text",
		"גם": "גם.text",
		"מדוייקת, מסתדרת, תואמת": "מדוייקת, מסתדרת, תואמת.text",
		"משנתינו": "משנתינו.text",
		"מתוך שהיה צריך": "מתוך שהיה צריך.text",
		"לשנות, לנסח": "לשנות, לנסח.text",
		"בסוף המשנה": "בסוף המשנה.text",
		"הביא נזק על עצמו": "הביא נזק על עצמו.text",
		"בתחילת המשנה": "בתחילת המשנה.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "למה דיבר דווקא על_1", "loc": { "x": -12, "y": -47 }, "rect": { "width": 122, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_2", "loc": { "x": 210, "y": -11 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוייקת, מסתדרת, תואמת_3", "loc": { "x": 192, "y": -83 }, "rect": { "width": 59, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משנתינו_4", "loc": { "x": 104, "y": -83 }, "rect": { "width": 92, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתוך שהיה צריך_5", "loc": { "x": 302, "y": 61 }, "rect": { "width": 131, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשנות, לנסח_6", "loc": { "x": 189, "y": 61 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בסוף המשנה_7", "loc": { "x": 110, "y": 61 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הביא נזק על עצמו_8", "loc": { "x": 9, "y": 133 }, "rect": { "width": 182, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתחילת המשנה_9", "loc": { "x": 336, "y": 169 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשנות, לנסח_10", "loc": { "x": -37, "y": 25 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשנות, לנסח_11", "loc": { "x": -125, "y": 133 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "למה דיבר דווקא על", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למה דיבר דווקא על_1"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_2"],
			},
			{
				"type": "מדוייקת, מסתדרת, תואמת", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוייקת, מסתדרת, תואמת_3"],
			},
			{
				"type": "משנתינו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משנתינו_4"],
			},
			{
				"type": "מתוך שהיה צריך", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתוך שהיה צריך_5"],
			},
			{
				"type": "לשנות, לנסח", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשנות, לנסח_6", "לשנות, לנסח_10", "לשנות, לנסח_11"],
			},
			{
				"type": "בסוף המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בסוף המשנה_7"],
			},
			{
				"type": "הביא נזק על עצמו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הביא נזק על עצמו_8"],
			},
			{
				"type": "בתחילת המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתחילת המשנה_9"],
			},
			{
				"type": "לשנות, לנסח", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשנות, לנסח_6", "לשנות, לנסח_10", "לשנות, לנסח_11"],
			},
			{
				"type": "לשנות, לנסח", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשנות, לנסח_6", "לשנות, לנסח_10", "לשנות, לנסח_11"],
			},
		],
		"ex_name": "babakama_27b_amar_rav_papa_lo_deika_matnitin_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}