var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33b_34a.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": 453, "y": -439 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_2", "loc": { "x": -93, "y": -439 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_3", "loc": { "x": 262, "y": -382 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 32, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_4", "loc": { "x": -422, "y": -382 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_5", "loc": { "x": -198, "y": -326 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_6", "loc": { "x": -421, "y": -326 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_7", "loc": { "x": -110, "y": -213 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_8", "loc": { "x": -310, "y": -100 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_9", "loc": { "x": -39, "y": 12 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_10", "loc": { "x": -191, "y": 12 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_11", "loc": { "x": -216, "y": 69 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_12", "loc": { "x": -419, "y": 69 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_13", "loc": { "x": -69, "y": 125 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_14", "loc": { "x": -414, "y": 182 }, "rect": { "width": 27, "height": 48 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_10"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_4"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_5", "quoteend_11"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_6", "interrobang_12"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_7", "qushia_8", "qushia_13"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_7", "qushia_8", "qushia_13"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_9", "resolution_14"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_10"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_5", "quoteend_11"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_6", "interrobang_12"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_7", "qushia_8", "qushia_13"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_9", "resolution_14"],
			},
		],
		"ex_name": "kidushin_34a_ein_lemedin_min_haklalot_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}