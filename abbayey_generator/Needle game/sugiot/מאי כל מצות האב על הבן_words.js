var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_middle.png",
		"מה פירוש": "מה פירוש.text",
		"אם נאמר, אם נפרש": "אם נאמר, אם נפרש.text",
		"בן": "בן.text",
		"לעשות": "לעשות.text",
		"והרי שנינו בברייתא": "והרי שנינו בברייתא.text",
		"כך אמר, הכי קאמר": "כך אמר, הכי קאמר.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "מה פירוש_1", "loc": { "x": -205, "y": -83 }, "rect": { "width": 47, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נאמר, אם נפרש_2", "loc": { "x": -83, "y": -47 }, "rect": { "width": 89, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בן_3", "loc": { "x": 117, "y": -11 }, "rect": { "width": 50, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעשות_4", "loc": { "x": 33, "y": -11 }, "rect": { "width": 95, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי שנינו בברייתא_5", "loc": { "x": 206, "y": 25 }, "rect": { "width": 86, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך אמר, הכי קאמר_6", "loc": { "x": -79, "y": 98 }, "rect": { "width": 47, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מה פירוש", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה פירוש_1"],
			},
			{
				"type": "אם נאמר, אם נפרש", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר, אם נפרש_2"],
			},
			{
				"type": "בן", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בן_3"],
			},
			{
				"type": "לעשות", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעשות_4"],
			},
			{
				"type": "והרי שנינו בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי שנינו בברייתא_5"],
			},
			{
				"type": "כך אמר, הכי קאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך אמר, הכי קאמר_6"],
			},
		],
		"ex_name": "מאי כל מצות האב על הבן_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}