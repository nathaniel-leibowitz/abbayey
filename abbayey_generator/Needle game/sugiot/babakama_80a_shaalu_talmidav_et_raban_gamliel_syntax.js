var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_80a_top.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "question_1", "loc": { "x": -165, "y": -195 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_2", "loc": { "x": -51, "y": -195 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_3", "loc": { "x": -189, "y": -195 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_4", "loc": { "x": -297, "y": -195 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_5", "loc": { "x": -370, "y": -195 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_6", "loc": { "x": 76, "y": -159 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_7", "loc": { "x": -47, "y": -159 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_8", "loc": { "x": -70, "y": -159 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_9", "loc": { "x": 158, "y": -123 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_10", "loc": { "x": 11, "y": -123 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_11", "loc": { "x": -13, "y": -123 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_12", "loc": { "x": -129, "y": -123 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_13", "loc": { "x": 84, "y": -50 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_14", "loc": { "x": 59, "y": -50 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_10"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_2", "speakbegin_4", "speakbegin_9", "speakbegin_12"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_5", "speakend_11", "speakend_13"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_2", "speakbegin_4", "speakbegin_9", "speakbegin_12"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_5", "speakend_11", "speakend_13"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_6"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_7"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_8"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_2", "speakbegin_4", "speakbegin_9", "speakbegin_12"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_10"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_5", "speakend_11", "speakend_13"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -760, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_2", "speakbegin_4", "speakbegin_9", "speakbegin_12"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -760, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_5", "speakend_11", "speakend_13"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_14"],
			},
		],
		"ex_name": "babakama_80a_shaalu_talmidav_et_raban_gamliel_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}