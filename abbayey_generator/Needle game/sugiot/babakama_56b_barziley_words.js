var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56b_top.png",
		"לשוליה, לתלמיד של הרועה": "לשוליה, לתלמיד של הרועה.text",
		"שדרכו של רועה, נוהג הוא": "שדרכו של רועה, נוהג הוא.text",
		"מה פירוש": "מה פירוש.text",
		"נובע מכאן": "נובע מכאן.text",
		"שמא": "שמא.text",
		"דוגמא שכיחה כתב": "דוגמא שכיחה כתב.text",
		"מתוך שכתוב בברייתא": "מתוך שכתוב בברייתא.text",
		"יש אומרים וגורסים סוגיא זו אחרת": "יש אומרים וגורסים סוגיא זו אחרת.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "לשוליה, לתלמיד של הרועה_1", "loc": { "x": -182, "y": 322 }, "rect": { "width": 107, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שדרכו של רועה, נוהג הוא_2", "loc": { "x": -339, "y": 322 }, "rect": { "width": 194, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה פירוש_3", "loc": { "x": 200, "y": 322 }, "rect": { "width": 47, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נובע מכאן_4", "loc": { "x": 258, "y": 322 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא_5", "loc": { "x": 63, "y": 394 }, "rect": { "width": 71, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דוגמא שכיחה כתב_6", "loc": { "x": -107, "y": 394 }, "rect": { "width": 260, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתוך שכתוב בברייתא_7", "loc": { "x": -90, "y": 286 }, "rect": { "width": 95, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יש אומרים וגורסים סוגיא זו אחרת_8", "loc": { "x": 50, "y": 286 }, "rect": { "width": 161, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "לשוליה, לתלמיד של הרועה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשוליה, לתלמיד של הרועה_1"],
			},
			{
				"type": "שדרכו של רועה, נוהג הוא", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שדרכו של רועה, נוהג הוא_2"],
			},
			{
				"type": "מה פירוש", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה פירוש_3"],
			},
			{
				"type": "נובע מכאן", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נובע מכאן_4"],
			},
			{
				"type": "שמא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא_5"],
			},
			{
				"type": "דוגמא שכיחה כתב", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דוגמא שכיחה כתב_6"],
			},
			{
				"type": "מתוך שכתוב בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתוך שכתוב בברייתא_7"],
			},
			{
				"type": "יש אומרים וגורסים סוגיא זו אחרת", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יש אומרים וגורסים סוגיא זו אחרת_8"],
			},
		],
		"ex_name": "babakama_56b_barziley_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}