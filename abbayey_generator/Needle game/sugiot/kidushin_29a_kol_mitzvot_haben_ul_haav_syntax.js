var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_middle.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "period_1", "loc": { "x": -147, "y": -384 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_2", "loc": { "x": 179, "y": -312 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_3", "loc": { "x": -81, "y": -276 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_4", "loc": { "x": 180, "y": -204 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_5", "loc": { "x": 189, "y": -384 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_6", "loc": { "x": 62, "y": -348 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_7", "loc": { "x": -277, "y": -312 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_8", "loc": { "x": 53, "y": -240 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_9", "loc": { "x": -95, "y": -168 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -760, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_4"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_5", "hyphen_6", "hyphen_7", "hyphen_8", "hyphen_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_5", "hyphen_6", "hyphen_7", "hyphen_8", "hyphen_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -670, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_5", "hyphen_6", "hyphen_7", "hyphen_8", "hyphen_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -760, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_5", "hyphen_6", "hyphen_7", "hyphen_8", "hyphen_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -640, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_5", "hyphen_6", "hyphen_7", "hyphen_8", "hyphen_9"],
			},
		],
		"ex_name": "kidushin_29a_kol_mitzvot_haben_ul_haav_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}