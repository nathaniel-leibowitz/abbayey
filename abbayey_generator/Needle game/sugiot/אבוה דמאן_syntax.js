var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_31b_b.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"assist": "storage/miniGames/gemara/syntax/assist.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
	},
	"parameters": {
		"numOfItems": 29,
		"placeholders": [
			{ "id": "quoteend_1", "loc": { "x": 190, "y": 247 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_2", "loc": { "x": 34, "y": 247 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_3", "loc": { "x": 150, "y": 103 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_4", "loc": { "x": -394, "y": 103 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_5", "loc": { "x": 343, "y": 139 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_6", "loc": { "x": 250, "y": 139 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_7", "loc": { "x": -280, "y": 139 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_8", "loc": { "x": -86, "y": 175 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_9", "loc": { "x": 421, "y": 211 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_10", "loc": { "x": 397, "y": 211 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_11", "loc": { "x": 270, "y": 211 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_12", "loc": { "x": 225, "y": 283 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_13", "loc": { "x": 206, "y": 283 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_14", "loc": { "x": -8, "y": 283 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_15", "loc": { "x": 418, "y": 319 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_16", "loc": { "x": 403, "y": 319 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_17", "loc": { "x": -318, "y": 319 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_18", "loc": { "x": -435, "y": 319 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_19", "loc": { "x": 292, "y": 354 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_20", "loc": { "x": 72, "y": 354 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "assist_21", "loc": { "x": 48, "y": 354 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_22", "loc": { "x": -173, "y": -4 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_23", "loc": { "x": 397, "y": 31 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_24", "loc": { "x": 207, "y": 31 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_25", "loc": { "x": 320, "y": 103 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 21, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_26", "loc": { "x": -277, "y": 247 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_27", "loc": { "x": 108, "y": 319 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_28", "loc": { "x": -342, "y": 31 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_29", "loc": { "x": -265, "y": 67 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_1", "quoteend_10", "quoteend_15"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_3", "question_24"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_3", "question_24"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_1", "quoteend_10", "quoteend_15"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_11", "quotebegin_14", "quotebegin_22"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_12"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_13"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_11", "quotebegin_14", "quotebegin_22"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -670, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_1", "quoteend_10", "quoteend_15"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_16"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -760, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -760, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -640, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -640, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
			{
				"type": "assist", 
				"score": 1,
				"loc": { "x": -700, "y": -150 },
				"scale": { "x": 0.25, "y": 0.25 },
				"placeholderIds": [ "assist_21"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -670, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_11", "quotebegin_14", "quotebegin_22"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_23", "period_25"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -670, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_3", "question_24"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_23", "period_25"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_26"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_27"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -790, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_6", "speakbegin_8", "speakbegin_17", "speakbegin_19", "speakbegin_28"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -790, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_7", "speakend_9", "speakend_18", "speakend_20", "speakend_29"],
			},
		],
		"ex_name": "אבוה דמאן_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}