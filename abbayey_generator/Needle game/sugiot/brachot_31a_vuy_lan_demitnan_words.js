var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_31a_top.png",
		"במשתה": "במשתה.text",
		"בנו של רבינא": "בנו של רבינא.text",
		"אוי": "אוי.text",
		"לנו": "לנו.text",
		"ישיר": "ישיר.text",
		"כבודו": "כבודו.text",
		"שנמות": "שנמות.text",
		"אנחנו": "אנחנו.text",
		"נענה": "נענה.text",
		"אחריך": "אחריך.text",
		"עלינו": "עלינו.text",
		"היכן": "היכן.text",
		"שיגנו": "שיגנו.text",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "במשתה_1", "loc": { "x": -111, "y": -213 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו של רבינא_2", "loc": { "x": 225, "y": -177 }, "rect": { "width": 155, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אוי_3", "loc": { "x": -145, "y": -177 }, "rect": { "width": 29, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לנו_4", "loc": { "x": 349, "y": -140 }, "rect": { "width": 26, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לנו_5", "loc": { "x": 178, "y": -140 }, "rect": { "width": 26, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אוי_6", "loc": { "x": 217, "y": -140 }, "rect": { "width": 29, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ישיר_7", "loc": { "x": 105, "y": -177 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לנו_8", "loc": { "x": 48, "y": -177 }, "rect": { "width": 26, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כבודו_9", "loc": { "x": 10, "y": -177 }, "rect": { "width": 35, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנמות_10", "loc": { "x": 113, "y": -140 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אנחנו_11", "loc": { "x": -89, "y": -140 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נענה_12", "loc": { "x": 338, "y": -104 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אחריך_13", "loc": { "x": 268, "y": -104 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עלינו_14", "loc": { "x": 341, "y": -68 }, "rect": { "width": 41, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנמות_15", "loc": { "x": 284, "y": -140 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכן_16", "loc": { "x": 133, "y": -104 }, "rect": { "width": 32, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכן_17", "loc": { "x": 13, "y": -104 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיגנו_18", "loc": { "x": -125, "y": -104 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "במשתה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במשתה_1"],
			},
			{
				"type": "בנו של רבינא", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו של רבינא_2"],
			},
			{
				"type": "אוי", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אוי_3", "אוי_6"],
			},
			{
				"type": "לנו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לנו_4", "לנו_5", "לנו_8"],
			},
			{
				"type": "לנו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לנו_4", "לנו_5", "לנו_8"],
			},
			{
				"type": "אוי", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אוי_3", "אוי_6"],
			},
			{
				"type": "ישיר", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ישיר_7"],
			},
			{
				"type": "לנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לנו_4", "לנו_5", "לנו_8"],
			},
			{
				"type": "כבודו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כבודו_9"],
			},
			{
				"type": "שנמות", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנמות_10", "שנמות_15"],
			},
			{
				"type": "אנחנו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אנחנו_11"],
			},
			{
				"type": "נענה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נענה_12"],
			},
			{
				"type": "אחריך", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אחריך_13"],
			},
			{
				"type": "עלינו", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עלינו_14"],
			},
			{
				"type": "שנמות", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנמות_10", "שנמות_15"],
			},
			{
				"type": "היכן", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכן_16", "היכן_17"],
			},
			{
				"type": "היכן", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכן_16", "היכן_17"],
			},
			{
				"type": "שיגנו", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיגנו_18"],
			},
		],
		"ex_name": "brachot_31a_vuy_lan_demitnan_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}