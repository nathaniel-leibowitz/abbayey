var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_61b_top.png",
		"וכי אין": "וכי אין.text",
		"והרי שנינו במשנה": "והרי שנינו במשנה.text",
		"דירה בקומה שנייה": "דירה בקומה שנייה.text",
		"רצפת קומה ב, תקרת קומה א": "רצפת קומה ב, תקרת קומה א.text",
		"תנור קטן": "תנור קטן.text",
		"רבי שמעון": "רבי שמעון.text",
		"דירה בקומה התחתונה": "דירה בקומה התחתונה.text",
		"נשאר רווח בין התנור לקומה השנייה": "נשאר רווח בין התנור לקומה השנייה.text",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "וכי אין_1", "loc": { "x": 137, "y": -41 }, "rect": { "width": 53, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי שנינו במשנה_2", "loc": { "x": 230, "y": -5 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דירה בקומה שנייה_3", "loc": { "x": 176, "y": 68 }, "rect": { "width": 89, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רצפת קומה ב, תקרת קומה א_4", "loc": { "x": -112, "y": 68 }, "rect": { "width": 92, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תנור קטן_5", "loc": { "x": 149, "y": 104 }, "rect": { "width": 86, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רבי שמעון_6", "loc": { "x": -10, "y": -41 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רבי שמעון_7", "loc": { "x": 100, "y": 321 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דירה בקומה התחתונה_8", "loc": { "x": 334, "y": 31 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נשאר רווח בין התנור לקומה השנייה_9", "loc": { "x": 50, "y": 31 }, "rect": { "width": 194, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "וכי אין", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכי אין_1"],
			},
			{
				"type": "והרי שנינו במשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי שנינו במשנה_2"],
			},
			{
				"type": "דירה בקומה שנייה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דירה בקומה שנייה_3"],
			},
			{
				"type": "רצפת קומה ב, תקרת קומה א", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רצפת קומה ב, תקרת קומה א_4"],
			},
			{
				"type": "תנור קטן", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תנור קטן_5"],
			},
			{
				"type": "רבי שמעון", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי שמעון_6", "רבי שמעון_7"],
			},
			{
				"type": "רבי שמעון", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי שמעון_6", "רבי שמעון_7"],
			},
			{
				"type": "דירה בקומה התחתונה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דירה בקומה התחתונה_8"],
			},
			{
				"type": "נשאר רווח בין התנור לקומה השנייה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נשאר רווח בין התנור לקומה השנייה_9"],
			},
		],
		"ex_name": "babakama_61b_leit_lerash_shiura_bidleka_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}