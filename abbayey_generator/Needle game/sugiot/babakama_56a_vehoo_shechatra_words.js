var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56a_bottom.png",
		"אם נאמר": "אם נאמר.text",
		"מדוע, על שום מה": "מדוע, על שום מה.text",
		"היה לו לעשות": "היה לו לעשות.text",
		"למי שאמר, למי שסובר": "למי שאמר, למי שסובר.text",
		"מה יש לאמר, זה לא מסתדר": "מה יש לאמר, זה לא מסתדר.text",
		"לא צריך, לא זו בלבד": "לא צריך, לא זו בלבד.text",
		"היכן": "היכן.text",
		"גם": "גם.text",
		"קא משמע לן": "קא משמע לן.text",
		"מה הטעם": "מה הטעם.text",
		"תחבולה": "תחבולה.text",
		"שיש באפשרותה לעשות": "שיש באפשרותה לעשות.text",
		"עושה ויוצאת": "עושה ויוצאת.text",
		"עזב, הניח, השאיר": "עזב, הניח, השאיר.text",
		"באיזה אופן מדובר": "באיזה אופן מדובר.text",
	},
	"parameters": {
		"numOfItems": 17,
		"placeholders": [
			{ "id": "אם נאמר_1", "loc": { "x": -92, "y": 51 }, "rect": { "width": 89, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוע, על שום מה_2", "loc": { "x": 15, "y": 87 }, "rect": { "width": 68, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה לו לעשות_3", "loc": { "x": -240, "y": 87 }, "rect": { "width": 152, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוע, על שום מה_4", "loc": { "x": 177, "y": 123 }, "rect": { "width": 68, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למי שאמר, למי שסובר_5", "loc": { "x": 395, "y": 160 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למי שאמר, למי שסובר_6", "loc": { "x": -243, "y": 160 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה יש לאמר, זה לא מסתדר_7", "loc": { "x": 50, "y": 196 }, "rect": { "width": 224, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא צריך, לא זו בלבד_8", "loc": { "x": 452, "y": 304 }, "rect": { "width": 137, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכן_9", "loc": { "x": 334, "y": 304 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_10", "loc": { "x": 500, "y": 340 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_11", "loc": { "x": -258, "y": 340 }, "rect": { "width": 68, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה הטעם_12", "loc": { "x": 321, "y": 377 }, "rect": { "width": 59, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תחבולה_13", "loc": { "x": 459, "y": 413 }, "rect": { "width": 89, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיש באפשרותה לעשות_14", "loc": { "x": 293, "y": 413 }, "rect": { "width": 221, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עושה ויוצאת_15", "loc": { "x": 96, "y": 413 }, "rect": { "width": 158, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עזב, הניח, השאיר_16", "loc": { "x": -181, "y": 377 }, "rect": { "width": 113, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באיזה אופן מדובר_17", "loc": { "x": 18, "y": 51 }, "rect": { "width": 110, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_1"],
			},
			{
				"type": "מדוע, על שום מה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוע, על שום מה_2", "מדוע, על שום מה_4"],
			},
			{
				"type": "היה לו לעשות", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה לו לעשות_3"],
			},
			{
				"type": "מדוע, על שום מה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוע, על שום מה_2", "מדוע, על שום מה_4"],
			},
			{
				"type": "למי שאמר, למי שסובר", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למי שאמר, למי שסובר_5", "למי שאמר, למי שסובר_6"],
			},
			{
				"type": "למי שאמר, למי שסובר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למי שאמר, למי שסובר_5", "למי שאמר, למי שסובר_6"],
			},
			{
				"type": "מה יש לאמר, זה לא מסתדר", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה יש לאמר, זה לא מסתדר_7"],
			},
			{
				"type": "לא צריך, לא זו בלבד", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא צריך, לא זו בלבד_8"],
			},
			{
				"type": "היכן", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכן_9"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_10"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_11"],
			},
			{
				"type": "מה הטעם", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה הטעם_12"],
			},
			{
				"type": "תחבולה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תחבולה_13"],
			},
			{
				"type": "שיש באפשרותה לעשות", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיש באפשרותה לעשות_14"],
			},
			{
				"type": "עושה ויוצאת", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עושה ויוצאת_15"],
			},
			{
				"type": "עזב, הניח, השאיר", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עזב, הניח, השאיר_16"],
			},
			{
				"type": "באיזה אופן מדובר", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באיזה אופן מדובר_17"],
			},
		],
		"ex_name": "babakama_56a_vehoo_shechatra_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}