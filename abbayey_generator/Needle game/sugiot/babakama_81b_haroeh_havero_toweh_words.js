var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_81b_bottom.png",
		"משוטט, מחפש את דרכו": "משוטט, מחפש את דרכו.text",
		"חותך ענפים לפסל הדרך": "חותך ענפים לפסל הדרך.text",
		"שתאמר, שתחשוב בטעות": "שתאמר, שתחשוב בטעות.text",
		"לאיפה, לאיזה מקום": "לאיפה, לאיזה מקום.text",
		"לעלות, לצאת מהסבך": "לעלות, לצאת מהסבך.text",
		"יחזור חזרה": "יחזור חזרה.text",
		"לגבול, לשביל": "לגבול, לשביל.text",
		"הרי": "הרי.text",
		"למדנו בברייתא": "למדנו בברייתא.text",
		"להשיב אדם, לעזור לו להתמצא": "להשיב אדם, לעזור לו להתמצא.text",
		"כשהוא על השביל": "כשהוא על השביל.text",
		"בא": "בא.text",
		"יהושע בן נון": "יהושע בן נון.text",
	},
	"parameters": {
		"numOfItems": 19,
		"placeholders": [
			{ "id": "משוטט, מחפש את דרכו_1", "loc": { "x": -69, "y": -258 }, "rect": { "width": 56, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חותך ענפים לפסל הדרך_2", "loc": { "x": 430, "y": -228 }, "rect": { "width": 203, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חותך ענפים לפסל הדרך_3", "loc": { "x": 312, "y": -198 }, "rect": { "width": 263, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חותך ענפים לפסל הדרך_4", "loc": { "x": 97, "y": -169 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חותך ענפים לפסל הדרך_5", "loc": { "x": 369, "y": -139 }, "rect": { "width": 62, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חותך ענפים לפסל הדרך_6", "loc": { "x": 341, "y": -79 }, "rect": { "width": 275, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שתאמר, שתחשוב בטעות_7", "loc": { "x": 496, "y": -169 }, "rect": { "width": 68, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לאיפה, לאיזה מקום_8", "loc": { "x": 245, "y": -169 }, "rect": { "width": 65, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לאיפה, לאיזה מקום_9", "loc": { "x": -182, "y": -169 }, "rect": { "width": 65, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעלות, לצאת מהסבך_10", "loc": { "x": 173, "y": -169 }, "rect": { "width": 59, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעלות, לצאת מהסבך_11", "loc": { "x": 468, "y": -139 }, "rect": { "width": 50, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יחזור חזרה_12", "loc": { "x": 265, "y": -139 }, "rect": { "width": 134, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לגבול, לשביל_13", "loc": { "x": 150, "y": -139 }, "rect": { "width": 86, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי_14", "loc": { "x": 25, "y": -139 }, "rect": { "width": 32, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למדנו בברייתא_15", "loc": { "x": -184, "y": -139 }, "rect": { "width": 62, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להשיב אדם, לעזור לו להתמצא_16", "loc": { "x": 463, "y": -109 }, "rect": { "width": 113, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כשהוא על השביל_17", "loc": { "x": -36, "y": -109 }, "rect": { "width": 146, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא_18", "loc": { "x": -142, "y": -109 }, "rect": { "width": 47, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יהושע בן נון_19", "loc": { "x": -195, "y": -109 }, "rect": { "width": 38, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "משוטט, מחפש את דרכו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משוטט, מחפש את דרכו_1"],
			},
			{
				"type": "חותך ענפים לפסל הדרך", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חותך ענפים לפסל הדרך_2", "חותך ענפים לפסל הדרך_3", "חותך ענפים לפסל הדרך_4", "חותך ענפים לפסל הדרך_5", "חותך ענפים לפסל הדרך_6"],
			},
			{
				"type": "חותך ענפים לפסל הדרך", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חותך ענפים לפסל הדרך_2", "חותך ענפים לפסל הדרך_3", "חותך ענפים לפסל הדרך_4", "חותך ענפים לפסל הדרך_5", "חותך ענפים לפסל הדרך_6"],
			},
			{
				"type": "חותך ענפים לפסל הדרך", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חותך ענפים לפסל הדרך_2", "חותך ענפים לפסל הדרך_3", "חותך ענפים לפסל הדרך_4", "חותך ענפים לפסל הדרך_5", "חותך ענפים לפסל הדרך_6"],
			},
			{
				"type": "חותך ענפים לפסל הדרך", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חותך ענפים לפסל הדרך_2", "חותך ענפים לפסל הדרך_3", "חותך ענפים לפסל הדרך_4", "חותך ענפים לפסל הדרך_5", "חותך ענפים לפסל הדרך_6"],
			},
			{
				"type": "חותך ענפים לפסל הדרך", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חותך ענפים לפסל הדרך_2", "חותך ענפים לפסל הדרך_3", "חותך ענפים לפסל הדרך_4", "חותך ענפים לפסל הדרך_5", "חותך ענפים לפסל הדרך_6"],
			},
			{
				"type": "שתאמר, שתחשוב בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שתאמר, שתחשוב בטעות_7"],
			},
			{
				"type": "לאיפה, לאיזה מקום", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לאיפה, לאיזה מקום_8", "לאיפה, לאיזה מקום_9"],
			},
			{
				"type": "לאיפה, לאיזה מקום", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לאיפה, לאיזה מקום_8", "לאיפה, לאיזה מקום_9"],
			},
			{
				"type": "לעלות, לצאת מהסבך", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעלות, לצאת מהסבך_10", "לעלות, לצאת מהסבך_11"],
			},
			{
				"type": "לעלות, לצאת מהסבך", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעלות, לצאת מהסבך_10", "לעלות, לצאת מהסבך_11"],
			},
			{
				"type": "יחזור חזרה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יחזור חזרה_12"],
			},
			{
				"type": "לגבול, לשביל", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לגבול, לשביל_13"],
			},
			{
				"type": "הרי", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי_14"],
			},
			{
				"type": "למדנו בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למדנו בברייתא_15"],
			},
			{
				"type": "להשיב אדם, לעזור לו להתמצא", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להשיב אדם, לעזור לו להתמצא_16"],
			},
			{
				"type": "כשהוא על השביל", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כשהוא על השביל_17"],
			},
			{
				"type": "בא", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא_18"],
			},
			{
				"type": "יהושע בן נון", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יהושע בן נון_19"],
			},
		],
		"ex_name": "babakama_81b_haroeh_havero_toweh_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}