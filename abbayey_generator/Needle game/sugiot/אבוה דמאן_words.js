var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_31b_b.png",
		"ואלו המילים, הדין הזה תקף כש...": "ואלו המילים, הדין הזה תקף כש....text",
		"אביו": "אביו.text",
		"של מי": "של מי.text",
		"אם נאמר": "אם נאמר.text",
		"של המתורגמן": "של המתורגמן.text",
		"וכי, האם, כלום": "וכי, האם, כלום.text",
		"והמתורגמן": "והמתורגמן.text",
		"כך": "כך.text",
		"אינו מחוייב": "אינו מחוייב.text",
		"כמו זה, כמו המקרה של": "כמו זה, כמו המקרה של.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "ואלו המילים, הדין הזה תקף כש..._1", "loc": { "x": -362, "y": 150 }, "rect": { "width": 134, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביו_2", "loc": { "x": 154, "y": 257 }, "rect": { "width": 71, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "של מי_3", "loc": { "x": 78, "y": 257 }, "rect": { "width": 71, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נאמר_4", "loc": { "x": -11, "y": 257 }, "rect": { "width": 98, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביו_5", "loc": { "x": -103, "y": 257 }, "rect": { "width": 71, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "של המתורגמן_6", "loc": { "x": -203, "y": 257 }, "rect": { "width": 134, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכי, האם, כלום_7", "loc": { "x": -306, "y": 257 }, "rect": { "width": 50, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והמתורגמן_8", "loc": { "x": 430, "y": 365 }, "rect": { "width": 110, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_9", "loc": { "x": 274, "y": 365 }, "rect": { "width": 50, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אינו מחוייב_10", "loc": { "x": 395, "y": 293 }, "rect": { "width": 203, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמו זה, כמו המקרה של_11", "loc": { "x": 373, "y": 329 }, "rect": { "width": 86, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ואלו המילים, הדין הזה תקף כש...", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואלו המילים, הדין הזה תקף כש..._1"],
			},
			{
				"type": "אביו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביו_2", "אביו_5"],
			},
			{
				"type": "של מי", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "של מי_3"],
			},
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_4"],
			},
			{
				"type": "אביו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביו_2", "אביו_5"],
			},
			{
				"type": "של המתורגמן", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "של המתורגמן_6"],
			},
			{
				"type": "וכי, האם, כלום", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכי, האם, כלום_7"],
			},
			{
				"type": "והמתורגמן", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והמתורגמן_8"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_9"],
			},
			{
				"type": "אינו מחוייב", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אינו מחוייב_10"],
			},
			{
				"type": "כמו זה, כמו המקרה של", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמו זה, כמו המקרה של_11"],
			},
		],
		"ex_name": "אבוה דמאן_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}