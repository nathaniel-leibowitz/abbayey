var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33b_34a.png",
		"ועוד, קושיא נוספת": "ועוד, קושיא נוספת.text",
		"במדנו במשנה": "במדנו במשנה.text",
		"ועוד אין, אין נוספים": "ועוד אין, אין נוספים.text",
		"והרי יש": "והרי יש.text",
		"למדנו בבריתא": "למדנו בבריתא.text",
	},
	"parameters": {
		"numOfItems": 5,
		"placeholders": [
			{ "id": "ועוד, קושיא נוספת_1", "loc": { "x": -151, "y": -197 }, "rect": { "width": 62, "height": 45 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במדנו במשנה_2", "loc": { "x": -90, "y": 28 }, "rect": { "width": 89, "height": 45 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ועוד אין, אין נוספים_3", "loc": { "x": -326, "y": 85 }, "rect": { "width": 179, "height": 45 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_4", "loc": { "x": 307, "y": 141 }, "rect": { "width": 143, "height": 45 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למדנו בבריתא_5", "loc": { "x": 507, "y": -423 }, "rect": { "width": 65, "height": 45 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ועוד, קושיא נוספת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ועוד, קושיא נוספת_1"],
			},
			{
				"type": "במדנו במשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במדנו במשנה_2"],
			},
			{
				"type": "ועוד אין, אין נוספים", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ועוד אין, אין נוספים_3"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_4"],
			},
			{
				"type": "למדנו בבריתא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למדנו בבריתא_5"],
			},
		],
		"ex_name": "kidushin_34a_ein_lemedin_min_haklalot_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}