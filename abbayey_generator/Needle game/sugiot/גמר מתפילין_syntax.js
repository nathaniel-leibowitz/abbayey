var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/kidushin_34a_bottom.png",
		"question": "storage/miniGames/gemara/question.png",
		"colon": "storage/miniGames/gemara/colon.png",
		"period": "storage/miniGames/gemara/period.png",
		"qushia": "storage/miniGames/gemara/qushia.png",
		"resolution": "storage/miniGames/gemara/resolution.png",
		"biblequote": "storage/miniGames/gemara/biblequote.gif",
		"interrobang": "storage/miniGames/gemara/interrobang.png",
		"hyphen": "storage/miniGames/gemara/hyphen.png",
	},
	"parameters": {
		"numOfItems": 23,
		"placeholders": [
			{ "id": "question_1", "loc": { "x": -295, "y": -300 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "colon_2", "loc": { "x": 114, "y": -260 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_3", "loc": { "x": -288, "y": -220 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 25, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "colon_4", "loc": { "x": -132, "y": -181 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_5", "loc": { "x": -289, "y": -141 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 25, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_6", "loc": { "x": 5, "y": -101 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_7", "loc": { "x": 170, "y": 18 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_8", "loc": { "x": -238, "y": 18 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_9", "loc": { "x": -32, "y": 57 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": -292, "y": 57 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "interrobang_11", "loc": { "x": -126, "y": 97 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_12", "loc": { "x": -144, "y": 97 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_13", "loc": { "x": -181, "y": 137 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_14", "loc": { "x": 19, "y": 176 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_15", "loc": { "x": 290, "y": 216 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": 185, "y": 216 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_17", "loc": { "x": 95, "y": 256 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_18", "loc": { "x": -94, "y": 295 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_19", "loc": { "x": -292, "y": 295 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_20", "loc": { "x": -100, "y": 375 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "hyphen_21", "loc": { "x": -89, "y": 176 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "hyphen_22", "loc": { "x": -293, "y": 216 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_23", "loc": { "x": -102, "y": 216 }, "rect": { "width": 21, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 25, "url": "storage/miniGames/gemara/arrow6.png" },
		],
		"items": [
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2", "colon_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_5", "period_23"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -730, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2", "colon_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_5", "period_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6", "qushia_8", "qushia_17"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_7", "resolution_12", "resolution_20"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6", "qushia_8", "qushia_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_11"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_7", "resolution_12", "resolution_20"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6", "qushia_8", "qushia_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_9", "biblequote_10", "biblequote_13", "biblequote_14", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -670, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_7", "resolution_12", "resolution_20"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_21", "hyphen_22"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_21", "hyphen_22"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_5", "period_23"],
			},
		],
		"ex_name": "גמר מתפילין_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}