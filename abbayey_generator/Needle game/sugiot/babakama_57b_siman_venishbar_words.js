var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_57b_top.png",
		"קל וחומר": "קל וחומר.text",
		"תעלה בדעתך": "תעלה בדעתך.text",
		"ואם": "ואם.text",
		"מדוע, על שום מה": "מדוע, על שום מה.text",
		"יש": "יש.text",
		"איכותי, בלתי ניתן להפרכה": "איכותי, בלתי ניתן להפרכה.text",
		"להפריך": "להפריך.text",
		"סובר": "סובר.text",
		"התנא הזה, החכם הזה": "התנא הזה, החכם הזה.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "קל וחומר_1", "loc": { "x": -196, "y": 102 }, "rect": { "width": 44, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קל וחומר_2", "loc": { "x": 83, "y": 248 }, "rect": { "width": 44, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תעלה בדעתך_3", "loc": { "x": -247, "y": 248 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואם_4", "loc": { "x": -194, "y": 248 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוע, על שום מה_5", "loc": { "x": -52, "y": 285 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יש_6", "loc": { "x": -329, "y": 285 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איכותי, בלתי ניתן להפרכה_7", "loc": { "x": -56, "y": 248 }, "rect": { "width": 224, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איכותי, בלתי ניתן להפרכה_8", "loc": { "x": -192, "y": 285 }, "rect": { "width": 200, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להפריך_9", "loc": { "x": 119, "y": 322 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סובר_10", "loc": { "x": -328, "y": 358 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התנא הזה, החכם הזה_11", "loc": { "x": 108, "y": 395 }, "rect": { "width": 104, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "קל וחומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קל וחומר_1", "קל וחומר_2"],
			},
			{
				"type": "קל וחומר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קל וחומר_1", "קל וחומר_2"],
			},
			{
				"type": "תעלה בדעתך", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תעלה בדעתך_3"],
			},
			{
				"type": "ואם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואם_4"],
			},
			{
				"type": "מדוע, על שום מה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוע, על שום מה_5"],
			},
			{
				"type": "יש", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יש_6"],
			},
			{
				"type": "איכותי, בלתי ניתן להפרכה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איכותי, בלתי ניתן להפרכה_7", "איכותי, בלתי ניתן להפרכה_8"],
			},
			{
				"type": "איכותי, בלתי ניתן להפרכה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איכותי, בלתי ניתן להפרכה_7", "איכותי, בלתי ניתן להפרכה_8"],
			},
			{
				"type": "להפריך", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להפריך_9"],
			},
			{
				"type": "סובר", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סובר_10"],
			},
			{
				"type": "התנא הזה, החכם הזה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התנא הזה, החכם הזה_11"],
			},
		],
		"ex_name": "babakama_57b_siman_venishbar_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}