var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_90b_top.png",
		"רבי עקיבא חולק ואינו מסכים": "רבי עקיבא חולק ואינו מסכים.text",
		"מתיחסים אליהם": "מתיחסים אליהם.text",
		"שהפסידו את כספם": "שהפסידו את כספם.text",
		"אנשים חופשיים ומכובדים": "אנשים חופשיים ומכובדים.text",
		"הסיר את כיסוי הראש": "הסיר את כיסוי הראש.text",
		"במקום ציבורי": "במקום ציבורי.text",
		"חכה עם פסק הדין": "חכה עם פסק הדין.text",
		"הגיע האשה להתלונן": "הגיע האשה להתלונן.text",
		"חייב רבי עקיבא את הפורע": "חייב רבי עקיבא את הפורע.text",
		"המתין לה, עקב אחריה": "המתין לה, עקב אחריה.text",
		"כמות שמן בשווי של מטבע איסר": "כמות שמן בשווי של מטבע איסר.text",
		"הסירה את כיסוי ראשה": "הסירה את כיסוי ראשה.text",
		"הביא הפורע עדים שראו זאת": "הביא הפורע עדים שראו זאת.text",
		"אמר רבי עקיבא לפורע": "אמר רבי עקיבא לפורע.text",
		"טענתך אינה מתקבלת": "טענתך אינה מתקבלת.text",
		"לאשה הזו שמבזה את עצמה בשביל שמן": "לאשה הזו שמבזה את עצמה בשביל שמן.text",
		"אינו חייב לשלם לעצמו": "אינו חייב לשלם לעצמו.text",
		"מחוייבים לשלם לו": "מחוייבים לשלם לו.text",
		"מרטיבה ידה בשמן": "מרטיבה ידה בשמן.text",
		"ומושחת את שערה בשמן": "ומושחת את שערה בשמן.text",
		"גובה הפיצוי כמעמד וכבוד הנפגע": "גובה הפיצוי כמעמד וכבוד הנפגע.text",
	},
	"parameters": {
		"numOfItems": 21,
		"placeholders": [
			{ "id": "רבי עקיבא חולק ואינו מסכים_1", "loc": { "x": -64, "y": -185 }, "rect": { "width": 188, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתיחסים אליהם_2", "loc": { "x": -2, "y": -149 }, "rect": { "width": 143, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהפסידו את כספם_3", "loc": { "x": 75, "y": -112 }, "rect": { "width": 203, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אנשים חופשיים ומכובדים_4", "loc": { "x": 251, "y": -112 }, "rect": { "width": 116, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הסיר את כיסוי הראש_5", "loc": { "x": -121, "y": -76 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במקום ציבורי_6", "loc": { "x": 164, "y": -40 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חכה עם פסק הדין_7", "loc": { "x": 247, "y": 32 }, "rect": { "width": 122, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הגיע האשה להתלונן_8", "loc": { "x": 87, "y": -40 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חייב רבי עקיבא את הפורע_9", "loc": { "x": 328, "y": -4 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "המתין לה, עקב אחריה_10", "loc": { "x": -22, "y": 32 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמות שמן בשווי של מטבע איסר_11", "loc": { "x": 297, "y": 105 }, "rect": { "width": 137, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הסירה את כיסוי ראשה_12", "loc": { "x": 187, "y": 105 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הביא הפורע עדים שראו זאת_13", "loc": { "x": -41, "y": 141 }, "rect": { "width": 233, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אמר רבי עקיבא לפורע_14", "loc": { "x": 207, "y": 213 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "טענתך אינה מתקבלת_15", "loc": { "x": 61, "y": 213 }, "rect": { "width": 209, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לאשה הזו שמבזה את עצמה בשביל שמן_16", "loc": { "x": 2, "y": 177 }, "rect": { "width": 32, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אינו חייב לשלם לעצמו_17", "loc": { "x": -39, "y": 250 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מחוייבים לשלם לו_18", "loc": { "x": 172, "y": 286 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מרטיבה ידה בשמן_19", "loc": { "x": -110, "y": 105 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ומושחת את שערה בשמן_20", "loc": { "x": 226, "y": 141 }, "rect": { "width": 278, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גובה הפיצוי כמעמד וכבוד הנפגע_21", "loc": { "x": 141, "y": -185 }, "rect": { "width": 194, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "רבי עקיבא חולק ואינו מסכים", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי עקיבא חולק ואינו מסכים_1"],
			},
			{
				"type": "מתיחסים אליהם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתיחסים אליהם_2"],
			},
			{
				"type": "שהפסידו את כספם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהפסידו את כספם_3"],
			},
			{
				"type": "אנשים חופשיים ומכובדים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אנשים חופשיים ומכובדים_4"],
			},
			{
				"type": "הסיר את כיסוי הראש", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הסיר את כיסוי הראש_5"],
			},
			{
				"type": "במקום ציבורי", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במקום ציבורי_6"],
			},
			{
				"type": "חכה עם פסק הדין", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חכה עם פסק הדין_7"],
			},
			{
				"type": "הגיע האשה להתלונן", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הגיע האשה להתלונן_8"],
			},
			{
				"type": "חייב רבי עקיבא את הפורע", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חייב רבי עקיבא את הפורע_9"],
			},
			{
				"type": "המתין לה, עקב אחריה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "המתין לה, עקב אחריה_10"],
			},
			{
				"type": "כמות שמן בשווי של מטבע איסר", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמות שמן בשווי של מטבע איסר_11"],
			},
			{
				"type": "הסירה את כיסוי ראשה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הסירה את כיסוי ראשה_12"],
			},
			{
				"type": "הביא הפורע עדים שראו זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הביא הפורע עדים שראו זאת_13"],
			},
			{
				"type": "אמר רבי עקיבא לפורע", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אמר רבי עקיבא לפורע_14"],
			},
			{
				"type": "טענתך אינה מתקבלת", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "טענתך אינה מתקבלת_15"],
			},
			{
				"type": "לאשה הזו שמבזה את עצמה בשביל שמן", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לאשה הזו שמבזה את עצמה בשביל שמן_16"],
			},
			{
				"type": "אינו חייב לשלם לעצמו", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אינו חייב לשלם לעצמו_17"],
			},
			{
				"type": "מחוייבים לשלם לו", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מחוייבים לשלם לו_18"],
			},
			{
				"type": "מרטיבה ידה בשמן", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מרטיבה ידה בשמן_19"],
			},
			{
				"type": "ומושחת את שערה בשמן", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ומושחת את שערה בשמן_20"],
			},
			{
				"type": "גובה הפיצוי כמעמד וכבוד הנפגע", 
				"score": 1,
				"loc": { "x": -700, "y": 400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גובה הפיצוי כמעמד וכבוד הנפגע_21"],
			},
		],
		"ex_name": "babakama_90b_ze_haklal_vakol_lefi_kvodo_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}