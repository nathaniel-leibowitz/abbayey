var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/hanisrafim_baniskalim.png",
		"כך": "כך.text",
		"חריף וחכם": "חריף וחכם.text",
		"לאביך": "לאביך.text",
		"שהרי שנינו בבריתא": "שהרי שנינו בבריתא.text",
		"תאמר": "תאמר.text",
		"מצער אותו, מבאס אותו": "מצער אותו, מבאס אותו.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "כך_1", "loc": { "x": -105, "y": 138 }, "rect": { "width": 59, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חריף וחכם_2", "loc": { "x": -267, "y": 95 }, "rect": { "width": 86, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לאביך_3", "loc": { "x": -11, "y": 138 }, "rect": { "width": 92, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי שנינו בבריתא_4", "loc": { "x": -203, "y": 138 }, "rect": { "width": 89, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תאמר_5", "loc": { "x": 177, "y": 138 }, "rect": { "width": 80, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצער אותו, מבאס אותו_6", "loc": { "x": 146, "y": 312 }, "rect": { "width": 314, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_1"],
			},
			{
				"type": "חריף וחכם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חריף וחכם_2"],
			},
			{
				"type": "לאביך", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לאביך_3"],
			},
			{
				"type": "שהרי שנינו בבריתא", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי שנינו בבריתא_4"],
			},
			{
				"type": "תאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תאמר_5"],
			},
			{
				"type": "מצער אותו, מבאס אותו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצער אותו, מבאס אותו_6"],
			},
		],
		"ex_name": "הנשרפים בנסקלים_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}