var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29b_top.png",
		"זה ברור מאליו": "זה ברור מאליו.text",
		"התנה, קישר": "התנה, קישר.text",
		"שתאמר": "שתאמר.text",
		"נלמד, בשיטת גזירה שווה": "נלמד, בשיטת גזירה שווה.text",
		"בהמשך, בנושא ההוא": "בהמשך, בנושא ההוא.text",
		"תחילת כוחו, בנו הראשון": "תחילת כוחו, בנו הראשון.text",
		"זה מה שבא להשמיע לנו": "זה מה שבא להשמיע לנו.text",
		"התורה, הקבה": "התורה, הקבה.text",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "זה ברור מאליו_1", "loc": { "x": -228, "y": 293 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התנה, קישר_2", "loc": { "x": 78, "y": 330 }, "rect": { "width": 53, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שתאמר_3", "loc": { "x": -168, "y": 330 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נלמד, בשיטת גזירה שווה_4", "loc": { "x": -246, "y": 330 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בהמשך, בנושא ההוא_5", "loc": { "x": -46, "y": 366 }, "rect": { "width": 53, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תחילת כוחו, בנו הראשון_6", "loc": { "x": -147, "y": 366 }, "rect": { "width": 134, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תחילת כוחו, בנו הראשון_7", "loc": { "x": 102, "y": 402 }, "rect": { "width": 158, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "זה מה שבא להשמיע לנו_8", "loc": { "x": -32, "y": 402 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הקבה_9", "loc": { "x": -2, "y": 330 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "זה ברור מאליו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זה ברור מאליו_1"],
			},
			{
				"type": "התנה, קישר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התנה, קישר_2"],
			},
			{
				"type": "שתאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שתאמר_3"],
			},
			{
				"type": "נלמד, בשיטת גזירה שווה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נלמד, בשיטת גזירה שווה_4"],
			},
			{
				"type": "בהמשך, בנושא ההוא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בהמשך, בנושא ההוא_5"],
			},
			{
				"type": "תחילת כוחו, בנו הראשון", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תחילת כוחו, בנו הראשון_6", "תחילת כוחו, בנו הראשון_7"],
			},
			{
				"type": "תחילת כוחו, בנו הראשון", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תחילת כוחו, בנו הראשון_6", "תחילת כוחו, בנו הראשון_7"],
			},
			{
				"type": "זה מה שבא להשמיע לנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זה מה שבא להשמיע לנו_8"],
			},
			{
				"type": "התורה, הקבה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הקבה_9"],
			},
		],
		"ex_name": "kidushin_29b_hamisha_banim_mehamesh_nashim_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}