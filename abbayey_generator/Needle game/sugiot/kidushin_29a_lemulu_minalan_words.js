var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_bottom.png",
		"מאיפה לנו, מנין לומדים זאת": "מאיפה לנו, מנין לומדים זאת.text",
		"והיכן ש, במידה ו": "והיכן ש, במידה ו.text",
		"אביו": "אביו.text",
		"עצמו": "עצמו.text",
		"מל אותו": "מל אותו.text",
		"הוא, הילד": "הוא, הילד.text",
		"בית הדין": "בית הדין.text",
		"שכתוב בפסוק": "שכתוב בפסוק.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "מאיפה לנו, מנין לומדים זאת_1", "loc": { "x": -297, "y": -354 }, "rect": { "width": 53, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיכן ש, במידה ו_2", "loc": { "x": -312, "y": -318 }, "rect": { "width": 71, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביו_3", "loc": { "x": -24, "y": -281 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיכן ש, במידה ו_4", "loc": { "x": -305, "y": -245 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עצמו_5", "loc": { "x": -378, "y": -209 }, "rect": { "width": 77, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מל אותו_6", "loc": { "x": 65, "y": -281 }, "rect": { "width": 83, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מל אותו_7", "loc": { "x": 48, "y": -209 }, "rect": { "width": 83, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא, הילד_8", "loc": { "x": -205, "y": -209 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית הדין_9", "loc": { "x": -41, "y": -209 }, "rect": { "width": 86, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_10", "loc": { "x": -377, "y": -354 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_11", "loc": { "x": 66, "y": -245 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_12", "loc": { "x": 448, "y": -173 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_1"],
			},
			{
				"type": "והיכן ש, במידה ו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיכן ש, במידה ו_2", "והיכן ש, במידה ו_4"],
			},
			{
				"type": "אביו", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביו_3"],
			},
			{
				"type": "והיכן ש, במידה ו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיכן ש, במידה ו_2", "והיכן ש, במידה ו_4"],
			},
			{
				"type": "עצמו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עצמו_5"],
			},
			{
				"type": "מל אותו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מל אותו_6", "מל אותו_7"],
			},
			{
				"type": "מל אותו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מל אותו_6", "מל אותו_7"],
			},
			{
				"type": "הוא, הילד", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא, הילד_8"],
			},
			{
				"type": "בית הדין", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית הדין_9"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_10", "שכתוב בפסוק_11", "שכתוב בפסוק_12"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_10", "שכתוב בפסוק_11", "שכתוב בפסוק_12"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_10", "שכתוב בפסוק_11", "שכתוב בפסוק_12"],
			},
		],
		"ex_name": "kidushin_29a_lemulu_minalan_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}