var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33a_top.png",
		"ושמא": "ושמא.text",
		"שאם כן": "שאם כן.text",
		"העולים לירושלים להביא ביכורים": "העולים לירושלים להביא ביכורים.text",
		"שונה שם, יש סיבה אחרת לדין שם": "שונה שם, יש סיבה אחרת לדין שם.text",
		"להבא ימנעו מהבאת ביכורים": "להבא ימנעו מהבאת ביכורים.text",
		"חשובים אנשים בזמן שמקיימים מצווה": "חשובים אנשים בזמן שמקיימים מצווה.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "ושמא_1", "loc": { "x": 236, "y": 366 }, "rect": { "width": 86, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאם כן_2", "loc": { "x": -44, "y": 366 }, "rect": { "width": 71, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "העולים לירושלים להביא ביכורים_3", "loc": { "x": -64, "y": 326 }, "rect": { "width": 101, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שונה שם, יש סיבה אחרת לדין שם_4", "loc": { "x": 92, "y": 366 }, "rect": { "width": 155, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להבא ימנעו מהבאת ביכורים_5", "loc": { "x": -384, "y": 366 }, "rect": { "width": 290, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חשובים אנשים בזמן שמקיימים מצווה_6", "loc": { "x": 239, "y": 326 }, "rect": { "width": 302, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ושמא", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושמא_1"],
			},
			{
				"type": "שאם כן", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאם כן_2"],
			},
			{
				"type": "העולים לירושלים להביא ביכורים", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "העולים לירושלים להביא ביכורים_3"],
			},
			{
				"type": "שונה שם, יש סיבה אחרת לדין שם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שונה שם, יש סיבה אחרת לדין שם_4"],
			},
			{
				"type": "להבא ימנעו מהבאת ביכורים", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להבא ימנעו מהבאת ביכורים_5"],
			},
			{
				"type": "חשובים אנשים בזמן שמקיימים מצווה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חשובים אנשים בזמן שמקיימים מצווה_6"],
			},
		],
		"ex_name": "חסרון כיס_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}