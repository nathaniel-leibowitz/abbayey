var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/babakama_56a_middle.png",
		"ועוד, ונוספים": "ועוד, ונוספים.text",
		"אין": "אין.text",
		"והרי יש": "והרי יש.text",
		"רעל": "רעל.text",
		"אכן כן": "אכן כן.text",
		"יש": "יש.text",
		"הרבה": "הרבה.text",
		"ואלו": "ואלו.text",
		"היה צורך להשמיע לנו": "היה צורך להשמיע לנו.text",
		"גם": "גם.text",
		"קא משמע לן": "קא משמע לן.text",
	},
	"parameters": {
		"numOfItems": 15,
		"placeholders": [
			{ "id": "ועוד, ונוספים_1", "loc": { "x": 210, "y": -248 }, "rect": { "width": 41, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אין_2", "loc": { "x": 150, "y": -248 }, "rect": { "width": 62, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_3", "loc": { "x": 63, "y": -248 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_4", "loc": { "x": 103, "y": -140 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_5", "loc": { "x": 163, "y": -67 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_6", "loc": { "x": 85, "y": 5 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי יש_7", "loc": { "x": 318, "y": 78 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רעל_8", "loc": { "x": -100, "y": -140 }, "rect": { "width": 119, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אכן כן_9", "loc": { "x": 345, "y": 186 }, "rect": { "width": 38, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יש_10", "loc": { "x": 201, "y": 186 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרבה_11", "loc": { "x": 123, "y": 186 }, "rect": { "width": 68, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואלו_12", "loc": { "x": 51, "y": 186 }, "rect": { "width": 50, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה צורך להשמיע לנו_13", "loc": { "x": -72, "y": 186 }, "rect": { "width": 176, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_14", "loc": { "x": 10, "y": 222 }, "rect": { "width": 41, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_15", "loc": { "x": 332, "y": 259 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ועוד, ונוספים", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ועוד, ונוספים_1"],
			},
			{
				"type": "אין", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אין_2"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_3", "והרי יש_4", "והרי יש_5", "והרי יש_6", "והרי יש_7"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_3", "והרי יש_4", "והרי יש_5", "והרי יש_6", "והרי יש_7"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_3", "והרי יש_4", "והרי יש_5", "והרי יש_6", "והרי יש_7"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_3", "והרי יש_4", "והרי יש_5", "והרי יש_6", "והרי יש_7"],
			},
			{
				"type": "והרי יש", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי יש_3", "והרי יש_4", "והרי יש_5", "והרי יש_6", "והרי יש_7"],
			},
			{
				"type": "רעל", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רעל_8"],
			},
			{
				"type": "אכן כן", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אכן כן_9"],
			},
			{
				"type": "יש", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יש_10"],
			},
			{
				"type": "הרבה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרבה_11"],
			},
			{
				"type": "ואלו", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואלו_12"],
			},
			{
				"type": "היה צורך להשמיע לנו", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה צורך להשמיע לנו_13"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_14"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_15"],
			},
		],
		"ex_name": "babakama_56a_vetoo_leika_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}