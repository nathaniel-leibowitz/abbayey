var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_61b_bottom.png",
		"אם כך, אם כפי שאתה אומר": "אם כך, אם כפי שאתה אומר.text",
		"עד ששנה, במקום לשנות": "עד ששנה, במקום לשנות.text",
		"בסוף המשנה": "בסוף המשנה.text",
		"בשני מקרים הוא חולק ומתווכח": "בשני מקרים הוא חולק ומתווכח.text",
		"הוא חולק": "הוא חולק.text",
		"והוא חולק גם": "והוא חולק גם.text",
		"במה דברים אמורים": "במה דברים אמורים.text",
		"שישנה ויחלוק באותו נושא": "שישנה ויחלוק באותו נושא.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "אם כך, אם כפי שאתה אומר_1", "loc": { "x": 207, "y": -209 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עד ששנה, במקום לשנות_2", "loc": { "x": 121, "y": -209 }, "rect": { "width": 77, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בסוף המשנה_3", "loc": { "x": 45, "y": -209 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בשני מקרים הוא חולק ומתווכח_4", "loc": { "x": -81, "y": 8 }, "rect": { "width": 158, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא חולק_5", "loc": { "x": 332, "y": 44 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והוא חולק גם_6", "loc": { "x": 11, "y": 117 }, "rect": { "width": 125, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במה דברים אמורים_7", "loc": { "x": 95, "y": -101 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שישנה ויחלוק באותו נושא_8", "loc": { "x": 248, "y": -101 }, "rect": { "width": 233, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אם כך, אם כפי שאתה אומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כך, אם כפי שאתה אומר_1"],
			},
			{
				"type": "עד ששנה, במקום לשנות", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עד ששנה, במקום לשנות_2"],
			},
			{
				"type": "בסוף המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בסוף המשנה_3"],
			},
			{
				"type": "בשני מקרים הוא חולק ומתווכח", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בשני מקרים הוא חולק ומתווכח_4"],
			},
			{
				"type": "הוא חולק", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא חולק_5"],
			},
			{
				"type": "והוא חולק גם", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והוא חולק גם_6"],
			},
			{
				"type": "במה דברים אמורים", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במה דברים אמורים_7"],
			},
			{
				"type": "שישנה ויחלוק באותו נושא", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שישנה ויחלוק באותו נושא_8"],
			},
		],
		"ex_name": "babakama_61b_amar_rava_betartey_pligey_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}