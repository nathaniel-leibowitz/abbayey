var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_32atop.png",
		"הורו": "הורו.text",
		"כמו מי שאמר": "כמו מי שאמר.text",
		"מביאים סתירה מברייתא": "מביאים סתירה מברייתא.text",
		"מה יוצא לו מזה": "מה יוצא לו מזה.text",
		"וכמו זה": "וכמו זה.text",
		"אם כך": "אם כך.text",
		"אלך": "אלך.text",
		"בפני, בנוכחות": "בפני, בנוכחות.text",
		"אראה": "אראה.text",
		"והרי": "והרי.text",
		"שעשה": "שעשה.text",
		"ושמא": "ושמא.text",
		"לכבוד שלו": "לכבוד שלו.text",
		"בא שמע, סתירה מברייתא": "בא שמע, סתירה מברייתא.text",
	},
	"parameters": {
		"numOfItems": 19,
		"placeholders": [
			{ "id": "הורו_1", "loc": { "x": -111, "y": -399 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמו מי שאמר_2", "loc": { "x": 216, "y": -330 }, "rect": { "width": 68, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מביאים סתירה מברייתא_3", "loc": { "x": 3, "y": -330 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה יוצא לו מזה_4", "loc": { "x": -119, "y": -227 }, "rect": { "width": 272, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכמו זה_5", "loc": { "x": -205, "y": 186 }, "rect": { "width": 89, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה יוצא לו מזה_6", "loc": { "x": 140, "y": 186 }, "rect": { "width": 266, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה יוצא לו מזה_7", "loc": { "x": 163, "y": 48 }, "rect": { "width": 197, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם כך_8", "loc": { "x": -49, "y": -55 }, "rect": { "width": 92, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אלך_9", "loc": { "x": -49, "y": 254 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בפני, בנוכחות_10", "loc": { "x": 214, "y": 254 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אראה_11", "loc": { "x": -118, "y": 254 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי_12", "loc": { "x": 206, "y": 358 }, "rect": { "width": 50, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שעשה_13", "loc": { "x": -217, "y": 392 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ושמא_14", "loc": { "x": 53, "y": 289 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכבוד שלו_15", "loc": { "x": -208, "y": 323 }, "rect": { "width": 89, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שעשה_16", "loc": { "x": -163, "y": 358 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ושמא_17", "loc": { "x": 92, "y": 392 }, "rect": { "width": 92, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא שמע, סתירה מברייתא_18", "loc": { "x": 41, "y": -193 }, "rect": { "width": 59, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא שמע, סתירה מברייתא_19", "loc": { "x": 221, "y": 82 }, "rect": { "width": 59, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "הורו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הורו_1"],
			},
			{
				"type": "כמו מי שאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמו מי שאמר_2"],
			},
			{
				"type": "מביאים סתירה מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מביאים סתירה מברייתא_3"],
			},
			{
				"type": "מה יוצא לו מזה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה יוצא לו מזה_4", "מה יוצא לו מזה_6", "מה יוצא לו מזה_7"],
			},
			{
				"type": "וכמו זה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכמו זה_5"],
			},
			{
				"type": "מה יוצא לו מזה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה יוצא לו מזה_4", "מה יוצא לו מזה_6", "מה יוצא לו מזה_7"],
			},
			{
				"type": "מה יוצא לו מזה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה יוצא לו מזה_4", "מה יוצא לו מזה_6", "מה יוצא לו מזה_7"],
			},
			{
				"type": "אם כך", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כך_8"],
			},
			{
				"type": "אלך", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלך_9"],
			},
			{
				"type": "בפני, בנוכחות", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בפני, בנוכחות_10"],
			},
			{
				"type": "אראה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אראה_11"],
			},
			{
				"type": "והרי", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי_12"],
			},
			{
				"type": "שעשה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שעשה_13", "שעשה_16"],
			},
			{
				"type": "ושמא", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושמא_14", "ושמא_17"],
			},
			{
				"type": "לכבוד שלו", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכבוד שלו_15"],
			},
			{
				"type": "שעשה", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שעשה_13", "שעשה_16"],
			},
			{
				"type": "ושמא", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושמא_14", "ושמא_17"],
			},
			{
				"type": "בא שמע, סתירה מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא שמע, סתירה מברייתא_18", "בא שמע, סתירה מברייתא_19"],
			},
			{
				"type": "בא שמע, סתירה מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא שמע, סתירה מברייתא_18", "בא שמע, סתירה מברייתא_19"],
			},
		],
		"ex_name": "משל מי_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}