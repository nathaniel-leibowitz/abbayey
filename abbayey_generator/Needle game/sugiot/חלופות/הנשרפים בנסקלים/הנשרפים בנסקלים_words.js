var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/hanisrafim_baniskalim.png",
		"אם כך": "אם כך.text",
		"ראה את הסוף": "ראה את הסוף.text",
		"שנה, לימד, ציטט": "שנה, לימד, ציטט.text",
		"כך": "כך.text",
		"מה קשור לכאן": "מה קשור לכאן.text",
		"תוציא את הדין מתוך זה ש": "תוציא את הדין מתוך זה ש.text",
		"בנו": "בנו.text",
		"חריף וחכם": "חריף וחכם.text",
	},
	"parameters": {
		"numOfItems": 10,
		"placeholders": [
			{ "id": "אם כך_1", "loc": { "x": 38, "y": -122 }, "rect": { "width": 113, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ראה את הסוף_2", "loc": { "x": -106, "y": -122 }, "rect": { "width": 185, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנה, לימד, ציטט_3", "loc": { "x": -53, "y": -383 }, "rect": { "width": 77, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_4", "loc": { "x": -297, "y": -253 }, "rect": { "width": 56, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה קשור לכאן_5", "loc": { "x": 233, "y": -209 }, "rect": { "width": 167, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תוציא את הדין מתוך זה ש_6", "loc": { "x": -152, "y": -209 }, "rect": { "width": 137, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_7", "loc": { "x": -105, "y": 138 }, "rect": { "width": 59, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_8", "loc": { "x": 181, "y": -340 }, "rect": { "width": 77, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_9", "loc": { "x": 48, "y": -253 }, "rect": { "width": 77, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חריף וחכם_10", "loc": { "x": -268, "y": 95 }, "rect": { "width": 110, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אם כך", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כך_1"],
			},
			{
				"type": "ראה את הסוף", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראה את הסוף_2"],
			},
			{
				"type": "שנה, לימד, ציטט", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנה, לימד, ציטט_3"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_4", "כך_7"],
			},
			{
				"type": "מה קשור לכאן", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה קשור לכאן_5"],
			},
			{
				"type": "תוציא את הדין מתוך זה ש", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תוציא את הדין מתוך זה ש_6"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_4", "כך_7"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_8", "בנו_9"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_8", "בנו_9"],
			},
			{
				"type": "חריף וחכם", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חריף וחכם_10"],
			},
		],
		"ex_name": "הנשרפים בנסקלים_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}