var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_27a_bottom.png",
		"בתחילת המשנה מופיע": "בתחילת המשנה מופיע.text",
		"משנה נוספת": "משנה נוספת.text",
		"שגם בה מעורבבים כד וחבית": "שגם בה מעורבבים כד וחבית.text",
		"כד וחבית שמות נרדפים": "כד וחבית שמות נרדפים.text",
		"ואילו בסופה מופיע": "ואילו בסופה מופיע.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "בתחילת המשנה מופיע_1", "loc": { "x": -272, "y": -39 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתחילת המשנה מופיע_2", "loc": { "x": -34, "y": 70 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתחילת המשנה מופיע_3", "loc": { "x": 435, "y": 216 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משנה נוספת_4", "loc": { "x": -51, "y": -3 }, "rect": { "width": 47, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משנה נוספת_5", "loc": { "x": -342, "y": 70 }, "rect": { "width": 47, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שגם בה מעורבבים כד וחבית_6", "loc": { "x": -109, "y": -3 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שגם בה מעורבבים כד וחבית_7", "loc": { "x": 134, "y": 107 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כד וחבית שמות נרדפים_8", "loc": { "x": -107, "y": 216 }, "rect": { "width": 239, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואילו בסופה מופיע_9", "loc": { "x": 122, "y": -3 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואילו בסופה מופיע_10", "loc": { "x": -197, "y": 70 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואילו בסופה מופיע_11", "loc": { "x": 307, "y": 216 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "בתחילת המשנה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתחילת המשנה מופיע_1", "בתחילת המשנה מופיע_2", "בתחילת המשנה מופיע_3"],
			},
			{
				"type": "בתחילת המשנה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתחילת המשנה מופיע_1", "בתחילת המשנה מופיע_2", "בתחילת המשנה מופיע_3"],
			},
			{
				"type": "בתחילת המשנה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתחילת המשנה מופיע_1", "בתחילת המשנה מופיע_2", "בתחילת המשנה מופיע_3"],
			},
			{
				"type": "משנה נוספת", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משנה נוספת_4", "משנה נוספת_5"],
			},
			{
				"type": "משנה נוספת", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משנה נוספת_4", "משנה נוספת_5"],
			},
			{
				"type": "שגם בה מעורבבים כד וחבית", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שגם בה מעורבבים כד וחבית_6", "שגם בה מעורבבים כד וחבית_7"],
			},
			{
				"type": "שגם בה מעורבבים כד וחבית", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שגם בה מעורבבים כד וחבית_6", "שגם בה מעורבבים כד וחבית_7"],
			},
			{
				"type": "כד וחבית שמות נרדפים", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כד וחבית שמות נרדפים_8"],
			},
			{
				"type": "ואילו בסופה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואילו בסופה מופיע_9", "ואילו בסופה מופיע_10", "ואילו בסופה מופיע_11"],
			},
			{
				"type": "ואילו בסופה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואילו בסופה מופיע_9", "ואילו בסופה מופיע_10", "ואילו בסופה מופיע_11"],
			},
			{
				"type": "ואילו בסופה מופיע", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואילו בסופה מופיע_9", "ואילו בסופה מופיע_10", "ואילו בסופה מופיע_11"],
			},
		],
		"ex_name": "babakama_27a_patach_bekad_vesiem_behavit_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}