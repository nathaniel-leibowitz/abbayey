var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_90a_bottom.png",
		"סכום מועט, ארבעה זוז": "סכום מועט, ארבעה זוז.text",
		"מאה זוז": "מאה זוז.text",
		"משך לו באוזן": "משך לו באוזן.text",
		"הפשיט אותו, הוריד לו בגד": "הפשיט אותו, הוריד לו בגד.text",
		"הסיר את כיסוי הראש": "הסיר את כיסוי הראש.text",
		"במקום ציבורי": "במקום ציבורי.text",
		"נתן לו אגרוף": "נתן לו אגרוף.text",
		"הרביץ לו בעזרת כף היד": "הרביץ לו בעזרת כף היד.text",
		"הרביץ לו בעזרת גב היד": "הרביץ לו בעזרת גב היד.text",
		"ירק עליו": "ירק עליו.text",
		"ופגע": "ופגע.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "סכום מועט, ארבעה זוז_1", "loc": { "x": -98, "y": 46 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאה זוז_2", "loc": { "x": -148, "y": 82 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משך לו באוזן_3", "loc": { "x": 41, "y": 154 }, "rect": { "width": 137, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפשיט אותו, הוריד לו בגד_4", "loc": { "x": -52, "y": 190 }, "rect": { "width": 173, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הסיר את כיסוי הראש_5", "loc": { "x": -263, "y": 190 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במקום ציבורי_6", "loc": { "x": 19, "y": 226 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נתן לו אגרוף_7", "loc": { "x": -325, "y": 10 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרביץ לו בעזרת כף היד_8", "loc": { "x": -232, "y": 82 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרביץ לו בעזרת גב היד_9", "loc": { "x": -44, "y": 118 }, "rect": { "width": 116, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ירק עליו_10", "loc": { "x": -258, "y": 154 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ופגע_11", "loc": { "x": -334, "y": 154 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "סכום מועט, ארבעה זוז", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סכום מועט, ארבעה זוז_1"],
			},
			{
				"type": "מאה זוז", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאה זוז_2"],
			},
			{
				"type": "משך לו באוזן", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משך לו באוזן_3"],
			},
			{
				"type": "הפשיט אותו, הוריד לו בגד", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפשיט אותו, הוריד לו בגד_4"],
			},
			{
				"type": "הסיר את כיסוי הראש", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הסיר את כיסוי הראש_5"],
			},
			{
				"type": "במקום ציבורי", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במקום ציבורי_6"],
			},
			{
				"type": "נתן לו אגרוף", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נתן לו אגרוף_7"],
			},
			{
				"type": "הרביץ לו בעזרת כף היד", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרביץ לו בעזרת כף היד_8"],
			},
			{
				"type": "הרביץ לו בעזרת גב היד", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרביץ לו בעזרת גב היד_9"],
			},
			{
				"type": "ירק עליו", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ירק עליו_10"],
			},
			{
				"type": "ופגע", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ופגע_11"],
			},
		],
		"ex_name": "babakama_89a_hatokeah_lehavero_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}