var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_27a_bottom.png",
		"קא משמע לן, זה העניין שרצה לחדש": "קא משמע לן, זה העניין שרצה לחדש.text",
		"איך דומה, באיזה מקרה מדובר": "איך דומה, באיזה מקרה מדובר.text",
		"אם נאמר": "אם נאמר.text",
		"במקום": "במקום.text",
		"הרי": "הרי.text",
		"מדובר על מקום שהרוב": "מדובר על מקום שהרוב.text",
		"ויש גם מעטים": "ויש גם מעטים.text",
		"שמא נטעה ונאמר": "שמא נטעה ונאמר.text",
		"שהולכים אחר הרוב, הרוב קובע": "שהולכים אחר הרוב, הרוב קובע.text",
		"איזה משמעות יוצאת מזה": "איזה משמעות יוצאת מזה.text",
		"לענייני קנייה ומכירה": "לענייני קנייה ומכירה.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "קא משמע לן, זה העניין שרצה לחדש_1", "loc": { "x": 71, "y": 358 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איך דומה, באיזה מקרה מדובר_2", "loc": { "x": 250, "y": 251 }, "rect": { "width": 107, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נאמר_3", "loc": { "x": 144, "y": 251 }, "rect": { "width": 86, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במקום_4", "loc": { "x": 48, "y": 251 }, "rect": { "width": 89, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי_5", "loc": { "x": 410, "y": 287 }, "rect": { "width": 38, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדובר על מקום שהרוב_6", "loc": { "x": 93, "y": 287 }, "rect": { "width": 77, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ויש גם מעטים_7", "loc": { "x": 472, "y": 323 }, "rect": { "width": 125, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא נטעה ונאמר_8", "loc": { "x": -109, "y": 323 }, "rect": { "width": 146, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהולכים אחר הרוב, הרוב קובע_9", "loc": { "x": -281, "y": 323 }, "rect": { "width": 173, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איזה משמעות יוצאת מזה_10", "loc": { "x": -300, "y": 215 }, "rect": { "width": 131, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לענייני קנייה ומכירה_11", "loc": { "x": 390, "y": 251 }, "rect": { "width": 161, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "קא משמע לן, זה העניין שרצה לחדש", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן, זה העניין שרצה לחדש_1"],
			},
			{
				"type": "איך דומה, באיזה מקרה מדובר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איך דומה, באיזה מקרה מדובר_2"],
			},
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_3"],
			},
			{
				"type": "במקום", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במקום_4"],
			},
			{
				"type": "הרי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי_5"],
			},
			{
				"type": "מדובר על מקום שהרוב", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדובר על מקום שהרוב_6"],
			},
			{
				"type": "ויש גם מעטים", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ויש גם מעטים_7"],
			},
			{
				"type": "שמא נטעה ונאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא נטעה ונאמר_8"],
			},
			{
				"type": "שהולכים אחר הרוב, הרוב קובע", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהולכים אחר הרוב, הרוב קובע_9"],
			},
			{
				"type": "איזה משמעות יוצאת מזה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איזה משמעות יוצאת מזה_10"],
			},
			{
				"type": "לענייני קנייה ומכירה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לענייני קנייה ומכירה_11"],
			},
		],
		"ex_name": "babakama_27a_lemuy_nafka_mina_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}