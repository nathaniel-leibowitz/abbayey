var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/tutorial.png",
		"interrobang": "storage/miniGames/gemara/interrobang.png",
		"qushia": "storage/miniGames/gemara/qushia.png",
		"resolution": "storage/miniGames/gemara/resolution.png",
		"speakbegin": "storage/miniGames/gemara/speakbegin.png",
		"question": "storage/miniGames/gemara/question.png",
		"speakend": "storage/miniGames/gemara/speakend.png",
		"period": "storage/miniGames/gemara/period.png",
		"hyphen": "storage/miniGames/gemara/hyphen.png",
		"quotebegin": "storage/miniGames/gemara/quotebegin.png",
		"biblequote": "storage/miniGames/gemara/biblequote.gif",
		"quoteend": "storage/miniGames/gemara/quoteend.png",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "interrobang_1", "loc": { "x": 81, "y": -248 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_2", "loc": { "x": -236, "y": -248 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_3", "loc": { "x": 161, "y": -194 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_4", "loc": { "x": 520, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "question_5", "loc": { "x": 373, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_6", "loc": { "x": 345, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_7", "loc": { "x": 248, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 31, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_8", "loc": { "x": 219, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_9", "loc": { "x": -435, "y": 22 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_10", "loc": { "x": 434, "y": 76 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 31, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "interrobang_11", "loc": { "x": 350, "y": 76 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_12", "loc": { "x": 126, "y": 76 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_13", "loc": { "x": -352, "y": 76 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "hyphen_14", "loc": { "x": 406, "y": 130 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_15", "loc": { "x": 315, "y": 184 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": 291, "y": 184 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_17", "loc": { "x": 186, "y": 184 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": 16, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_18", "loc": { "x": -122, "y": 184 }, "rect": { "width": 27, "height": 46 }, "isReused": false, "offsetX": 0, "offsetY": -8, "url": "storage/miniGames/gemara/arrow6.png" },
		],
		"items": [
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_1", "interrobang_11"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_2", "qushia_12"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_13"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_8"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_5"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_6", "speakend_9"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_7", "period_10"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_8"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_6", "speakend_9"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_7", "period_10"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_1", "interrobang_11"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_2", "qushia_12"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_13"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_14"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_15"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_16", "biblequote_17"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_18"],
			},
		],
		"ex_name": "הדגמה_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}