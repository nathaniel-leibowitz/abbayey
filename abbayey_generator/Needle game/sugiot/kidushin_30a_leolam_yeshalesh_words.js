var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_30a_bottom.png",
		"אל תיקרא את המילה": "אל תיקרא את המילה.text",
		"מהו שכתוב": "מהו שכתוב.text",
		"האם": "האם.text",
		"כמה שנים יחיה": "כמה שנים יחיה.text",
		"יחלק לשלושה חלקים": "יחלק לשלושה חלקים.text",
	},
	"parameters": {
		"numOfItems": 5,
		"placeholders": [
			{ "id": "אל תיקרא את המילה_1", "loc": { "x": -409, "y": -145 }, "rect": { "width": 125, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מהו שכתוב_2", "loc": { "x": -30, "y": -145 }, "rect": { "width": 158, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "האם_3", "loc": { "x": 183, "y": -66 }, "rect": { "width": 32, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמה שנים יחיה_4", "loc": { "x": 22, "y": -66 }, "rect": { "width": 119, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יחלק לשלושה חלקים_5", "loc": { "x": 34, "y": -105 }, "rect": { "width": 77, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אל תיקרא את המילה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אל תיקרא את המילה_1"],
			},
			{
				"type": "מהו שכתוב", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהו שכתוב_2"],
			},
			{
				"type": "האם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "האם_3"],
			},
			{
				"type": "כמה שנים יחיה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמה שנים יחיה_4"],
			},
			{
				"type": "יחלק לשלושה חלקים", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יחלק לשלושה חלקים_5"],
			},
		],
		"ex_name": "kidushin_30a_leolam_yeshalesh_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}