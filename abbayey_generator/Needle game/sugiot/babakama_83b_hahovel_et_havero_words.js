var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_83b_top.png",
		"מעריכים, מודדים": "מעריכים, מודדים.text",
		"שרף אותו": "שרף אותו.text",
		"היה שווה לפני הנזק": "היה שווה לפני הנזק.text",
		"מוגלה, פריחה, זיהום": "מוגלה, פריחה, זיהום.text",
		"הבריא, הגליד": "הבריא, הגליד.text",
		"לגמרי, לחלוטין": "לגמרי, לחלוטין.text",
		"הזדהם מחדש": "הזדהם מחדש.text",
		"שווה עכשיו אחרי הנזק": "שווה עכשיו אחרי הנזק.text",
		"בגלל, כתוצאה מ": "בגלל, כתוצאה מ.text",
		"עיוור אותו": "עיוור אותו.text",
		"הפוגע, המכה": "הפוגע, המכה.text",
		"אדם אחר": "אדם אחר.text",
		"סוגי פיצויים": "סוגי פיצויים.text",
		"פצע": "פצע.text",
		"שכר מינימום": "שכר מינימום.text",
		"דמי אבטלה": "דמי אבטלה.text",
		"פיצוי על ההתפאדחות. בושה": "פיצוי על ההתפאדחות. בושה.text",
	},
	"parameters": {
		"numOfItems": 20,
		"placeholders": [
			{ "id": "מעריכים, מודדים_1", "loc": { "x": -82, "y": -114 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שרף אותו_2", "loc": { "x": -52, "y": -77 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה שווה לפני הנזק_3", "loc": { "x": -238, "y": -114 }, "rect": { "width": 101, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מעריכים, מודדים_4", "loc": { "x": 107, "y": -4 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מוגלה, פריחה, זיהום_5", "loc": { "x": 2, "y": 69 }, "rect": { "width": 212, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הבריא, הגליד_6", "loc": { "x": -324, "y": 106 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הבריא, הגליד_7", "loc": { "x": 22, "y": 142 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לגמרי, לחלוטין_8", "loc": { "x": 99, "y": 179 }, "rect": { "width": 122, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הזדהם מחדש_9", "loc": { "x": 112, "y": 142 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הזדהם מחדש_10", "loc": { "x": -67, "y": 142 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שווה עכשיו אחרי הנזק_11", "loc": { "x": 109, "y": -77 }, "rect": { "width": 101, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בגלל, כתוצאה מ_12", "loc": { "x": -227, "y": 69 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עיוור אותו_13", "loc": { "x": -160, "y": -187 }, "rect": { "width": 176, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפוגע, המכה_14", "loc": { "x": 105, "y": -260 }, "rect": { "width": 110, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אדם אחר_15", "loc": { "x": -9, "y": -260 }, "rect": { "width": 86, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סוגי פיצויים_16", "loc": { "x": -2, "y": -224 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פצע_17", "loc": { "x": -323, "y": -41 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכר מינימום_18", "loc": { "x": -141, "y": 216 }, "rect": { "width": 173, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דמי אבטלה_19", "loc": { "x": -262, "y": 179 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פיצוי על ההתפאדחות. בושה_20", "loc": { "x": -204, "y": 252 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מעריכים, מודדים", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מעריכים, מודדים_1", "מעריכים, מודדים_4"],
			},
			{
				"type": "שרף אותו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שרף אותו_2"],
			},
			{
				"type": "היה שווה לפני הנזק", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה שווה לפני הנזק_3"],
			},
			{
				"type": "מעריכים, מודדים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מעריכים, מודדים_1", "מעריכים, מודדים_4"],
			},
			{
				"type": "מוגלה, פריחה, זיהום", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מוגלה, פריחה, זיהום_5"],
			},
			{
				"type": "הבריא, הגליד", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הבריא, הגליד_6", "הבריא, הגליד_7"],
			},
			{
				"type": "הבריא, הגליד", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הבריא, הגליד_6", "הבריא, הגליד_7"],
			},
			{
				"type": "לגמרי, לחלוטין", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לגמרי, לחלוטין_8"],
			},
			{
				"type": "הזדהם מחדש", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הזדהם מחדש_9", "הזדהם מחדש_10"],
			},
			{
				"type": "הזדהם מחדש", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הזדהם מחדש_9", "הזדהם מחדש_10"],
			},
			{
				"type": "שווה עכשיו אחרי הנזק", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שווה עכשיו אחרי הנזק_11"],
			},
			{
				"type": "בגלל, כתוצאה מ", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בגלל, כתוצאה מ_12"],
			},
			{
				"type": "עיוור אותו", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עיוור אותו_13"],
			},
			{
				"type": "הפוגע, המכה", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפוגע, המכה_14"],
			},
			{
				"type": "אדם אחר", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אדם אחר_15"],
			},
			{
				"type": "סוגי פיצויים", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סוגי פיצויים_16"],
			},
			{
				"type": "פצע", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פצע_17"],
			},
			{
				"type": "שכר מינימום", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכר מינימום_18"],
			},
			{
				"type": "דמי אבטלה", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דמי אבטלה_19"],
			},
			{
				"type": "פיצוי על ההתפאדחות. בושה", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פיצוי על ההתפאדחות. בושה_20"],
			},
		],
		"ex_name": "babakama_83b_hahovel_et_havero_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}