var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_79b_bottom.png",
		"ברייתא אחרת, נוספת": "ברייתא אחרת, נוספת.text",
		"עיזים, מיני צאן": "עיזים, מיני צאן.text",
		"שבגבול": "שבגבול.text",
		"פרות": "פרות.text",
		"מטילים איסור, הגבלות": "מטילים איסור, הגבלות.text",
		"לקיים אותה, להקפיד עליה": "לקיים אותה, להקפיד עליה.text",
		"קל, פשוט": "קל, פשוט.text",
		"קשה, מסובך": "קשה, מסובך.text",
		"מחזיק ומגדל באופן זמני": "מחזיק ומגדל באופן זמני.text",
		"לפני": "לפני.text",
		"שלושת הרגלים, חגים": "שלושת הרגלים, חגים.text",
		"מסיבת חתונה או ברית": "מסיבת חתונה או ברית.text",
		"בתנאי שיקפיד": "בתנאי שיקפיד.text",
		"הבהמות שנשארו אחרי הרגל או המשתה": "הבהמות שנשארו אחרי הרגל או המשתה.text",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "ברייתא אחרת, נוספת_1", "loc": { "x": -157, "y": 236 }, "rect": { "width": 113, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עיזים, מיני צאן_2", "loc": { "x": -405, "y": 236 }, "rect": { "width": 119, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שבגבול_3", "loc": { "x": -17, "y": 266 }, "rect": { "width": 74, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עיזים, מיני צאן_4", "loc": { "x": 505, "y": 296 }, "rect": { "width": 44, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פרות_5", "loc": { "x": 264, "y": 296 }, "rect": { "width": 122, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פרות_6", "loc": { "x": -115, "y": 326 }, "rect": { "width": 122, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מטילים איסור, הגבלות_7", "loc": { "x": -8, "y": 296 }, "rect": { "width": 116, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לקיים אותה, להקפיד עליה_8", "loc": { "x": 471, "y": 326 }, "rect": { "width": 113, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קל, פשוט_9", "loc": { "x": 230, "y": 326 }, "rect": { "width": 59, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קשה, מסובך_10", "loc": { "x": -242, "y": 326 }, "rect": { "width": 101, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מחזיק ומגדל באופן זמני_11", "loc": { "x": -20, "y": 355 }, "rect": { "width": 65, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מחזיק ומגדל באופן זמני_12", "loc": { "x": 311, "y": 385 }, "rect": { "width": 56, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפני_13", "loc": { "x": -138, "y": 355 }, "rect": { "width": 50, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפני_14", "loc": { "x": -385, "y": 355 }, "rect": { "width": 59, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שלושת הרגלים, חגים_15", "loc": { "x": -198, "y": 355 }, "rect": { "width": 47, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מסיבת חתונה או ברית_16", "loc": { "x": -478, "y": 355 }, "rect": { "width": 107, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתנאי שיקפיד_17", "loc": { "x": 431, "y": 385 }, "rect": { "width": 62, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הבהמות שנשארו אחרי הרגל או המשתה_18", "loc": { "x": 190, "y": 385 }, "rect": { "width": 92, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ברייתא אחרת, נוספת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברייתא אחרת, נוספת_1"],
			},
			{
				"type": "עיזים, מיני צאן", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עיזים, מיני צאן_2", "עיזים, מיני צאן_4"],
			},
			{
				"type": "שבגבול", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שבגבול_3"],
			},
			{
				"type": "עיזים, מיני צאן", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עיזים, מיני צאן_2", "עיזים, מיני צאן_4"],
			},
			{
				"type": "פרות", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פרות_5", "פרות_6"],
			},
			{
				"type": "פרות", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פרות_5", "פרות_6"],
			},
			{
				"type": "מטילים איסור, הגבלות", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מטילים איסור, הגבלות_7"],
			},
			{
				"type": "לקיים אותה, להקפיד עליה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לקיים אותה, להקפיד עליה_8"],
			},
			{
				"type": "קל, פשוט", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קל, פשוט_9"],
			},
			{
				"type": "קשה, מסובך", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קשה, מסובך_10"],
			},
			{
				"type": "מחזיק ומגדל באופן זמני", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מחזיק ומגדל באופן זמני_11", "מחזיק ומגדל באופן זמני_12"],
			},
			{
				"type": "מחזיק ומגדל באופן זמני", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מחזיק ומגדל באופן זמני_11", "מחזיק ומגדל באופן זמני_12"],
			},
			{
				"type": "לפני", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפני_13", "לפני_14"],
			},
			{
				"type": "לפני", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפני_13", "לפני_14"],
			},
			{
				"type": "שלושת הרגלים, חגים", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שלושת הרגלים, חגים_15"],
			},
			{
				"type": "מסיבת חתונה או ברית", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מסיבת חתונה או ברית_16"],
			},
			{
				"type": "בתנאי שיקפיד", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתנאי שיקפיד_17"],
			},
			{
				"type": "הבהמות שנשארו אחרי הרגל או המשתה", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הבהמות שנשארו אחרי הרגל או המשתה_18"],
			},
		],
		"ex_name": "babakama_79b_ein_megadlin_bryta_2_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}