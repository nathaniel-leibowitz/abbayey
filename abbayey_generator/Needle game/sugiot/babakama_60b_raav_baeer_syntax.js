var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60b_top.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"wrongly": "storage/miniGames/gemara/syntax/wrongly.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": -42, "y": 144 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_2", "loc": { "x": -161, "y": 144 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_3", "loc": { "x": 163, "y": 180 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_4", "loc": { "x": 57, "y": 216 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_5", "loc": { "x": -34, "y": 216 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": -24, "y": 252 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_7", "loc": { "x": -36, "y": 252 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_8", "loc": { "x": -162, "y": 252 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_9", "loc": { "x": 232, "y": 288 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_10", "loc": { "x": -85, "y": 324 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_11", "loc": { "x": 359, "y": 360 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": -164, "y": 360 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_13", "loc": { "x": -187, "y": 360 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_2"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_7"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_8"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -700, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_9", "wrongly_10"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -730, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_9", "wrongly_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_5", "biblequote_6", "biblequote_11", "biblequote_12"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_13"],
			},
		],
		"ex_name": "babakama_60b_raav_baeer_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}