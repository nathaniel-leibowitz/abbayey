var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60b_top.png",
		"תנו רבנן, פתיחה לברייתא": "תנו רבנן, פתיחה לברייתא.text",
		"מגיפה": "מגיפה.text",
		"בידוד, סגר, להישאר בבית": "בידוד, סגר, להישאר בבית.text",
		"מקור נוסף שמדגים זאת": "מקור נוסף שמדגים זאת.text",
		"תגרום למוות ושכול, תהרוג": "תגרום למוות ושכול, תהרוג.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "תנו רבנן, פתיחה לברייתא_1", "loc": { "x": 232, "y": -279 }, "rect": { "width": 47, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מגיפה_2", "loc": { "x": 172, "y": -279 }, "rect": { "width": 47, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בידוד, סגר, להישאר בבית_3", "loc": { "x": 4, "y": -279 }, "rect": { "width": 128, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מקור נוסף שמדגים זאת_4", "loc": { "x": 327, "y": -207 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מקור נוסף שמדגים זאת_5", "loc": { "x": 246, "y": -170 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תגרום למוות ושכול, תהרוג_6", "loc": { "x": 67, "y": -170 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "תנו רבנן, פתיחה לברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תנו רבנן, פתיחה לברייתא_1"],
			},
			{
				"type": "מגיפה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מגיפה_2"],
			},
			{
				"type": "בידוד, סגר, להישאר בבית", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בידוד, סגר, להישאר בבית_3"],
			},
			{
				"type": "מקור נוסף שמדגים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקור נוסף שמדגים זאת_4", "מקור נוסף שמדגים זאת_5"],
			},
			{
				"type": "מקור נוסף שמדגים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקור נוסף שמדגים זאת_4", "מקור נוסף שמדגים זאת_5"],
			},
			{
				"type": "תגרום למוות ושכול, תהרוג", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תגרום למוות ושכול, תהרוג_6"],
			},
		],
		"ex_name": "babakama_60b_dever_baeer_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}