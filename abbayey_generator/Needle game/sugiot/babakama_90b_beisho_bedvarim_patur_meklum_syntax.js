var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_91a_bottom.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "colon_1", "loc": { "x": 16, "y": 101 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_2", "loc": { "x": -325, "y": 101 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_3", "loc": { "x": -362, "y": 101 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 19, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_4", "loc": { "x": 138, "y": 131 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_5", "loc": { "x": -330, "y": 131 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_6", "loc": { "x": 133, "y": 161 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_7", "loc": { "x": -9, "y": 161 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_8", "loc": { "x": -228, "y": 71 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_9", "loc": { "x": -526, "y": 71 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_1", "colon_5"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_2", "hyphen_6"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_4"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -730, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_1", "colon_5"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_2", "hyphen_6"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_7"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_8"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_9"],
			},
		],
		"ex_name": "babakama_90b_beisho_bedvarim_patur_meklum_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}