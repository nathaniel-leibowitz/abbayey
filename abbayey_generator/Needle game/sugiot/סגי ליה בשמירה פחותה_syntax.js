var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/babakama_55b_middle.png",
		"question": "storage/miniGames/gemara/question.png",
		"quotebegin": "storage/miniGames/gemara/quotebegin.png",
		"period": "storage/miniGames/gemara/period.png",
		"biblequote": "storage/miniGames/gemara/biblequote.gif",
		"quoteend": "storage/miniGames/gemara/quoteend.png",
		"qushia": "storage/miniGames/gemara/qushia.png",
		"colon": "storage/miniGames/gemara/colon.png",
		"hyphen": "storage/miniGames/gemara/hyphen.png",
		"resolution": "storage/miniGames/gemara/resolution.png",
	},
	"parameters": {
		"numOfItems": 29,
		"placeholders": [
			{ "id": "question_1", "loc": { "x": -182, "y": -390 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_2", "loc": { "x": 144, "y": -353 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_3", "loc": { "x": 106, "y": -281 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_4", "loc": { "x": 46, "y": -245 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_5", "loc": { "x": -367, "y": -208 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "qushia_6", "loc": { "x": -394, "y": -208 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_7", "loc": { "x": -18, "y": -100 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_8", "loc": { "x": -363, "y": 154 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "colon_9", "loc": { "x": -185, "y": -64 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_10", "loc": { "x": 103, "y": -28 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_11", "loc": { "x": -57, "y": -28 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": -167, "y": 9 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "hyphen_13", "loc": { "x": -299, "y": 9 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_14", "loc": { "x": -365, "y": 9 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_15", "loc": { "x": -12, "y": 45 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": 62, "y": 81 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_17", "loc": { "x": -227, "y": 81 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_18", "loc": { "x": 152, "y": 117 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_19", "loc": { "x": -84, "y": 117 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_20", "loc": { "x": -367, "y": 117 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_21", "loc": { "x": 5, "y": 154 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_22", "loc": { "x": -77, "y": 154 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_23", "loc": { "x": 52, "y": 190 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_24", "loc": { "x": 91, "y": 298 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_25", "loc": { "x": -318, "y": 298 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "hyphen_26", "loc": { "x": 40, "y": 334 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "resolution_27", "loc": { "x": -13, "y": 334 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "biblequote_28", "loc": { "x": -208, "y": -245 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "period_29", "loc": { "x": 124, "y": -208 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/arrow6.png" },
		],
		"items": [
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 270 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_7", "quotebegin_23"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_5", "quoteend_8", "quoteend_24"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": -30 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 270 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_7", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_5", "quoteend_8", "quoteend_24"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 220 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_9"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": 420 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_13", "hyphen_26"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -760, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -640, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -580, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -670, "y": 270 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_7", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -670, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_5", "quoteend_8", "quoteend_24"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -790, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": 420 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_13", "hyphen_26"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 70 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_27"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -850, "y": 20 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_4", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_18", "biblequote_19", "biblequote_21", "biblequote_22", "biblequote_28"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -610, "y": 370 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_3", "period_10", "period_14", "period_17", "period_20", "period_25", "period_29"],
			},
		],
		"ex_name": "סגי ליה בשמירה פחותה_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}