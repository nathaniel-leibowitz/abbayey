var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/taanit_2ab.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"question": "storage/miniGames/gemara/syntax/question.png",
	},
	"parameters": {
		"numOfItems": 10,
		"placeholders": [
			{ "id": "colon_1", "loc": { "x": 429, "y": -211 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_2", "loc": { "x": -375, "y": -211 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_3", "loc": { "x": 203, "y": -175 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_4", "loc": { "x": 321, "y": -139 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_5", "loc": { "x": 81, "y": -139 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": -18, "y": -139 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": -227, "y": 187 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_8", "loc": { "x": -74, "y": 223 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_9", "loc": { "x": -179, "y": 223 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": -144, "y": 260 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_1"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_5", "question_8"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_5", "question_8"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
		],
		"ex_name": "taanit_2a_gimel_maftehot_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}