var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/rava_bey_hiloola.png",
		"מפניו": "מפניו.text",
		"אלו": "אלו.text",
		"ועוד, וכן": "ועוד, וכן.text",
		"כוס": "כוס.text",
		"מזג": "מזג.text",
		"והרי": "והרי.text",
		"של בנו": "של בנו.text",
		"בנו": "בנו.text",
		"אפילו כך, בכל זאת": "אפילו כך, בכל זאת.text",
		"צריכים": "צריכים.text",
		"לעשות": "לעשות.text",
		"האמנם": "האמנם.text",
		"כן, אכן": "כן, אכן.text",
		"שלו": "שלו.text",
		"משתה, חתונה": "משתה, חתונה.text",
		"אחר כך": "אחר כך.text",
	},
	"parameters": {
		"numOfItems": 20,
		"placeholders": [
			{ "id": "מפניו_1", "loc": { "x": 55, "y": -1 }, "rect": { "width": 104, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אלו_2", "loc": { "x": 163, "y": 90 }, "rect": { "width": 47, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ועוד, וכן_3", "loc": { "x": 327, "y": 135 }, "rect": { "width": 50, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כוס_4", "loc": { "x": 43, "y": 180 }, "rect": { "width": 68, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מזג_5", "loc": { "x": 200, "y": 180 }, "rect": { "width": 62, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי_6", "loc": { "x": 210, "y": -91 }, "rect": { "width": 56, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "של בנו_7", "loc": { "x": 449, "y": -46 }, "rect": { "width": 95, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_8", "loc": { "x": 460, "y": 225 }, "rect": { "width": 74, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מפניו_9", "loc": { "x": 24, "y": 225 }, "rect": { "width": 104, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אפילו כך, בכל זאת_10", "loc": { "x": 456, "y": 270 }, "rect": { "width": 86, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריכים_11", "loc": { "x": 75, "y": 270 }, "rect": { "width": 56, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעשות_12", "loc": { "x": 225, "y": 270 }, "rect": { "width": 101, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "האמנם_13", "loc": { "x": 284, "y": -91 }, "rect": { "width": 59, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מזג_14", "loc": { "x": 344, "y": -46 }, "rect": { "width": 53, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מפניו_15", "loc": { "x": 447, "y": 90 }, "rect": { "width": 104, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כן, אכן_16", "loc": { "x": -131, "y": -181 }, "rect": { "width": 44, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שלו_17", "loc": { "x": 350, "y": -136 }, "rect": { "width": 83, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משתה, חתונה_18", "loc": { "x": -105, "y": -91 }, "rect": { "width": 95, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משתה, חתונה_19", "loc": { "x": -105, "y": 135 }, "rect": { "width": 98, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אחר כך_20", "loc": { "x": 117, "y": -181 }, "rect": { "width": 65, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מפניו_1", "מפניו_9", "מפניו_15"],
			},
			{
				"type": "אלו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלו_2"],
			},
			{
				"type": "ועוד, וכן", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ועוד, וכן_3"],
			},
			{
				"type": "כוס", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כוס_4"],
			},
			{
				"type": "מזג", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מזג_5", "מזג_14"],
			},
			{
				"type": "והרי", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי_6"],
			},
			{
				"type": "של בנו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "של בנו_7"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_8"],
			},
			{
				"type": "מפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מפניו_1", "מפניו_9", "מפניו_15"],
			},
			{
				"type": "אפילו כך, בכל זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אפילו כך, בכל זאת_10"],
			},
			{
				"type": "צריכים", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריכים_11"],
			},
			{
				"type": "לעשות", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעשות_12"],
			},
			{
				"type": "האמנם", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "האמנם_13"],
			},
			{
				"type": "מזג", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מזג_5", "מזג_14"],
			},
			{
				"type": "מפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מפניו_1", "מפניו_9", "מפניו_15"],
			},
			{
				"type": "כן, אכן", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כן, אכן_16"],
			},
			{
				"type": "שלו", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שלו_17"],
			},
			{
				"type": "משתה, חתונה", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משתה, חתונה_18", "משתה, חתונה_19"],
			},
			{
				"type": "משתה, חתונה", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משתה, חתונה_18", "משתה, חתונה_19"],
			},
			{
				"type": "אחר כך", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אחר כך_20"],
			},
		],
		"ex_name": "רבא משקי בי הילולא דבריה_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}