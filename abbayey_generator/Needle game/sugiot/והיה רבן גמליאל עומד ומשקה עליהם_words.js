var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/gamliel_bey_hiloola.png",
		"אם נאמר": "אם נאמר.text",
		"לפי מי שאמר, לשיטתו של": "לפי מי שאמר, לשיטתו של.text",
		"כך נאמר": "כך נאמר.text",
		"מביאים סתירה מברייתא": "מביאים סתירה מברייתא.text",
		"הקבה": "הקבה.text",
		"עננים, ואולי רמז לרבן גמליאל": "עננים, ואולי רמז לרבן גמליאל.text",
	},
	"parameters": {
		"numOfItems": 7,
		"placeholders": [
			{ "id": "אם נאמר_1", "loc": { "x": -51, "y": 300 }, "rect": { "width": 122, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפי מי שאמר, לשיטתו של_2", "loc": { "x": 132, "y": -408 }, "rect": { "width": 65, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפי מי שאמר, לשיטתו של_3", "loc": { "x": -59, "y": 336 }, "rect": { "width": 62, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך נאמר_4", "loc": { "x": -194, "y": 300 }, "rect": { "width": 140, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מביאים סתירה מברייתא_5", "loc": { "x": 124, "y": -337 }, "rect": { "width": 80, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הקבה_6", "loc": { "x": 215, "y": 159 }, "rect": { "width": 62, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עננים, ואולי רמז לרבן גמליאל_7", "loc": { "x": -59, "y": 194 }, "rect": { "width": 86, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_1"],
			},
			{
				"type": "לפי מי שאמר, לשיטתו של", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפי מי שאמר, לשיטתו של_2", "לפי מי שאמר, לשיטתו של_3"],
			},
			{
				"type": "לפי מי שאמר, לשיטתו של", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפי מי שאמר, לשיטתו של_2", "לפי מי שאמר, לשיטתו של_3"],
			},
			{
				"type": "כך נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך נאמר_4"],
			},
			{
				"type": "מביאים סתירה מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מביאים סתירה מברייתא_5"],
			},
			{
				"type": "הקבה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הקבה_6"],
			},
			{
				"type": "עננים, ואולי רמז לרבן גמליאל", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עננים, ואולי רמז לרבן גמליאל_7"],
			},
		],
		"ex_name": "והיה רבן גמליאל עומד ומשקה עליהם_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}