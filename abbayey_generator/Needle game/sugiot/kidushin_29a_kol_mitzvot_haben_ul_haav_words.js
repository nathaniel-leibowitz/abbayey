var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_middle.png",
		"ברית מילה למשל": "ברית מילה למשל.text",
		"כיבוד הורים למשל": "כיבוד הורים למשל.text",
		"ישיבה בסוכה למשל": "ישיבה בסוכה למשל.text",
		"איסור גזל למשל": "איסור גזל למשל.text",
		"איסור תספורת פטריה": "איסור תספורת פטריה.text",
		"איסור גילוח בתער": "איסור גילוח בתער.text",
		"איסור על כהנים להיטמא בטומאת המת": "איסור על כהנים להיטמא בטומאת המת.text",
		"איסור הדלקת אש בשבת למשל": "איסור הדלקת אש בשבת למשל.text",
		"גם האיש וגם האשה": "גם האיש וגם האשה.text",
		"צדקה למשל": "צדקה למשל.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "ברית מילה למשל_1", "loc": { "x": -169, "y": -409 }, "rect": { "width": 209, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כיבוד הורים למשל_2", "loc": { "x": 162, "y": -337 }, "rect": { "width": 179, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ישיבה בסוכה למשל_3", "loc": { "x": -103, "y": -301 }, "rect": { "width": 338, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור גזל למשל_4", "loc": { "x": 16, "y": -157 }, "rect": { "width": 209, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור תספורת פטריה_5", "loc": { "x": -138, "y": -121 }, "rect": { "width": 128, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור גילוח בתער_6", "loc": { "x": 206, "y": -85 }, "rect": { "width": 89, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור על כהנים להיטמא בטומאת המת_7", "loc": { "x": 28, "y": -85 }, "rect": { "width": 224, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור הדלקת אש בשבת למשל_8", "loc": { "x": -88, "y": -193 }, "rect": { "width": 371, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם האיש וגם האשה_9", "loc": { "x": -109, "y": -337 }, "rect": { "width": 329, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם האיש וגם האשה_10", "loc": { "x": -111, "y": -229 }, "rect": { "width": 323, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צדקה למשל_11", "loc": { "x": 154, "y": -229 }, "rect": { "width": 194, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ברית מילה למשל", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברית מילה למשל_1"],
			},
			{
				"type": "כיבוד הורים למשל", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כיבוד הורים למשל_2"],
			},
			{
				"type": "ישיבה בסוכה למשל", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ישיבה בסוכה למשל_3"],
			},
			{
				"type": "איסור גזל למשל", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור גזל למשל_4"],
			},
			{
				"type": "איסור תספורת פטריה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור תספורת פטריה_5"],
			},
			{
				"type": "איסור גילוח בתער", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור גילוח בתער_6"],
			},
			{
				"type": "איסור על כהנים להיטמא בטומאת המת", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור על כהנים להיטמא בטומאת המת_7"],
			},
			{
				"type": "איסור הדלקת אש בשבת למשל", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור הדלקת אש בשבת למשל_8"],
			},
			{
				"type": "גם האיש וגם האשה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם האיש וגם האשה_9", "גם האיש וגם האשה_10"],
			},
			{
				"type": "גם האיש וגם האשה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם האיש וגם האשה_9", "גם האיש וגם האשה_10"],
			},
			{
				"type": "צדקה למשל", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צדקה למשל_11"],
			},
		],
		"ex_name": "kidushin_29a_kol_mitzvot_haben_ul_haav_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}