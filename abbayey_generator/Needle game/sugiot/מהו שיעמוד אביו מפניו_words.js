var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/kidushin_33b_middle.png",
		"אני": "אני.text",
		"רב שלו": "רב שלו.text",
		"הרי, אך אם": "הרי, אך אם.text",
		"הוא": "הוא.text",
		"רב שלי": "רב שלי.text",
		"הייתי קם": "הייתי קם.text",
		"מלפניו": "מלפניו.text",
		"כך אמר": "כך אמר.text",
		"שהרי": "שהרי.text",
		"אבא שלו": "אבא שלו.text",
		"הסיבה לכך": "הסיבה לכך.text",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "אני_1", "loc": { "x": -353, "y": -34 }, "rect": { "width": 80, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רב שלו_2", "loc": { "x": -437, "y": -34 }, "rect": { "width": 74, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי, אך אם_3", "loc": { "x": 80, "y": 5 }, "rect": { "width": 44, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא_4", "loc": { "x": 12, "y": 5 }, "rect": { "width": 68, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רב שלי_5", "loc": { "x": -68, "y": 5 }, "rect": { "width": 74, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הייתי קם_6", "loc": { "x": -166, "y": 5 }, "rect": { "width": 113, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_7", "loc": { "x": -280, "y": 5 }, "rect": { "width": 101, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך אמר_8", "loc": { "x": -384, "y": 5 }, "rect": { "width": 59, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רב שלי_9", "loc": { "x": -40, "y": 45 }, "rect": { "width": 77, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי_10", "loc": { "x": -118, "y": 45 }, "rect": { "width": 65, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אני_11", "loc": { "x": -191, "y": 45 }, "rect": { "width": 59, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אבא שלו_12", "loc": { "x": -268, "y": 45 }, "rect": { "width": 80, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הסיבה לכך_13", "loc": { "x": -259, "y": -34 }, "rect": { "width": 89, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אני", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אני_1", "אני_11"],
			},
			{
				"type": "רב שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רב שלו_2"],
			},
			{
				"type": "הרי, אך אם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי, אך אם_3"],
			},
			{
				"type": "הוא", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא_4"],
			},
			{
				"type": "רב שלי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רב שלי_5", "רב שלי_9"],
			},
			{
				"type": "הייתי קם", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הייתי קם_6"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_7"],
			},
			{
				"type": "כך אמר", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך אמר_8"],
			},
			{
				"type": "רב שלי", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רב שלי_5", "רב שלי_9"],
			},
			{
				"type": "שהרי", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי_10"],
			},
			{
				"type": "אני", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אני_1", "אני_11"],
			},
			{
				"type": "אבא שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אבא שלו_12"],
			},
			{
				"type": "הסיבה לכך", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הסיבה לכך_13"],
			},
		],
		"ex_name": "מהו שיעמוד אביו מפניו_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}