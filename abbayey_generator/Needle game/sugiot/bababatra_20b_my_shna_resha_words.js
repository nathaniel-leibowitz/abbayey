var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/bababatra_20b_bottom.png",
		"מאי שנא, מה ההבדל": "מאי שנא, מה ההבדל.text",
		"הדין בתחילת המשנה": "הדין בתחילת המשנה.text",
		"הדין בסוף המשנה": "הדין בסוף המשנה.text",
		"החנות והשכן אינם באותה חצר": "החנות והשכן אינם באותה חצר.text",
		"אמר ליה": "אמר ליה.text",
		"שהמשנה תאמר בפירוש": "שהמשנה תאמר בפירוש.text",
		"באה לדון במקרה של": "באה לדון במקרה של.text",
		"פתיחה של בית ספר בחצר המשותפת": "פתיחה של בית ספר בחצר המשותפת.text",
		"אם כך": "אם כך.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "מאי שנא, מה ההבדל_1", "loc": { "x": 62, "y": 297 }, "rect": { "width": 47, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאי שנא, מה ההבדל_2", "loc": { "x": -82, "y": 297 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הדין בתחילת המשנה_3", "loc": { "x": -8, "y": 297 }, "rect": { "width": 59, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הדין בסוף המשנה_4", "loc": { "x": -155, "y": 297 }, "rect": { "width": 53, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הדין בסוף המשנה_5", "loc": { "x": -354, "y": 297 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החנות והשכן אינם באותה חצר_6", "loc": { "x": 294, "y": 329 }, "rect": { "width": 137, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אמר ליה_7", "loc": { "x": 198, "y": 329 }, "rect": { "width": 44, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהמשנה תאמר בפירוש_8", "loc": { "x": 6, "y": 329 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החנות והשכן אינם באותה חצר_9", "loc": { "x": -91, "y": 329 }, "rect": { "width": 119, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הדין בסוף המשנה_10", "loc": { "x": -47, "y": 361 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באה לדון במקרה של_11", "loc": { "x": 399, "y": 329 }, "rect": { "width": 59, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באה לדון במקרה של_12", "loc": { "x": -112, "y": 361 }, "rect": { "width": 59, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פתיחה של בית ספר בחצר המשותפת_13", "loc": { "x": -267, "y": 361 }, "rect": { "width": 236, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם כך_14", "loc": { "x": 78, "y": 329 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאי שנא, מה ההבדל", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאי שנא, מה ההבדל_1", "מאי שנא, מה ההבדל_2"],
			},
			{
				"type": "מאי שנא, מה ההבדל", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאי שנא, מה ההבדל_1", "מאי שנא, מה ההבדל_2"],
			},
			{
				"type": "הדין בתחילת המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הדין בתחילת המשנה_3"],
			},
			{
				"type": "הדין בסוף המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הדין בסוף המשנה_4", "הדין בסוף המשנה_5", "הדין בסוף המשנה_10"],
			},
			{
				"type": "הדין בסוף המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הדין בסוף המשנה_4", "הדין בסוף המשנה_5", "הדין בסוף המשנה_10"],
			},
			{
				"type": "החנות והשכן אינם באותה חצר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החנות והשכן אינם באותה חצר_6", "החנות והשכן אינם באותה חצר_9"],
			},
			{
				"type": "אמר ליה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אמר ליה_7"],
			},
			{
				"type": "שהמשנה תאמר בפירוש", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהמשנה תאמר בפירוש_8"],
			},
			{
				"type": "החנות והשכן אינם באותה חצר", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החנות והשכן אינם באותה חצר_6", "החנות והשכן אינם באותה חצר_9"],
			},
			{
				"type": "הדין בסוף המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הדין בסוף המשנה_4", "הדין בסוף המשנה_5", "הדין בסוף המשנה_10"],
			},
			{
				"type": "באה לדון במקרה של", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באה לדון במקרה של_11", "באה לדון במקרה של_12"],
			},
			{
				"type": "באה לדון במקרה של", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באה לדון במקרה של_11", "באה לדון במקרה של_12"],
			},
			{
				"type": "פתיחה של בית ספר בחצר המשותפת", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פתיחה של בית ספר בחצר המשותפת_13"],
			},
			{
				"type": "אם כך", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כך_14"],
			},
		],
		"ex_name": "bababatra_20b_my_shna_resha_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}