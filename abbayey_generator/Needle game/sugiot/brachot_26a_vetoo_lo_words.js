var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"ויותר מאוחר": "ויותר מאוחר.text",
		"וכל שאר העולם": "וכל שאר העולם.text",
		"והרי אמר": "והרי אמר.text",
		"בנו": "בנו.text",
		"כל משך היום": "כל משך היום.text",
		"מתפלל": "מתפלל.text",
		"והולך": "והולך.text",
		"נותנים לו": "נותנים לו.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "ויותר מאוחר_1", "loc": { "x": -244, "y": 54 }, "rect": { "width": 38, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכל שאר העולם_2", "loc": { "x": -98, "y": 54 }, "rect": { "width": 50, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי אמר_3", "loc": { "x": -341, "y": 54 }, "rect": { "width": 74, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_4", "loc": { "x": -487, "y": 54 }, "rect": { "width": 56, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_5", "loc": { "x": 474, "y": 86 }, "rect": { "width": 53, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כל משך היום_6", "loc": { "x": 263, "y": 117 }, "rect": { "width": 107, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתפלל_7", "loc": { "x": 172, "y": 117 }, "rect": { "width": 53, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והולך_8", "loc": { "x": 107, "y": 117 }, "rect": { "width": 56, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נותנים לו_9", "loc": { "x": -92, "y": 117 }, "rect": { "width": 98, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נותנים לו_10", "loc": { "x": 437, "y": 149 }, "rect": { "width": 98, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נותנים לו_11", "loc": { "x": 65, "y": 149 }, "rect": { "width": 101, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ויותר מאוחר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ויותר מאוחר_1"],
			},
			{
				"type": "וכל שאר העולם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכל שאר העולם_2"],
			},
			{
				"type": "והרי אמר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי אמר_3"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_4", "בנו_5"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_4", "בנו_5"],
			},
			{
				"type": "כל משך היום", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כל משך היום_6"],
			},
			{
				"type": "מתפלל", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתפלל_7"],
			},
			{
				"type": "והולך", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והולך_8"],
			},
			{
				"type": "נותנים לו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נותנים לו_9", "נותנים לו_10", "נותנים לו_11"],
			},
			{
				"type": "נותנים לו", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נותנים לו_9", "נותנים לו_10", "נותנים לו_11"],
			},
			{
				"type": "נותנים לו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נותנים לו_9", "נותנים לו_10", "נותנים לו_11"],
			},
		],
		"ex_name": "brachot_26a_vetoo_lo_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}