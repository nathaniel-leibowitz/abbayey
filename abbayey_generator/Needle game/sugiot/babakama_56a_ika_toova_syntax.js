var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/babakama_56a_middle2.png",
		"quotebegin": "storage/miniGames/gemara/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/quoteend.png",
		"wrongly": "storage/miniGames/gemara/wrongly.png",
		"speakbegin": "storage/miniGames/gemara/speakbegin.png",
		"speakend": "storage/miniGames/gemara/speakend.png",
	},
	"parameters": {
		"numOfItems": 28,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": 283, "y": -331 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_2", "loc": { "x": -99, "y": -331 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_3", "loc": { "x": 254, "y": -295 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_4", "loc": { "x": 14, "y": -259 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_5", "loc": { "x": -80, "y": -259 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_6", "loc": { "x": 167, "y": -222 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_7", "loc": { "x": -55, "y": -222 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_8", "loc": { "x": 80, "y": -150 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_9", "loc": { "x": -130, "y": -222 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_10", "loc": { "x": -84, "y": -186 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_11", "loc": { "x": 224, "y": -114 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_12", "loc": { "x": 65, "y": -114 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_13", "loc": { "x": -95, "y": -114 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_14", "loc": { "x": -116, "y": -114 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_15", "loc": { "x": 110, "y": -78 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_16", "loc": { "x": 285, "y": -42 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_17", "loc": { "x": 190, "y": -42 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_18", "loc": { "x": -39, "y": -42 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_19", "loc": { "x": 262, "y": -5 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_20", "loc": { "x": 171, "y": -5 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_21", "loc": { "x": 174, "y": 31 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_22", "loc": { "x": -160, "y": 31 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quotebegin_23", "loc": { "x": 283, "y": 67 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "quoteend_24", "loc": { "x": -160, "y": 67 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_25", "loc": { "x": 174, "y": 103 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakbegin_26", "loc": { "x": 157, "y": 103 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "speakend_27", "loc": { "x": 271, "y": 176 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/arrow6.png" },
			{ "id": "wrongly_28", "loc": { "x": -73, "y": 176 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_11", "quotebegin_17", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_6", "quoteend_12", "quoteend_18", "quoteend_24"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -700, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -730, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_11", "quotebegin_17", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_6", "quoteend_12", "quoteend_18", "quoteend_24"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -670, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -760, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_9", "speakbegin_14", "speakbegin_20", "speakbegin_26"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_10", "speakend_15", "speakend_21", "speakend_27"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -670, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_11", "quotebegin_17", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -670, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_6", "quoteend_12", "quoteend_18", "quoteend_24"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -640, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_9", "speakbegin_14", "speakbegin_20", "speakbegin_26"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_10", "speakend_15", "speakend_21", "speakend_27"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -790, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -760, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_11", "quotebegin_17", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -760, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_6", "quoteend_12", "quoteend_18", "quoteend_24"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -610, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_9", "speakbegin_14", "speakbegin_20", "speakbegin_26"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_10", "speakend_15", "speakend_21", "speakend_27"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -820, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -640, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_11", "quotebegin_17", "quotebegin_23"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -640, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_6", "quoteend_12", "quoteend_18", "quoteend_24"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -580, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -760, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_9", "speakbegin_14", "speakbegin_20", "speakbegin_26"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -760, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_10", "speakend_15", "speakend_21", "speakend_27"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -850, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_7", "wrongly_8", "wrongly_13", "wrongly_16", "wrongly_19", "wrongly_22", "wrongly_25", "wrongly_28"],
			},
		],
		"ex_name": "babakama_56a_ika_toova_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}