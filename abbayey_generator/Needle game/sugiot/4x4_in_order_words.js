var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/4by4_inorder.png",
		"1": "1.text",
		"2": "2.text",
		"3": "3.text",
		"4": "4.text",
		"6": "6.text",
		"8": "8.text",
		"9": "9.text",
		"12": "12.text",
		"16": "16.text",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "1_1", "loc": { "x": -120, "y": -115 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "2_2", "loc": { "x": 0, "y": -115 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "3_3", "loc": { "x": 121, "y": -115 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "4_4", "loc": { "x": 243, "y": -115 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "2_5", "loc": { "x": -120, "y": 8 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "4_6", "loc": { "x": 0, "y": 8 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "6_7", "loc": { "x": 123, "y": 8 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "8_8", "loc": { "x": 244, "y": 8 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "3_9", "loc": { "x": -120, "y": 130 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "6_10", "loc": { "x": 0, "y": 130 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "9_11", "loc": { "x": 123, "y": 130 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "12_12", "loc": { "x": 244, "y": 130 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "4_13", "loc": { "x": -120, "y": 253 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "8_14", "loc": { "x": 0, "y": 253 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "12_15", "loc": { "x": 122, "y": 253 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "16_16", "loc": { "x": 243, "y": 253 }, "rect": { "width": 110, "height": 69 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "1", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "1_1"],
			},
			{
				"type": "2", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "2_2", "2_5"],
			},
			{
				"type": "3", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "3_3", "3_9"],
			},
			{
				"type": "4", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "4_4", "4_6", "4_13"],
			},
			{
				"type": "2", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "2_2", "2_5"],
			},
			{
				"type": "4", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "4_4", "4_6", "4_13"],
			},
			{
				"type": "6", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "6_7", "6_10"],
			},
			{
				"type": "8", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "8_8", "8_14"],
			},
			{
				"type": "3", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "3_3", "3_9"],
			},
			{
				"type": "6", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "6_7", "6_10"],
			},
			{
				"type": "9", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "9_11"],
			},
			{
				"type": "12", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "12_12", "12_15"],
			},
			{
				"type": "4", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "4_4", "4_6", "4_13"],
			},
			{
				"type": "8", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "8_8", "8_14"],
			},
			{
				"type": "12", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "12_12", "12_15"],
			},
			{
				"type": "16", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "16_16"],
			},
		],
		"ex_name": "4x4_in_order_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}