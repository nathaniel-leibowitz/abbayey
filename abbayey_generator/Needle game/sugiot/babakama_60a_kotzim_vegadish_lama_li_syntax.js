var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"wrongly": "storage/miniGames/gemara/syntax/wrongly.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 7,
		"placeholders": [
			{ "id": "qushia_1", "loc": { "x": 69, "y": -216 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_2", "loc": { "x": -4, "y": -216 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_3", "loc": { "x": 145, "y": -179 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_4", "loc": { "x": -326, "y": -106 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_5", "loc": { "x": 157, "y": -70 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_6", "loc": { "x": 93, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_7", "loc": { "x": 76, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -700, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_5", "wrongly_6"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -730, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_5", "wrongly_6"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -670, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_5", "wrongly_6"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -760, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_3", "wrongly_4", "wrongly_5", "wrongly_6"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_7"],
			},
		],
		"ex_name": "babakama_60a_kotzim_vegadish_lama_li_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}