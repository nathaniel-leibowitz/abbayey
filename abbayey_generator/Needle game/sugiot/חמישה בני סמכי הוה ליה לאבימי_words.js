var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_31b_top.png",
		"בני": "בני.text",
		"רציניים, מלומדים, נחשבים": "רציניים, מלומדים, נחשבים.text",
		"היה": "היה.text",
		"לו": "לו.text",
		"בא": "בא.text",
		"קורא": "קורא.text",
		"בדלת": "בדלת.text",
		"מיהר, רץ": "מיהר, רץ.text",
		"והלך": "והלך.text",
		"כן! כן!": "כן! כן!.text",
		"שהגיע": "שהגיע.text",
		"שם": "שם.text",
		"עד שהביא": "עד שהביא.text",
		"עמד": "עמד.text",
		"שהתעורר": "שהתעורר.text",
		"עליו": "עליו.text",
	},
	"parameters": {
		"numOfItems": 19,
		"placeholders": [
			{ "id": "בני_1", "loc": { "x": 78, "y": -395 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רציניים, מלומדים, נחשבים_2", "loc": { "x": 176, "y": -355 }, "rect": { "width": 80, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה_3", "loc": { "x": 97, "y": -355 }, "rect": { "width": 65, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לו_4", "loc": { "x": 31, "y": -355 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה_5", "loc": { "x": 239, "y": -315 }, "rect": { "width": 62, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא_6", "loc": { "x": 171, "y": -315 }, "rect": { "width": 71, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קורא_7", "loc": { "x": -36, "y": -315 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בדלת_8", "loc": { "x": -109, "y": -315 }, "rect": { "width": 92, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מיהר, רץ_9", "loc": { "x": -192, "y": -315 }, "rect": { "width": 77, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והלך_10", "loc": { "x": -273, "y": -315 }, "rect": { "width": 83, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לו_11", "loc": { "x": 167, "y": -275 }, "rect": { "width": 53, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כן! כן!_12", "loc": { "x": 27, "y": -275 }, "rect": { "width": 95, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהגיע_13", "loc": { "x": -126, "y": -275 }, "rect": { "width": 98, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שם_14", "loc": { "x": -211, "y": -275 }, "rect": { "width": 74, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עד שהביא_15", "loc": { "x": -118, "y": -235 }, "rect": { "width": 125, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לו_16", "loc": { "x": -211, "y": -235 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עמד_17", "loc": { "x": 183, "y": -194 }, "rect": { "width": 59, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהתעורר_18", "loc": { "x": -10, "y": -194 }, "rect": { "width": 122, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עליו_19", "loc": { "x": 119, "y": -194 }, "rect": { "width": 74, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "בני", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בני_1"],
			},
			{
				"type": "רציניים, מלומדים, נחשבים", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רציניים, מלומדים, נחשבים_2"],
			},
			{
				"type": "היה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה_3", "היה_5"],
			},
			{
				"type": "לו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לו_4", "לו_11", "לו_16"],
			},
			{
				"type": "היה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה_3", "היה_5"],
			},
			{
				"type": "בא", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא_6"],
			},
			{
				"type": "קורא", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קורא_7"],
			},
			{
				"type": "בדלת", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בדלת_8"],
			},
			{
				"type": "מיהר, רץ", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מיהר, רץ_9"],
			},
			{
				"type": "והלך", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והלך_10"],
			},
			{
				"type": "לו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לו_4", "לו_11", "לו_16"],
			},
			{
				"type": "כן! כן!", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כן! כן!_12"],
			},
			{
				"type": "שהגיע", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהגיע_13"],
			},
			{
				"type": "שם", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שם_14"],
			},
			{
				"type": "עד שהביא", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עד שהביא_15"],
			},
			{
				"type": "לו", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לו_4", "לו_11", "לו_16"],
			},
			{
				"type": "עמד", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עמד_17"],
			},
			{
				"type": "שהתעורר", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהתעורר_18"],
			},
			{
				"type": "עליו", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עליו_19"],
			},
		],
		"ex_name": "חמישה בני סמכי הוה ליה לאבימי_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}