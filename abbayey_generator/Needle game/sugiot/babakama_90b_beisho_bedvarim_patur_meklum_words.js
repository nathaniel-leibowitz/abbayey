var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_91a_bottom.png",
		"ירק על בן אדם": "ירק על בן אדם.text",
		"ופגע": "ופגע.text",
		"לא דברה המשנה אלא על המקרה": "לא דברה המשנה אלא על המקרה.text",
		"שהרוק פגע בגוף האדם": "שהרוק פגע בגוף האדם.text",
		"אם הרוק פגע רק בבגד": "אם הרוק פגע רק בבגד.text",
		"במילים, שיימינג": "במילים, שיימינג.text",
		"אמרו, הסבירו": "אמרו, הסבירו.text",
		"בארץ ישראל, שהיא מערבית לבבל": "בארץ ישראל, שהיא מערבית לבבל.text",
		"משמו, על פי דבריו של": "משמו, על פי דבריו של.text",
		"רבי יוסי בנו של אבין": "רבי יוסי בנו של אבין.text",
		"לא חייב לפצות אותו על הביוש": "לא חייב לפצות אותו על הביוש.text",
		"שיהיה, נחשיב את זה": "שיהיה, נחשיב את זה.text",
		"כמו, כאילו": "כמו, כאילו.text",
	},
	"parameters": {
		"numOfItems": 15,
		"placeholders": [
			{ "id": "ירק על בן אדם_1", "loc": { "x": -248, "y": 81 }, "rect": { "width": 41, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ופגע_2", "loc": { "x": -310, "y": 81 }, "rect": { "width": 56, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא דברה המשנה אלא על המקרה_3", "loc": { "x": -71, "y": 111 }, "rect": { "width": 140, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרוק פגע בגוף האדם_4", "loc": { "x": -167, "y": 111 }, "rect": { "width": 23, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם הרוק פגע רק בבגד_5", "loc": { "x": -287, "y": 111 }, "rect": { "width": 59, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במילים, שיימינג_6", "loc": { "x": 183, "y": 141 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אמרו, הסבירו_7", "loc": { "x": 108, "y": 141 }, "rect": { "width": 50, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בארץ ישראל, שהיא מערבית לבבל_8", "loc": { "x": 29, "y": 141 }, "rect": { "width": 86, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "משמו, על פי דבריו של_9", "loc": { "x": -92, "y": 141 }, "rect": { "width": 74, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רבי יוסי בנו של אבין_10", "loc": { "x": -231, "y": 141 }, "rect": { "width": 191, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא חייב לפצות אותו על הביוש_11", "loc": { "x": 63, "y": 170 }, "rect": { "width": 134, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא חייב לפצות אותו על הביוש_12", "loc": { "x": -346, "y": 111 }, "rect": { "width": 29, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במילים, שיימינג_13", "loc": { "x": 182, "y": 170 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיהיה, נחשיב את זה_14", "loc": { "x": -402, "y": 111 }, "rect": { "width": 53, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמו, כאילו_15", "loc": { "x": -454, "y": 111 }, "rect": { "width": 23, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ירק על בן אדם", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ירק על בן אדם_1"],
			},
			{
				"type": "ופגע", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ופגע_2"],
			},
			{
				"type": "לא דברה המשנה אלא על המקרה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא דברה המשנה אלא על המקרה_3"],
			},
			{
				"type": "שהרוק פגע בגוף האדם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרוק פגע בגוף האדם_4"],
			},
			{
				"type": "אם הרוק פגע רק בבגד", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם הרוק פגע רק בבגד_5"],
			},
			{
				"type": "במילים, שיימינג", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במילים, שיימינג_6", "במילים, שיימינג_13"],
			},
			{
				"type": "אמרו, הסבירו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אמרו, הסבירו_7"],
			},
			{
				"type": "בארץ ישראל, שהיא מערבית לבבל", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בארץ ישראל, שהיא מערבית לבבל_8"],
			},
			{
				"type": "משמו, על פי דבריו של", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "משמו, על פי דבריו של_9"],
			},
			{
				"type": "רבי יוסי בנו של אבין", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבי יוסי בנו של אבין_10"],
			},
			{
				"type": "לא חייב לפצות אותו על הביוש", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא חייב לפצות אותו על הביוש_11", "לא חייב לפצות אותו על הביוש_12"],
			},
			{
				"type": "לא חייב לפצות אותו על הביוש", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא חייב לפצות אותו על הביוש_11", "לא חייב לפצות אותו על הביוש_12"],
			},
			{
				"type": "במילים, שיימינג", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במילים, שיימינג_6", "במילים, שיימינג_13"],
			},
			{
				"type": "שיהיה, נחשיב את זה", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיהיה, נחשיב את זה_14"],
			},
			{
				"type": "כמו, כאילו", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמו, כאילו_15"],
			},
		],
		"ex_name": "babakama_90b_beisho_bedvarim_patur_meklum_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}