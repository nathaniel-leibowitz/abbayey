var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"נשאלו שאלה": "נשאלו שאלה.text",
		"אם תמצי לומר, אפשר לסבור ש": "אם תמצי לומר, אפשר לסבור ש.text",
		"שיום אחד הוא, אותה יממה": "שיום אחד הוא, אותה יממה.text",
		"שהרי כתוב": "שהרי כתוב.text",
		"כאן, כלומר בשכח מנחה": "כאן, כלומר בשכח מנחה.text",
		"או שמא, אפשר לסבור אחרת": "או שמא, אפשר לסבור אחרת.text",
		"תפילה": "תפילה.text",
		"מתי שירצה": "מתי שירצה.text",
		"מתפלל והולך, מתפלל כרצונו": "מתפלל והולך, מתפלל כרצונו.text",
		"בוא שמע תשובה": "בוא שמע תשובה.text",
		"פעמיים": "פעמיים.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "נשאלו שאלה_1", "loc": { "x": -63, "y": 150 }, "rect": { "width": 131, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם תמצי לומר, אפשר לסבור ש_2", "loc": { "x": 416, "y": 182 }, "rect": { "width": 62, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיום אחד הוא, אותה יממה_3", "loc": { "x": -277, "y": 182 }, "rect": { "width": 167, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי כתוב_4", "loc": { "x": -408, "y": 182 }, "rect": { "width": 71, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן, כלומר בשכח מנחה_5", "loc": { "x": 282, "y": 213 }, "rect": { "width": 50, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "או שמא, אפשר לסבור אחרת_6", "loc": { "x": -368, "y": 213 }, "rect": { "width": 104, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תפילה_7", "loc": { "x": -518, "y": 213 }, "rect": { "width": 86, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתי שירצה_8", "loc": { "x": 362, "y": 245 }, "rect": { "width": 167, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתפלל והולך, מתפלל כרצונו_9", "loc": { "x": 209, "y": 245 }, "rect": { "width": 122, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בוא שמע תשובה_10", "loc": { "x": 115, "y": 245 }, "rect": { "width": 53, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פעמיים_11", "loc": { "x": 470, "y": 182 }, "rect": { "width": 26, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פעמיים_12", "loc": { "x": 224, "y": 277 }, "rect": { "width": 26, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "נשאלו שאלה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נשאלו שאלה_1"],
			},
			{
				"type": "אם תמצי לומר, אפשר לסבור ש", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם תמצי לומר, אפשר לסבור ש_2"],
			},
			{
				"type": "שיום אחד הוא, אותה יממה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיום אחד הוא, אותה יממה_3"],
			},
			{
				"type": "שהרי כתוב", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי כתוב_4"],
			},
			{
				"type": "כאן, כלומר בשכח מנחה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן, כלומר בשכח מנחה_5"],
			},
			{
				"type": "או שמא, אפשר לסבור אחרת", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "או שמא, אפשר לסבור אחרת_6"],
			},
			{
				"type": "תפילה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תפילה_7"],
			},
			{
				"type": "מתי שירצה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתי שירצה_8"],
			},
			{
				"type": "מתפלל והולך, מתפלל כרצונו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתפלל והולך, מתפלל כרצונו_9"],
			},
			{
				"type": "בוא שמע תשובה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בוא שמע תשובה_10"],
			},
			{
				"type": "פעמיים", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פעמיים_11", "פעמיים_12"],
			},
			{
				"type": "פעמיים", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פעמיים_11", "פעמיים_12"],
			},
		],
		"ex_name": "brachot_62a_taa_velo_hitpalel_tashma_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}