var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"שאלו להם, התחבטו": "שאלו להם, התחבטו.text",
		"שייכים לאותה יממה": "שייכים לאותה יממה.text",
		"שהרי כתוב": "שהרי כתוב.text",
		"כאן, כששכח להתפלל מנחה": "כאן, כששכח להתפלל מנחה.text",
		"שמא": "שמא.text",
		"שתפילה": "שתפילה.text",
		"שרוצה": "שרוצה.text",
		"מתפלל והולך, מתפלל כרצונו": "מתפלל והולך, מתפלל כרצונו.text",
		"בא שמע תשובה": "בא שמע תשובה.text",
		"מקשים מברייתא": "מקשים מברייתא.text",
		"כאן עוסקים במקרה שבו": "כאן עוסקים במקרה שבו.text",
		"שמע מינה, אכן כך משתמע": "שמע מינה, אכן כך משתמע.text",
		"אם תמצי לומר, אולי נאמר ש": "אם תמצי לומר, אולי נאמר ש.text",
		"ניתן גם לדייק זאת מהברייתא": "ניתן גם לדייק זאת מהברייתא.text",
		"שהרי כתב": "שהרי כתב.text",
		"ולא כתב": "ולא כתב.text",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "שאלו להם, התחבטו_1", "loc": { "x": -63, "y": 150 }, "rect": { "width": 134, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שייכים לאותה יממה_2", "loc": { "x": -251, "y": 182 }, "rect": { "width": 110, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי כתוב_3", "loc": { "x": -407, "y": 182 }, "rect": { "width": 68, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן, כששכח להתפלל מנחה_4", "loc": { "x": 281, "y": 213 }, "rect": { "width": 50, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא_5", "loc": { "x": -385, "y": 213 }, "rect": { "width": 71, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שתפילה_6", "loc": { "x": -518, "y": 213 }, "rect": { "width": 86, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שרוצה_7", "loc": { "x": 306, "y": 245 }, "rect": { "width": 53, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתפלל והולך, מתפלל כרצונו_8", "loc": { "x": 209, "y": 245 }, "rect": { "width": 122, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא שמע תשובה_9", "loc": { "x": 114, "y": 245 }, "rect": { "width": 53, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מקשים מברייתא_10", "loc": { "x": -264, "y": 277 }, "rect": { "width": 74, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן עוסקים במקרה שבו_11", "loc": { "x": 295, "y": 371 }, "rect": { "width": 197, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמע מינה, אכן כך משתמע_12", "loc": { "x": -528, "y": 371 }, "rect": { "width": 50, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם תמצי לומר, אולי נאמר ש_13", "loc": { "x": 416, "y": 182 }, "rect": { "width": 62, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ניתן גם לדייק זאת מהברייתא_14", "loc": { "x": -157, "y": 371 }, "rect": { "width": 98, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי כתב_15", "loc": { "x": -246, "y": 371 }, "rect": { "width": 65, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ולא כתב_16", "loc": { "x": -389, "y": 371 }, "rect": { "width": 98, "height": 28 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שאלו להם, התחבטו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאלו להם, התחבטו_1"],
			},
			{
				"type": "שייכים לאותה יממה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שייכים לאותה יממה_2"],
			},
			{
				"type": "שהרי כתוב", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי כתוב_3"],
			},
			{
				"type": "כאן, כששכח להתפלל מנחה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן, כששכח להתפלל מנחה_4"],
			},
			{
				"type": "שמא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא_5"],
			},
			{
				"type": "שתפילה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שתפילה_6"],
			},
			{
				"type": "שרוצה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שרוצה_7"],
			},
			{
				"type": "מתפלל והולך, מתפלל כרצונו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתפלל והולך, מתפלל כרצונו_8"],
			},
			{
				"type": "בא שמע תשובה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא שמע תשובה_9"],
			},
			{
				"type": "מקשים מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקשים מברייתא_10"],
			},
			{
				"type": "כאן עוסקים במקרה שבו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן עוסקים במקרה שבו_11"],
			},
			{
				"type": "שמע מינה, אכן כך משתמע", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמע מינה, אכן כך משתמע_12"],
			},
			{
				"type": "אם תמצי לומר, אולי נאמר ש", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם תמצי לומר, אולי נאמר ש_13"],
			},
			{
				"type": "ניתן גם לדייק זאת מהברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ניתן גם לדייק זאת מהברייתא_14"],
			},
			{
				"type": "שהרי כתב", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי כתב_15"],
			},
			{
				"type": "ולא כתב", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ולא כתב_16"],
			},
		],
		"ex_name": "brachot_26a_taa_velo_hitpalel_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}