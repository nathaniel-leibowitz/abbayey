﻿window.onload = () => {
};

resetGroupsDropdown();
resetSchoolsDropdown();
resetNamesDropdown();

function resetSchoolsDropdown() {
    let usersElement = document.getElementById("schools");
    schools.forEach(function (value, key) {
        var opt = document.createElement('option');
        // create text node to add to option element (opt)
        opt.appendChild(document.createTextNode(key));
        // set value property of opt
        opt.value = key;
        // add opt to end of select box (sel)
        usersElement.appendChild(opt);
    });
}

function resetGroupsDropdown() {
    let usersElement = document.getElementById("player_group");
    usersElement.options.length = 0;
    var opt = document.createElement('option');
    // create text node to add to option element (opt)
    opt.appendChild(document.createTextNode("קבוצה"));
    // set value property of opt
    opt.value = "קבוצה";
    // add opt to end of select box (sel)
    usersElement.appendChild(opt);
}

function resetNamesDropdown() {
    let usersElement = document.getElementById("player_name_configured");
    usersElement.options.length = 0;
    var opt = document.createElement('option');
    // create text node to add to option element (opt)
    opt.appendChild(document.createTextNode("רשימת השמות"));
    // set value property of opt
    opt.value = "רשימת השמות";
    // add opt to end of select box (sel)
    usersElement.appendChild(opt);
}

function displayGroups(schoolName) {
    let usersElement = document.getElementById("player_group");
    usersElement.options.length = 0;
    var opt = document.createElement('option');
    // create text node to add to option element (opt)
    opt.appendChild(document.createTextNode("קבוצה"));
    // set value property of opt
    opt.value = "קבוצה";
    // add opt to end of select box (sel)
    usersElement.appendChild(opt);
    schools.get(schoolName).forEach(function (group) {
        var opt = document.createElement('option');
        // create text node to add to option element (opt)
        opt.appendChild(document.createTextNode(group));
        // set value property of opt
        opt.value = group;
        // add opt to end of select box (sel)
        usersElement.appendChild(opt);
    });
    //if (usersElement.options.length == 2) //the first is the directrive. The second is the first real group
      //  usersElement.options[1].selected = 1;
}

function displayNames(groupName) {
    let users = document.getElementById("player_name_configured");
    users.options.length = 0;
    var opt = document.createElement('option');
    // create text node to add to option element (opt)
    opt.appendChild(document.createTextNode("רשימת השמות"));
    // set value property of opt
    opt.value = "רשימת השמות";
    // add opt to end of select box (sel)
    users.appendChild(opt);
    var group = groups.get(groupName);
    if (group == null)
        return;
    group.forEach(function (name) {
        var opt = document.createElement('option');
        // create text node to add to option element (opt)
        opt.appendChild(document.createTextNode(name));
        // set value property of opt
        opt.value = name;
        // add opt to end of select box (sel)
        users.appendChild(opt);
    });
    //if (usersElement.options.length == 2) //the first is the directrive. The second is the first real group
      //  usersElement.options[1].selected = 1;
}