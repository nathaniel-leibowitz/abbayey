var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56b_top.png",
		"ברור מאליו, מיותר לומר": "ברור מאליו, מיותר לומר.text",
		"שהוציאו אותה": "שהוציאו אותה.text",
		"לכל עניין ודבר": "לכל עניין ודבר.text",
		"עמדו בפניה וכיוונו את הליכתה": "עמדו בפניה וכיוונו את הליכתה.text",
		"כמו הדין הזה": "כמו הדין הזה.text",
		"עמד בפניה וכיוון את דרכה": "עמד בפניה וכיוון את דרכה.text",
		"לנו": "לנו.text",
		"גם": "גם.text",
		"הצליף בה במקל": "הצליף בה במקל.text",
		"שהצליפו בה במקל": "שהצליפו בה במקל.text",
		"לא מיותר, יש צורך לומר": "לא מיותר, יש צורך לומר.text",
		"הופכת ליהיות רכושו": "הופכת ליהיות רכושו.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "ברור מאליו, מיותר לומר_1", "loc": { "x": 321, "y": -258 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהוציאו אותה_2", "loc": { "x": 161, "y": -258 }, "rect": { "width": 98, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכל עניין ודבר_3", "loc": { "x": 308, "y": -222 }, "rect": { "width": 113, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עמדו בפניה וכיוונו את הליכתה_4", "loc": { "x": 20, "y": -222 }, "rect": { "width": 191, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמו הדין הזה_5", "loc": { "x": -121, "y": -222 }, "rect": { "width": 74, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עמד בפניה וכיוון את דרכה_6", "loc": { "x": 48, "y": -113 }, "rect": { "width": 182, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לנו_7", "loc": { "x": -35, "y": -76 }, "rect": { "width": 26, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_8", "loc": { "x": 343, "y": -40 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ברור מאליו, מיותר לומר_9", "loc": { "x": 321, "y": -113 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הצליף בה במקל_10", "loc": { "x": 118, "y": -76 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהצליפו בה במקל_11", "loc": { "x": 254, "y": -40 }, "rect": { "width": 113, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא מיותר, יש צורך לומר_12", "loc": { "x": 183, "y": -222 }, "rect": { "width": 122, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא מיותר, יש צורך לומר_13", "loc": { "x": 208, "y": -113 }, "rect": { "width": 122, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הופכת ליהיות רכושו_14", "loc": { "x": -29, "y": -258 }, "rect": { "width": 266, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ברור מאליו, מיותר לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברור מאליו, מיותר לומר_1", "ברור מאליו, מיותר לומר_9"],
			},
			{
				"type": "שהוציאו אותה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהוציאו אותה_2"],
			},
			{
				"type": "לכל עניין ודבר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכל עניין ודבר_3"],
			},
			{
				"type": "עמדו בפניה וכיוונו את הליכתה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עמדו בפניה וכיוונו את הליכתה_4"],
			},
			{
				"type": "כמו הדין הזה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמו הדין הזה_5"],
			},
			{
				"type": "עמד בפניה וכיוון את דרכה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עמד בפניה וכיוון את דרכה_6"],
			},
			{
				"type": "לנו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לנו_7"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_8"],
			},
			{
				"type": "ברור מאליו, מיותר לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברור מאליו, מיותר לומר_1", "ברור מאליו, מיותר לומר_9"],
			},
			{
				"type": "הצליף בה במקל", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הצליף בה במקל_10"],
			},
			{
				"type": "שהצליפו בה במקל", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהצליפו בה במקל_11"],
			},
			{
				"type": "לא מיותר, יש צורך לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא מיותר, יש צורך לומר_12", "לא מיותר, יש צורך לומר_13"],
			},
			{
				"type": "לא מיותר, יש צורך לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא מיותר, יש צורך לומר_12", "לא מיותר, יש צורך לומר_13"],
			},
			{
				"type": "הופכת ליהיות רכושו", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הופכת ליהיות רכושו_14"],
			},
		],
		"ex_name": "babakama_56b_hikisha_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}