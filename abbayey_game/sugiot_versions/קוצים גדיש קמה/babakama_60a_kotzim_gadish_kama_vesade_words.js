var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"הייתי אומר בטעות": "הייתי אומר בטעות.text",
		"מאיפה לו, מהיכן הוא למד זאת": "מאיפה לו, מהיכן הוא למד זאת.text",
		"הוא מוציא את הדין, לומד את הדין": "הוא מוציא את הדין, לומד את הדין.text",
		"צריך לו, דרוש לו": "צריך לו, דרוש לו.text",
		"אגב, אפרופו": "אגב, אפרופו.text",
		"להביא את הדין, ללמוד את הדין": "להביא את הדין, ללמוד את הדין.text",
		"ואז לא יהיה צורך באלו האחרים": "ואז לא יהיה צורך באלו האחרים.text",
		"כן": "כן.text",
		"דברים אחרים": "דברים אחרים.text",
		"קא משמע לן": "קא משמע לן.text",
		"נומר": "נומר.text",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "הייתי אומר בטעות_1", "loc": { "x": -350, "y": -204 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הייתי אומר בטעות_2", "loc": { "x": 235, "y": -59 }, "rect": { "width": 137, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הייתי אומר בטעות_3", "loc": { "x": 98, "y": 196 }, "rect": { "width": 134, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לו, מהיכן הוא למד זאת_4", "loc": { "x": 30, "y": 51 }, "rect": { "width": 110, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לו, מהיכן הוא למד זאת_5", "loc": { "x": -41, "y": 87 }, "rect": { "width": 110, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא מוציא את הדין, לומד את הדין_6", "loc": { "x": -100, "y": 51 }, "rect": { "width": 125, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא מוציא את הדין, לומד את הדין_7", "loc": { "x": -167, "y": 87 }, "rect": { "width": 122, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריך לו, דרוש לו_8", "loc": { "x": 316, "y": 87 }, "rect": { "width": 128, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אגב, אפרופו_9", "loc": { "x": 332, "y": 123 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להביא את הדין, ללמוד את הדין_10", "loc": { "x": 485, "y": 160 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואז לא יהיה צורך באלו האחרים_11", "loc": { "x": -293, "y": 160 }, "rect": { "width": 173, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כן_12", "loc": { "x": -151, "y": 196 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דברים אחרים_13", "loc": { "x": -255, "y": 196 }, "rect": { "width": 155, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_14", "loc": { "x": 494, "y": 233 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נומר_15", "loc": { "x": -247, "y": -95 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נומר_16", "loc": { "x": 168, "y": -22 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הייתי אומר בטעות_1", "הייתי אומר בטעות_2", "הייתי אומר בטעות_3"],
			},
			{
				"type": "הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הייתי אומר בטעות_1", "הייתי אומר בטעות_2", "הייתי אומר בטעות_3"],
			},
			{
				"type": "הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הייתי אומר בטעות_1", "הייתי אומר בטעות_2", "הייתי אומר בטעות_3"],
			},
			{
				"type": "מאיפה לו, מהיכן הוא למד זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לו, מהיכן הוא למד זאת_4", "מאיפה לו, מהיכן הוא למד זאת_5"],
			},
			{
				"type": "מאיפה לו, מהיכן הוא למד זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לו, מהיכן הוא למד זאת_4", "מאיפה לו, מהיכן הוא למד זאת_5"],
			},
			{
				"type": "הוא מוציא את הדין, לומד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא מוציא את הדין, לומד את הדין_6", "הוא מוציא את הדין, לומד את הדין_7"],
			},
			{
				"type": "הוא מוציא את הדין, לומד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא מוציא את הדין, לומד את הדין_6", "הוא מוציא את הדין, לומד את הדין_7"],
			},
			{
				"type": "צריך לו, דרוש לו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך לו, דרוש לו_8"],
			},
			{
				"type": "אגב, אפרופו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אגב, אפרופו_9"],
			},
			{
				"type": "להביא את הדין, ללמוד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להביא את הדין, ללמוד את הדין_10"],
			},
			{
				"type": "ואז לא יהיה צורך באלו האחרים", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואז לא יהיה צורך באלו האחרים_11"],
			},
			{
				"type": "כן", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כן_12"],
			},
			{
				"type": "דברים אחרים", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דברים אחרים_13"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_14"],
			},
			{
				"type": "נומר", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נומר_15", "נומר_16"],
			},
			{
				"type": "נומר", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נומר_15", "נומר_16"],
			},
		],
		"ex_name": "babakama_60a_kotzim_gadish_kama_vesade_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}