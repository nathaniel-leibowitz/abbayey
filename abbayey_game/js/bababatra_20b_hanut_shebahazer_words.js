var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/bababatra_20b_bottom.png",
		"מהרעש": "מהרעש.text",
		"הלקוחות הנכנסים לחנות": "הלקוחות הנכנסים לחנות.text",
		"הלקוחות היוצאים מהחנות": "הלקוחות היוצאים מהחנות.text",
		"השכנים מתנגדים ומונעים ממנו": "השכנים מתנגדים ומונעים ממנו.text",
		"דייר הפותח חנות בחצר משותפת": "דייר הפותח חנות בחצר משותפת.text",
		"דייר הפותח בית מלאכהבחצר משותפת": "דייר הפותח בית מלאכהבחצר משותפת.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "מהרעש_1", "loc": { "x": -254, "y": 136 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הלקוחות הנכנסים לחנות_2", "loc": { "x": -341, "y": 136 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הלקוחות היוצאים מהחנות_3", "loc": { "x": -23, "y": 168 }, "rect": { "width": 80, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מהרעש_4", "loc": { "x": 55, "y": 168 }, "rect": { "width": 62, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "השכנים מתנגדים ומונעים ממנו_5", "loc": { "x": -315, "y": 103 }, "rect": { "width": 137, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "השכנים מתנגדים ומונעים ממנו_6", "loc": { "x": -247, "y": 201 }, "rect": { "width": 131, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דייר הפותח חנות בחצר משותפת_7", "loc": { "x": -98, "y": 103 }, "rect": { "width": 152, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דייר הפותח בית מלאכהבחצר משותפת_8", "loc": { "x": -198, "y": 168 }, "rect": { "width": 122, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מהרעש", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהרעש_1", "מהרעש_4"],
			},
			{
				"type": "הלקוחות הנכנסים לחנות", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הלקוחות הנכנסים לחנות_2"],
			},
			{
				"type": "הלקוחות היוצאים מהחנות", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הלקוחות היוצאים מהחנות_3"],
			},
			{
				"type": "מהרעש", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהרעש_1", "מהרעש_4"],
			},
			{
				"type": "השכנים מתנגדים ומונעים ממנו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "השכנים מתנגדים ומונעים ממנו_5", "השכנים מתנגדים ומונעים ממנו_6"],
			},
			{
				"type": "השכנים מתנגדים ומונעים ממנו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "השכנים מתנגדים ומונעים ממנו_5", "השכנים מתנגדים ומונעים ממנו_6"],
			},
			{
				"type": "דייר הפותח חנות בחצר משותפת", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דייר הפותח חנות בחצר משותפת_7"],
			},
			{
				"type": "דייר הפותח בית מלאכהבחצר משותפת", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דייר הפותח בית מלאכהבחצר משותפת_8"],
			},
		],
		"ex_name": "bababatra_20b_hanut_shebahazer_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}