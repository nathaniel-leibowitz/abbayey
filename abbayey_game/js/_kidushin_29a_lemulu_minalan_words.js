var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_bottom.png",
		"מאיפה לנו, מנין לומדים זאת": "מאיפה לנו, מנין לומדים זאת.text",
		"והיכן ש, במידה ו": "והיכן ש, במידה ו.text",
		"אביו": "אביו.text",
		"עצמו": "עצמו.text",
		"היא, האשה": "היא, האשה.text",
		"מצאנו, ידוע לנו": "מצאנו, ידוע לנו.text",
		"מל אותו": "מל אותו.text",
		"הוא, הילד": "הוא, הילד.text",
		"בית הדין": "בית הדין.text",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "מאיפה לנו, מנין לומדים זאת_1", "loc": { "x": -294, "y": -354 }, "rect": { "width": 59, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיכן ש, במידה ו_2", "loc": { "x": -309, "y": -318 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביו_3", "loc": { "x": -21, "y": -281 }, "rect": { "width": 71, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיכן ש, במידה ו_4", "loc": { "x": -305, "y": -245 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עצמו_5", "loc": { "x": -373, "y": -209 }, "rect": { "width": 86, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היא, האשה_6", "loc": { "x": -257, "y": -173 }, "rect": { "width": 62, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_7", "loc": { "x": -322, "y": -173 }, "rect": { "width": 59, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצאנו, ידוע לנו_8", "loc": { "x": -310, "y": -136 }, "rect": { "width": 95, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מל אותו_9", "loc": { "x": 70, "y": -281 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מל אותו_10", "loc": { "x": 51, "y": -209 }, "rect": { "width": 95, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא, הילד_11", "loc": { "x": -202, "y": -209 }, "rect": { "width": 62, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית הדין_12", "loc": { "x": -36, "y": -209 }, "rect": { "width": 95, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_13", "loc": { "x": 365, "y": -100 }, "rect": { "width": 62, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_1", "מאיפה לנו, מנין לומדים זאת_7", "מאיפה לנו, מנין לומדים זאת_13"],
			},
			{
				"type": "והיכן ש, במידה ו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיכן ש, במידה ו_2", "והיכן ש, במידה ו_4"],
			},
			{
				"type": "אביו", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביו_3"],
			},
			{
				"type": "והיכן ש, במידה ו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיכן ש, במידה ו_2", "והיכן ש, במידה ו_4"],
			},
			{
				"type": "עצמו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עצמו_5"],
			},
			{
				"type": "היא, האשה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היא, האשה_6"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_1", "מאיפה לנו, מנין לומדים זאת_7", "מאיפה לנו, מנין לומדים זאת_13"],
			},
			{
				"type": "מצאנו, ידוע לנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצאנו, ידוע לנו_8"],
			},
			{
				"type": "מל אותו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מל אותו_9", "מל אותו_10"],
			},
			{
				"type": "מל אותו", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מל אותו_9", "מל אותו_10"],
			},
			{
				"type": "הוא, הילד", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא, הילד_11"],
			},
			{
				"type": "בית הדין", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית הדין_12"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_1", "מאיפה לנו, מנין לומדים זאת_7", "מאיפה לנו, מנין לומדים זאת_13"],
			},
		],
		"ex_name": "kidushin_29a_lemulu_minalan_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}