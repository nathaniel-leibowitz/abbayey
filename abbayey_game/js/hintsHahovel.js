function addHahovelHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "babakama_83b_hahovel_et_havero_syntax":
            hints.push({ text: 'השתמשו בקו המפריד הצהוב, להפריד בין המקרה לדין. למשל היכהו - חייב לרפאותו', symbol: "hyphen" });
            hints.push({ text: 'ומקמו בסופי המשפט את הנקודות', symbol: "period" });
            hints.push({ text: 'וכשהמשנה שואלת סמנו זאת ב', symbol: "question" });
            break;

        case "babakama_89a_hatokeah_lehavero_syntax":
            hints.push({ text: 'המשנה דנה בעוצמת הבושה של סוגים שונים של מכות. השתמשו בנקודות להפריד בין הסוגים השונים של המכות', symbol: "period" });
            hints.push({ text: 'במשנה הרבה משפטי תנאי. השתמשו בקו המפריד הצהוב, להפריד בין המקרה לדין. למשל סטרו - נותן לו 400 זוז', symbol: "hyphen" });
            break;

        case "babakama_90b_ze_haklal_vakol_lefi_kvodo_syntax":
            hints.push({ text: 'במשנה מובא סיפור. השתמשו ברמקולים להקיף את הדו-שיח בין הדמויות', symbol: "" });
            hints.push({ text: 'כשדמות מתחילה לדבר השתמשו ב', symbol: "speakbegin" });
            hints.push({ text: 'וכשהיא מסיימת את דבריה השתמשו ב', symbol: "speakend" });
            hints.push({ text: 'בסוף הסיפור מופיעים משפטי תנאי. השתמשו בקו הצהוב להפריד בין המקרה לדין', symbol: "hyphen" });
            hints.push({ text: 'אתגר קשה: הגיבור שואל שאלה רטורית, כלומר תמיהה, מצאו אותה וסמנו בסופה', symbol: "interrobang" });
            break;

        case "babakama_90b_beisho_bedvarim_patur_meklum_syntax":
            hints.push({ text: 'הסוגיא פותחת בציטוט מתוך המשנה שלנו. הקיפו את הציטוט הרלונטי ב', symbol: "quotebegin" });
            hints.push({ text: 'רב פפא מעיר על המשנה. מיצאו היכן מסתיימים דבריו ומקמו שם נקודה', symbol: "period" });
            hints.push({ text: 'בדבריו יש מקרה ואז דין. השתמשו בקו בשביל להפריד בין המקרה לדין', symbol: "hyphen" });
            hints.push({ text: 'הגענו ללב הסוגיא! הגמרא מקשה על דברי רב פפא. מיצאו בדיוק היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'ומיצא היכן מסיים התרצן לתרץ ומקמו שם', symbol: "resolution" });
            break;
    }

    addHint(div, hints);
}
