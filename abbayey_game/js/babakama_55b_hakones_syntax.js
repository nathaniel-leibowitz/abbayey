var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_55b_top.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "hyphen_1", "loc": { "x": 45, "y": -166 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_2", "loc": { "x": -26, "y": -166 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_3", "loc": { "x": 6, "y": -129 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_4", "loc": { "x": -57, "y": -129 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_5", "loc": { "x": -167, "y": -93 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_6", "loc": { "x": 301, "y": -56 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_7", "loc": { "x": 33, "y": -56 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_8", "loc": { "x": -160, "y": -56 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_9", "loc": { "x": 102, "y": 16 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_10", "loc": { "x": 35, "y": 16 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_11", "loc": { "x": -166, "y": 16 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_12", "loc": { "x": 120, "y": 53 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_13", "loc": { "x": -163, "y": 53 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_14", "loc": { "x": 110, "y": 89 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_15", "loc": { "x": -165, "y": 89 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_16", "loc": { "x": 95, "y": 126 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -670, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -760, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -760, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -640, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -640, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -790, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -790, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -610, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -610, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -820, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1", "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_11", "hyphen_13", "hyphen_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -820, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2", "period_4", "period_6", "period_8", "period_10", "period_12", "period_14", "period_16"],
			},
		],
		"ex_name": "babakama_55b_hakones_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}