function addHagozelEtzimHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "babakama_94b_ein_mekablim_mehem_syntax":
            hints.push({ text: 'הסוגיא מורכבת משתי ברייתות, הקיפו כל אחת ב', symbol: "quotebegin" });
            hints.push({ text: 'נתמקד בברייתא השנייה. היא מספרת סיפור ואז מצטטת את הברייתא הראשונה, הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'בסיפור שהיא מספרת, הקף ברמקולים את דברי האשה לבעלה', symbol: "speakbegin" });
            hints.push({ text: 'אתגר: הברייתות מורכבות ממשפטי תנאי: המקרה ואז הדין שלו. הפרידו בין המקרה לדין בעזרת הקו המפריד', symbol: "hyphen" });
            break;

        case "babakama_94b_hiniach_lahem_avihem_syntax":
            hints.push({ text: 'סוגיא קשה! היא העוסקת בברייתא שלכאורה סותרת את התקנה שלמדנו בסוגיא הקודמת', symbol: "" });
            hints.push({ text: 'הסוגיא מצטטת את חלקה הראשון של הברייתא, דנה בה, ואז מצטטת את חלקה השני. הקיפו את שני החלקים ב', symbol: "quotebegin" });
            hints.push({ text: 'המקשן מדייק מהחלק הראשון של הברייתא סתירה לתקנה שלמדנו. אתרו היכן המקשן מסיים להתקיף את קושייתו', symbol: "qushia" });
            hints.push({ text: 'אתרו היכן התרצן מסיים להשיב את תירוצו', symbol: "resolution" });
            hints.push({ text: 'אתגר: במהלך הסוגיא חבויים שני משפטי תנאי, השתמשו בקו להפריד בין המקרה לבין הדין בכל אחד מהם', symbol: "hyphen" });
            break;

        case "babakama_94b_venasi_beumcha_syntax":
            hints.push({ text: 'שוב סוגייא קשה! היא דנה בברייתא הקודמת ומקשה עליה בשני סבבים, קושיא ותירוץ וחוזר חלילה', symbol: "" });
            hints.push({ text: 'המקשן פותח בתמיהה, שאלה רטורית. סמנוה ב', symbol: "interrobang" });
            hints.push({ text: 'ואז מצטט מדרש הלכה לביסוס קושייתו. הקיפוהו ב', symbol: "quotebegin" });
            hints.push({ text: 'המדרש מצטט פסוק הקיפוהו ב', symbol: "biblequote" });
            hints.push({ text: 'אתרו היכן המקשן מסיים להתקיף את קושייתו הראשונה', symbol: "qushia" });
            hints.push({ text: 'התרצן מצטט היגד של רב פנחס, הקיפוהו ב', symbol: "quotebegin" });
            hints.push({ text: 'ואתרו היכן התרצן מסיים להשיב את תירוצו הראשון', symbol: "resolution" });
            hints.push({ text: 'כעת אתרו היכן המקשן מסיים להתקיף את קושייתו השנייה', symbol: "qushia" });
            hints.push({ text: 'והיכן התרצן מסיים להשיב את תירוצו השני', symbol: "resolution" });
            break;

        case "babakama_94b_ufulpi_shegavu_machazirim_syntax":
            hints.push({ text: 'שוב סוגיא קשה! היא עוסקת בברייתא שלכאורה סותרת את התקנה הראשונה שלמדנו בסוגיא הראשונה', symbol: "" });
            hints.push({ text: 'המקשן מברר תחילה את הנוסח המדוייק של הברייתא. הקיפו הן את הנוסח התחילי והן את הנוסח הסופי של הברייתא', symbol: "quotebegin" });
            hints.push({ text: 'ואתרו היכן המקשן מסיים להתקיף את קושייתו הראשונה', symbol: "qushia" });
            hints.push({ text: 'אתרו היכן התרצן מסיים להשיב את תירוצו הראשון', symbol: "resolution" });
            hints.push({ text: 'אתרו היכן המקשן מסיים להתקיף את קושייתו השנייה', symbol: "qushia" });
            hints.push({ text: 'והיכן התרצן מסיים להשיב את תירוצו השני', symbol: "resolution" });
            hints.push({ text: 'אתגר: במהלך הסוגיא חבויים שני משפטי תנאי, השתמשו בקו להפריד בין המקרה לבין הדין בכל אחד מהם', symbol: "hyphen" });
            break;
    }
    addHint(div, hints);
}
