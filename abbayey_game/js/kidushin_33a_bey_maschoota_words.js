var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33a_middle.png",
		"והרי": "והרי.text",
		"היה יושב": "היה יושב.text",
		"בית מרחץ": "בית מרחץ.text",
		"מלפניו": "מלפניו.text",
		"שמא": "שמא.text",
		"ועוד": "ועוד.text",
		"הרי": "הרי.text",
		"כך": "כך.text",
		"זה": "זה.text",
		"הכי נמי, כך גם": "הכי נמי, כך גם.text",
		"במקרה של אונס, כשאין ברירה": "במקרה של אונס, כשאין ברירה.text",
		"שונה, הדין הוא אחרת": "שונה, הדין הוא אחרת.text",
		"פנימיים": "פנימיים.text",
		"חיצוניים": "חיצוניים.text",
		"והלך": "והלך.text",
		"ובא": "ובא.text",
	},
	"parameters": {
		"numOfItems": 21,
		"placeholders": [
			{ "id": "והרי_1", "loc": { "x": -391, "y": -438 }, "rect": { "width": 62, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה יושב_2", "loc": { "x": 470, "y": -394 }, "rect": { "width": 155, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית מרחץ_3", "loc": { "x": 295, "y": -394 }, "rect": { "width": 197, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_4", "loc": { "x": -204, "y": -264 }, "rect": { "width": 107, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא_5", "loc": { "x": 382, "y": -46 }, "rect": { "width": 101, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ועוד_6", "loc": { "x": 349, "y": -307 }, "rect": { "width": 50, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה יושב_7", "loc": { "x": -425, "y": -307 }, "rect": { "width": 155, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_8", "loc": { "x": -495, "y": -394 }, "rect": { "width": 110, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית מרחץ_9", "loc": { "x": 478, "y": -264 }, "rect": { "width": 140, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי_10", "loc": { "x": -325, "y": -177 }, "rect": { "width": 47, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_11", "loc": { "x": -462, "y": -177 }, "rect": { "width": 56, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "זה_12", "loc": { "x": 347, "y": -133 }, "rect": { "width": 47, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "זה_13", "loc": { "x": 118, "y": -133 }, "rect": { "width": 50, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הכי נמי, כך גם_14", "loc": { "x": -125, "y": -133 }, "rect": { "width": 56, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במקרה של אונס, כשאין ברירה_15", "loc": { "x": 255, "y": -46 }, "rect": { "width": 134, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שונה, הדין הוא אחרת_16", "loc": { "x": 134, "y": -46 }, "rect": { "width": 80, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פנימיים_17", "loc": { "x": 190, "y": -133 }, "rect": { "width": 65, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חיצוניים_18", "loc": { "x": -46, "y": -133 }, "rect": { "width": 80, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והלך_19", "loc": { "x": 45, "y": -394 }, "rect": { "width": 80, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ובא_20", "loc": { "x": 378, "y": -351 }, "rect": { "width": 89, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והלך_21", "loc": { "x": 301, "y": -264 }, "rect": { "width": 80, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "והרי", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי_1"],
			},
			{
				"type": "היה יושב", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה יושב_2", "היה יושב_7"],
			},
			{
				"type": "בית מרחץ", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית מרחץ_3", "בית מרחץ_9"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_4", "מלפניו_8"],
			},
			{
				"type": "שמא", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא_5"],
			},
			{
				"type": "ועוד", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ועוד_6"],
			},
			{
				"type": "היה יושב", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה יושב_2", "היה יושב_7"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_4", "מלפניו_8"],
			},
			{
				"type": "בית מרחץ", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית מרחץ_3", "בית מרחץ_9"],
			},
			{
				"type": "הרי", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי_10"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_11"],
			},
			{
				"type": "זה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זה_12", "זה_13"],
			},
			{
				"type": "זה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זה_12", "זה_13"],
			},
			{
				"type": "הכי נמי, כך גם", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הכי נמי, כך גם_14"],
			},
			{
				"type": "במקרה של אונס, כשאין ברירה", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במקרה של אונס, כשאין ברירה_15"],
			},
			{
				"type": "שונה, הדין הוא אחרת", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שונה, הדין הוא אחרת_16"],
			},
			{
				"type": "פנימיים", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פנימיים_17"],
			},
			{
				"type": "חיצוניים", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חיצוניים_18"],
			},
			{
				"type": "והלך", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והלך_19", "והלך_21"],
			},
			{
				"type": "ובא", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ובא_20"],
			},
			{
				"type": "והלך", 
				"score": 1,
				"loc": { "x": -700, "y": 400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והלך_19", "והלך_21"],
			},
		],
		"ex_name": "kidushin_33a_bey_maschoota_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}