var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_90b_top.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
	},
	"parameters": {
		"numOfItems": 20,
		"placeholders": [
			{ "id": "period_1", "loc": { "x": 120, "y": -88 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_2", "loc": { "x": 127, "y": -52 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_3", "loc": { "x": -45, "y": -15 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_4", "loc": { "x": 364, "y": 21 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_5", "loc": { "x": 183, "y": 21 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_6", "loc": { "x": 29, "y": 21 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_7", "loc": { "x": 229, "y": 93 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_8", "loc": { "x": 88, "y": 129 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_9", "loc": { "x": 119, "y": 166 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_10", "loc": { "x": 21, "y": 166 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_11", "loc": { "x": 250, "y": 202 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_12", "loc": { "x": 230, "y": 202 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_13", "loc": { "x": 164, "y": 202 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_14", "loc": { "x": -48, "y": 202 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_15", "loc": { "x": -3, "y": 238 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_16", "loc": { "x": -70, "y": 238 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_17", "loc": { "x": 222, "y": 274 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_18", "loc": { "x": 131, "y": 274 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_19", "loc": { "x": 248, "y": -196 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_20", "loc": { "x": 42, "y": -196 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_10", "speakbegin_13"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_12", "speakend_18"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -760, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -640, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -790, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -610, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_10", "speakbegin_13"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_11"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_12", "speakend_18"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_10", "speakbegin_13"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_14", "colon_19"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_15", "hyphen_17"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -820, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_15", "hyphen_17"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_5", "speakend_12", "speakend_18"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -730, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_14", "colon_19"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -580, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1", "period_2", "period_3", "period_6", "period_7", "period_8", "period_9", "period_16", "period_20"],
			},
		],
		"ex_name": "babakama_90b_ze_haklal_vakol_lefi_kvodo_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}