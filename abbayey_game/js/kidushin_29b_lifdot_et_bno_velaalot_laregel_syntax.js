var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29b_top.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": -144, "y": -44 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_2", "loc": { "x": -11, "y": 100 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_3", "loc": { "x": -74, "y": 64 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_4", "loc": { "x": -278, "y": 64 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_5", "loc": { "x": 53, "y": 137 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_6", "loc": { "x": -278, "y": 137 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": 99, "y": 173 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_8", "loc": { "x": -159, "y": 173 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_9", "loc": { "x": -233, "y": 173 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": 53, "y": 209 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_11", "loc": { "x": 30, "y": 209 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_12", "loc": { "x": 107, "y": 28 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_13", "loc": { "x": 36, "y": -8 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_4"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_12"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_12"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_13"],
			},
		],
		"ex_name": "kidushin_29b_lifdot_et_bno_velaalot_laregel_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}