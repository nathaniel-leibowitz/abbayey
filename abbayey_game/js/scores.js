﻿

window.onload = ()=>{
};
	
var sh = [];

let pathBuilder = window.location.href;
let aUrl = new URL(pathBuilder);
let currentGroup = aUrl.searchParams.get("player_group");
var ref = firebase.database().ref("Score Board/" + currentGroup);
ref.once("value", function (groupNode) {
    groupNode.forEach(function (studentNode) {
        studentNode.forEach(function (exerciseNode) {
            exerciseNode.forEach(function (instance) {
                var data = instance.val();
                if (data.msFromEpoch != null)
                    sh.push({ name: data.student_name, date: data.date, time: data.time, msFromEpoch: data.msFromEpoch, group: data.group_name, exercise: data.ex_name, success: data.success });
            })
        })
    })

    var examMap = generateMapFromSnapshot(sh);
    var scores = generateScoresArrayFromMap(groups.get(currentGroup),examMap)
    fillExamsTable(scores);
}, function (error) {
    console.log("Error: " + error.code);
});


//actually, this step is redundant. The retrieval of the db, could have been easily altered to acheive the same result
function generateMapFromSnapshot(snapshot) {
	snapshot.sort(function (a, b) {
	    if (a.name === b.name) {
	        if (a.exercise === b.exercise)
	            return (a.msFromEpoch > b.msFromEpoch);
	        else return a.exercise.localeCompare(b.exercise);
	    }
	    else
	        return a.name.localeCompare(b.name);
	});


	let examMap = new Map();

	for (var i = 0; i < snapshot.length; i++) {
	    var ex = snapshot[i].exercise.slice(0, snapshot[i].exercise.lastIndexOf("_"));
	    var type = snapshot[i].exercise.substring(snapshot[i].exercise.lastIndexOf("_") + 1)
	    if (!examMap.has(snapshot[i].name)) {
	        let exercises = new Map();
	        exercises.set(ex, { translate: false, syntax: false });
	        examMap.set(snapshot[i].name, exercises)
	    }   
	    if (!examMap.get(snapshot[i].name).has(ex)) {
	        var res = new Object();
	        examMap.get(snapshot[i].name).set(ex, { translate: false, syntax: false });
	    }
	    if (snapshot[i].success) {
	        res = examMap.get(snapshot[i].name).get(ex);
	        if (type === "words")
	            res.translate = true;
	        if (type === "syntax")
	            res.syntax = true;
	        examMap.get(snapshot[i].name).set(ex, res);
	    }
	}

	return examMap;
}

function generateScoresArrayFromMap(map) {
    scores = [];
    map.forEach(function (studentMap, studentName) {
        scores = scores.concat(addStudentSores(studentName, studentMap));
    });
    return scores;
}

function generateScoresArrayFromMap(group, map) {
    scores = [];
    if (group == null)
        return;
    group.forEach(function (name) {
        scores = scores.concat(addStudentSores(name, map.get(name)));
    });
    return scores;
}

function addStudentSores(_name, map) {
    scores = [];
    var _score = 0;
    if (map != null) {
        map.forEach(function (ex) {
            if (ex.translate && ex.syntax)
                _score++;
        });
    }
    scores.push({ name: _name, score: _score });
    return scores;
}

function fillExamsTable(scores) {
    htmlString = scores.map(function (score) {
        return '<tr class=\"mytr\"><td class=\"mytd_scores\">' + score.name + '</td><td class=\"mytd_scores\">' + score.score + '</td></tr>';
    }).join('');
    htmlString = '<caption class=\"mycaption_scores\">הסוגיות שהושלמו ב' + currentGroup + '</caption><tr class=\"mytr_scores\"><th class=\"myth_scores\">שם</th><th class=\"myth_scores\">סוגיות</th></tr>' + htmlString;
    document.getElementById("exams").innerHTML = htmlString;
}