var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/ibarto_zkeina.png",
		"רוצה, צריכה, מבקשת": "רוצה, צריכה, מבקשת.text",
		"עשה": "עשה.text",
		"אחפש, אבדוק": "אחפש, אבדוק.text",
		"שיפה, שטוב": "שיפה, שטוב.text",
		"כמוך": "כמוך.text",
		"עזב אותה": "עזב אותה.text",
		"והלך": "והלך.text",
		"לארץ": "לארץ.text",
		"הלכה": "הלכה.text",
		"אחריו, בעקבותיו": "אחריו, בעקבותיו.text",
		"בא": "בא.text",
		"מעט, קצת": "מעט, קצת.text",
		"חיכה": "חיכה.text",
		"שמא": "שמא.text",
		"יש, יש מצב": "יש, יש מצב.text",
		"עד שכך וכך, מהון להון": "עד שכך וכך, מהון להון.text",
		"יצאתי": "יצאתי.text",
		"שבא": "שבא.text",
		"לפניו": "לפניו.text",
		"אחר כך": "אחר כך.text",
	},
	"parameters": {
		"numOfItems": 21,
		"placeholders": [
			{ "id": "רוצה, צריכה, מבקשת_1", "loc": { "x": 85, "y": -120 }, "rect": { "width": 95, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עשה_2", "loc": { "x": -174, "y": -120 }, "rect": { "width": 68, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רוצה, צריכה, מבקשת_3", "loc": { "x": -349, "y": -120 }, "rect": { "width": 95, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אחפש, אבדוק_4", "loc": { "x": 90, "y": -77 }, "rect": { "width": 80, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיפה, שטוב_5", "loc": { "x": -228, "y": -77 }, "rect": { "width": 104, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמוך_6", "loc": { "x": -323, "y": -77 }, "rect": { "width": 83, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עזב אותה_7", "loc": { "x": -452, "y": -77 }, "rect": { "width": 98, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והלך_8", "loc": { "x": 94, "y": -33 }, "rect": { "width": 68, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לארץ_9", "loc": { "x": -13, "y": -33 }, "rect": { "width": 113, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הלכה_10", "loc": { "x": -461, "y": -33 }, "rect": { "width": 80, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אחריו, בעקבותיו_11", "loc": { "x": 71, "y": 10 }, "rect": { "width": 131, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא_12", "loc": { "x": -42, "y": 10 }, "rect": { "width": 77, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מעט, קצת_13", "loc": { "x": 82, "y": 141 }, "rect": { "width": 107, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חיכה_14", "loc": { "x": -442, "y": 97 }, "rect": { "width": 98, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא_15", "loc": { "x": -342, "y": 228 }, "rect": { "width": 92, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יש, יש מצב_16", "loc": { "x": 97, "y": 315 }, "rect": { "width": 86, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עד שכך וכך, מהון להון_17", "loc": { "x": -407, "y": 315 }, "rect": { "width": 191, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יצאתי_18", "loc": { "x": -60, "y": 358 }, "rect": { "width": 65, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שבא_19", "loc": { "x": 322, "y": 358 }, "rect": { "width": 107, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפניו_20", "loc": { "x": -141, "y": 10 }, "rect": { "width": 104, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אחר כך_21", "loc": { "x": -24, "y": 141 }, "rect": { "width": 74, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "רוצה, צריכה, מבקשת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רוצה, צריכה, מבקשת_1", "רוצה, צריכה, מבקשת_3"],
			},
			{
				"type": "עשה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עשה_2"],
			},
			{
				"type": "רוצה, צריכה, מבקשת", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רוצה, צריכה, מבקשת_1", "רוצה, צריכה, מבקשת_3"],
			},
			{
				"type": "אחפש, אבדוק", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אחפש, אבדוק_4"],
			},
			{
				"type": "שיפה, שטוב", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיפה, שטוב_5"],
			},
			{
				"type": "כמוך", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמוך_6"],
			},
			{
				"type": "עזב אותה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עזב אותה_7"],
			},
			{
				"type": "והלך", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והלך_8"],
			},
			{
				"type": "לארץ", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לארץ_9"],
			},
			{
				"type": "הלכה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הלכה_10"],
			},
			{
				"type": "אחריו, בעקבותיו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אחריו, בעקבותיו_11"],
			},
			{
				"type": "בא", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא_12"],
			},
			{
				"type": "מעט, קצת", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מעט, קצת_13"],
			},
			{
				"type": "חיכה", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חיכה_14"],
			},
			{
				"type": "שמא", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא_15"],
			},
			{
				"type": "יש, יש מצב", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יש, יש מצב_16"],
			},
			{
				"type": "עד שכך וכך, מהון להון", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עד שכך וכך, מהון להון_17"],
			},
			{
				"type": "יצאתי", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יצאתי_18"],
			},
			{
				"type": "שבא", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שבא_19"],
			},
			{
				"type": "לפניו", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפניו_20"],
			},
			{
				"type": "אחר כך", 
				"score": 1,
				"loc": { "x": -700, "y": 400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אחר כך_21"],
			},
		],
		"ex_name": "kidushin_31b_asy_hava_ley_hahi_ima_zkeina_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}