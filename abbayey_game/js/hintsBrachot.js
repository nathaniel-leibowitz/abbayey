function addBrachotHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "brachot_13a_mitzvot_tzrichot_kavana_syntax":
            hints.push({ text: 'הבסיס לסוגיא זו היא הקביעה של רבה שמצוות אין צריכות כוונה', symbol: "" });
            hints.push({ text: 'הגמרא מקשה ממשנתינו על הקביעה של רבה. סמן היכן מסיים המקשן להתקיף את קושייתו', symbol: "qushia" });
            hints.push({ text: 'מצא היכן התרצן מסיים ליישב את תירוצו', symbol: "resolution" });
            hints.push({ text: 'הסוגיא ממשיכה ומקשה על התירוץ, סמן היכן מסיים המקשן להתקיף את קושייתו השנייה', symbol: "qushia" });
            hints.push({ text: 'ומצא היכן התרצן מסיים ליישב את תירוצו השני', symbol: "resolution" });
            hints.push({ text: 'אתגר: בשתי הקושיות נשאלות שאלות, אך באחת הקושיות זו שאלה רגילה', symbol: "question" });
            hints.push({ text: 'ובאחרת זו שאלה רטורית, תמיהה', symbol: "interrobang" });
            break;

        case "brachot_26a_vatikin_syntax":
            hints.push({ text: 'הסוגיא מצטטת ברייתא הסותרת את המשנה, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסופה את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'מיצאו היכן מסתיים התירוץ', symbol: "resolution" });
            hints.push({ text: 'הגמרא מביאה חיזוק לתירוץ, מקמו בסופו את סימן החיזוק', symbol: "assist" });
            break;

        case "brachot_26a_vetoo_lo_syntax":
            hints.push({ text: 'הסוגיא מצטטת ברייתא הסותרת את המשנה, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסופה את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'מיצאו היכן מסתיים התירוץ', symbol: "resolution" });
            hints.push({ text: 'הגמרא מביאה חיזוק לתירוץ, מקמו בסופו את סימן החיזוק', symbol: "assist" });
            hints.push({ text: 'אתגר: התירוץ בנוי כמשפט ייחוד, השתמשו בקו המפריד לחלק אותו לשניים', symbol: "hyphen" });
            break;

        case "brachot_26a_taa_velo_hitpalel_tashma_syntax":
            hints.push({ text: 'הגמרא מתלבטת, מקמו באפשרויות השונות את סימני השאלה', symbol: "question" });
            hints.push({ text: 'בהמשך הגמרא מביאה סיוע לאחת הדעות מברייתא. הקיפו את הברייתא', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסופה את סימן הסיוע', symbol: "assist" });
            break;

        case "brachot_26a_taa_velo_hitpalel_meytivey_syntax":
            hints.push({ text: 'הגמרא מקשה על דעת רבי יוחנן מקודם, ומביאה ברייתא סותרת, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'ומקם בסופה את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'הברייתא הוא מדרש הדורש פסוקים. הקיפו את הפסוקים', symbol: "biblequote" });
            hints.push({ text: 'אתרו היכן התרצן מסיים להשיב את תירוצו', symbol: "resolution" });
            hints.push({ text: 'לבסוף מובא חיזוק לתירוץ, מקם בסופו את סימן הסיוע', symbol: "assist" });
            hints.push({ text: 'אתגר: הסיוע מתמקד בשתי מילים, הקף אותם', symbol: "quotebegin" });
            break;

        case "brachot_30b_ein_omdin_syntax":
            hints.push({ text: 'המשנה מפרטת שלוש הנהגות בקשר לתפילה. הפרידו ביניהם בעזרת הנקודה', symbol: "period" });
            hints.push({ text: 'ההנחייה השלישית מורכבת משני משפטי תנאי: המקרה ואז הדין שלו. הפרידו בין המקרים לדינים בעזרת הקו המפריד ', symbol: "hyphen" });
            break;

        case "brachot_30b_mina_haney_miley_syntax":
            hints.push({ text: 'סוגיא מעט ארוכה, אך אם נפענח אותה שלב אחר שלב נבין אותה בקלות', symbol: "" });
            hints.push({ text: 'הסוגיא פותחת בשאלה מהו המקור מהתורה לדברי המשנה. סמנו את השאלה ב', symbol: "question" });
            hints.push({ text: 'רבי אליעזר מציע פסוק, הקיפוהו בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'כעת הסוגיא פותחת בסדרה של קושיות ותירוצים לגבי המקורות האפשריים למשנה', symbol: "" });
            hints.push({ text: 'מצאו היכן המקשן מסיים את קושייתו הראשונה וסמנו ב', symbol: "qushia" });
            hints.push({ text: 'רבי יוסי מתרץ ומציע מקור אחר, הקיפוהו בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'וסמנו היכן מסתיים תירוצו של רבי יוסי', symbol: "resolution" });
            hints.push({ text: 'הסוגיא חוזרת ומקשה ומתרצת, סמנו זאת בדיוק כמו שסימנתם את הקודם', symbol: "" });
            hints.push({ text: 'הסוגיא חוזרת ומקשה ומתרצת, סמנו זאת בדיוק כמו שסימנתם את הקודם', symbol: "" });
            break;

        case "brachot_30b_ka_badach_toova_raba_syntax":
            hints.push({ text: 'סיפור מעניין שמכיל דו שיח רב משמעות', symbol: "" });
            hints.push({ text: 'תחילו אתרו את המשפט שאומר אביי והקיפוהו ברמקול דלוק וכבוי', symbol: "speakbegin" });
            hints.push({ text: 'המשפט הזה מכיל בתוכו ציטוט של פסוק הקיפוהו בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'אתגר: שימו לב שאביי למעשה מקשה על רבה. סמנו זאת בעזרת אייקון הקושיא', symbol: "qushia" });
            hints.push({ text: 'הקיפו את תגובתו של רבה ברמקול דלוק וכבוי', symbol: "speakbegin" });
            hints.push({ text: 'אתגר: שימו לב שרבה למעשה מתרץ את קושייתו של אביי. סמנו זאת בעזרת אייקון התירוץ', symbol: "resolution" });
            break;

        case "brachot_30b_ka_badach_toova_zeira_syntax":
                hints.push({ text: 'סיפור רב משמעות שמאוד דומה לקודם', symbol: "" });
                hints.push({ text: 'תחילו אתרו את המשפט שאומר רבי ירמיה והקיפוהו ברמקול דלוק וכבוי', symbol: "speakbegin" });
                hints.push({ text: 'המשפט הזה מכיל בתוכו ציטוט של פסוק הקיפוהו בספר תורה', symbol: "biblequote" });
                hints.push({ text: 'אתגר: שימו לב שרבי ירמיה למעשה מקשה על רבי זירא. סמנו זאת בעזרת אייקון הקושיא', symbol: "qushia" });
                hints.push({ text: 'הקיפו את תגובתו של רבי זירא ברמקול דלוק וכבוי', symbol: "speakbegin" });
                hints.push({ text: 'אתגר: שימו לב שרבי זירא למעשה מתרץ את קושייתו של רבי ירמיה. סמנו זאת בעזרת אייקון התירוץ', symbol: "resolution" });
                break;
    }
    addHint(div, hints);
}
