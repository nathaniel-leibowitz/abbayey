var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "interrobang_1", "loc": { "x": -301, "y": 44 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_2", "loc": { "x": 50, "y": 76 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_3", "loc": { "x": -218, "y": 76 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_4", "loc": { "x": -470, "y": 76 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 19, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_5", "loc": { "x": -566, "y": 76 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_6", "loc": { "x": 332, "y": 108 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_7", "loc": { "x": 318, "y": 108 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_8", "loc": { "x": 79, "y": 108 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 19, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_9", "loc": { "x": -352, "y": 108 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 19, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_10", "loc": { "x": 489, "y": 140 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_11", "loc": { "x": 164, "y": 140 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_12", "loc": { "x": 11, "y": 140 }, "rect": { "width": 16, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_1"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_10", "hyphen_11"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_8", "period_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_10", "hyphen_11"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_6"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_7"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_8", "period_9"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_8", "period_9"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -670, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_10", "hyphen_11"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -760, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_10", "hyphen_11"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_12"],
			},
		],
		"ex_name": "brachot_26a_vetoo_lo_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 0.8,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}