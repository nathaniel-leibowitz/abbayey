var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/ibarto_zkeina.png",
		"ראה אותם": "ראה אותם.text",
		"האמנם, האם כך": "האמנם, האם כך.text",
		"והרי אמר": "והרי אמר.text",
		"אומנת שלו": "אומנת שלו.text",
		"היתה": "היתה.text",
	},
	"parameters": {
		"numOfItems": 5,
		"placeholders": [
			{ "id": "ראה אותם_1", "loc": { "x": -459, "y": -337 }, "rect": { "width": 86, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "האמנם, האם כך_2", "loc": { "x": -242, "y": -250 }, "rect": { "width": 59, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי אמר_3", "loc": { "x": -358, "y": -250 }, "rect": { "width": 110, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אומנת שלו_4", "loc": { "x": -279, "y": -207 }, "rect": { "width": 158, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היתה_5", "loc": { "x": -407, "y": -207 }, "rect": { "width": 74, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ראה אותם", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראה אותם_1"],
			},
			{
				"type": "האמנם, האם כך", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "האמנם, האם כך_2"],
			},
			{
				"type": "והרי אמר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי אמר_3"],
			},
			{
				"type": "אומנת שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אומנת שלו_4"],
			},
			{
				"type": "היתה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היתה_5"],
			},
		],
		"ex_name": "kidushin_31b_ashrey_me_shelo_hamaan_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}