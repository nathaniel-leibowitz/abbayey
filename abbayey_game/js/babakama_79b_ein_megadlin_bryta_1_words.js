var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_79b_bottom.png",
		"תנו רבנן, למדנו בברייתא": "תנו רבנן, למדנו בברייתא.text",
		"יערות, שאינם לגידולים חקלאיים": "יערות, שאינם לגידולים חקלאיים.text",
		"אזור מיושב עם גידולים חקלאיים": "אזור מיושב עם גידולים חקלאיים.text",
		"וברור מאליו": "וברור מאליו.text",
		"בסוריא מותר לגדל": "בסוריא מותר לגדל.text",
		"בחול מותר לגדל אפליו בישוב": "בחול מותר לגדל אפליו בישוב.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "תנו רבנן, למדנו בברייתא_1", "loc": { "x": 85, "y": 206 }, "rect": { "width": 38, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יערות, שאינם לגידולים חקלאיים_2", "loc": { "x": -407, "y": 206 }, "rect": { "width": 83, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אזור מיושב עם גידולים חקלאיים_3", "loc": { "x": 263, "y": 236 }, "rect": { "width": 62, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וברור מאליו_4", "loc": { "x": 139, "y": 236 }, "rect": { "width": 164, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בסוריא מותר לגדל_5", "loc": { "x": 412, "y": 236 }, "rect": { "width": 74, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בחול מותר לגדל אפליו בישוב_6", "loc": { "x": -21, "y": 236 }, "rect": { "width": 134, "height": 25 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "תנו רבנן, למדנו בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תנו רבנן, למדנו בברייתא_1"],
			},
			{
				"type": "יערות, שאינם לגידולים חקלאיים", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יערות, שאינם לגידולים חקלאיים_2"],
			},
			{
				"type": "אזור מיושב עם גידולים חקלאיים", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אזור מיושב עם גידולים חקלאיים_3"],
			},
			{
				"type": "וברור מאליו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וברור מאליו_4"],
			},
			{
				"type": "בסוריא מותר לגדל", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בסוריא מותר לגדל_5"],
			},
			{
				"type": "בחול מותר לגדל אפליו בישוב", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בחול מותר לגדל אפליו בישוב_6"],
			},
		],
		"ex_name": "babakama_79b_ein_megadlin_bryta_1_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}