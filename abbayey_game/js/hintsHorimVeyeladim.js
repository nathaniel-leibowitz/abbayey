function addHorimVeyeladimHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        /*case "kidushin_29a_kol_mitzvot_haben_ul_haav_syntax":
            hints.push({ text: 'המשנה מגדירה 5 קטגוריות שונות של מצוות. הפרידו ביניהם בעזרת הנקודות', symbol: "period" });
            hints.push({ text: 'כל קטגוריה מנוסחת כמשפט תנאי: המקרה ואז הדין שלו. הפרידו בין המקרה לדין בעזרת הקו המפריד', symbol: "hyphen" });
            break;

        case "kidushin_29a_muy_kol_mitzvot_haben_ul_haav_syntax":
            hints.push({ text: 'הסוגיא מצטטת ביטוי מתוך המשנה. הקיפוהו', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא פותחת בשאלה לבירור הביטוי. מקמו שם את סימן השאלה', symbol: "question" });
            hints.push({ text: 'הסוגיא מציעה פירוש ראשוני לביטוי אך מקשה על זה מתוך ברייתא. הקיפו את הברייתא ב', symbol: "quotebegin" });
            hints.push({ text: 'הברייתא הוא מדרש הלכה, המצטט מילים מתוך פסוק. הקיפו את חלקי הפסוק ב', symbol: "biblequote" });
            hints.push({ text: 'אתרו היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'התרצן מציע פירוש או ניסוח אחר לביטוי מהמשנה, הקיפוהו ב', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסוף תירוצו', symbol: "resolution" });
            hints.push({ text: 'אתגר: חיזרו לקושיא ואתרו את השאלה הרטורית ששואל המקשן בתמיהה, ומקמו שם', symbol: "interrobang" });
            break;

        case "kidushin_29a_listut_salka_daatach_syntax":
            hints.push({ text: 'הסוגיא מצטטת ברייתא, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'בברייתא שתי דעות, הפרידו ביניהם בנקודה', symbol: "period" });
            hints.push({ text: 'המקשן מקשה על הניסוח בברייתא, מקמו את סימן הקושיא בסוף קושייתו', symbol: "qushia" });
            hints.push({ text: 'סמנו את סוף דברי התרצן ב', symbol: "resolution" });
            hints.push({ text: 'אתגר: חיזרו לקושיא ואתרו את השאלה הרטורית ששואל המקשן בתמיהה, ומקמו שם', symbol: "interrobang" });
            break;

        case "kidushin_29a_lemulu_minalan_syntax":
            hints.push({ text: 'הסוגיא פותחת בשאלה לבירור מצוות ברית מילה, מקמו אחריה', symbol: "question" });
            hints.push({ text: 'במהלך הבירור הגמרא מצטטת 3 פסוקים הקיפו כל אחד ב', symbol: "biblequote" });
            hints.push({ text: 'פרטי המצוה מנוסחים כמשפט תנאי. הפרידו בין המקרה לבין הדין שלו בעזרת הקו המפריד', symbol: "hyphen" });
            break;

        case "kidushin_29a_lemulu_ihi_minalan_delo_syntax":
            hints.push({ text: 'הסוגיא ממשיכה לברר את פרטי מצוות ברית מילה. אתרו את השאלה הראשונה', symbol: "question" });
            hints.push({ text: 'התשובה מתבססת על פסוק, הקיפוהו', symbol: "biblequote" });
            hints.push({ text: 'ומקמו נקודה בסוף התשובה', symbol: "period" });
            hints.push({ text: 'הסוגיא ממשיכה לברר את פרטי מצוות ברית מילה. אתרו את השאלה השנייה', symbol: "question" });
            hints.push({ text: 'התשובה מסתמכת על ברייתא, הקיפוה ב', symbol: "quotebegin" });
            hints.push({ text: 'התמקדו בברייתא, היא מצטטת שני פסוקים. הקיפו כל אחד ב', symbol: "biblequote" });
            hints.push({ text: 'אתגר: פסקו את הברייתא בעזרת הנקודותיים', symbol: "colon" });
            break;

        case "kidushin_29a_lifdoto_minalan_syntax":
            hints.push({ text: 'סוגיא ארוכה וקשה להבנה, אך קלה לקריאה! היא מבררת את פרטי מצוות הפדיון', symbol: "" });
            hints.push({ text: 'מיצאו את ארבע השאלות שהסוגיא מבררת לגבי המצווה, ומקמו בסוף כל שאלה', symbol: "question" });
            hints.push({ text: 'התשובות מתבססות על פסוקים, הקיפו כל פסוק ב', symbol: "biblequote" });
            hints.push({ text: 'בסיומי המשפטים מקמו נקודות', symbol: "period" });
            hints.push({ text: 'התשובות מנוסחות כמשפט תנאי. הפרידו בין המקרה לבין הדין שלו בעזרת הקו המפריד', symbol: "hyphen" });
            break;

        case "kidushin_29b_lifdot_et_bno_velaalot_laregel_syntax":
            hints.push({ text: 'הקיפו את הברייתא שאותה מנסה הסוגיא לברר ב', symbol: "quotebegin" });
            hints.push({ text: 'בברייתא שתי דעות, הפרידו ביניהם בעזרת הנקודה', symbol: "period" });
            hints.push({ text: 'אתגר: רבי יהודה מנגיד בין שתי מצוות, הפרידו בין חלקי ההנגדה בעזרת הקו', symbol: "hyphen" });
            hints.push({ text: 'הגמרא מקשה על הדעה הראשונה. אתרו היכן מסיים המקשן את קושייתו וסמנו', symbol: "qushia" });
            hints.push({ text: 'התרצן מתרץ בעזרת שני פסוקים, הקיפם ב', symbol: "biblequote" });
            hints.push({ text: 'וסמנו היכן הוא מסיים את תירוצו', symbol: "resolution" });
            break;

        case "kidushin_29b_hamisha_banim_mehamesh_nashim_syntax":
            hints.push({ text: 'בסיום סוגיא זו, ניתקל בתבנית לוגית טריקית. אך תחילה הקיפו את הברייתא שאותה מנסה הסוגיא לברר ב', symbol: "quotebegin" });
            hints.push({ text: 'הברייתא דנה בשאלה, סמנוה ב', symbol: "question" });
            hints.push({ text: 'הברייתא משיבה בעזרת שני פסוקים, הקיפום', symbol: "biblequote" });
            hints.push({ text: 'הסוגיא מקשה על הברייתא. סמנו היכן המקשן מסיים את קושייתו', symbol: "qushia" });
            hints.push({ text: 'והנה הטריק: התרצן נעזר בטענת סרק מופרכת ושגויה, הקיפוה ב', symbol: "wrongly" });
            hints.push({ text: 'סמנו היכן התרצן מסיים את תירוצו', symbol: "resolution" });
            hints.push({ text: 'אתגר: מקמו נקודותיים גם בתדברי המקשן וגם בדברי התרצן', symbol: "colon" });
            break;*/

        case "bababatra_20b_hanut_shebahazer_syntax":
            hints.push({ text: 'המשנה עוסקת בסכסוך שכנים ומצטטת שתי טענות של אחד השכנים כלפי דייר שמרעיש', symbol: "" });
            hints.push({ text: 'מצאו את הטענות והקיפו כל אחת מהן ברמקולים', symbol: "speakbegin" });
            break;

        case "bababatra_20b_my_shna_resha_syntax":
            hints.push({ text: 'הגמרא מקשה על סתירה בין תחילת המשנה לסופה. מצאו היכן המקשן מסיים את קושייתו', symbol: "qushia" });
            hints.push({ text: 'מצאו היכן אביי מסיים את תירוצו וסמנו', symbol: "resolution" });
            hints.push({ text: 'מצאו היכן רבא מסיים את קושייתו על תירוצו של אביי', symbol: "qushia" });
            hints.push({ text: 'ומצאו היכן הוא מסיים את התירוץ שלו לקושיית הגמרא', symbol: "resolution" });
            hints.push({ text: 'אתגר: בהתקפה של רבא על אביי, הוא מביא ניסוח חלופי למשנה. הקיפו את הניסוח בגרשיים', symbol: "quotebegin" });
            break;

    }
    addHint(div, hints);
}
