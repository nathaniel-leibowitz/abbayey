var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_30b_bottom.png",
		"מהיכן, מאיפה לומדים": "מהיכן, מאיפה לומדים.text",
		"שהתורה אומרת": "שהתורה אומרת.text",
		"מכאן, מפסוק זה": "מכאן, מפסוק זה.text",
		"שמא, אולי": "שמא, אולי.text",
		"הרבה": "הרבה.text",
		"ואחר כך ": "ואחר כך .text",
		"מתפלל": "מתפלל.text",
		"כמו זה, כמו הסיפור": "כמו זה, כמו הסיפור.text",
		"מניין לך": "מניין לך.text",
		"מצער עצמו בתפילה, מתאמץ": "מצער עצמו בתפילה, מתאמץ.text",
		"שונה, יוצא דופן": "שונה, יוצא דופן.text",
		"מרת לב, עצובה": "מרת לב, עצובה.text",
		"אלו הדברים, דין זה": "אלו הדברים, דין זה.text",
	},
	"parameters": {
		"numOfItems": 20,
		"placeholders": [
			{ "id": "מהיכן, מאיפה לומדים_1", "loc": { "x": -495, "y": -37 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהתורה אומרת_2", "loc": { "x": 113, "y": -1 }, "rect": { "width": 131, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מכאן, מפסוק זה_3", "loc": { "x": -200, "y": 35 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מכאן, מפסוק זה_4", "loc": { "x": -358, "y": 108 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מכאן, מפסוק זה_5", "loc": { "x": 165, "y": 216 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, אולי_6", "loc": { "x": -277, "y": -1 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, אולי_7", "loc": { "x": -265, "y": 71 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, אולי_8", "loc": { "x": -319, "y": 144 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרבה_9", "loc": { "x": 198, "y": 35 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרבה_10", "loc": { "x": 83, "y": 108 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואחר כך _11", "loc": { "x": -298, "y": 180 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מתפלל_12", "loc": { "x": -368, "y": 180 }, "rect": { "width": 59, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמו זה, כמו הסיפור_13", "loc": { "x": 150, "y": 180 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מניין לך_14", "loc": { "x": -196, "y": -1 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מניין לך_15", "loc": { "x": -177, "y": 71 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מניין לך_16", "loc": { "x": -238, "y": 144 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצער עצמו בתפילה, מתאמץ_17", "loc": { "x": 256, "y": 108 }, "rect": { "width": 260, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שונה, יוצא דופן_18", "loc": { "x": -416, "y": -1 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מרת לב, עצובה_19", "loc": { "x": 313, "y": 35 }, "rect": { "width": 143, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אלו הדברים, דין זה_20", "loc": { "x": 356, "y": -1 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מהיכן, מאיפה לומדים", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהיכן, מאיפה לומדים_1"],
			},
			{
				"type": "שהתורה אומרת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהתורה אומרת_2"],
			},
			{
				"type": "מכאן, מפסוק זה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכאן, מפסוק זה_3", "מכאן, מפסוק זה_4", "מכאן, מפסוק זה_5"],
			},
			{
				"type": "מכאן, מפסוק זה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכאן, מפסוק זה_3", "מכאן, מפסוק זה_4", "מכאן, מפסוק זה_5"],
			},
			{
				"type": "מכאן, מפסוק זה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכאן, מפסוק זה_3", "מכאן, מפסוק זה_4", "מכאן, מפסוק זה_5"],
			},
			{
				"type": "שמא, אולי", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, אולי_6", "שמא, אולי_7", "שמא, אולי_8"],
			},
			{
				"type": "שמא, אולי", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, אולי_6", "שמא, אולי_7", "שמא, אולי_8"],
			},
			{
				"type": "שמא, אולי", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, אולי_6", "שמא, אולי_7", "שמא, אולי_8"],
			},
			{
				"type": "הרבה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרבה_9", "הרבה_10"],
			},
			{
				"type": "הרבה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרבה_9", "הרבה_10"],
			},
			{
				"type": "ואחר כך ", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואחר כך _11"],
			},
			{
				"type": "מתפלל", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מתפלל_12"],
			},
			{
				"type": "כמו זה, כמו הסיפור", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמו זה, כמו הסיפור_13"],
			},
			{
				"type": "מניין לך", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מניין לך_14", "מניין לך_15", "מניין לך_16"],
			},
			{
				"type": "מניין לך", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מניין לך_14", "מניין לך_15", "מניין לך_16"],
			},
			{
				"type": "מניין לך", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מניין לך_14", "מניין לך_15", "מניין לך_16"],
			},
			{
				"type": "מצער עצמו בתפילה, מתאמץ", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצער עצמו בתפילה, מתאמץ_17"],
			},
			{
				"type": "שונה, יוצא דופן", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שונה, יוצא דופן_18"],
			},
			{
				"type": "מרת לב, עצובה", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מרת לב, עצובה_19"],
			},
			{
				"type": "אלו הדברים, דין זה", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלו הדברים, דין זה_20"],
			},
		],
		"ex_name": "brachot_30b_mina_haney_miley_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}