var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama59b_bottom.png",
		"עורם את תבואתו שלו": "עורם את תבואתו שלו.text",
		"המגדיש חייב לפצות את החבר": "המגדיש חייב לפצות את החבר.text",
		"בהמת החבר ניזוקה מהגדיש": "בהמת החבר ניזוקה מהגדיש.text",
		"החבר חייב לפצות את המגדיש": "החבר חייב לפצות את המגדיש.text",
		"החבר אינו חייב לפצות את המגדיש": "החבר אינו חייב לפצות את המגדיש.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "עורם את תבואתו שלו_1", "loc": { "x": -145, "y": -24 }, "rect": { "width": 101, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עורם את תבואתו שלו_2", "loc": { "x": 240, "y": 48 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "המגדיש חייב לפצות את החבר_3", "loc": { "x": 387, "y": 48 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בהמת החבר ניזוקה מהגדיש_4", "loc": { "x": -244, "y": 12 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החבר חייב לפצות את המגדיש_5", "loc": { "x": -104, "y": 48 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החבר אינו חייב לפצות את המגדיש_6", "loc": { "x": -108, "y": 12 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "עורם את תבואתו שלו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עורם את תבואתו שלו_1", "עורם את תבואתו שלו_2"],
			},
			{
				"type": "עורם את תבואתו שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עורם את תבואתו שלו_1", "עורם את תבואתו שלו_2"],
			},
			{
				"type": "המגדיש חייב לפצות את החבר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "המגדיש חייב לפצות את החבר_3"],
			},
			{
				"type": "בהמת החבר ניזוקה מהגדיש", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בהמת החבר ניזוקה מהגדיש_4"],
			},
			{
				"type": "החבר חייב לפצות את המגדיש", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החבר חייב לפצות את המגדיש_5"],
			},
			{
				"type": "החבר אינו חייב לפצות את המגדיש", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החבר אינו חייב לפצות את המגדיש_6"],
			},
		],
		"ex_name": "babakama_59b_hamagdish_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}