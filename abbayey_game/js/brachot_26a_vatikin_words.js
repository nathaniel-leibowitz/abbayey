var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"מקשים מברייתא סותרת": "מקשים מברייתא סותרת.text",
		"יצמיד": "יצמיד.text",
		"שמונה עשרה": "שמונה עשרה.text",
		"חסידים משקימי הקום": "חסידים משקימי הקום.text",
		"ברכת גאל ישראל": "ברכת גאל ישראל.text",
		"שאמר רבי": "שאמר רבי.text",
		"אמר את הברייתא": "אמר את הברייתא.text",
		"את קריאת שמע": "את קריאת שמע.text",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "מקשים מברייתא סותרת_1", "loc": { "x": -3, "y": -8 }, "rect": { "width": 80, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יצמיד_2", "loc": { "x": -439, "y": -8 }, "rect": { "width": 86, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמונה עשרה_3", "loc": { "x": 190, "y": 23 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חסידים משקימי הקום_4", "loc": { "x": -270, "y": 23 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חסידים משקימי הקום_5", "loc": { "x": -488, "y": 23 }, "rect": { "width": 65, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ברכת גאל ישראל_6", "loc": { "x": -528, "y": -8 }, "rect": { "width": 68, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאמר רבי_7", "loc": { "x": -361, "y": 23 }, "rect": { "width": 59, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אמר את הברייתא_8", "loc": { "x": -130, "y": 23 }, "rect": { "width": 53, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "את קריאת שמע_9", "loc": { "x": 121, "y": 55 }, "rect": { "width": 62, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מקשים מברייתא סותרת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקשים מברייתא סותרת_1"],
			},
			{
				"type": "יצמיד", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יצמיד_2"],
			},
			{
				"type": "שמונה עשרה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמונה עשרה_3"],
			},
			{
				"type": "חסידים משקימי הקום", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חסידים משקימי הקום_4", "חסידים משקימי הקום_5"],
			},
			{
				"type": "חסידים משקימי הקום", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חסידים משקימי הקום_4", "חסידים משקימי הקום_5"],
			},
			{
				"type": "ברכת גאל ישראל", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברכת גאל ישראל_6"],
			},
			{
				"type": "שאמר רבי", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאמר רבי_7"],
			},
			{
				"type": "אמר את הברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אמר את הברייתא_8"],
			},
			{
				"type": "את קריאת שמע", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "את קריאת שמע_9"],
			},
		],
		"ex_name": "brachot_26a_vatikin_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}