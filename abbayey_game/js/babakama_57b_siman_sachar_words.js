var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_57b_middle.png",
		"שומר שכר": "שומר שכר.text",
		"נובע מכך": "נובע מכך.text",
		"שאם": "שאם.text",
		"רצה, ביקש": "רצה, ביקש.text",
		"את עצמו": "את עצמו.text",
		"באיזה מקרה בדובר, היכי דמי": "באיזה מקרה בדובר, היכי דמי.text",
		"נובע מכאן, שמע מינה": "נובע מכאן, שמע מינה.text",
		"האם": "האם.text",
		"דומה": "דומה.text",
		"שמא": "שמא.text",
		"הוא סובר": "הוא סובר.text",
		"אם תרצה תאמר, אפשרות אחרת": "אם תרצה תאמר, אפשרות אחרת.text",
		"ומתוך שכתוב בברייתא": "ומתוך שכתוב בברייתא.text",
		"וכתוב בברייתא": "וכתוב בברייתא.text",
		"כאן": "כאן.text",
		"התגלה, התברר": "התגלה, התברר.text",
	},
	"parameters": {
		"numOfItems": 19,
		"placeholders": [
			{ "id": "שומר שכר_1", "loc": { "x": -60, "y": -169 }, "rect": { "width": 143, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שומר שכר_2", "loc": { "x": -151, "y": 48 }, "rect": { "width": 149, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נובע מכך_3", "loc": { "x": -206, "y": -133 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאם_4", "loc": { "x": -279, "y": -133 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "רצה, ביקש_5", "loc": { "x": -340, "y": -133 }, "rect": { "width": 44, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "את עצמו_6", "loc": { "x": -8, "y": -96 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באיזה מקרה בדובר, היכי דמי_7", "loc": { "x": -205, "y": -96 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נובע מכאן, שמע מינה_8", "loc": { "x": 132, "y": 12 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "האם_9", "loc": { "x": -266, "y": 12 }, "rect": { "width": 29, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דומה_10", "loc": { "x": -258, "y": 48 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא_11", "loc": { "x": -326, "y": 48 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא סובר_12", "loc": { "x": 61, "y": 85 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דומה_13", "loc": { "x": -338, "y": 85 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם תרצה תאמר, אפשרות אחרת_14", "loc": { "x": 77, "y": 121 }, "rect": { "width": 167, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דומה_15", "loc": { "x": -161, "y": -169 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ומתוך שכתוב בברייתא_16", "loc": { "x": -241, "y": -169 }, "rect": { "width": 101, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכתוב בברייתא_17", "loc": { "x": -230, "y": -60 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן_18", "loc": { "x": -333, "y": 193 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התגלה, התברר_19", "loc": { "x": 122, "y": 266 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שומר שכר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שומר שכר_1", "שומר שכר_2"],
			},
			{
				"type": "שומר שכר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שומר שכר_1", "שומר שכר_2"],
			},
			{
				"type": "נובע מכך", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נובע מכך_3"],
			},
			{
				"type": "שאם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאם_4"],
			},
			{
				"type": "רצה, ביקש", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רצה, ביקש_5"],
			},
			{
				"type": "את עצמו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "את עצמו_6"],
			},
			{
				"type": "באיזה מקרה בדובר, היכי דמי", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באיזה מקרה בדובר, היכי דמי_7"],
			},
			{
				"type": "נובע מכאן, שמע מינה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נובע מכאן, שמע מינה_8"],
			},
			{
				"type": "האם", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "האם_9"],
			},
			{
				"type": "דומה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דומה_10", "דומה_13", "דומה_15"],
			},
			{
				"type": "שמא", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא_11"],
			},
			{
				"type": "הוא סובר", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא סובר_12"],
			},
			{
				"type": "דומה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דומה_10", "דומה_13", "דומה_15"],
			},
			{
				"type": "אם תרצה תאמר, אפשרות אחרת", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם תרצה תאמר, אפשרות אחרת_14"],
			},
			{
				"type": "דומה", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דומה_10", "דומה_13", "דומה_15"],
			},
			{
				"type": "ומתוך שכתוב בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ומתוך שכתוב בברייתא_16"],
			},
			{
				"type": "וכתוב בברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכתוב בברייתא_17"],
			},
			{
				"type": "כאן", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן_18"],
			},
			{
				"type": "התגלה, התברר", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התגלה, התברר_19"],
			},
		],
		"ex_name": "babakama_57b_siman_sachar_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}