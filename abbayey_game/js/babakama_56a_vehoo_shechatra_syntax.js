var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56a_bottom.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"wrongly": "storage/miniGames/gemara/syntax/wrongly.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "question_1", "loc": { "x": -387, "y": 4 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_2", "loc": { "x": 82, "y": 40 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 22, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_3", "loc": { "x": -39, "y": 40 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_4", "loc": { "x": -305, "y": 40 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_5", "loc": { "x": -321, "y": 76 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_6", "loc": { "x": 357, "y": 112 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_7", "loc": { "x": -391, "y": 112 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_8", "loc": { "x": -133, "y": 148 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_9", "loc": { "x": -66, "y": 185 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_10", "loc": { "x": -168, "y": 221 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_11", "loc": { "x": -72, "y": 257 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_12", "loc": { "x": -387, "y": 257 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_13", "loc": { "x": 296, "y": 330 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_14", "loc": { "x": -210, "y": 330 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_15", "loc": { "x": 290, "y": 366 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_16", "loc": { "x": 117, "y": 366 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_17", "loc": { "x": 21, "y": 402 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_18", "loc": { "x": 0, "y": 402 }, "rect": { "width": 19, "height": 33 }, "isReused": false, "offsetX": 0, "offsetY": 11, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_3", "question_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_2"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_3", "question_15"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_4"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_5", "qushia_7", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_6", "resolution_8", "resolution_18"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_5", "qushia_7", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_6", "resolution_8", "resolution_18"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_5", "qushia_7", "qushia_9"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_10"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_11"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_12"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -700, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_13", "wrongly_14"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -730, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_13", "wrongly_14"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -670, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_3", "question_15"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_16"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_17"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -670, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_6", "resolution_8", "resolution_18"],
			},
		],
		"ex_name": "babakama_56a_vehoo_shechatra_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 0.85,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}