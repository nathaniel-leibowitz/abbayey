var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/hanisrafim_baniskalim.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
	},
	"parameters": {
		"numOfItems": 17,
		"placeholders": [
			{ "id": "speakbegin_1", "loc": { "x": -12, "y": -266 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_2", "loc": { "x": 154, "y": -136 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_3", "loc": { "x": 126, "y": -353 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_4", "loc": { "x": -254, "y": -310 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_5", "loc": { "x": -197, "y": -179 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_6", "loc": { "x": 177, "y": -136 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_7", "loc": { "x": 87, "y": -136 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_8", "loc": { "x": -217, "y": -136 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_9", "loc": { "x": -327, "y": -92 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_10", "loc": { "x": 104, "y": -5 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_11", "loc": { "x": 22, "y": -5 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_12", "loc": { "x": 176, "y": 82 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_13", "loc": { "x": 156, "y": 38 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_14", "loc": { "x": 200, "y": 82 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_15", "loc": { "x": -89, "y": -223 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_16", "loc": { "x": -90, "y": -49 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_17", "loc": { "x": -184, "y": 38 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_1", "speakbegin_7", "speakbegin_11"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_2", "speakend_10", "speakend_12"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_3", "quotebegin_5", "quotebegin_8", "quotebegin_13"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_6", "quoteend_9", "quoteend_14"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_3", "quotebegin_5", "quotebegin_8", "quotebegin_13"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_6", "quoteend_9", "quoteend_14"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_1", "speakbegin_7", "speakbegin_11"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -670, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_3", "quotebegin_5", "quotebegin_8", "quotebegin_13"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -670, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_6", "quoteend_9", "quoteend_14"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_2", "speakend_10", "speakend_12"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_1", "speakbegin_7", "speakbegin_11"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_2", "speakend_10", "speakend_12"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -760, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_3", "quotebegin_5", "quotebegin_8", "quotebegin_13"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -760, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_6", "quoteend_9", "quoteend_14"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_15", "question_16"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_15", "question_16"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_17"],
			},
		],
		"ex_name": "kidushin_32a_hanisrafim_baniskalim_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}