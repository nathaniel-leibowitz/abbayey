﻿var mGameComponent;
var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDebugMode": false,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
        "gift1": "storage/miniGames/christmas-presents/gifts/gift1.png",
        "gift2": "storage/miniGames/christmas-presents/gifts/gift2.png",
        "gift3": "storage/miniGames/christmas-presents/gifts/gift3.png",
        "gift4": "storage/miniGames/christmas-presents/gifts/gift4.png",
        "gift5": "storage/miniGames/christmas-presents/gifts/gift5.png",
        "gift6": "storage/miniGames/christmas-presents/gifts/gift6.png",
        "gift7": "storage/miniGames/christmas-presents/gifts/gift7.png",

        "container_1": "storage/miniGames/christmas-presents/spots/spot1.png",
        "container_1_json": "storage/miniGames/christmas-presents/spots/spot1.json",
        "container_2": "storage/miniGames/christmas-presents/spots/spot2.png",
        "container_2_json": "storage/miniGames/christmas-presents/spots/spot2.json",
        "container_3": "storage/miniGames/christmas-presents/spots/spot3.png",
        "container_3_json": "storage/miniGames/christmas-presents/spots/spot3.json",
        "container_4": "storage/miniGames/christmas-presents/spots/spot4.png",
        "container_4_json": "storage/miniGames/christmas-presents/spots/spot4.json",
        "container_5": "storage/miniGames/christmas-presents/spots/spot5.png",
        "container_5_json": "storage/miniGames/christmas-presents/spots/spot5.json",
        "container_6": "storage/miniGames/christmas-presents/spots/spot6.png",
        "container_6_json": "storage/miniGames/christmas-presents/spots/spot6.json",
        "container_7": "storage/miniGames/christmas-presents/spots/spot7.png",
        "container_7_json": "storage/miniGames/christmas-presents/spots/spot7.json",


        "backGround": "storage/miniGames/christmas-presents/background.jpg",
        //"header": "storage/miniGames/christmas-presents/header.jpg",

        "timer_bar": "storage/miniGames/christmas-presents/timer_bar.png",
        "timer_bkg": "storage/miniGames/christmas-presents/timer_bkg.png",
        "timer_icon": "storage/miniGames/christmas-presents/timer_icon.png",

        "fail_screen": "storage/miniGames/christmas-presents/fail.png",
        "start_screen": "storage/miniGames/christmas-presents/open.png",
        "start_btn": "storage/miniGames/christmas-presents/start_btn.png",
        "success_screen": "storage/miniGames/christmas-presents/success.png"
    },
    "parameters": {
        "showVersion": true,
        "pointsNormalizationFunction": "function(gameScore, gameParameters) { return Math.round((gameScore / gameParameters.numOfItems) * 50); }",
        "numOfItems": 7,
        "time": 18,
        "showScore": false,
        "isPlaceholderOnTop": false,
        "startButtonLocation": { "x": 0, "y": 320 },
        "timerYPosition": 50,
        "placeholders": [
            { "id": "placeholder_0", "loc": { "x": 5000, "y": 5000 }, "rect": { "width": 1, "height": 1 }, "isReused": false },
            { "id": "placeholder_1", "loc": { "x": 160, "y": -108 }, "rect": { "width": 212, "height": 296 }, "isReused": false },
            { "id": "placeholder_2", "loc": { "x": -200, "y": -137 }, "rect": { "width": 253, "height": 62 }, "isReused": false },
            { "id": "placeholder_3", "loc": { "x": -15, "y": -200 }, "rect": { "width": 87, "height": 98 }, "isReused": false },
            { "id": "placeholder_4", "loc": { "x": -144, "y": 1 }, "rect": { "width": 338, "height": 65 }, "isReused": false },
            { "id": "placeholder_5", "loc": { "x": 225, "y": -55 }, "rect": { "width": 142, "height": 188 }, "isReused": false },
            { "id": "placeholder_6", "loc": { "x": -177, "y": -68 }, "rect": { "width": 207, "height": 74 }, "isReused": false },
            { "id": "placeholder_7", "loc": { "x": -10, "y": -91 }, "rect": { "width": 127, "height": 120 }, "isReused": false }
        ],
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
        "containers": [
            {
                "type": "container_1",
                "score": 1,
                "placeholderId": "placeholder_1",
                "removeOnHit": true
            },
            {
                "type": "container_2",
                "score": 1,
                "placeholderId": "placeholder_2",
                "removeOnHit": true
            },
            {
                "type": "container_3",
                "score": 1,
                "placeholderId": "placeholder_3",
                "removeOnHit": true
            }
            ,
            {
                "type": "container_4",
                "score": 1,
                "placeholderId": "placeholder_4",
                "removeOnHit": true
            },
            {
                "type": "container_5",
                "score": 1,
                "placeholderId": "placeholder_5",
                "removeOnHit": true
            },
            {
                "type": "container_6",
                "score": 1,
                "placeholderId": "placeholder_6",
                "removeOnHit": true
            },
            {
                "type": "container_7",
                "score": 1,
                "placeholderId": "placeholder_7",
                "removeOnHit": true
            }
        ],

        "items": [
            {
                "type": "gift1",
                "score": 1,
                "loc": { "x": -240, "y": 290 },
                "placeholderIds": ["placeholder_1"]
            },
            {
                "type": "gift2",
                "score": 1,
                "loc": { "x": -90, "y": 300 },
                "placeholderIds": ["placeholder_2"]
            },
            {
                "type": "gift3",
                "score": 1,
                "loc": { "x": 305, "y": 230 },
                "placeholderIds": ["placeholder_3"]
            },
            {
                "type": "gift4",
                "score": 1,
                "loc": { "x": 60, "y": 170 },
                "placeholderIds": ["placeholder_4"]
            },
            {
                "type": "gift5",
                "score": 1,
                "loc": { "x": 140, "y": 340 },
                "placeholderIds": ["placeholder_5"]
            },
            {
                "type": "gift6",
                "score": 1,
                "loc": { "x": -80, "y": 390 },
                "placeholderIds": ["placeholder_6"]
            },
            {
                "type": "gift7",
                "score": 1,
                "loc": { "x": 305, "y": 370 },
                "placeholderIds": ["placeholder_7"]
            }
        ]
    }
}
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
    
}

function startGame(){
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
    setTimeout(startGame, 50000)
};
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}