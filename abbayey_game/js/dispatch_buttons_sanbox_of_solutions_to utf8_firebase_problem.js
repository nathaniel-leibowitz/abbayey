﻿
	window.onload = ()=>{
        let aUrl = new URL(document.location.href);
        let sugiyaHebrewName = aUrl.searchParams.get("sugiya_hebrew_name");
        document.getElementById("sugiya_hebrew_name").value = sugiyaHebrewName;
        //document.getElementById("syntax_button").disabled = false;
    };
	
	if( document.readyState !== 'loading' ) {
	    document.getElementById("syntax_button").disabled = false;
	}
	else {
	    document.addEventListener('DOMContentLoaded', function () {
	        document.getElementById("syntax_button").disabled = false;
	    })};

	function  onClick(pExerciseType) {
		let pathBuilder = window.location.href;
		let aUrl = new URL(pathBuilder);
		let sugiya = aUrl.searchParams.get("sugiya_file_name");
		let sugiyaFullFileName = sugiya + "_" + pExerciseType;
		let student = aUrl.searchParams.get("player_name");
		let currentGroup = aUrl.searchParams.get("player_group");
		if (student != "נתנאל ליבוביץ")
		    console.log('name is ' + student);
		
		var aDate = new Date();
        var aDateStr = aDate.toLocaleDateString() + "    " + aDate.toLocaleTimeString();
		var msEpoch = aDate.getTime();
        var aDataJson = {
                    student_name: "nathaniel leibowitz",
                    ex_name: sugiyaFullFileName,
                    group_name: currentGroup,
                    date: aDateStr,
					msFromEpoch: msEpoch
        };
        if (aDataJson.student_name != "נתנאל ליבוביץ")
            console.log('name is ' + aDataJson.student_name);
        aDataJson.student_name = toUTF8Array(aDataJson.student_name);

		firebase.database().ref("Entrance").child(aDate.getTime().toString()).set(aDataJson, function () {
                });
		
		pathBuilder = pathBuilder.replace(sugiya, "sugiya_dispatcher");
		pathBuilder = pathBuilder.replace(sugiya, sugiyaFullFileName);
		window.location.href = pathBuilder;
    }
	
	function  onEnter(pId) {
		var x = document.getElementById(pId);
		if (x != null)
			x.style.display = "block";
	}
	
	function  onLeave(pId) {
		var x = document.getElementById(pId);
		if (x != null)
			x.style.display = "none";
	}
	
	
	var ref = firebase.database().ref();

	ref.on("value", function(snapshot) {
		let pathBuilder = window.location.href;
		let aUrl = new URL(pathBuilder);
		let sugiyaWords = aUrl.searchParams.get("sugiya_file_name") + "_" + "words";
		let sugiyaSyntax = aUrl.searchParams.get("sugiya_file_name") + "_" + "syntax";
		let sugiyaHebrewWords = aUrl.searchParams.get("sugiya_hebrew_name") + "_" + "words";
		let sugiyaHebrewSyntax = aUrl.searchParams.get("sugiya_hebrew_name") + "_" + "syntax";
		let student = aUrl.searchParams.get("player_name");
		let currentGroup = aUrl.searchParams.get("player_group");
		var completers = [];
		var wordsCompleters = [];
		var syntaxCompleters = [];
		var wordsCompletersThisGroup = [];
		var syntaxCompletersThisGroup = [];
		
		
		var scoreboard = snapshot.child("Score Board/");
		scoreboard.forEach(function(groupNode) {
			groupNode.forEach(function(studentNode) {
				studentNode.forEach(function(exerciseNode) {
					exerciseNode.forEach(function(instance) {
						var data = instance.val();
						if (data.didComplete && data.success)
							completers.push({name: data.student_name, date: data.date, time: data.time, group: data.group_name, exercise: data.ex_name});
					})
				})
			})
		})
		
		completers.forEach(function (item, index) {
			if ((item.exercise == sugiyaWords) || (item.exercise == sugiyaHebrewWords)) {
				wordsCompleters.push({name: item.name, date: item.date, time: item.time, group: item.group});
				if (item.group == currentGroup)
					wordsCompletersThisGroup.push({name: item.name, date: item.date, time: item.time});
			}
			if ((item.exercise == sugiyaSyntax) || (item.exercise == sugiyaHebrewSyntax)) {
				syntaxCompleters.push({name: item.name, date: item.date, time: item.time, group: item.group});
				if (item.group == currentGroup)
					syntaxCompletersThisGroup.push({name: item.name, date: item.date, time: item.time});
			}
		});
		
		wordsCompletersThisGroup.sort(function(a, b){return a.date - b.date});
		syntaxCompletersThisGroup.sort(function (a, b) { return a.date - b.date });
		//wordsCompletersThisGroup.length = 0;//disabling the feature for anonymity purposes
		//syntaxCompletersThisGroup.length = 0;


		var htmlString;
		htmlString = wordsCompletersThisGroup.map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
		htmlString = "<caption class=\"mycaption\">תלמידי הקבוצה שהשלימו תרגול מילולי. רשימה זו מוסתרת כעת ותוצג באישור המורה</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("wordCompletions").innerHTML = htmlString;
		
		htmlString = "";
		htmlString = syntaxCompletersThisGroup.map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
		htmlString = "<caption class=\"mycaption\">תלמידי הקבוצה שהשלימו תרגול לוגי. רשימה זו מוסתרת כעת ותוצג באישור המורה</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("syntaxCompletions").innerHTML = htmlString;
		
		wordsCompleters.sort(function(a, b){return a.time - b.time});
		syntaxCompleters.sort(function(a, b){return a.time - b.time});
		
		htmlString = wordsCompleters.slice(0,3).map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.time + '</td><td class=\"mytd\">' + completer.group + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
			htmlString = "<caption class=\"mycaption\">הזריזים מבין כל הקבוצות</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">שניות</th><th class=\"myth\">קבוצה</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("wordsFastestCompletions").innerHTML = htmlString;
		
		htmlString = syntaxCompleters.slice(0,3).map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.time + '</td><td class=\"mytd\">' + completer.group + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
			htmlString = "<caption class=\"mycaption\">הזריזים מבין כל הקבוצות</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">שניות</th><th class=\"myth\">קבוצה</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("syntaxFastestCompletions").innerHTML = htmlString;
		
		
	}, function (error) {
		console.log("Error: " + error.code);
	});


	function toUTF8Array(str) {
	    var utf8 = [];
	    for (var i = 0; i < str.length; i++) {
	        var charcode = str.charCodeAt(i);
	        if (charcode < 0x80) utf8.push(charcode);
	        else if (charcode < 0x800) {
	            utf8.push(0xc0 | (charcode >> 6),
                          0x80 | (charcode & 0x3f));
	        }
	        else if (charcode < 0xd800 || charcode >= 0xe000) {
	            utf8.push(0xe0 | (charcode >> 12),
                          0x80 | ((charcode >> 6) & 0x3f),
                          0x80 | (charcode & 0x3f));
	        }
	            // surrogate pair
	        else {
	            i++;
	            charcode = ((charcode & 0x3ff) << 10) | (str.charCodeAt(i) & 0x3ff)
	            utf8.push(0xf0 | (charcode >> 18),
                          0x80 | ((charcode >> 12) & 0x3f),
                          0x80 | ((charcode >> 6) & 0x3f),
                          0x80 | (charcode & 0x3f));
	        }
	    }
	    return utf8;
	}