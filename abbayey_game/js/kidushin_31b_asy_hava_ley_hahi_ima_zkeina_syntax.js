var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/ibarto_zkeina.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
	},
	"parameters": {
		"numOfItems": 34,
		"placeholders": [
			{ "id": "period_1", "loc": { "x": -216, "y": -47 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 27, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_2", "loc": { "x": -138, "y": 84 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_3", "loc": { "x": -507, "y": -134 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_4", "loc": { "x": -316, "y": -134 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_5", "loc": { "x": 119, "y": -90 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_6", "loc": { "x": 4, "y": -90 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_7", "loc": { "x": -19, "y": -90 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_8", "loc": { "x": -371, "y": -90 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_9", "loc": { "x": 116, "y": 40 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_10", "loc": { "x": -320, "y": 40 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_11", "loc": { "x": -336, "y": 40 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_12", "loc": { "x": -408, "y": 40 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_13", "loc": { "x": -491, "y": 40 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_14", "loc": { "x": 121, "y": 84 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_15", "loc": { "x": -153, "y": 84 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_16", "loc": { "x": -228, "y": 84 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_17", "loc": { "x": -360, "y": 84 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_18", "loc": { "x": -318, "y": 127 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_19", "loc": { "x": -384, "y": 127 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_20", "loc": { "x": -305, "y": 171 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_21", "loc": { "x": -139, "y": 214 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_22", "loc": { "x": 52, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_23", "loc": { "x": 38, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_24", "loc": { "x": -40, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_25", "loc": { "x": -232, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_26", "loc": { "x": -248, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_27", "loc": { "x": -403, "y": 258 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_28", "loc": { "x": 377, "y": 301 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_29", "loc": { "x": 203, "y": 301 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_30", "loc": { "x": -322, "y": 301 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_31", "loc": { "x": 165, "y": 345 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_32", "loc": { "x": 37, "y": 345 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_33", "loc": { "x": -97, "y": 345 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_34", "loc": { "x": 34, "y": 171 }, "rect": { "width": 23, "height": 41 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_1"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_10", "question_22", "question_25"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -760, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_10", "question_22", "question_25"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -760, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -640, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -640, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -790, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -790, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -610, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -610, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -820, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_19", "hyphen_32"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -820, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -580, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -670, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_10", "question_22", "question_25"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -580, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -850, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -760, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2", "question_10", "question_22", "question_25"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -850, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -550, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -550, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -880, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -880, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -520, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_4", "speakbegin_5", "speakbegin_7", "speakbegin_9", "speakbegin_12", "speakbegin_14", "speakbegin_16", "speakbegin_18", "speakbegin_21", "speakbegin_24", "speakbegin_27", "speakbegin_29", "speakbegin_31"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_19", "hyphen_32"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -520, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_3", "speakend_6", "speakend_8", "speakend_11", "speakend_13", "speakend_15", "speakend_17", "speakend_20", "speakend_23", "speakend_26", "speakend_28", "speakend_30", "speakend_33"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_34"],
			},
		],
		"ex_name": "kidushin_31b_asy_hava_ley_hahi_ima_zkeina_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}