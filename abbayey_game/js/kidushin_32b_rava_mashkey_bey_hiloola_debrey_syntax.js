var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/rava_bey_hiloola.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": -108, "y": -195 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_2", "loc": { "x": 71, "y": -150 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_3", "loc": { "x": 322, "y": -105 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_4", "loc": { "x": 304, "y": -105 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_5", "loc": { "x": 244, "y": -105 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_6", "loc": { "x": 207, "y": -15 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_7", "loc": { "x": -9, "y": 30 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_8", "loc": { "x": 331, "y": 121 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_9", "loc": { "x": -165, "y": 211 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_10", "loc": { "x": 38, "y": 256 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_11", "loc": { "x": 2, "y": -15 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 29, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_12", "loc": { "x": 394, "y": 75 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 29, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_13", "loc": { "x": 186, "y": 75 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_14", "loc": { "x": 366, "y": 121 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_15", "loc": { "x": 353, "y": 121 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_16", "loc": { "x": -155, "y": -195 }, "rect": { "width": 24, "height": 43 }, "isReused": false, "offsetX": 0, "offsetY": 29, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_5", "interrobang_14"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_6", "hyphen_7"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_6", "hyphen_7"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_8", "qushia_9"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_8", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_10"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_11", "period_12", "period_16"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_11", "period_12", "period_16"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_13"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_5", "interrobang_14"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_15"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_11", "period_12", "period_16"],
			},
		],
		"ex_name": "kidushin_32b_rava_mashkey_bey_hiloola_debrey_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}