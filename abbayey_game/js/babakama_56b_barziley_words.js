var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56b_top.png",
		"של מי": "של מי.text",
		"אם נומר": "אם נומר.text",
		"שנינו ולמדו כבר פעם אחת": "שנינו ולמדו כבר פעם אחת.text",
		"ראשון": "ראשון.text",
		"פטור מאחריות": "פטור מאחריות.text",
		"נומר": "נומר.text",
		"שזה מהווה": "שזה מהווה.text",
		"קושיא": "קושיא.text",
		"מה פירוש": "מה פירוש.text",
		"לשוליה, לתלמיד של הרועה": "לשוליה, לתלמיד של הרועה.text",
		"שדרכו של רועה, נוהג הוא": "שדרכו של רועה, נוהג הוא.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "של מי_1", "loc": { "x": 83, "y": -4 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נומר_2", "loc": { "x": -15, "y": -4 }, "rect": { "width": 89, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנינו ולמדו כבר פעם אחת_3", "loc": { "x": 61, "y": 32 }, "rect": { "width": 236, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ראשון_4", "loc": { "x": 156, "y": 141 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פטור מאחריות_5", "loc": { "x": 83, "y": 141 }, "rect": { "width": 71, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נומר_6", "loc": { "x": -127, "y": 141 }, "rect": { "width": 62, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שזה מהווה_7", "loc": { "x": 330, "y": 177 }, "rect": { "width": 68, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קושיא_8", "loc": { "x": 232, "y": 177 }, "rect": { "width": 95, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה פירוש_9", "loc": { "x": -134, "y": 213 }, "rect": { "width": 50, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשוליה, לתלמיד של הרועה_10", "loc": { "x": 123, "y": 250 }, "rect": { "width": 107, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשוליה, לתלמיד של הרועה_11", "loc": { "x": 198, "y": 286 }, "rect": { "width": 107, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שדרכו של רועה, נוהג הוא_12", "loc": { "x": -55, "y": 250 }, "rect": { "width": 209, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "של מי", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "של מי_1"],
			},
			{
				"type": "אם נומר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נומר_2"],
			},
			{
				"type": "שנינו ולמדו כבר פעם אחת", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנינו ולמדו כבר פעם אחת_3"],
			},
			{
				"type": "ראשון", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראשון_4"],
			},
			{
				"type": "פטור מאחריות", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פטור מאחריות_5"],
			},
			{
				"type": "נומר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נומר_6"],
			},
			{
				"type": "שזה מהווה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שזה מהווה_7"],
			},
			{
				"type": "קושיא", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קושיא_8"],
			},
			{
				"type": "מה פירוש", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה פירוש_9"],
			},
			{
				"type": "לשוליה, לתלמיד של הרועה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשוליה, לתלמיד של הרועה_10", "לשוליה, לתלמיד של הרועה_11"],
			},
			{
				"type": "לשוליה, לתלמיד של הרועה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשוליה, לתלמיד של הרועה_10", "לשוליה, לתלמיד של הרועה_11"],
			},
			{
				"type": "שדרכו של רועה, נוהג הוא", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שדרכו של רועה, נוהג הוא_12"],
			},
		],
		"ex_name": "babakama_56b_barziley_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}