function addHameniachEtHakadHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "babakama_27a_hameniach_et_hakad_syntax":
            hints.push({ text: 'המשנה עוסקת בשני מצבים שונים הפרידו ביניהם בעזרת הנקודה', symbol: "period" });
            hints.push({ text: 'כל אחד מהמצבים מנוסח כמשפט תנאי: המקרה ואז הדין. הפרידו בין המקרים לדינים בעזרת הקווים', symbol: "hyphen" });
            break;

        case "babakama_27a_patach_bekad_vesiem_behavit_syntax":
            hints.push({ text: 'הסוגיא תוקפת את המשנה ומקשה על הערבוב בין כד וחבית', symbol: "" });
            hints.push({ text: 'המקשן מצטט שתי משניות נוספות עם בעיה דומה. הקיפום ב', symbol: "quotebegin" });
            hints.push({ text: 'כל אחת מהמשניות מנוסחת כמשפט תנאי: המקרה ואז הדין. הפרידו בין המקרים לדינים בעזרת הקווים', symbol: "hyphen" });
            hints.push({ text: 'מיצאו היכן המקשן מסיים את התקפתו וסמנו ב', symbol: "qushia" });
            hints.push({ text: 'התרצן משיב בקצרה סמנו היכן הוא מסיים להשיבה', symbol: "resolution" });
            break;

        case "babakama_27a_lemuy_nafka_mina_syntax":
            hints.push({ text: 'הסוגיא מתעמתת עם תירוצו של רב פפא בסוגיא הקודמת שכד וחבית זהים, בשני סבבים של קושיא ותירוץ', symbol: "" });
            hints.push({ text: 'סמנו היכן מסיים המקשן את קושייתו הראשונה הקצרה', symbol: "qushia" });
            hints.push({ text: 'התרצן משיב בקצרה סמנו היכן היא מסתיימת', symbol: "resolution" });
            hints.push({ text: 'כעת המקשן מנסח קושייא ארוכה על התירוץ, מיצאו היכן הוא מסיים את התקפתו', symbol: "qushia" });
            hints.push({ text: 'שימו לב למשפט התנאי שבבסיס דברי המקשן. הפרידו בין חלקי משפט זה בעזרת הקו', symbol: "hyphen" });
            hints.push({ text: 'התרצן משיב תשובה ארוכה סמנו היכן הוא מסיים ליישב את תירוצו', symbol: "resolution" });
            hints.push({ text: 'אתגר: התירוץ נעזר בטענת סרק מופרכת ושגויה, הקיפו אותה ב', symbol: "wrongly" });
            hints.push({ text: 'אתגר: היעזרו בנקודה בנקודתיים ובסימן השאלה לפסק את דברי התרצן', symbol: "" });
            break;






        case "babakama_27b_umuy_patur_syntax":
            hints.push({ text: 'הסוגיא מבררת מדוע המשנה פוטרת. שימו לב שתירוץ רביעי מובא לאחר סוגיא נפרדת עליה נדלג', symbol: "" });
            hints.push({ text: 'המקשן מקשה על המשנה, סמנו היכן הוא מסיים את התקפתו', symbol: "qushia" });
            hints.push({ text: 'שימו לב שהמקשן פותח את קושייתו בשאלה, סמנוה ב', symbol: "question" });
            hints.push({ text: 'שלושה תרצנים מיישבים את הקושיא. סמנו את סיום כל תירוץ ב', symbol: "resolution" });
            hints.push({ text: 'דלגו על הסוגיא הצדדית לתרצן הרביעי. היא מנוסחת כדו שיח. הקיפו את השיחה ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'ומקמו בסיום התירוץ את', symbol: "resolution" });
            break;
    }
    addHint(div, hints);
}
