var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60b_top.png",
		"ואם תאמר": "ואם תאמר.text",
		"מבפנים": "מבפנים.text",
		"כאשר יצא החוצה": "כאשר יצא החוצה.text",
		"ישב, יסתודד": "ישב, יסתודד.text",
		"יותר מעולה, עדיף": "יותר מעולה, עדיף.text",
		"היכן": "היכן.text",
		"שאין": "שאין.text",
		"שיש": "שיש.text",
		"מדוע הדרשן הביא פסוקים נוספים מעבר לראשון": "מדוע הדרשן הביא פסוקים נוספים מעבר לראשון.text",
		"בלילה": "בלילה.text",
		"ביום": "ביום.text",
		"אין צורך להסתגר": "אין צורך להסתגר.text",
		"הני מיליל, ההנחיה הזו להסתגר": "הני מיליל, ההנחיה הזו להסתגר.text",
		"בין אנשים, בלי ריחוק חברתי": "בין אנשים, בלי ריחוק חברתי.text",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "ואם תאמר_1", "loc": { "x": 70, "y": -134 }, "rect": { "width": 119, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואם תאמר_2", "loc": { "x": 50, "y": -62 }, "rect": { "width": 110, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מבפנים_3", "loc": { "x": 171, "y": -26 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מבפנים_4", "loc": { "x": 330, "y": 11 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאשר יצא החוצה_5", "loc": { "x": 228, "y": 11 }, "rect": { "width": 101, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ישב, יסתודד_6", "loc": { "x": 132, "y": 11 }, "rect": { "width": 56, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יותר מעולה, עדיף_7", "loc": { "x": 203, "y": 47 }, "rect": { "width": 122, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכן_8", "loc": { "x": -116, "y": -62 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאין_9", "loc": { "x": 326, "y": -26 }, "rect": { "width": 77, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכן_10", "loc": { "x": 38, "y": -26 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיש_11", "loc": { "x": -43, "y": -26 }, "rect": { "width": 80, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוע הדרשן הביא פסוקים נוספים מעבר לראשון_12", "loc": { "x": 212, "y": -134 }, "rect": { "width": 140, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בלילה_13", "loc": { "x": -115, "y": -134 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ביום_14", "loc": { "x": 252, "y": -98 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אין צורך להסתגר_15", "loc": { "x": 175, "y": -98 }, "rect": { "width": 35, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הני מיליל, ההנחיה הזו להסתגר_16", "loc": { "x": -32, "y": -134 }, "rect": { "width": 50, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הני מיליל, ההנחיה הזו להסתגר_17", "loc": { "x": -40, "y": -62 }, "rect": { "width": 50, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בין אנשים, בלי ריחוק חברתי_18", "loc": { "x": -35, "y": 11 }, "rect": { "width": 245, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ואם תאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואם תאמר_1", "ואם תאמר_2"],
			},
			{
				"type": "ואם תאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואם תאמר_1", "ואם תאמר_2"],
			},
			{
				"type": "מבפנים", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מבפנים_3", "מבפנים_4"],
			},
			{
				"type": "מבפנים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מבפנים_3", "מבפנים_4"],
			},
			{
				"type": "כאשר יצא החוצה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאשר יצא החוצה_5"],
			},
			{
				"type": "ישב, יסתודד", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ישב, יסתודד_6"],
			},
			{
				"type": "יותר מעולה, עדיף", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יותר מעולה, עדיף_7"],
			},
			{
				"type": "היכן", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכן_8", "היכן_10"],
			},
			{
				"type": "שאין", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאין_9"],
			},
			{
				"type": "היכן", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכן_8", "היכן_10"],
			},
			{
				"type": "שיש", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיש_11"],
			},
			{
				"type": "מדוע הדרשן הביא פסוקים נוספים מעבר לראשון", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוע הדרשן הביא פסוקים נוספים מעבר לראשון_12"],
			},
			{
				"type": "בלילה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בלילה_13"],
			},
			{
				"type": "ביום", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ביום_14"],
			},
			{
				"type": "אין צורך להסתגר", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אין צורך להסתגר_15"],
			},
			{
				"type": "הני מיליל, ההנחיה הזו להסתגר", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הני מיליל, ההנחיה הזו להסתגר_16", "הני מיליל, ההנחיה הזו להסתגר_17"],
			},
			{
				"type": "הני מיליל, ההנחיה הזו להסתגר", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הני מיליל, ההנחיה הזו להסתגר_16", "הני מיליל, ההנחיה הזו להסתגר_17"],
			},
			{
				"type": "בין אנשים, בלי ריחוק חברתי", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בין אנשים, בלי ריחוק חברתי_18"],
			},
		],
		"ex_name": "babakama_60b_dever_baeer_my_veomer_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}