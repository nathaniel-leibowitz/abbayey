var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_26a_bottom.png",
		"כאן": "כאן.text",
		"במה מדובר, באיזה מקרה": "במה מדובר, באיזה מקרה.text",
		"ניתן גם לדייק זאת מלשון הברייתא": "ניתן גם לדייק זאת מלשון הברייתא.text",
		"ולא שנה, ולא אמר": "ולא שנה, ולא אמר.text",
		"שהרי שנה, אמר": "שהרי שנה, אמר.text",
		"מקשים מברייתא סותרת": "מקשים מברייתא סותרת.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "כאן_1", "loc": { "x": 366, "y": 371 }, "rect": { "width": 50, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במה מדובר, באיזה מקרה_2", "loc": { "x": 267, "y": 371 }, "rect": { "width": 137, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ניתן גם לדייק זאת מלשון הברייתא_3", "loc": { "x": -156, "y": 371 }, "rect": { "width": 101, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ולא שנה, ולא אמר_4", "loc": { "x": -389, "y": 371 }, "rect": { "width": 98, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרי שנה, אמר_5", "loc": { "x": -246, "y": 371 }, "rect": { "width": 65, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מקשים מברייתא סותרת_6", "loc": { "x": -264, "y": 276 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "כאן", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן_1"],
			},
			{
				"type": "במה מדובר, באיזה מקרה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במה מדובר, באיזה מקרה_2"],
			},
			{
				"type": "ניתן גם לדייק זאת מלשון הברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ניתן גם לדייק זאת מלשון הברייתא_3"],
			},
			{
				"type": "ולא שנה, ולא אמר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ולא שנה, ולא אמר_4"],
			},
			{
				"type": "שהרי שנה, אמר", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרי שנה, אמר_5"],
			},
			{
				"type": "מקשים מברייתא סותרת", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקשים מברייתא סותרת_6"],
			},
		],
		"ex_name": "brachot_26a_taa_velo_hitpalel_meytivey_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}