var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"מאיפה לו, מהיכן הוא למד זאת": "מאיפה לו, מהיכן הוא למד זאת.text",
		"הוא מוציא את הדין, לומד את הדין": "הוא מוציא את הדין, לומד את הדין.text",
		"צריך לו, דרוש לו": "צריך לו, דרוש לו.text",
		"אגב, אפרופו": "אגב, אפרופו.text",
		"להפריד בין המקרים השונים": "להפריד בין המקרים השונים.text",
		"התורה, הפסוק": "התורה, הפסוק.text",
		"למה נחוצה לנו הכפילות": "למה נחוצה לנו הכפילות.text",
		"להוסיף את החובה לפצות אם נשרף בעל חי ": "להוסיף את החובה לפצות אם נשרף בעל חי .text",
		"החלק העליון שחשוף ומגולה": "החלק העליון שחשוף ומגולה.text",
		"החלק החבוי שמוסתר בתוך התבואה": "החלק החבוי שמוסתר בתוך התבואה.text",
	},
	"parameters": {
		"numOfItems": 15,
		"placeholders": [
			{ "id": "מאיפה לו, מהיכן הוא למד זאת_1", "loc": { "x": 30, "y": 51 }, "rect": { "width": 110, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לו, מהיכן הוא למד זאת_2", "loc": { "x": -41, "y": 87 }, "rect": { "width": 110, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא מוציא את הדין, לומד את הדין_3", "loc": { "x": -100, "y": 51 }, "rect": { "width": 125, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא מוציא את הדין, לומד את הדין_4", "loc": { "x": -167, "y": 87 }, "rect": { "width": 122, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריך לו, דרוש לו_5", "loc": { "x": 316, "y": 87 }, "rect": { "width": 128, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אגב, אפרופו_6", "loc": { "x": 332, "y": 123 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להפריד בין המקרים השונים_7", "loc": { "x": 208, "y": 87 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להפריד בין המקרים השונים_8", "loc": { "x": 56, "y": 87 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הפסוק_9", "loc": { "x": 166, "y": 123 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למה נחוצה לנו הכפילות_10", "loc": { "x": -41, "y": -22 }, "rect": { "width": 92, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להוסיף את החובה לפצות אם נשרף בעל חי _11", "loc": { "x": -281, "y": 14 }, "rect": { "width": 194, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להוסיף את החובה לפצות אם נשרף בעל חי _12", "loc": { "x": 234, "y": 51 }, "rect": { "width": 281, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החלק העליון שחשוף ומגולה_13", "loc": { "x": -251, "y": -22 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החלק העליון שחשוף ומגולה_14", "loc": { "x": 495, "y": 14 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החלק החבוי שמוסתר בתוך התבואה_15", "loc": { "x": 92, "y": 14 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאיפה לו, מהיכן הוא למד זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לו, מהיכן הוא למד זאת_1", "מאיפה לו, מהיכן הוא למד זאת_2"],
			},
			{
				"type": "מאיפה לו, מהיכן הוא למד זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לו, מהיכן הוא למד זאת_1", "מאיפה לו, מהיכן הוא למד זאת_2"],
			},
			{
				"type": "הוא מוציא את הדין, לומד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא מוציא את הדין, לומד את הדין_3", "הוא מוציא את הדין, לומד את הדין_4"],
			},
			{
				"type": "הוא מוציא את הדין, לומד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא מוציא את הדין, לומד את הדין_3", "הוא מוציא את הדין, לומד את הדין_4"],
			},
			{
				"type": "צריך לו, דרוש לו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך לו, דרוש לו_5"],
			},
			{
				"type": "אגב, אפרופו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אגב, אפרופו_6"],
			},
			{
				"type": "להפריד בין המקרים השונים", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להפריד בין המקרים השונים_7", "להפריד בין המקרים השונים_8"],
			},
			{
				"type": "להפריד בין המקרים השונים", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להפריד בין המקרים השונים_7", "להפריד בין המקרים השונים_8"],
			},
			{
				"type": "התורה, הפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הפסוק_9"],
			},
			{
				"type": "למה נחוצה לנו הכפילות", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למה נחוצה לנו הכפילות_10"],
			},
			{
				"type": "להוסיף את החובה לפצות אם נשרף בעל חי ", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להוסיף את החובה לפצות אם נשרף בעל חי _11", "להוסיף את החובה לפצות אם נשרף בעל חי _12"],
			},
			{
				"type": "להוסיף את החובה לפצות אם נשרף בעל חי ", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להוסיף את החובה לפצות אם נשרף בעל חי _11", "להוסיף את החובה לפצות אם נשרף בעל חי _12"],
			},
			{
				"type": "החלק העליון שחשוף ומגולה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החלק העליון שחשוף ומגולה_13", "החלק העליון שחשוף ומגולה_14"],
			},
			{
				"type": "החלק העליון שחשוף ומגולה", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החלק העליון שחשוף ומגולה_13", "החלק העליון שחשוף ומגולה_14"],
			},
			{
				"type": "החלק החבוי שמוסתר בתוך התבואה", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החלק החבוי שמוסתר בתוך התבואה_15"],
			},
		],
		"ex_name": "babakama_60a_kama_lama_li_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}