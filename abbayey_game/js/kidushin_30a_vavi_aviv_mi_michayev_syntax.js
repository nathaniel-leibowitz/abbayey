var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_30a_middle1.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 23,
		"placeholders": [
			{ "id": "interrobang_1", "loc": { "x": 26, "y": -66 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_2", "loc": { "x": -84, "y": -66 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_3", "loc": { "x": -105, "y": -66 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_4", "loc": { "x": -463, "y": -66 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_5", "loc": { "x": -88, "y": -26 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 25, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": -328, "y": -26 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": -93, "y": 13 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_8", "loc": { "x": -115, "y": 13 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_9", "loc": { "x": -304, "y": 92 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_10", "loc": { "x": -327, "y": 92 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_11", "loc": { "x": -116, "y": 132 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": -136, "y": 132 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_13", "loc": { "x": -460, "y": 132 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_14", "loc": { "x": -146, "y": 172 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 25, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_15", "loc": { "x": -380, "y": 172 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": 121, "y": 211 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_17", "loc": { "x": -256, "y": 211 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_18", "loc": { "x": 119, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_19", "loc": { "x": 42, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_20", "loc": { "x": 24, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_21", "loc": { "x": -62, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_22", "loc": { "x": -240, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_23", "loc": { "x": -261, "y": 251 }, "rect": { "width": 21, "height": 38 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_1"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_11"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_14"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_8", "question_15", "question_20"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_9", "quoteend_22"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_10"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_11"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_14"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_8", "question_15", "question_20"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -580, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -850, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_3", "biblequote_4", "biblequote_6", "biblequote_7", "biblequote_12", "biblequote_13", "biblequote_16", "biblequote_17", "biblequote_18", "biblequote_19"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -670, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_8", "question_15", "question_20"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_21"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_9", "quoteend_22"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_23"],
			},
		],
		"ex_name": "kidushin_30a_vavi_aviv_mi_michayev_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}