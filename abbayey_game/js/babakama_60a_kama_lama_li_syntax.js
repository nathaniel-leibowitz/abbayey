var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
	},
	"parameters": {
		"numOfItems": 23,
		"placeholders": [
			{ "id": "qushia_1", "loc": { "x": -94, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_2", "loc": { "x": -288, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_3", "loc": { "x": 460, "y": 3 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_4", "loc": { "x": -179, "y": 3 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_5", "loc": { "x": 456, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_6", "loc": { "x": -30, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": -191, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_8", "loc": { "x": -311, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_9", "loc": { "x": -328, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_10", "loc": { "x": 447, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_11", "loc": { "x": 422, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": 384, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_13", "loc": { "x": 166, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_14", "loc": { "x": -102, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_15", "loc": { "x": -258, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": -382, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_17", "loc": { "x": -407, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_18", "loc": { "x": 381, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_19", "loc": { "x": 113, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_20", "loc": { "x": -10, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_21", "loc": { "x": -79, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_22", "loc": { "x": -201, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_23", "loc": { "x": -219, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_2"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -670, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -760, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -760, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -640, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -640, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -790, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_4", "qushia_6", "qushia_10", "qushia_14", "qushia_18"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -580, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -850, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_7", "biblequote_8", "biblequote_11", "biblequote_12", "biblequote_15", "biblequote_16", "biblequote_19", "biblequote_20", "biblequote_21", "biblequote_22"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -790, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_5", "resolution_9", "resolution_13", "resolution_17", "resolution_23"],
			},
		],
		"ex_name": "babakama_60a_kama_lama_li_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}