var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_bottom.png",
		"והיכן ש, במידה ו": "והיכן ש, במידה ו.text",
		"מאיפה לנו, מנין לומדים זאת": "מאיפה לנו, מנין לומדים זאת.text",
		"פדה אותו": "פדה אותו.text",
		"אביו": "אביו.text",
		"הוא": "הוא.text",
		"לפדות": "לפדות.text",
		"עצמה": "עצמה.text",
		"מצווה, מחויבת": "מצווה, מחויבת.text",
		"והיא": "והיא.text",
		"הפסוק, התורה": "הפסוק, התורה.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "והיכן ש, במידה ו_1", "loc": { "x": 366, "y": 9 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_2", "loc": { "x": -114, "y": -28 }, "rect": { "width": 59, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_3", "loc": { "x": 389, "y": 45 }, "rect": { "width": 59, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_4", "loc": { "x": 93, "y": 118 }, "rect": { "width": 59, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פדה אותו_5", "loc": { "x": 226, "y": 9 }, "rect": { "width": 86, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביו_6", "loc": { "x": 149, "y": 9 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוא_7", "loc": { "x": -5, "y": 9 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפדות_8", "loc": { "x": -104, "y": 9 }, "rect": { "width": 122, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפדות_9", "loc": { "x": -182, "y": 118 }, "rect": { "width": 101, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עצמה_10", "loc": { "x": -282, "y": 118 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצווה, מחויבת_11", "loc": { "x": 248, "y": 45 }, "rect": { "width": 107, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיא_12", "loc": { "x": 171, "y": 118 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפסוק, התורה_13", "loc": { "x": 97, "y": 227 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והיא_14", "loc": { "x": 458, "y": 45 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "והיכן ש, במידה ו", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיכן ש, במידה ו_1"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_2", "מאיפה לנו, מנין לומדים זאת_3", "מאיפה לנו, מנין לומדים זאת_4"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_2", "מאיפה לנו, מנין לומדים זאת_3", "מאיפה לנו, מנין לומדים זאת_4"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_2", "מאיפה לנו, מנין לומדים זאת_3", "מאיפה לנו, מנין לומדים זאת_4"],
			},
			{
				"type": "פדה אותו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פדה אותו_5"],
			},
			{
				"type": "אביו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביו_6"],
			},
			{
				"type": "הוא", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוא_7"],
			},
			{
				"type": "לפדות", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפדות_8", "לפדות_9"],
			},
			{
				"type": "לפדות", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפדות_8", "לפדות_9"],
			},
			{
				"type": "עצמה", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עצמה_10"],
			},
			{
				"type": "מצווה, מחויבת", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצווה, מחויבת_11"],
			},
			{
				"type": "והיא", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיא_12", "והיא_14"],
			},
			{
				"type": "הפסוק, התורה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפסוק, התורה_13"],
			},
			{
				"type": "והיא", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והיא_12", "והיא_14"],
			},
		],
		"ex_name": "kidushin_29a_lifdoto_minalan_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}