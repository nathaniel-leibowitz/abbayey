var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/taanit_2ab.png",
		"שלושה": "שלושה.text",
		"הריון, יולדת": "הריון, יולדת.text",
		"מאיפה לנו, מאיזה פסוק לומדים": "מאיפה לנו, מאיזה פסוק לומדים.text",
		"שכתוב בפסוק": "שכתוב בפסוק.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "שלושה_1", "loc": { "x": 230, "y": -236 }, "rect": { "width": 23, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הריון, יולדת_2", "loc": { "x": -17, "y": -199 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הריון, יולדת_3", "loc": { "x": 164, "y": -127 }, "rect": { "width": 44, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מאיזה פסוק לומדים_4", "loc": { "x": 110, "y": -127 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מאיזה פסוק לומדים_5", "loc": { "x": -45, "y": 235 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_6", "loc": { "x": 263, "y": -163 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_7", "loc": { "x": 38, "y": -127 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_8", "loc": { "x": -119, "y": 235 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שלושה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שלושה_1"],
			},
			{
				"type": "הריון, יולדת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הריון, יולדת_2", "הריון, יולדת_3"],
			},
			{
				"type": "הריון, יולדת", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הריון, יולדת_2", "הריון, יולדת_3"],
			},
			{
				"type": "מאיפה לנו, מאיזה פסוק לומדים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מאיזה פסוק לומדים_4", "מאיפה לנו, מאיזה פסוק לומדים_5"],
			},
			{
				"type": "מאיפה לנו, מאיזה פסוק לומדים", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מאיזה פסוק לומדים_4", "מאיפה לנו, מאיזה פסוק לומדים_5"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_6", "שכתוב בפסוק_7", "שכתוב בפסוק_8"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_6", "שכתוב בפסוק_7", "שכתוב בפסוק_8"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_6", "שכתוב בפסוק_7", "שכתוב בפסוק_8"],
			},
		],
		"ex_name": "taanit_2a_gimel_maftehot_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}