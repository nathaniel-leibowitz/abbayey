var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_32b_bottom_web.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
	},
	"parameters": {
		"numOfItems": 25,
		"placeholders": [
			{ "id": "biblequote_1", "loc": { "x": 250, "y": -184 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_2", "loc": { "x": -169, "y": -184 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_3", "loc": { "x": 13, "y": -74 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_4", "loc": { "x": -205, "y": -38 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_5", "loc": { "x": 267, "y": -256 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": 248, "y": -256 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": 13, "y": -256 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_8", "loc": { "x": 164, "y": -220 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_9", "loc": { "x": 100, "y": -220 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": 59, "y": -220 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_11", "loc": { "x": 151, "y": -111 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": -122, "y": -111 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_13", "loc": { "x": -53, "y": -74 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_14", "loc": { "x": -222, "y": -74 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_15", "loc": { "x": 86, "y": -2 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_16", "loc": { "x": 21, "y": -2 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_17", "loc": { "x": -144, "y": -2 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_18", "loc": { "x": 510, "y": 71 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_19", "loc": { "x": -45, "y": 71 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_20", "loc": { "x": 134, "y": 108 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_21", "loc": { "x": -269, "y": 108 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_22", "loc": { "x": 225, "y": 180 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_23", "loc": { "x": 181, "y": 217 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_24", "loc": { "x": -82, "y": 144 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_25", "loc": { "x": 45, "y": 180 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 23, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_18", "period_20", "period_24", "period_25"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_5"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -580, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -850, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -670, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -550, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -880, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13", "biblequote_14", "biblequote_16", "biblequote_17"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_18", "period_20", "period_24", "period_25"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -760, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_18", "period_20", "period_24", "period_25"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -640, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -790, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_8", "interrobang_15", "interrobang_19", "interrobang_21", "interrobang_22"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_23"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -760, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_18", "period_20", "period_24", "period_25"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -640, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_4", "period_18", "period_20", "period_24", "period_25"],
			},
		],
		"ex_name": "kidushin_32b_mipney_seiva_takoom_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}