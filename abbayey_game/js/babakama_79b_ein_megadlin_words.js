var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_79b_bottom.png",
		"עיזים ומיני צאן": "עיזים ומיני צאן.text",
		"מעשר שני שצריך לאכול בירושלים בטהרה": "מעשר שני שצריך לאכול בירושלים בטהרה.text",
		"לכהנים אסור לגדל תרנגולים בכל אי": "לכהנים אסור לגדל תרנגולים בכל אי.text",
		"שרשרת לקשירה": "שרשרת לקשירה.text",
		"מפזרים, מניחים": "מפזרים, מניחים.text",
		"מלכודות": "מלכודות.text",
		"כמאה וחמישים מטר": "כמאה וחמישים מטר.text",
		"ההקפדה של הכהן להישאר טהור": "ההקפדה של הכהן להישאר טהור.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "עיזים ומיני צאן_1", "loc": { "x": 66, "y": -3 }, "rect": { "width": 128, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מעשר שני שצריך לאכול בירושלים בטהרה_2", "loc": { "x": -176, "y": 57 }, "rect": { "width": 86, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכהנים אסור לגדל תרנגולים בכל אי_3", "loc": { "x": 71, "y": 86 }, "rect": { "width": 266, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שרשרת לקשירה_4", "loc": { "x": 167, "y": 176 }, "rect": { "width": 92, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מפזרים, מניחים_5", "loc": { "x": 21, "y": 176 }, "rect": { "width": 62, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלכודות_6", "loc": { "x": -59, "y": 176 }, "rect": { "width": 77, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כמאה וחמישים מטר_7", "loc": { "x": 194, "y": 206 }, "rect": { "width": 35, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ההקפדה של הכהן להישאר טהור_8", "loc": { "x": -176, "y": 86 }, "rect": { "width": 83, "height": 27 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "עיזים ומיני צאן", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עיזים ומיני צאן_1"],
			},
			{
				"type": "מעשר שני שצריך לאכול בירושלים בטהרה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מעשר שני שצריך לאכול בירושלים בטהרה_2"],
			},
			{
				"type": "לכהנים אסור לגדל תרנגולים בכל אי", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכהנים אסור לגדל תרנגולים בכל אי_3"],
			},
			{
				"type": "שרשרת לקשירה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שרשרת לקשירה_4"],
			},
			{
				"type": "מפזרים, מניחים", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מפזרים, מניחים_5"],
			},
			{
				"type": "מלכודות", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלכודות_6"],
			},
			{
				"type": "כמאה וחמישים מטר", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כמאה וחמישים מטר_7"],
			},
			{
				"type": "ההקפדה של הכהן להישאר טהור", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ההקפדה של הכהן להישאר טהור_8"],
			},
		],
		"ex_name": "babakama_79b_ein_megadlin_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}