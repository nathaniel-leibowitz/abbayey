var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama56a_top.png",
		"צריך": "צריך.text",
		"גם": "גם.text",
		"באיזה מקרה מדובר": "באיזה מקרה מדובר.text",
		"אם נאמר": "אם נאמר.text",
		"לשלם": "לשלם.text",
		"לעצמו": "לעצמו.text",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "צריך_1", "loc": { "x": -164, "y": 14 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_2", "loc": { "x": 66, "y": 54 }, "rect": { "width": 47, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באיזה מקרה מדובר_3", "loc": { "x": 254, "y": 14 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נאמר_4", "loc": { "x": 160, "y": 14 }, "rect": { "width": 101, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשלם_5", "loc": { "x": -252, "y": 14 }, "rect": { "width": 83, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעצמו_6", "loc": { "x": 39, "y": 14 }, "rect": { "width": 104, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "צריך", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך_1"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_2"],
			},
			{
				"type": "באיזה מקרה מדובר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באיזה מקרה מדובר_3"],
			},
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_4"],
			},
			{
				"type": "לשלם", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשלם_5"],
			},
			{
				"type": "לעצמו", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעצמו_6"],
			},
		],
		"ex_name": "babakama_56a_hasocher_edey_sheker_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}