var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33a_top.png",
		"שמא, היתכן": "שמא, היתכן.text",
		"תלמוד לומר": "תלמוד לומר.text",
		"הפסד כספי": "הפסד כספי.text",
		"אין": "אין.text",
		"האם": "האם.text",
		"נוקב פנינים, תכשיטן": "נוקב פנינים, תכשיטן.text",
		"עד שכך וכך, בינתיים": "עד שכך וכך, בינתיים.text",
		"מלפניו": "מלפניו.text",
		"לימד בשיטת ההיקש": "לימד בשיטת ההיקש.text",
		"גם": "גם.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "שמא, היתכן_1", "loc": { "x": 382, "y": -271 }, "rect": { "width": 56, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_2", "loc": { "x": 112, "y": -271 }, "rect": { "width": 53, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפסד כספי_3", "loc": { "x": 111, "y": -231 }, "rect": { "width": 143, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפסד כספי_4", "loc": { "x": 270, "y": -192 }, "rect": { "width": 143, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אין_5", "loc": { "x": 64, "y": -192 }, "rect": { "width": 50, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "האם_6", "loc": { "x": 378, "y": -152 }, "rect": { "width": 32, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נוקב פנינים, תכשיטן_7", "loc": { "x": -11, "y": -152 }, "rect": { "width": 197, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עד שכך וכך, בינתיים_8", "loc": { "x": 385, "y": -112 }, "rect": { "width": 161, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_9", "loc": { "x": 168, "y": -112 }, "rect": { "width": 89, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לימד בשיטת ההיקש_10", "loc": { "x": 353, "y": -72 }, "rect": { "width": 71, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לימד בשיטת ההיקש_11", "loc": { "x": 423, "y": 7 }, "rect": { "width": 83, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_12", "loc": { "x": 342, "y": 7 }, "rect": { "width": 41, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שמא, היתכן", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, היתכן_1"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_2"],
			},
			{
				"type": "הפסד כספי", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפסד כספי_3", "הפסד כספי_4"],
			},
			{
				"type": "הפסד כספי", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפסד כספי_3", "הפסד כספי_4"],
			},
			{
				"type": "אין", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אין_5"],
			},
			{
				"type": "האם", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "האם_6"],
			},
			{
				"type": "נוקב פנינים, תכשיטן", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נוקב פנינים, תכשיטן_7"],
			},
			{
				"type": "עד שכך וכך, בינתיים", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עד שכך וכך, בינתיים_8"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_9"],
			},
			{
				"type": "לימד בשיטת ההיקש", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לימד בשיטת ההיקש_10", "לימד בשיטת ההיקש_11"],
			},
			{
				"type": "לימד בשיטת ההיקש", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לימד בשיטת ההיקש_10", "לימד בשיטת ההיקש_11"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_12"],
			},
		],
		"ex_name": "kidushin_33a_hesron_kees_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}