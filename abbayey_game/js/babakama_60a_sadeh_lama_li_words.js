var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"הייתי אומר בטעות": "הייתי אומר בטעות.text",
		"להביא את הדין, ללמוד את הדין": "להביא את הדין, ללמוד את הדין.text",
		"ואז לא יהיה צורך באלו האחרים": "ואז לא יהיה צורך באלו האחרים.text",
		"כן": "כן.text",
		"דברים אחרים": "דברים אחרים.text",
		"קא משמע לן": "קא משמע לן.text",
		"למה נחוצה לנו הכפילות": "למה נחוצה לנו הכפילות.text",
		"חרך ושרף את התלם החרוש": "חרך ושרף את התלם החרוש.text",
		"חרף ושרף את האבנים": "חרף ושרף את האבנים.text",
		"התורה, הפסוק": "התורה, הפסוק.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "הייתי אומר בטעות_1", "loc": { "x": 98, "y": 196 }, "rect": { "width": 134, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להביא את הדין, ללמוד את הדין_2", "loc": { "x": 485, "y": 160 }, "rect": { "width": 80, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואז לא יהיה צורך באלו האחרים_3", "loc": { "x": -293, "y": 160 }, "rect": { "width": 173, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כן_4", "loc": { "x": -151, "y": 196 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דברים אחרים_5", "loc": { "x": -255, "y": 196 }, "rect": { "width": 155, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_6", "loc": { "x": 494, "y": 233 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למה נחוצה לנו הכפילות_7", "loc": { "x": -331, "y": 123 }, "rect": { "width": 92, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חרך ושרף את התלם החרוש_8", "loc": { "x": 352, "y": 160 }, "rect": { "width": 131, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חרף ושרף את האבנים_9", "loc": { "x": 178, "y": 160 }, "rect": { "width": 185, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הפסוק_10", "loc": { "x": -74, "y": 160 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הפסוק_11", "loc": { "x": 284, "y": 196 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הייתי אומר בטעות_1"],
			},
			{
				"type": "להביא את הדין, ללמוד את הדין", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להביא את הדין, ללמוד את הדין_2"],
			},
			{
				"type": "ואז לא יהיה צורך באלו האחרים", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואז לא יהיה צורך באלו האחרים_3"],
			},
			{
				"type": "כן", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כן_4"],
			},
			{
				"type": "דברים אחרים", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דברים אחרים_5"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_6"],
			},
			{
				"type": "למה נחוצה לנו הכפילות", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למה נחוצה לנו הכפילות_7"],
			},
			{
				"type": "חרך ושרף את התלם החרוש", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חרך ושרף את התלם החרוש_8"],
			},
			{
				"type": "חרף ושרף את האבנים", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חרף ושרף את האבנים_9"],
			},
			{
				"type": "התורה, הפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הפסוק_10", "התורה, הפסוק_11"],
			},
			{
				"type": "התורה, הפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הפסוק_10", "התורה, הפסוק_11"],
			},
		],
		"ex_name": "babakama_60a_sadeh_lama_li_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}