var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babametzia_21a_middle.png",
		"איך דומה, מה בדיוק קרה": "איך דומה, מה בדיוק קרה.text",
		"אם": "אם.text",
		"הרבה, יותר מקב": "הרבה, יותר מקב.text",
		"פחות": "פחות.text",
		"מכך, מקב": "מכך, מקב.text",
		"גם, יהיה למוצא גם במצב זה": "גם, יהיה למוצא גם במצב זה.text",
		"גם לא, לא יהיה למוצא גם במצב זה": "גם לא, לא יהיה למוצא גם במצב זה.text",
		"בבית הגרנות": "בבית הגרנות.text",
		"אנו עוסקים": "אנו עוסקים.text",
		"שמרובה": "שמרובה.text",
		"חוזר": "חוזר.text",
		"בא": "בא.text",
		"ולוקח": "ולוקח.text",
		"מכך": "מכך.text",
		"בזמן כינוס תבואה": "בזמן כינוס תבואה.text",
	},
	"parameters": {
		"numOfItems": 20,
		"placeholders": [
			{ "id": "איך דומה, מה בדיוק קרה_1", "loc": { "x": -100, "y": 168 }, "rect": { "width": 119, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם_2", "loc": { "x": -193, "y": 168 }, "rect": { "width": 29, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם_3", "loc": { "x": -98, "y": 204 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרבה, יותר מקב_4", "loc": { "x": 28, "y": 204 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פחות_5", "loc": { "x": 125, "y": 240 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מכך, מקב_6", "loc": { "x": 49, "y": 240 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם, יהיה למוצא גם במצב זה_7", "loc": { "x": -42, "y": 204 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם לא, לא יהיה למוצא גם במצב זה_8", "loc": { "x": -42, "y": 240 }, "rect": { "width": 89, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בבית הגרנות_9", "loc": { "x": -49, "y": 276 }, "rect": { "width": 104, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אנו עוסקים_10", "loc": { "x": -159, "y": 276 }, "rect": { "width": 80, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמרובה_11", "loc": { "x": 35, "y": 312 }, "rect": { "width": 74, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חוזר_12", "loc": { "x": 129, "y": 348 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא_13", "loc": { "x": 60, "y": 348 }, "rect": { "width": 47, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ולוקח_14", "loc": { "x": -18, "y": 348 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ולוקח_15", "loc": { "x": -220, "y": 384 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בא_16", "loc": { "x": -149, "y": 384 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חוזר_17", "loc": { "x": -84, "y": 384 }, "rect": { "width": 59, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "פחות_18", "loc": { "x": 125, "y": 384 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מכך_19", "loc": { "x": 54, "y": 384 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בזמן כינוס תבואה_20", "loc": { "x": 82, "y": 276 }, "rect": { "width": 125, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "איך דומה, מה בדיוק קרה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איך דומה, מה בדיוק קרה_1"],
			},
			{
				"type": "אם", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם_2", "אם_3"],
			},
			{
				"type": "אם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם_2", "אם_3"],
			},
			{
				"type": "הרבה, יותר מקב", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרבה, יותר מקב_4"],
			},
			{
				"type": "פחות", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פחות_5", "פחות_18"],
			},
			{
				"type": "מכך, מקב", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכך, מקב_6"],
			},
			{
				"type": "גם, יהיה למוצא גם במצב זה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם, יהיה למוצא גם במצב זה_7"],
			},
			{
				"type": "גם לא, לא יהיה למוצא גם במצב זה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם לא, לא יהיה למוצא גם במצב זה_8"],
			},
			{
				"type": "בבית הגרנות", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בבית הגרנות_9"],
			},
			{
				"type": "אנו עוסקים", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אנו עוסקים_10"],
			},
			{
				"type": "שמרובה", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמרובה_11"],
			},
			{
				"type": "חוזר", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חוזר_12", "חוזר_17"],
			},
			{
				"type": "בא", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא_13", "בא_16"],
			},
			{
				"type": "ולוקח", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ולוקח_14", "ולוקח_15"],
			},
			{
				"type": "ולוקח", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ולוקח_14", "ולוקח_15"],
			},
			{
				"type": "בא", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בא_13", "בא_16"],
			},
			{
				"type": "חוזר", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חוזר_12", "חוזר_17"],
			},
			{
				"type": "פחות", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "פחות_5", "פחות_18"],
			},
			{
				"type": "מכך", 
				"score": 1,
				"loc": { "x": -700, "y": 360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכך_19"],
			},
			{
				"type": "בזמן כינוס תבואה", 
				"score": 1,
				"loc": { "x": -700, "y": -400 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בזמן כינוס תבואה_20"],
			},
		],
		"ex_name": "babametzia_21a_matza_perot_mefuzarin_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}