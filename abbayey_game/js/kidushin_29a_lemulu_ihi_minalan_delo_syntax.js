var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_bottom.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
	},
	"parameters": {
		"numOfItems": 16,
		"placeholders": [
			{ "id": "question_1", "loc": { "x": 381, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_2", "loc": { "x": 275, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_3", "loc": { "x": -37, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_4", "loc": { "x": -123, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_5", "loc": { "x": -260, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 24, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_6", "loc": { "x": -418, "y": -148 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 24, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_7", "loc": { "x": 328, "y": -112 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_8", "loc": { "x": 65, "y": -112 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_9", "loc": { "x": 387, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 24, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": 232, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_11", "loc": { "x": -135, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_12", "loc": { "x": -384, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_13", "loc": { "x": 36, "y": -40 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_14", "loc": { "x": 13, "y": -40 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": -6, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_15", "loc": { "x": 330, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_16", "loc": { "x": -286, "y": -76 }, "rect": { "width": 21, "height": 36 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_7"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_4"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_6", "period_9"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -730, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_6", "period_9"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_1", "question_7"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_8"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -670, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_5", "period_6", "period_9"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_2", "biblequote_3", "biblequote_10", "biblequote_11", "biblequote_12", "biblequote_13"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_14"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_15", "colon_16"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -730, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_15", "colon_16"],
			},
		],
		"ex_name": "kidushin_29a_lemulu_ihi_minalan_delo_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}