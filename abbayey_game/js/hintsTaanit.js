function addTaanitHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "taanit_2a_meymatay_mazkirin_gvurot_geshamim_syntax":
            hints.push({ text: 'המשנה פותחת בשאלה, מקמו בסופה את סימן השאלה', symbol: "question" });
            hints.push({ text: 'המשנה מציגה שתי דעות. הפרידו ביניהם בעזרת הנקודות', symbol: "period" });
            break;
        case "taanit_2a_amar_lo_rabi_yehoshua_syntax":
            hints.push({ text: ' המשנה מביאה דו שיח נוקב ומרתק בין התנאים, שבו האחד תוקף את דעתו של השני', symbol: "" });
            hints.push({ text: 'זהו את המשפטים שכל אחד מהתנאים אומר לשני והקיפום ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'רבי יהושע מנסה להפריך את דעת רבי אליעזר, מקמו בסוף התקפתו את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'רבי אליעזר מנסה להדוף, מקמו בסוף דבריו את סימן התירוץ', symbol: "resolution" });
            hints.push({ text: 'רבי יהושע מתעקש וממשיך להקשות עליו, סמנו בסוף דבריו את הקושיא', symbol: "qushia" });
            break;
        case "taanit_2a_gimel_maftehot_syntax":
            hints.push({ text: 'הדרשה המעניינת של רבי יוחנן על ג מפתחות מעט ארוכה, נפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'רבי יוחנן מביא פסוק עבור כל מפתח. הקיפו כל פסוק בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'רבי יוחנן משתמש בשתי שאלות, סמנו אותם בסימן שאלה', symbol: "question" });
            hints.push({ text: 'אתגר: מיצאו את המיקום המתאים ביותר לנקודותיים', symbol: "colon" });
            hints.push({ text: 'ואת המיקום של הנקודה', symbol: "period" });
            break;
        case "taanit_2b_bemaarava_umrey_syntax":
            hints.push({ text: ' הגמרא מביאה גרסא אחרת למדרש שבו מפתח רביעי', symbol: "" });
            hints.push({ text: 'הקיפו את הפסוק שממנו לומדים את המפתח הרביעי בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'הגמרא מקשה על רבי יוחנן. זהו היכן המקשן מסיים להתקיף את קושייתו ומקמו שם', symbol: "qushia" });
            hints.push({ text: 'זהו היכן מסתיים התירוץ ומקמו שם', symbol: "resolution" });
            break;
    }

    addHint(div, hints);
}
