var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/brachot_13a_middle.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
	},
	"parameters": {
		"numOfItems": 6,
		"placeholders": [
			{ "id": "qushia_1", "loc": { "x": 70, "y": 323 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_2", "loc": { "x": -160, "y": 323 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_3", "loc": { "x": -262, "y": 323 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_4", "loc": { "x": -360, "y": 323 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_5", "loc": { "x": -522, "y": 323 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_6", "loc": { "x": 318, "y": 365 }, "rect": { "width": 23, "height": 40 }, "isReused": false, "offsetX": 0, "offsetY": 14, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_5"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_2"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_6"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_4"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_5"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_3", "resolution_6"],
			},
		],
		"ex_name": "brachot_13a_mitzvot_tzrichot_kavana_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}