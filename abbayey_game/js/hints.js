﻿let pathBuilder = window.location.href;
let aUrl = new URL(pathBuilder);
let sugiya = aUrl.searchParams.get("sugiya_file_name");

window.addEventListener('load', setupHints);

function setupHints() {
    var div = document.createElement('div');
    div.id = 'hints2';
    div.style = 'z-index: 0; display: none; position: relative; background-color: white; border: 14px solid red;; top:0px; text-align: center;'
    addHakonesHint(sugiya, div);
    addHahovelHint(sugiya, div);
    addHaavUlHabenHint(sugiya, div);
    addBrachotHint(sugiya, div);
    addHagozelEtzimHint(sugiya, div);
    addHameniachEtHakadHint(sugiya, div);
    addHorimVeyeladimHint(sugiya, div);
    addTaanitHint(sugiya, div);
    addBabametziaHint(sugiya, div);
    document.body.appendChild(div);
}

function onEnter() {
    var x = document.getElementById("hints2");
    if (x != null)
        x.style.display = "block";
}

function onLeave() {
    var x = document.getElementById("hints2");
    if (x != null)
        x.style.display = "none";
}

function addHint(div, hints)
{
    if (hints.length == 0)
        return;
    hints.forEach((hint, count) => {
        div.innerHTML += parseSymbol(hint.symbol);
        div.innerHTML += '<font class="hints2_text">' + ' ' + hint.text + '</font>';
        div.innerHTML += '<br><br>';
    });
    var hintsButton = document.getElementById("hintsButton");
    if (hintsButton != null)
        hintsButton.style.display = "block";
}

function parseSymbol(symbol)
{
    switch (symbol) {
        case "qushia": return '<img class="hints_qushia" src="storage/miniGames/gemara/syntax/qushia.png">';
        case "resolution": return '<img class="hints_resolution" src="storage/miniGames/gemara/syntax/resolution.png">';
        case "quotebegin": return '<img class="hints_quotebegin" src="storage/miniGames/gemara/syntax/quotebegin.png">';
        case "quoteend": return '<img class="hints_quoteend" src="storage/miniGames/gemara/syntax/quoteend.png">';
        case "biblequote": return '<img class="hints_biblequote" src="storage/miniGames/gemara/syntax/biblequote.gif">';
        case "hyphen": return '<img class="hints_hyphen" src="storage/miniGames/gemara/syntax/hyphen.png">';
        case "question": return '<img class="hints_question" src="storage/miniGames/gemara/syntax/question.png">';
        case "period": return '<img class="hints_period" src="storage/miniGames/gemara/syntax/period.png">';
        case "interrobang": return '<img class="hints_interrobang" src="storage/miniGames/gemara/syntax/interrobang.png">';
        case "wrongly": return '<img class="hints_wrongly" src="storage/miniGames/gemara/syntax/wrongly.png">';
        case "colon": return '<img class="hints_colon" src="storage/miniGames/gemara/syntax/colon.png">';
        case "speakbegin": return '<img class="hints_speakbegin" src="storage/miniGames/gemara/syntax/speakbegin.png">';
        case "speakend": return '<img class="hints_speakend" src="storage/miniGames/gemara/syntax/speakend.png">';
        case "assist": return '<img class="hints_assist" src="storage/miniGames/gemara/syntax/assist.png">';
        case "noassist": return '<img class="hints_nonassist" src="storage/miniGames/gemara/syntax/nonassist.png">';
        default: return "";
    }
    return "";
}

