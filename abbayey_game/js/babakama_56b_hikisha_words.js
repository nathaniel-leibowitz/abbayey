var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56b_top.png",
		"ברור מאליו, מיותר לומר": "ברור מאליו, מיותר לומר.text",
		"שהוציאו אותה": "שהוציאו אותה.text",
		"לכל עניין ודבר": "לכל עניין ודבר.text",
		"גם": "גם.text",
		"הצליף בה במקל": "הצליף בה במקל.text",
		"שהצליפו בה במקל": "שהצליפו בה במקל.text",
		"הופכת ליהיות רכושו": "הופכת ליהיות רכושו.text",
		"מרת לנו במעמדי בהמת חברו": "מרת לנו במעמדי בהמת חברו.text",
		"אז אגם במקרה של לסטים שהוציאוה": "אז אגם במקרה של לסטים שהוציאוה.text",
		"כאן מתחיל התירוץ הראשון שכבר למדנו": "כאן מתחיל התירוץ הראשון שכבר למדנו.text",
		"כאן מסתיים התירוץ הראשון שכבר למדנו": "כאן מסתיים התירוץ הראשון שכבר למדנו.text",
		"אביי מציע תירוץ שני": "אביי מציע תירוץ שני.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "ברור מאליו, מיותר לומר_1", "loc": { "x": 321, "y": -258 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהוציאו אותה_2", "loc": { "x": 161, "y": -258 }, "rect": { "width": 98, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכל עניין ודבר_3", "loc": { "x": 308, "y": -222 }, "rect": { "width": 113, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_4", "loc": { "x": 343, "y": -40 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הצליף בה במקל_5", "loc": { "x": 118, "y": -76 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהצליפו בה במקל_6", "loc": { "x": 254, "y": -40 }, "rect": { "width": 113, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הופכת ליהיות רכושו_7", "loc": { "x": -29, "y": -258 }, "rect": { "width": 266, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מרת לנו במעמדי בהמת חברו_8", "loc": { "x": 8, "y": -76 }, "rect": { "width": 110, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אז אגם במקרה של לסטים שהוציאוה_9", "loc": { "x": -112, "y": -76 }, "rect": { "width": 89, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן מתחיל התירוץ הראשון שכבר למדנו_10", "loc": { "x": 84, "y": -222 }, "rect": { "width": 326, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן מסתיים התירוץ הראשון שכבר למדנו_11", "loc": { "x": 113, "y": -113 }, "rect": { "width": 314, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אביי מציע תירוץ שני_12", "loc": { "x": -105, "y": -113 }, "rect": { "width": 107, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "ברור מאליו, מיותר לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ברור מאליו, מיותר לומר_1"],
			},
			{
				"type": "שהוציאו אותה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהוציאו אותה_2"],
			},
			{
				"type": "לכל עניין ודבר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכל עניין ודבר_3"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_4"],
			},
			{
				"type": "הצליף בה במקל", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הצליף בה במקל_5"],
			},
			{
				"type": "שהצליפו בה במקל", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהצליפו בה במקל_6"],
			},
			{
				"type": "הופכת ליהיות רכושו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הופכת ליהיות רכושו_7"],
			},
			{
				"type": "מרת לנו במעמדי בהמת חברו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מרת לנו במעמדי בהמת חברו_8"],
			},
			{
				"type": "אז אגם במקרה של לסטים שהוציאוה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אז אגם במקרה של לסטים שהוציאוה_9"],
			},
			{
				"type": "כאן מתחיל התירוץ הראשון שכבר למדנו", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן מתחיל התירוץ הראשון שכבר למדנו_10"],
			},
			{
				"type": "כאן מסתיים התירוץ הראשון שכבר למדנו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן מסתיים התירוץ הראשון שכבר למדנו_11"],
			},
			{
				"type": "אביי מציע תירוץ שני", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אביי מציע תירוץ שני_12"],
			},
		],
		"ex_name": "babakama_56b_hikisha_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}