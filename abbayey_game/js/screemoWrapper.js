var ocBase;
(function (ocBase) {
    var CoComponentBase = (function () {
        function CoComponentBase(pData, pSkin, pContainer, pNoWrapper, pIsAutoCreate) {
            if (pIsAutoCreate === void 0) { pIsAutoCreate = true; }
            this.mSkinPath = "";
            this.mInitialized = false;
            this.mIsNeedSetToActive = false;
            this.mIsVisible = false;
            this.mIsIncludeInLayout = true;
            this.mIsIncludeParentInLayout = true;
            this.mIsEnabled = true;
            this.mIsStartInitialized = false;
            this.isDisposed = false;
            CoComponentBase.mInstanceCounter++;
            this.mInstanceID = CoComponentBase.mInstanceCounter;
            this.mData = pData;
            this.mNoWraper = pNoWrapper;
            this.mSkinPath = pSkin;
            this.mContainer = pContainer;
            if ((typeof this.mSkinPath) == "string") {
                this.mContentWrapper = document.createElement("div");
                this.handleContainer(this.mContainer);
            }
            else if (this.mSkinPath != undefined) {
                this.mContentWrapper = this.mSkinPath;
                this.handleContainer(this.mContainer);
            }
            else {
                this.mContentWrapper = this.mContainer;
            }
            if (pIsAutoCreate) {
                this.create();
            }
        }
        CoComponentBase.prototype.create = function () {
            var _this = this;
            this.mIsStartInitialized = true;
            if ((typeof this.mSkinPath) == "string") {
                if (this.mSkinPath.substr(this.mSkinPath.length - 5).toLowerCase() != ".html") {
                    this.mSkinPath += ".html";
                }
                this.loadSkin(this.mContentWrapper, this.mSkinPath, function () { return _this.onSkinReady(); });
            }
            else {
                setTimeout(function () { return _this.onSkinReady(); }, 0);
            }
        };
        CoComponentBase.prototype.onAddToStage = function () {
            var e = new ocBase.events.AsEvent(CoComponentBase.EVENT_ADDED_TO_STAGE, true, this);
            this.dispatchEvent(e.event);
        };
        Object.defineProperty(CoComponentBase.prototype, "parentContainer", {
            get: function () {
                return this.mParentContainer;
            },
            enumerable: true,
            configurable: true
        });
        CoComponentBase.prototype.handleContainer = function (pContainer) {
            if (pContainer != undefined) {
                this.mParentContainer = pContainer;
                if (this.mNoWraper != true) {
                    this.mParentContainer.appendChild(this.mContentWrapper);
                }
            }
        };
        CoComponentBase.prototype.loadSkin = function (pTo, pHTMLPath, pOnLoad) {
            ocBase.SkinManager.loadSkin(pHTMLPath, pOnLoad, pTo);
        };
        CoComponentBase.prototype.onSkinReady = function () {
            if (this.isDisposed) {
                return;
            }
            if (document.body.contains(this.mContentWrapper)) {
                if (this.mSkinPath != "") {
                    this.onAddToStage();
                }
            }
            this.creationComplete();
            if (this.mNoWraper == true) {
                for (var i = 0; i < this.mContentWrapper.childNodes.length; i++) {
                    var aNode = this.mContentWrapper.childNodes[i];
                    this.mParentContainer.appendChild(aNode);
                }
            }
            if (this.mIsNeedSetToActive && this.mData) {
                this.updateData();
            }
            this.mInitialized = true;
            this.dispatchEvent(new ocBase.events.AsEvent(CoComponentBase.EVENT_CREATION_COMPLITE, false, this).event);
        };
        CoComponentBase.prototype.getPart = function (iID, pHTMLElement) {
            if (pHTMLElement == undefined) {
                pHTMLElement = this.mContentWrapper;
            }
            if (pHTMLElement == undefined) {
                pHTMLElement = this.mParentContainer;
            }
            var aElement = ocBase.Utils.getElementIn(pHTMLElement, iID);
            if (aElement == null) {
            }
            return (aElement);
        };
        Object.defineProperty(CoComponentBase.prototype, "isInitialized", {
            get: function () {
                return (this.mInitialized);
            },
            enumerable: true,
            configurable: true
        });
        CoComponentBase.prototype.creationComplete = function () {
        };
        CoComponentBase.prototype.setToActive = function () {
            if (!this.mInitialized) {
                this.mIsNeedSetToActive = true;
                return;
            }
            if (this.mData) {
                this.updateData();
            }
            this.dispatchEvent(new ocBase.events.AsEvent(CoComponentBase.EVENT_COMPONENT_ACTIVATE, false, this).event);
        };
        CoComponentBase.prototype.setToSleep = function () {
        };
        CoComponentBase.prototype.dispose = function () {
            this.remove();
            this.isDisposed = true;
        };
        CoComponentBase.prototype.remove = function () {
            if (this.mParentContainer) {
                if (this.mNoWraper != true && this.mContentWrapper.parentElement == this.mParentContainer) {
                    this.mParentContainer.removeChild(this.mContentWrapper);
                }
                this.mParentContainer = null;
            }
        };
        CoComponentBase.prototype.addEventListener = function (pType, pEventListener, pOwner, useCapture) {
            if (this.mEvents == null) {
                this.mEvents = new Array();
            }
            if (this.mEvents[pType] == null) {
                this.mEvents[pType] = Array();
            }
            var aEventsList = this.mEvents[pType];
            for (var i = 0; i < aEventsList.length; i++) {
                if (aEventsList[i].owner == pOwner) {
                    return;
                }
            }
            this.mEvents[pType].push(new EventListenerHolder(pEventListener, pOwner));
            if (this.mContentWrapper == null) {
                return false;
            }
            if (useCapture != null) {
                this.mContentWrapper.addEventListener(pType, pEventListener, useCapture);
                return true;
            }
            this.mContentWrapper.addEventListener(pType, pEventListener);
        };
        CoComponentBase.prototype.removeEventListener = function (pType, pOwner, useCapture) {
            if (this.mEvents == null) {
                return;
            }
            if (this.mEvents[pType] == null) {
                return;
            }
            var aEventListener;
            var aEventsList = this.mEvents[pType];
            for (var i = aEventsList.length - 1; i >= 0; i--) {
                if (aEventsList[i].owner == pOwner) {
                    aEventListener = aEventsList[i].callback;
                    aEventsList.splice(i, 1);
                }
            }
            if (aEventListener == null) {
                return;
            }
            if (this.mContentWrapper == null) {
                return false;
            }
            if (useCapture != null) {
                this.mContentWrapper.removeEventListener(pType, aEventListener, useCapture);
                return true;
            }
            this.mContentWrapper.removeEventListener(pType, aEventListener);
        };
        CoComponentBase.prototype.removeAllOwnerEvents = function (pOwner, useCapture) {
            if (this.mEvents == null) {
                return;
            }
            for (var aType in this.mEvents) {
                var aEventListener = void 0;
                var aEventsList = this.mEvents[aType];
                for (var i = aEventsList.length - 1; i >= 0; i--) {
                    if (aEventsList[i].owner == pOwner) {
                        aEventListener = aEventsList[i].callback;
                        aEventsList.splice(i, 1);
                        if (this.mContentWrapper == null) {
                            return false;
                        }
                        if (useCapture != null) {
                            this.mContentWrapper.removeEventListener(aType, aEventListener, useCapture);
                        }
                        else {
                            this.mContentWrapper.removeEventListener(aType, aEventListener);
                        }
                    }
                }
            }
        };
        CoComponentBase.prototype.dispatchEvent = function (pEvent) {
            if (this.mContentWrapper == null) {
                return false;
            }
            this.mContentWrapper.dispatchEvent(pEvent);
            return true;
        };
        CoComponentBase.prototype.addClass = function (iClassName) {
            if (!this.mContentWrapper) {
                return;
            }
            if (!this.mContentWrapper.classList.contains(iClassName)) {
                this.mContentWrapper.classList.add(iClassName);
            }
        };
        CoComponentBase.prototype.removeClass = function (iClassName) {
            if (!this.mContentWrapper) {
                return;
            }
            if (this.mContentWrapper.classList.contains(iClassName)) {
                this.mContentWrapper.classList.remove(iClassName);
            }
        };
        CoComponentBase.prototype.setEnabled = function (iIsEnabled) {
            this.mIsEnabled = iIsEnabled;
        };
        Object.defineProperty(CoComponentBase.prototype, "data", {
            get: function () {
                return this.mData;
            },
            set: function (pData) {
                this.mData = pData;
            },
            enumerable: true,
            configurable: true
        });
        CoComponentBase.prototype.updateData = function () {
            this.updateView();
        };
        CoComponentBase.prototype.updateView = function () {
        };
        CoComponentBase.prototype.getContentWrapper = function () {
            return this.mContentWrapper;
        };
        Object.defineProperty(CoComponentBase.prototype, "contentWrapper", {
            get: function () {
                return this.mContentWrapper;
            },
            enumerable: true,
            configurable: true
        });
        CoComponentBase.prototype.getInnerContent = function () {
            return this.getContentWrapper().innerHTML;
        };
        Object.defineProperty(CoComponentBase.prototype, "skinPath", {
            get: function () {
                return this.mSkinPath;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "className", {
            get: function () {
                return this.constructor.name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "innerHTML", {
            get: function () {
                if (!this.mContentWrapper) {
                    return "";
                }
                return this.mContentWrapper.innerHTML;
            },
            set: function (value) {
                if (!this.mContentWrapper) {
                    return;
                }
                this.mContentWrapper.innerHTML = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "visible", {
            get: function () {
                return !this.contentWrapper.classList.contains("visibleNone");
            },
            set: function (value) {
                this.mIsVisible = value;
                ocBase.Utils.showPart(this.mContentWrapper, value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "includeInLayout", {
            get: function () {
                return (this.mIsIncludeInLayout);
            },
            set: function (value) {
                this.mIsIncludeInLayout = value;
                if (this.mContentWrapper) {
                    ocBase.Utils.includePart(this.mContentWrapper, value);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "includeParentInLayout", {
            set: function (value) {
                this.mIsIncludeParentInLayout = value;
                if (this.mParentContainer) {
                    ocBase.Utils.includePart(this.mParentContainer, value);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "enabled", {
            get: function () {
                return this.mIsEnabled;
            },
            set: function (value) {
                this.setEnabled(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "instanceID", {
            get: function () {
                return this.className + ":" + this.mInstanceID;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "isStartInitialized", {
            get: function () {
                return this.mIsStartInitialized;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoComponentBase.prototype, "isActive", {
            get: function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        return CoComponentBase;
    }());
    CoComponentBase.EVENT_ADDED_TO_STAGE = "EVENT_ADDED_TO_STAGE";
    CoComponentBase.EVENT_CREATION_COMPLITE = "EVENT_CREATION_COMPLITE";
    CoComponentBase.EVENT_COMPONENT_ACTIVATE = "EVENT_COMPONENT_ACTIVATE";
    CoComponentBase.mInstanceCounter = 0;
    CoComponentBase.version = "";
    ocBase.CoComponentBase = CoComponentBase;
    var EventListenerHolder = (function () {
        function EventListenerHolder(pCallback, pOwner) {
            this.callback = pCallback;
            this.owner = pOwner;
        }
        return EventListenerHolder;
    }());
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var AsEvent = (function () {
            function AsEvent(pKey, pBubbles, pSender, pCancelable) {
                if (pBubbles === void 0) { pBubbles = false; }
                if (pCancelable === void 0) { pCancelable = false; }
                this.mKey = pKey;
                this.mBubbles = pBubbles;
                this.mEvent = document.createEvent("CustomEvent");
                this.mEvent.initCustomEvent(pKey, pBubbles, pCancelable, this);
                this.mSender = pSender;
            }
            Object.defineProperty(AsEvent.prototype, "event", {
                get: function () {
                    return (this.mEvent);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AsEvent.prototype, "sender", {
                get: function () {
                    return (this.mSender);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AsEvent.prototype, "key", {
                get: function () {
                    return this.mKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AsEvent.prototype, "bubbles", {
                get: function () {
                    return this.mBubbles;
                },
                enumerable: true,
                configurable: true
            });
            return AsEvent;
        }());
        events.AsEvent = AsEvent;
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var EventBase = (function () {
            function EventBase(pKey, pCallBack, pOwner, pAttachedData, pFunction) {
                EventBase.mInstanceCounter++;
                this.mInstanceID = EventBase.mInstanceCounter;
                this.mAttachedData = pAttachedData;
                this.mOwner = pOwner;
                this.mKey = pKey;
                this.mCallBack = pCallBack;
            }
            Object.defineProperty(EventBase.prototype, "callBack", {
                get: function () {
                    return this.mCallBack;
                },
                set: function (value) {
                    this.mCallBack = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EventBase.prototype, "data", {
                get: function () {
                    return this.mData;
                },
                set: function (value) {
                    this.mData = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EventBase.prototype, "owner", {
                get: function () {
                    return this.mOwner;
                },
                set: function (value) {
                    this.mOwner = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EventBase.prototype, "sender", {
                get: function () {
                    return this.mSender;
                },
                set: function (value) {
                    this.mSender = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EventBase.prototype, "attachedData", {
                get: function () {
                    return this.mAttachedData;
                },
                set: function (value) {
                    this.mAttachedData = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EventBase.prototype, "key", {
                get: function () {
                    return this.mKey;
                },
                set: function (value) {
                    this.mKey = value;
                },
                enumerable: true,
                configurable: true
            });
            return EventBase;
        }());
        EventBase.mInstanceCounter = 0;
        events.EventBase = EventBase;
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var EventDispatcher = (function () {
            function EventDispatcher() {
            }
            EventDispatcher.prototype.addEventListener = function (pType, pCallback, pOwner) {
                if (pCallback == undefined) {
                    return;
                }
                if (this.mEventsArray == null) {
                    this.mEventsArray = new Array();
                }
                if (this.mEventsArray[pType] == null) {
                    this.mEventsArray[pType] = new Array();
                }
                var aEventsList = this.mEventsArray[pType];
                for (var i = aEventsList.length - 1; i >= 0; i--) {
                    if (aEventsList[i].owner == pOwner) {
                        aEventsList[i].callback = pCallback;
                        return;
                    }
                }
                this.mEventsArray[pType].push(new CallbackHolder(pCallback, pOwner));
            };
            EventDispatcher.prototype.removeEventListener = function (pType, pOwner) {
                if (this.mEventsArray == null) {
                    return;
                }
                if (this.mEventsArray[pType] == null) {
                    return;
                }
                var aEventsList = this.mEventsArray[pType];
                for (var i = aEventsList.length - 1; i >= 0; i--) {
                    if (aEventsList[i].owner == pOwner) {
                        aEventsList.splice(i, 1);
                    }
                }
            };
            EventDispatcher.prototype.dispatchEvent = function (pType, pData) {
                if (this.mEventsArray == null) {
                    return;
                }
                if (this.mEventsArray[pType] == null) {
                    return;
                }
                var aEventsList = this.mEventsArray[pType].slice(0);
                if (pData == null) {
                    for (var i = 0; i < aEventsList.length; i++) {
                        aEventsList[i].callback();
                    }
                }
                else {
                    for (var i = 0; i < aEventsList.length; i++) {
                        aEventsList[i].callback(pData);
                    }
                }
            };
            EventDispatcher.prototype.removeAllOwnerEvents = function (pType, pOwner) {
                if (this.mEventsArray == null) {
                    return;
                }
                for (var aTypes in this.mEventsArray) {
                    var aEventsList = this.mEventsArray[aTypes];
                    for (var i = aEventsList.length - 1; i >= 0; i--) {
                        if (aEventsList[i].owner == pOwner) {
                            aEventsList.splice(i, 1);
                        }
                    }
                }
            };
            return EventDispatcher;
        }());
        events.EventDispatcher = EventDispatcher;
        var CallbackHolder = (function () {
            function CallbackHolder(pCallback, pOwner) {
                this.callback = pCallback;
                this.owner = pOwner;
            }
            return CallbackHolder;
        }());
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var EventManager = (function () {
            function EventManager() {
            }
            EventManager.dispatchCustomEvent = function (pEvent) {
                if (EventManager.mEvents == null) {
                    return;
                }
                if (EventManager.mEvents[pEvent.key] == null) {
                    return;
                }
                var aEventsList = EventManager.mEvents[pEvent.key];
                for (var i = 0; i < aEventsList.length; i++) {
                    pEvent.attachedData = aEventsList[i].attachedData;
                    pEvent.owner = aEventsList[i].owner;
                    aEventsList[i].callBack(pEvent);
                }
            };
            EventManager.dispatchEvent = function (pKey, pOwner, pData) {
                if (EventManager.mEvents == null) {
                    return;
                }
                if (EventManager.mEvents[pKey] == null) {
                    return;
                }
                var aEventsList = EventManager.mEvents[pKey].slice(0);
                for (var i = 0; i < aEventsList.length; i++) {
                    aEventsList[i].data = pData;
                    aEventsList[i].sender = pOwner;
                    aEventsList[i].callBack(aEventsList[i]);
                }
                aEventsList = null;
            };
            EventManager.addEventListener = function (pKey, pCallback, pOwner, pAtachedData, pFunction) {
                if (EventManager.mEvents == null) {
                    EventManager.mEvents = new Array();
                }
                if (EventManager.mEvents[pKey] == null) {
                    EventManager.mEvents[pKey] = Array();
                }
                if (EventManager.hasEventListener(pKey, pOwner)) {
                    return;
                }
                var aEvent = new events.EventBase(pKey, pCallback, pOwner, pAtachedData, pFunction);
                EventManager.mEvents[pKey].push(aEvent);
            };
            EventManager.hasEventListener = function (pKey, pOwner) {
                var aArray = EventManager.mEvents[pKey];
                for (var i = 0; i < aArray.length; i++) {
                    if (aArray[i].owner == pOwner) {
                        return true;
                    }
                }
                return false;
            };
            EventManager.removeEventListener = function (pKey, pOwner) {
                if (EventManager.mEvents == null) {
                    return;
                }
                if (EventManager.mEvents[pKey] == null) {
                    return;
                }
                var aEventsList = EventManager.mEvents[pKey];
                for (var i = aEventsList.length - 1; i >= 0; i--) {
                    if (aEventsList[i].owner == pOwner) {
                        aEventsList.splice(i, 1);
                    }
                }
            };
            EventManager.removeAllOwnerEvents = function (pOwner) {
                if (EventManager.mEvents == null) {
                    return;
                }
                for (var aKey in EventManager.mEvents) {
                    var aEventsList = EventManager.mEvents[aKey];
                    if (aEventsList == null) {
                        console.log("Error - EventManager::removeAllOwnerEvents() aKey = " + aKey);
                        return;
                    }
                    for (var i = aEventsList.length - 1; i >= 0; i--) {
                        if (aEventsList[i].owner == pOwner) {
                            aEventsList.splice(i, 1);
                        }
                    }
                }
            };
            return EventManager;
        }());
        events.EventManager = EventManager;
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var KeyboardCodes = (function () {
            function KeyboardCodes() {
            }
            return KeyboardCodes;
        }());
        KeyboardCodes.TAB = 9;
        KeyboardCodes.ENTER = 13;
        KeyboardCodes.ESC = 27;
        KeyboardCodes.PAGE_UP = 33;
        KeyboardCodes.PAGE_DOWN = 34;
        KeyboardCodes.END = 35;
        KeyboardCodes.HOME = 36;
        KeyboardCodes.ARROW_UP = 38;
        KeyboardCodes.ARROW_LEFT = 37;
        KeyboardCodes.ARROW_RIGHT = 39;
        KeyboardCodes.ARROW_DOWN = 40;
        KeyboardCodes.F1 = 112;
        KeyboardCodes.F2 = 113;
        KeyboardCodes.NUM_1 = 49;
        KeyboardCodes.NUM_2 = 50;
        KeyboardCodes.NUM_3 = 51;
        KeyboardCodes.BACKSPACE = 8;
        KeyboardCodes.SPACE = 32;
        KeyboardCodes.DELETE = 46;
        KeyboardCodes.HYPHEN = 109;
        KeyboardCodes.DIGIT_0 = 48;
        KeyboardCodes.LETTER_Z = 90;
        KeyboardCodes.LOWEST_INPUT_CHAR = 48;
        KeyboardCodes.HIGHEST_INPUT_CHAR = 90;
        KeyboardCodes.LETTER_S = 83;
        events.KeyboardCodes = KeyboardCodes;
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var events;
    (function (events) {
        var MouseEvents = (function () {
            function MouseEvents() {
            }
            return MouseEvents;
        }());
        MouseEvents.CLICK = "click";
        MouseEvents.TOGGLE_CLICK = "toggleclick";
        MouseEvents.DOUBLE_CLICK = "dblclick";
        MouseEvents.MOUSE_OVER = "mouseover";
        MouseEvents.MOUSE_OUT = "mouseout";
        MouseEvents.MOUSE_LEAVE = "mouseleave";
        MouseEvents.ROLL_OVER = "mouseenter";
        MouseEvents.ROLL_OUT = "mouseleave";
        MouseEvents.MOUSE_DOWN = "mousedown";
        MouseEvents.MOUSE_UP = "mouseup";
        MouseEvents.MOUSE_MOVE = "mousemove";
        MouseEvents.MOUSE_WHEEL = "mousewheel";
        MouseEvents.TOUCH_MOVE = "touchmove";
        MouseEvents.TOUCH_START = "touchstart";
        MouseEvents.TOUCH_END = "touchend";
        MouseEvents.DRAG_OVER = "dragover";
        MouseEvents.DRAG_LEAVE = "dragleave";
        MouseEvents.DROP = "drop";
        MouseEvents.SCROLL = "scroll";
        events.MouseEvents = MouseEvents;
    })(events = ocBase.events || (ocBase.events = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var Globals = (function () {
        function Globals() {
        }
        Globals.urlGetParameter = function (pVal) {
            if (Globals.urlGetParameters[pVal] == null) {
                return false;
            }
            if (Globals.urlGetParameters[pVal].toLowerCase() == "false") {
                return false;
            }
            if (Globals.urlGetParameters[pVal].toLowerCase() == "true") {
                return true;
            }
            return (Globals.urlGetParameters[pVal]);
        };
        Globals.setBrowserName = function (value) {
            this.browserName = value;
            switch (value) {
                case Globals.chrome:
                    Globals.isChrome = true;
                    break;
                case Globals.firefox:
                    Globals.isFirefox = true;
                    break;
                case Globals.safari:
                    Globals.isSafari = true;
                    break;
                case Globals.edge:
                    Globals.isEdge = true;
                    break;
            }
        };
        Object.defineProperty(Globals, "browser", {
            get: function () {
                return (this.browserName);
            },
            enumerable: true,
            configurable: true
        });
        return Globals;
    }());
    Globals.mIsAltDown = false;
    Globals.mIsControlDown = false;
    Globals.sUnivFactor = 1;
    Globals.isChairTBMode = false;
    Globals.isTableTBMode = false;
    Globals.tableColor = -1;
    Globals.chrome = "Chrome";
    Globals.firefox = "Firefox";
    Globals.safari = "Safari";
    Globals.edge = "Edge";
    ocBase.Globals = Globals;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var JsonLoader = (function () {
        function JsonLoader(pPath, pCallBack, pErrorCallBack, pReqHeader, pParams) {
            if (pReqHeader === void 0) { pReqHeader = "application/json"; }
            if (pParams === void 0) { pParams = null; }
            var aHttpRequest = new XMLHttpRequest();
            aHttpRequest.open(pParams != null ? "POST" : "GET", pPath);
            aHttpRequest.setRequestHeader("Content-type", pReqHeader);
            aHttpRequest.onreadystatechange = function () {
                if (aHttpRequest.readyState === 4) {
                    if (aHttpRequest.status === 200) {
                        var data = aHttpRequest.responseText;
                        if (pCallBack) {
                            pCallBack(data);
                        }
                        else {
                            if (pErrorCallBack) {
                                pErrorCallBack(aHttpRequest);
                            }
                        }
                    }
                }
                else {
                    console.log("httpRequest.readyState = " + aHttpRequest.readyState);
                }
            };
            aHttpRequest.send(pParams);
        }
        return JsonLoader;
    }());
    ocBase.JsonLoader = JsonLoader;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var math;
    (function (math) {
        var Point = (function () {
            function Point(iX, iY) {
                if (iX === void 0) { iX = 0; }
                if (iY === void 0) { iY = 0; }
                this.x = iX;
                this.y = iY;
            }
            Point.prototype.clone = function () {
                return new Point(this.x, this.y);
            };
            Point.prototype.subtract = function (p) {
                return (new Point(this.x - p.x, this.y - p.y));
            };
            Point.prototype.add = function (p) {
                return (new Point(this.x + p.x, this.y + p.y));
            };
            Point.prototype.normalize = function (pNewLength) {
                if (pNewLength === void 0) { pNewLength = 1; }
                var aFactor = pNewLength / this.length;
                this.x *= aFactor;
                this.y *= aFactor;
            };
            Point.prototype.distanceSqrTo = function (pX, pY) {
                var aX = this.x - pX;
                var aY = this.y - pY;
                return (aX * aX + aY * aY);
            };
            Point.interpolate = function (p1, p2, pFrac) {
                var aX = p1.x + (p2.x - p1.x) * pFrac;
                var aY = p1.y + (p2.y - p1.y) * pFrac;
                return (new Point(aX, aY));
            };
            Point.polar = function (pRadius, pAngle) {
                var aX = pRadius * Math.cos(pAngle);
                var aY = pRadius * Math.sin(pAngle);
                return (new Point(aX, aY));
            };
            Point.distanceSqr = function (p1, p2) {
                var aX = p1.x - p2.x;
                var aY = p1.y - p2.y;
                return (aX * aX + aY * aY);
            };
            Point.distance = function (p1, p2) {
                return (Math.sqrt(this.distanceSqr(p1, p2)));
            };
            Object.defineProperty(Point.prototype, "length", {
                get: function () {
                    return (Math.sqrt(this.x * this.x + this.y * this.y));
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Point, "myName", {
                get: function () {
                    return "Point";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Point.prototype, "myClassName", {
                get: function () {
                    return "Point";
                },
                enumerable: true,
                configurable: true
            });
            return Point;
        }());
        math.Point = Point;
    })(math = ocBase.math || (ocBase.math = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var math;
    (function (math) {
        var MathUtils = (function () {
            function MathUtils() {
            }
            MathUtils.interpolate = function (pP1, pP2, pVal) {
                var aDx = (pP2.x - pP1.x) * pVal;
                var aDy = (pP2.y - pP1.y) * pVal;
                return (new ocBase.math.Point(pP1.x + aDx, pP1.y + aDy));
            };
            MathUtils.distance = function (pP1, pP2) {
                var aDx = (pP1.x - pP2.x);
                var aDy = (pP1.y - pP2.y);
                return (Math.sqrt((aDx * aDx) + (aDy * aDy)));
            };
            MathUtils.distance2 = function (pX1, pY1, pX2, pY2) {
                var aDx = pX1 - pX2;
                var aDy = pY1 - pY2;
                return (Math.sqrt((aDx * aDx) + (aDy * aDy)));
            };
            MathUtils.rotatePoint = function (pPoint, pAngle) {
                var aRadAngle = pAngle * MathUtils.DEG_TO_RAD;
                var aX = pPoint.x * Math.cos(aRadAngle) - pPoint.y * Math.sin(aRadAngle);
                var aY = pPoint.x * Math.sin(aRadAngle) + pPoint.y * Math.cos(aRadAngle);
                return (new ocBase.math.Point(aX, aY));
            };
            MathUtils.isRectOverlap = function (pRect1, pRect2) {
                return !(pRect2.left > pRect1.right ||
                    pRect2.right < pRect1.left ||
                    pRect2.top > pRect1.bottom ||
                    pRect2.bottom < pRect1.top);
            };
            MathUtils.combineRectToBaseRect = function (pBaseRect, pWithRect) {
                pBaseRect.left = Math.min(pBaseRect.left, pWithRect.left);
                pBaseRect.right = Math.max(pBaseRect.right, pWithRect.right);
                pBaseRect.top = Math.min(pBaseRect.top, pWithRect.top);
                pBaseRect.bottom = Math.max(pBaseRect.bottom, pWithRect.bottom);
                return pBaseRect;
            };
            return MathUtils;
        }());
        MathUtils.RAD_TO_DEG = 180 / Math.PI;
        MathUtils.DEG_TO_RAD = Math.PI / 180;
        math.MathUtils = MathUtils;
    })(math = ocBase.math || (ocBase.math = {}));
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var ResourcesLoader = (function () {
        function ResourcesLoader(pResources) {
            this.mCurrentLoadingIndex = -1;
            this.mResources = pResources;
        }
        ResourcesLoader.prototype.onLoad = function () {
            ocBase.ResourcesManager.addResource(this.mCurrentPath, this.mCurrentElement);
            this.loadNext();
        };
        ResourcesLoader.prototype.onLoadJson = function (pData) {
            ocBase.ResourcesManager.addResource(this.mCurrentPath, JSON.parse(pData));
            this.loadNext();
        };

        ResourcesLoader.prototype.TextCreator = function (pText) {
            
            ocBase.ResourcesManager.addResource(this.mCurrentPath, pText);
            this.loadNext();
        };

        ResourcesLoader.prototype.onError = function () {
            this.mCurrentPath = this.mResources[this.mCurrentLoadingIndex];
            ocBase.ResourcesManager.addResource(this.mCurrentPath, -1);
            if ((this.mCurrentElement != null) && (this.mCurrentElement.parentElement != null)) {
                this.mCurrentElement.parentElement.removeChild(this.mCurrentElement);
            }
        };
        ResourcesLoader.prototype.loadNext = function () {
            var _this = this;
            this.mCurrentLoadingIndex++;
            if (this.mCurrentLoadingIndex >= this.mResources.length) {
                ocBase.ResourcesManager.loadNext();
                return;
            }
            this.mCurrentPath = this.mResources[this.mCurrentLoadingIndex];
            var aType = this.mCurrentPath.substr(this.mCurrentPath.lastIndexOf('.') + 1);
            switch (aType) {
                case "jpg":
                case "png":
                case "gif":
                case "svg":
                    this.mCurrentElement = this.loadImage(this.mCurrentPath, function () { return _this.onLoad(); }, function () { return _this.onError(); });
                    break;
                case "js":
                    this.mCurrentElement = this.loadScript(this.mCurrentPath, function () { return _this.onLoad(); }, function () { return _this.onError(); });
                    break;
                case "css":
                    this.mCurrentElement = this.loadStyle(this.mCurrentPath, function () { return _this.onLoad(); }, function () { return _this.onError(); });
                    break;
                case "json":
                    new ocBase.JsonLoader(this.mCurrentPath, function (pData) { return _this.onLoadJson(pData); }, function () { return _this.onError(); });
                    break;
                case "text":
                    _this.TextCreator(this.mCurrentPath);
                    break;
                default:
                    this.mCurrentLoadingIndex++;
                    this.loadNext();
            }
        };
        ResourcesLoader.prototype.loadImage = function (pPath, pScriptLoadedCallback, pErrorCallback) {
            var aImage = document.createElement("img");
            aImage.src = pPath;
            aImage.onload = pScriptLoadedCallback;
            aImage.onerror = pErrorCallback;
            return aImage;
        };
        ResourcesLoader.prototype.loadScript = function (pPath, pScriptLoadedCallback, pErrorCallback) {
            var aScript = document.createElement("script");
            aScript.src = pPath;
            aScript.type = "text/javascript";
            aScript.onload = pScriptLoadedCallback;
            aScript.onerror = pErrorCallback;
            document.body.appendChild(aScript);
            return aScript;
        };
        ResourcesLoader.prototype.loadStyle = function (pPath, pLinkLoadedCallback, pErrorCallback) {
            var aScript = document.createElement("link");
            aScript.href = pPath;
            aScript.rel = "stylesheet";
            aScript.type = "text/css";
            aScript.onload = pLinkLoadedCallback;
            aScript.onerror = pErrorCallback;
            document.head.appendChild(aScript);
            return aScript;
        };
        return ResourcesLoader;
    }());
    ocBase.ResourcesLoader = ResourcesLoader;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var ResourcesManager = (function () {
        function ResourcesManager() {
        }
        Object.defineProperty(ResourcesManager, "recourcesHash", {
            get: function () {
                if (ResourcesManager.mRecourcesHash == null) {
                    ResourcesManager.mRecourcesHash = {};
                }
                return ResourcesManager.mRecourcesHash;
            },
            enumerable: true,
            configurable: true
        });
        ResourcesManager.addResource = function (pPath, pElement) {
            ResourcesManager.recourcesHash[pPath] = pElement;
        };
        ResourcesManager.load = function (pResources) {
            if (ResourcesManager.mRecourcesLoaderList == null) {
                ResourcesManager.mRecourcesLoaderList = new Array();
            }
            ResourcesManager.mRecourcesLoaderList.push(new ocBase.ResourcesLoader(pResources));
            if (ResourcesManager.mRecourcesLoaderList.length == 1) {
                ResourcesManager.mRecourcesLoaderList[0].loadNext();
            }
        };
        ResourcesManager.loadNext = function () {
            ResourcesManager.isLoaded();
            ResourcesManager.mRecourcesLoaderList.shift();
            if (ResourcesManager.mRecourcesLoaderList.length > 0) {
                ResourcesManager.mRecourcesLoaderList[0].loadNext();
            }
        };
        ResourcesManager.isResourcesLoaded = function (pResources, pLoadCompliteCallback) {
            ResourcesManager.mLoadCompliteCallback = pLoadCompliteCallback;
            ResourcesManager.mResurcesToCheck = pResources;
            ResourcesManager.isLoaded();
        };
        ResourcesManager.isLoaded = function () {
            if (ResourcesManager.mLoadCompliteCallback == null) {
                return;
            }
            var aIsAllOK = true;
            for (var i = 0; i < ResourcesManager.mResurcesToCheck.length; i++) {
                if (ResourcesManager.recourcesHash[ResourcesManager.mResurcesToCheck[i]] == null) {
                    return;
                }
                if (ResourcesManager.recourcesHash[ResourcesManager.mResurcesToCheck[i]] == -1) {
                    aIsAllOK = false;
                }
            }
            ResourcesManager.mLoadCompliteCallback(aIsAllOK);
            ResourcesManager.mLoadCompliteCallback = null;
        };
        return ResourcesManager;
    }());
    ocBase.ResourcesManager = ResourcesManager;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var SkinLoader = (function () {
        function SkinLoader(pPath, pFunction) {
            var _this = this;
            this.mCallback = pFunction;
            this.mPath = pPath;
            if (pPath == null) {
                return;
            }
            this.mHttpRequest = new XMLHttpRequest;
            this.mHttpRequest.open('get', pPath, true);
            this.mHttpRequest.onreadystatechange = function () { return _this.onReadyStatecChange(); };
            this.mHttpRequest.send();
        }
        SkinLoader.prototype.setHtmlDataFromData = function (pHTML) {
            SkinLoader.mCounter++;
            if (pHTML != null) {
                this.mHTML = pHTML;
            }
            if (this.mCallback != null) {
                this.mCallback(this);
            }
        };
        SkinLoader.prototype.onReadyStatecChange = function () {
            if (this.mHttpRequest.readyState != 4)
                return;
            var aHtml = this.mHttpRequest.responseText;
            if (aHtml == null) {
                console.log("Error Loading Html");
                this.setHtmlDataFromData(null);
                return;
            }
            this.setHtmlDataFromData(aHtml);
        };
        Object.defineProperty(SkinLoader.prototype, "url", {
            get: function () {
                return this.mPath;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SkinLoader.prototype, "element", {
            get: function () {
                return this.mHTML;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SkinLoader, "myName", {
            get: function () {
                return "SkinLoader";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SkinLoader.prototype, "myClassName", {
            get: function () {
                return "SkinLoader";
            },
            enumerable: true,
            configurable: true
        });
        return SkinLoader;
    }());
    SkinLoader.mCounter = 0;
    ocBase.SkinLoader = SkinLoader;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var SkinManager = (function () {
        function SkinManager() {
            this.mSkinDictionary = {};
        }
        SkinManager.loadSkin = function (iURL, iCallback, iTo, pIsImmediate) {
            if (pIsImmediate === void 0) { pIsImmediate = false; }
            if (SkinManager.mInstance == null) {
                SkinManager.mInstance = new SkinManager();
            }
            SkinManager.mInstance.loadSkin(iURL, iCallback, iTo, pIsImmediate);
        };
        SkinManager.prototype.loadSkin = function (iURL, iCallback, iTo, pIsImmediate) {
            var _this = this;
            if (pIsImmediate === void 0) { pIsImmediate = false; }
            var aCallerSkins = new CallerSkin();
            aCallerSkins.parentElement = iTo;
            aCallerSkins.callback = iCallback;
            if (this.mSkinDictionary[iURL] == null) {
                this.mSkinDictionary[iURL] = new SkinLoadData();
                this.mSkinDictionary[iURL].url = iURL;
                this.mSkinDictionary[iURL].loadingState = LoadingState.LOADING;
                this.mSkinDictionary[iURL].callerSkins = new Array();
                this.mSkinDictionary[iURL].callerSkins.push(aCallerSkins);
                this.importSkin(iURL);
                return;
            }
            if (this.mSkinDictionary[iURL].loadingState == LoadingState.LOADING) {
                this.mSkinDictionary[iURL].callerSkins.push(aCallerSkins);
                return;
            }
            if (pIsImmediate) {
                this.setLoadedSkin(iURL, iCallback, iTo);
            }
            else {
                setTimeout(function (iURL, iCallback, iTo) { return _this.setLoadedSkin(iURL, iCallback, iTo); }, 0, iURL, iCallback, iTo);
            }
        };
        SkinManager.prototype.setLoadedSkin = function (iURL, iCallback, iTo) {
            if (iTo != null) {
                if (this.mSkinDictionary[iURL].element != undefined) {
                    iTo.innerHTML = this.mSkinDictionary[iURL].element;
                    if (iTo.innerHTML == "") {
                    }
                }
            }
            iCallback(this.mSkinDictionary[iURL]);
        };
        SkinManager.prototype.importSkin = function (iURL) {
            var _this = this;
            new ocBase.SkinLoader(iURL, function (iSkinLoader) { return _this.onLoadSkin(iSkinLoader); });
        };
        SkinManager.prototype.onLoadSkin = function (iSkinLoader) {
            var aLoadData = this.mSkinDictionary[iSkinLoader.url];
            aLoadData.element = iSkinLoader.element;
            aLoadData.loadingState = LoadingState.LOADED;
            var aCallerSkins = aLoadData.callerSkins;
            for (var i = 0; i < aCallerSkins.length; i++) {
                if (aCallerSkins[i].parentElement != null) {
                    if (aLoadData.element != undefined) {
                        aCallerSkins[i].parentElement.innerHTML = aLoadData.element;
                    }
                }
                aCallerSkins[i].callback(aLoadData);
            }
        };
        return SkinManager;
    }());
    ocBase.SkinManager = SkinManager;
})(ocBase || (ocBase = {}));
var LoadingState;
(function (LoadingState) {
    LoadingState[LoadingState["LOADING"] = 0] = "LOADING";
    LoadingState[LoadingState["LOADED"] = 1] = "LOADED";
})(LoadingState || (LoadingState = {}));
var CallerSkin = (function () {
    function CallerSkin() {
    }
    return CallerSkin;
}());
var SkinLoadData = (function () {
    function SkinLoadData() {
    }
    return SkinLoadData;
}());
var ocBase;
(function (ocBase) {
    var Utils = (function () {
        function Utils() {
        }
        Utils.getElementIn = function (pComponent, pId) {
            if (pComponent == null) {
                return null;
            }
            if (pComponent.id == pId) {
                return pComponent;
            }
            for (var i = 0; i < pComponent.childNodes.length; i++) {
                if (pComponent.childNodes[i] != null) {
                    if (pComponent.childNodes[i].id == pId) {
                        var aComponent = pComponent.childNodes[i];
                        aComponent.id += this.ID_ITERATOR;
                        this.ID_ITERATOR++;
                        return (aComponent);
                    }
                    var aElement = Utils.getElementIn(pComponent.childNodes[i], pId);
                    if (aElement != null) {
                        return aElement;
                    }
                }
            }
            return null;
        };
        Utils.isElementIn = function (pComponent, pElement) {
            if (pComponent == pElement) {
                return true;
            }
            for (var i = 0; i < pComponent.childNodes.length; i++) {
                if (pComponent.childNodes[i] != null) {
                    if (pComponent.childNodes[i] == pElement) {
                        return true;
                    }
                    if (Utils.isElementIn(pComponent.childNodes[i], pElement)) {
                        return true;
                    }
                }
            }
            return false;
        };
        Utils.areIdenticalArrays = function (iA, iB) {
            if (iA == iB || !iA && !iB) {
                return true;
            }
            if (!iA || !iB) {
                return false;
            }
            if (iA.length == 0 && iB.length == 0) {
                return true;
            }
            if (iA.length != iB.length) {
                return false;
            }
            for (var i = 0; i < iA.length; ++i) {
                if (iA[i] != iB[i]) {
                    return false;
                }
            }
            return true;
        };
        Utils.toArray = function (x) {
            for (var i = 0, a = []; i < x.length; i++)
                a.push(x[i]);
            return a;
        };
        Utils.cloneObject = function (object) {
            var objectCopy = {};
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    objectCopy[key] = object[key];
                }
            }
            return objectCopy;
        };
        Object.defineProperty(Utils, "uniqueId", {
            get: function () {
                Utils.ID_ITERATOR++;
                return Utils.ID_ITERATOR;
            },
            enumerable: true,
            configurable: true
        });
        Utils.switchIncludeParts = function (iSkinPart1, iSkinPart2) {
            if (iSkinPart1 && iSkinPart2) {
                if (iSkinPart1.classList.contains("displayNone")) {
                    Utils.includePart(iSkinPart1, true);
                    Utils.includePart(iSkinPart2, false);
                }
                else {
                    Utils.includePart(iSkinPart1, false);
                    Utils.includePart(iSkinPart2, true);
                }
            }
        };
        Utils.includePart = function (iSkinPart, iIsInclude) {
            if (iSkinPart) {
                if (iIsInclude) {
                    if (iSkinPart.classList.contains("displayNone")) {
                        iSkinPart.classList.remove("displayNone");
                    }
                }
                else {
                    if (iSkinPart.classList.contains("displayNone")) {
                        return;
                    }
                    iSkinPart.classList.add("displayNone");
                }
            }
        };
        Utils.includeSkinPartInLayout = function (iSkinPart) {
            if (iSkinPart) {
                if (iSkinPart.classList.contains("displayNone")) {
                    iSkinPart.classList.remove("displayNone");
                }
            }
        };
        Utils.removeSkinPartFromLayout = function (iSkinPart) {
            if (iSkinPart) {
                if (iSkinPart.classList.contains("displayNone")) {
                    return;
                }
                iSkinPart.classList.add("displayNone");
            }
        };
        Utils.enableElement = function (iSkin) {
            if (iSkin.classList.contains("mouseInactive")) {
                iSkin.classList.remove("mouseInactive");
            }
        };
        Utils.disableElement = function (iSkin) {
            if (iSkin.classList.contains("mouseInactive")) {
                return;
            }
            iSkin.classList.add("mouseInactive");
        };
        Utils.showPart = function (iSkinPart, iIsShow) {
            if (!iSkinPart) {
                return;
            }
            if (iIsShow) {
                iSkinPart.classList.remove("visibleNone");
            }
            else {
                if (iSkinPart.classList.contains("visibleNone")) {
                    return;
                }
                iSkinPart.classList.add("visibleNone");
            }
        };
        Utils.showSkinPart = function (iSkinPart) {
            if (iSkinPart) {
                if (iSkinPart.classList.contains("visibleNone")) {
                    iSkinPart.classList.remove("visibleNone");
                }
            }
        };
        Utils.hideSkinPart = function (iSkinPart) {
            if (iSkinPart) {
                if (iSkinPart.classList.contains("visibleNone")) {
                    return;
                }
                iSkinPart.classList.add("visibleNone");
            }
        };
        Utils.addClassToElement = function (iElement, iClassName) {
            if (iElement.classList.contains(iClassName)) {
                return;
            }
            iElement.classList.add(iClassName);
        };
        Utils.removeClassFromElement = function (iElement, iClassName) {
            if (iElement.classList.contains(iClassName)) {
                iElement.classList.remove(iClassName);
            }
        };
        Utils.validateEmail = function (iHTMLElement) {
            return (iHTMLElement.checkValidity());
        };
        Utils.fromHTMLColorToNumberColor = function (pColor) {
            pColor = pColor.replace("#", "0x");
            return (parseInt(pColor));
        };
        Utils.fromNumberColorToHTMLColor = function (pColor) {
            if (pColor == null) {
                pColor = 0;
            }
            var aBase16 = pColor.toString(16);
            while (aBase16.length < 6) {
                aBase16 = "0" + aBase16;
            }
            var aColor = "#" + aBase16;
            return aColor;
        };
        Utils.safeRemove = function (pElement) {
            if (pElement.parentElement != null) {
                pElement.parentElement.removeChild(pElement);
                return true;
            }
            return false;
        };
        Utils.parseURLParams = function (url) {
            var queryStart = url.indexOf("?") + 1, queryEnd = url.indexOf("#") + 1 || url.length + 1, query = url.slice(queryStart, queryEnd - 1), pairs = query.replace(/\+/g, " ").split("&"), parms = {}, i, n, v, nv;
            if (query === url || query === "") {
                return;
            }
            for (i = 0; i < pairs.length; i++) {
                nv = pairs[i].split("=");
                n = decodeURIComponent(nv[0]);
                v = decodeURIComponent(nv[1]);
                if (!parms.hasOwnProperty(n)) {
                    parms[n] = [];
                }
                parms[n].push(nv.length === 2 ? v : null);
                if (parms[n].length > 1) {
                    parms[n] = parms[n][0];
                }
            }
            return parms;
        };
        Utils.isMobile = function () {
            if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            }
            else {
                return false;
            }
        };
        Utils.debugHTMLElement = function (aEle, pW) {
            var aDebugImagesCont = document.getElementById("debugImage");
            if (aDebugImagesCont == null) {
                aDebugImagesCont = document.createElement("div");
                aDebugImagesCont.id = "debugImage";
                document.body.appendChild(aDebugImagesCont);
                aDebugImagesCont.style.zIndex = "5000";
            }
            aDebugImagesCont.appendChild(aEle);
            aEle.style.position = "fixed";
            aEle.style.top = "300px";
            aEle.style.left = "10px";
            aEle.style.width = pW + "px";
            aEle.style.zIndex = "5000";
        };
        Utils.replaceAll = function (pString, pText, pWith) {
            while (pString.indexOf(pText) > -1) {
                pString = pString.replace(pText, pWith);
            }
            return pString;
        };
        Utils.loadModule = function (pString, onScriptLoaded) {
            var aScript = document.createElement("script");
            aScript.src = "js/" + pString + ".js";
            aScript.type = "text/javascript";
            aScript.onload = onScriptLoaded;
            document.head.appendChild(aScript);
        };
        return Utils;
    }());
    Utils.ID_ITERATOR = 1;
    ocBase.Utils = Utils;
})(ocBase || (ocBase = {}));
var ocBase;
(function (ocBase) {
    var WindowLocator = (function () {
        function WindowLocator() {
        }
        WindowLocator.centerOn = function (pElement, pCom, pExtraHeight) {
            var body = document.body;
            var aBodyHeight = body.clientHeight;
            var aWindowHeight = pElement.clientHeight;
            if (pExtraHeight != undefined) {
                aWindowHeight += pExtraHeight;
            }
            var aTop = (aBodyHeight - aWindowHeight) / 2 - 30;
            if (aTop < 0) {
                aTop = 0;
            }
            pElement.style.marginTop = aTop + "px";
            WindowLocator.currentDialog = pCom;
            WindowLocator.currentDialogElement = pElement;
        };
        return WindowLocator;
    }());
    ocBase.WindowLocator = WindowLocator;
})(ocBase || (ocBase = {}));
var onecode;
(function (onecode) {
    var Events = (function () {
        function Events() {
        }
        return Events;
    }());
    Events.EVENT_MINI_GAME_FINISH = "EVENT_MINI_GAME_FINISH";
    Events.EVENT_NEXT_SCREEN = "EVENT_NEXT_SCREEN";
    onecode.Events = Events;
})(onecode || (onecode = {}));
var onecode;
(function (onecode) {
    var GlobalContext = (function () {
        function GlobalContext() {
        }
        return GlobalContext;
    }());
    GlobalContext.totalScore = 0;
    GlobalContext.lives = 0;
    onecode.GlobalContext = GlobalContext;
})(onecode || (onecode = {}));
var onecode;
(function (onecode) {
    var Main = (function () {
        function Main() {
            var _this = this;
            this.mGameJsonPath = "storage/games/game_01.json";
            this.mCurrentScreenIndex = 0;
            this.mGameSuccsess = 0;
            this.mGameLosse = 0;
            this.mStartTime = Date.now();
            new ocBase.JsonLoader(this.mGameJsonPath, function (pData) { return _this.onGameLoaded(pData); }, function (pRequest) { return _this.onGameLoadedError(pRequest); });
            this.mScreenDiv = document.getElementById(Main.MAIN_DIV_ID);
            ocBase.events.EventManager.addEventListener(onecode.Events.EVENT_MINI_GAME_FINISH, function (pData) { return _this.onGameFinished(pData); }, this);
            ocBase.events.EventManager.addEventListener(onecode.Events.EVENT_NEXT_SCREEN, function () { return _this.onNextScreen(); }, this);
        }
        Main.prototype.onGameFinished = function (pData) {
            if (!pData.data.finishGame) {
                this.mGameLosse++;
                onecode.GlobalContext.lives--;
            }
            else {
                this.mGameSuccsess++;
            }
            onecode.GlobalContext.totalScore += pData.data.score;
            var aData = { score: pData.data.score, success: pData.data.finishGame, gameTime: Date.now() - this.mPageStartTime };
            this.mScreemoApi.events.send("END-GAME", "app", onecode.GlobalContext.gameID, aData);
            this.onNextScreen();
        };
        Main.prototype.onNextScreen = function () {
            this.mCurrentScreenIndex++;
            this.setNextScreen();
        };
        Main.prototype.onGameLoaded = function (pData) {
            this.mGameData = JSON.parse(pData);
            onecode.GlobalContext.lives = this.mGameData.globals.lives;
            onecode.GlobalContext.maxLives = this.mGameData.globals.lives;
            onecode.GlobalContext.gameID = this.mGameData.globals.appId;
            this.mScreemoApi = new screemo.API(screemoGlobals.server, 80, onecode.GlobalContext.gameID);
            this.mScreemoApi.events.send("OPEN-APP", "app", onecode.GlobalContext.gameID, {});
            this.loadAssets();
            this.setNextScreen();
        };
        Main.prototype.loadAssets = function () {
            for (var i = 0; i < this.mGameData.sequence.length; i++) {
                Main.loadPageAssets(this.mGameData.sequence[i]);
            }
        };
        Main.loadPageAssets = function (pGameData) {
            if (pGameData.resources == null) {
                pGameData.resources = [];
            }
            if (pGameData.assets == null) {
                pGameData.assets = {};
            }
            for (var key in pGameData.assets) {
                pGameData.resources.push(pGameData.assets[key]);
            }
            ocBase.ResourcesManager.load(pGameData.resources);
        };
        Main.prototype.onGameLoadedError = function (pRequest) {
            alert(pRequest.response);
        };
        Main.prototype.setNextScreen = function () {
            var _this = this;
            if (this.mCurrentComponent != null) {
                this.mCurrentComponent.removeAllOwnerEvents(this);
            }
            if (this.mGameData.sequence.length - 1 == this.mCurrentScreenIndex) {
                var aData_1 = { totalScore: onecode.GlobalContext.totalScore, gameTime: Date.now() - this.mStartTime, success: this.mGameSuccsess, fail: this.mGameLosse };
                this.mScreemoApi.events.send("FINISH-GAME-SEQUENCE", "app", onecode.GlobalContext.gameID, aData_1);
            }
            if (this.mGameData.sequence.length == this.mCurrentScreenIndex) {
                this.mCurrentScreenIndex = 1;
            }
            if (onecode.GlobalContext.lives == 0) {
                this.mCurrentScreenIndex = this.mGameData.sequence.length - 1;
            }
            this.mCurrentGameData = this.mGameData.sequence[this.mCurrentScreenIndex];
            var aData = {};
            aData.lives = onecode.GlobalContext.lives;
            aData.pageIndex = this.mCurrentScreenIndex;
            ocBase.ResourcesManager.isResourcesLoaded(this.mCurrentGameData.resources, function () { return _this.resourcesLoaded(); });
        };
        Main.prototype.resourcesLoaded = function () {
            var _this = this;
            this.mLastComponent = this.mCurrentComponent;
            this.mCurrentComponent = new ocBase.CoComponentBase(this.mCurrentGameData, this.mCurrentGameData.skin, this.mScreenDiv);
            this.mCurrentComponent.addEventListener(ocBase.CoComponentBase.EVENT_CREATION_COMPLITE, function () { return _this.onScreenLoad(); }, this);
        };
        Main.prototype.resourcesLoadingError = function (pPath) {
            alert("Error: can't find - " + pPath);
        };
        Main.prototype.onScreenLoad = function () {
            if (this.mLastComponent != null) {
                this.mLastComponent.remove();
            }
            if (this.mCurrentScreenIndex == 0) {
                var aDTime = Date.now() - this.mStartTime;
                var aToSend = { time: aDTime };
                this.mScreemoApi.events.send("LOAD-APP", "app", onecode.GlobalContext.gameID, aToSend);
            }
            if (this.mCurrentScreenIndex == 1) {
                this.mStartTime = Date.now();
                this.mGameSuccsess = 0;
                this.mGameLosse = 0;
                onecode.GlobalContext.totalScore = 0;
                onecode.GlobalContext.lives = onecode.GlobalContext.maxLives;
                this.mScreemoApi.events.send("START-GAME-SEQUENCE", "app", onecode.GlobalContext.gameID, {});
            }
            this.mPageStartTime = Date.now();
            window[this.mCurrentGameData.id].init(this.mCurrentComponent);
            this.mScreemoApi.events.send("PAGE-VIEW", "app", onecode.GlobalContext.gameID, this.mCurrentComponent.data);
        };
        return Main;
    }());
    Main.MAIN_DIV_ID = "screen";
    onecode.Main = Main;
})(onecode || (onecode = {}));
var ocBase;
(function (ocBase) {
    var math;
    (function (math) {
        var Rectangle = (function () {
            function Rectangle(pClientRect) {
                this.left = 0;
                this.right = 0;
                this.top = 0;
                this.bottom = 0;
                if (pClientRect != null) {
                    this.left = pClientRect.left;
                    this.right = pClientRect.right;
                    this.top = pClientRect.top;
                    this.bottom = pClientRect.bottom;
                }
            }
            Rectangle.prototype.intersectsPoint = function (pX, pY) {
                return !((pX < this.left) || (pX > this.right) || (pY < this.top) || (pY > this.bottom));
            };
            Rectangle.prototype.intersects = function (pRectB) {
                return !(pRectB.left > this.right ||
                    pRectB.right < this.left ||
                    pRectB.top > this.bottom ||
                    pRectB.bottom < this.top);
            };
            Rectangle.intersectRect = function (pRectA, pRectB) {
                return !(pRectB.left > pRectA.right ||
                    pRectB.right < pRectA.left ||
                    pRectB.top > pRectA.bottom ||
                    pRectB.bottom < pRectA.top);
            };
            Rectangle.intersectPoint = function (pRectA, iX, iY) {
                return !(pRectA.left > iX ||
                    pRectA.right < iX ||
                    pRectA.top > iY ||
                    pRectA.bottom < iY);
            };
            Rectangle.create = function (iX, iY, iWidth, iHeight) {
                var aRet = new Rectangle();
                aRet.left = iX;
                aRet.right = iX + iWidth;
                aRet.top = iY;
                aRet.bottom = iY + iHeight;
                return aRet;
            };
            Object.defineProperty(Rectangle.prototype, "height", {
                get: function () {
                    return Math.abs(this.top - this.bottom);
                },
                set: function (pVal) {
                    this.bottom = this.top + pVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "width", {
                get: function () {
                    return Math.abs(this.left - this.right);
                },
                set: function (pVal) {
                    this.right = this.left + pVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "y", {
                get: function () {
                    return this.top;
                },
                set: function (pVal) {
                    this.top = pVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "x", {
                get: function () {
                    return this.left;
                },
                set: function (pVal) {
                    this.left = pVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle, "myClassName", {
                get: function () {
                    return "Rectangle";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "myClassName", {
                get: function () {
                    return "Rectangle";
                },
                enumerable: true,
                configurable: true
            });
            return Rectangle;
        }());
        math.Rectangle = Rectangle;
    })(math = ocBase.math || (ocBase.math = {}));
})(ocBase || (ocBase = {}));
//# sourceMappingURL=screemoWrapper.js.map