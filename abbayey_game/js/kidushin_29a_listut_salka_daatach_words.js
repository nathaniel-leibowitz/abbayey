var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_middle.png",
		"שנינו את זה, כבר למדנו זאת": "שנינו את זה, כבר למדנו זאת.text",
		"סלקא דעתך, יעלה על הדעת": "סלקא דעתך, יעלה על הדעת.text",
		"לעשות לו ברית מילה": "לעשות לו ברית מילה.text",
		"ויש אומרים": "ויש אומרים.text",
		"גזלנות": "גזלנות.text",
		"דתנו רבנן, למדנו בבריתא": "דתנו רבנן, למדנו בבריתא.text",
		"לעשות לו פדיון בכורות": "לעשות לו פדיון בכורות.text",
		"ללמד אותו מקצוע או עסקים": "ללמד אותו מקצוע או עסקים.text",
		"ללמדו לשחות": "ללמדו לשחות.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "שנינו את זה, כבר למדנו זאת_1", "loc": { "x": 10, "y": 170 }, "rect": { "width": 131, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סלקא דעתך, יעלה על הדעת_2", "loc": { "x": -180, "y": 315 }, "rect": { "width": 50, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעשות לו ברית מילה_3", "loc": { "x": 155, "y": 207 }, "rect": { "width": 68, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ויש אומרים_4", "loc": { "x": -31, "y": 243 }, "rect": { "width": 50, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גזלנות_5", "loc": { "x": 10, "y": 315 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גזלנות_6", "loc": { "x": -95, "y": 315 }, "rect": { "width": 89, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גזלנות_7", "loc": { "x": 28, "y": 351 }, "rect": { "width": 92, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דתנו רבנן, למדנו בבריתא_8", "loc": { "x": -97, "y": 170 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לעשות לו פדיון בכורות_9", "loc": { "x": 60, "y": 207 }, "rect": { "width": 98, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ללמד אותו מקצוע או עסקים_10", "loc": { "x": 98, "y": 243 }, "rect": { "width": 173, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ללמדו לשחות_11", "loc": { "x": -187, "y": 243 }, "rect": { "width": 170, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שנינו את זה, כבר למדנו זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנינו את זה, כבר למדנו זאת_1"],
			},
			{
				"type": "סלקא דעתך, יעלה על הדעת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סלקא דעתך, יעלה על הדעת_2"],
			},
			{
				"type": "לעשות לו ברית מילה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעשות לו ברית מילה_3"],
			},
			{
				"type": "ויש אומרים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ויש אומרים_4"],
			},
			{
				"type": "גזלנות", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גזלנות_5", "גזלנות_6", "גזלנות_7"],
			},
			{
				"type": "גזלנות", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גזלנות_5", "גזלנות_6", "גזלנות_7"],
			},
			{
				"type": "גזלנות", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גזלנות_5", "גזלנות_6", "גזלנות_7"],
			},
			{
				"type": "דתנו רבנן, למדנו בבריתא", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דתנו רבנן, למדנו בבריתא_8"],
			},
			{
				"type": "לעשות לו פדיון בכורות", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לעשות לו פדיון בכורות_9"],
			},
			{
				"type": "ללמד אותו מקצוע או עסקים", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ללמד אותו מקצוע או עסקים_10"],
			},
			{
				"type": "ללמדו לשחות", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ללמדו לשחות_11"],
			},
		],
		"ex_name": "kidushin_29a_listut_salka_daatach_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}