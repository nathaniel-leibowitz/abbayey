var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babametzia_21a_middle.png",
		"מטבעות": "מטבעות.text",
		"אלומות תבואה": "אלומות תבואה.text",
		"תאנים מיובשות": "תאנים מיובשות.text",
		"אופה": "אופה.text",
		"גבעולי פשתן": "גבעולי פשתן.text",
		"חתיכות צמר": "חתיכות צמר.text",
		"בצבע סגול": "בצבע סגול.text",
		"סימן מיוחד": "סימן מיוחד.text",
		"עיגול דבילה שבתוכו חרס": "עיגול דבילה שבתוכו חרס.text",
		"כלים חדשים מהחנות": "כלים חדשים מהחנות.text",
		"לחם, חלות": "לחם, חלות.text",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "מטבעות_1", "loc": { "x": 113, "y": -194 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מטבעות_2", "loc": { "x": -44, "y": 60 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אלומות תבואה_3", "loc": { "x": -116, "y": -194 }, "rect": { "width": 86, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תאנים מיובשות_4", "loc": { "x": 22, "y": -158 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אופה_5", "loc": { "x": -201, "y": -158 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גבעולי פשתן_6", "loc": { "x": -168, "y": -85 }, "rect": { "width": 164, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חתיכות צמר_7", "loc": { "x": -319, "y": -85 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בצבע סגול_8", "loc": { "x": 91, "y": -49 }, "rect": { "width": 125, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סימן מיוחד_9", "loc": { "x": -268, "y": -13 }, "rect": { "width": 59, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עיגול דבילה שבתוכו חרס_10", "loc": { "x": -247, "y": 23 }, "rect": { "width": 242, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כלים חדשים מהחנות_11", "loc": { "x": -54, "y": 96 }, "rect": { "width": 158, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לחם, חלות_12", "loc": { "x": -65, "y": -158 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לחם, חלות_13", "loc": { "x": 129, "y": 60 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מטבעות", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מטבעות_1", "מטבעות_2"],
			},
			{
				"type": "מטבעות", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מטבעות_1", "מטבעות_2"],
			},
			{
				"type": "אלומות תבואה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלומות תבואה_3"],
			},
			{
				"type": "תאנים מיובשות", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תאנים מיובשות_4"],
			},
			{
				"type": "אופה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אופה_5"],
			},
			{
				"type": "גבעולי פשתן", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גבעולי פשתן_6"],
			},
			{
				"type": "חתיכות צמר", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חתיכות צמר_7"],
			},
			{
				"type": "בצבע סגול", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בצבע סגול_8"],
			},
			{
				"type": "סימן מיוחד", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סימן מיוחד_9"],
			},
			{
				"type": "עיגול דבילה שבתוכו חרס", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עיגול דבילה שבתוכו חרס_10"],
			},
			{
				"type": "כלים חדשים מהחנות", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כלים חדשים מהחנות_11"],
			},
			{
				"type": "לחם, חלות", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לחם, חלות_12", "לחם, חלות_13"],
			},
			{
				"type": "לחם, חלות", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לחם, חלות_12", "לחם, חלות_13"],
			},
		],
		"ex_name": "babametzia_21a_elu_metziot_shelo_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}