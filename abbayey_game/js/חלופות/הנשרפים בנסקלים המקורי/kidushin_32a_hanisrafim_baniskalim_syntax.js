var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/hanisrafim_baniskalim.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
	},
	"parameters": {
		"numOfItems": 23,
		"placeholders": [
			{ "id": "hyphen_1", "loc": { "x": -162, "y": -353 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_2", "loc": { "x": -253, "y": 125 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_3", "loc": { "x": -327, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_4", "loc": { "x": -347, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_5", "loc": { "x": -371, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_6", "loc": { "x": -15, "y": 299 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_7", "loc": { "x": 121, "y": -353 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_8", "loc": { "x": -250, "y": -310 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_9", "loc": { "x": -12, "y": -266 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_10", "loc": { "x": 169, "y": -136 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_11", "loc": { "x": -224, "y": 82 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_12", "loc": { "x": -275, "y": 125 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_13", "loc": { "x": 252, "y": 212 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_14", "loc": { "x": -155, "y": 212 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_15", "loc": { "x": 252, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_16", "loc": { "x": -261, "y": 299 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_17", "loc": { "x": -57, "y": 343 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_18", "loc": { "x": -77, "y": 343 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_19", "loc": { "x": -55, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_20", "loc": { "x": -66, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_21", "loc": { "x": -81, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_22", "loc": { "x": -123, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_23", "loc": { "x": -108, "y": 256 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_1"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_5", "interrobang_19"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_23"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_5", "interrobang_19"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_6"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_12", "quotebegin_22"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -760, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -640, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -790, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_7", "speakbegin_9", "speakbegin_11", "speakbegin_13", "speakbegin_15", "speakbegin_16"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -760, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_18"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -670, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_3", "interrobang_5", "interrobang_19"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -640, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -790, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_8", "speakend_10", "speakend_14", "speakend_17", "speakend_20", "speakend_21"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_12", "quotebegin_22"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_23"],
			},
		],
		"ex_name": "kidushin_32a_hanisrafim_baniskalim_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}