var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56a_top.png",
		"אלומה, ערימת שחת": "אלומה, ערימת שחת.text",
		"באיזה מקרה מדובר": "באיזה מקרה מדובר.text",
		"אם נאמר": "אם נאמר.text",
		"שהיתה מגיעה": "שהיתה מגיעה.text",
		"נדירה": "נדירה.text",
		"שכיחה, רגילה": "שכיחה, רגילה.text",
		"גם": "גם.text",
		"שגרם לו ליהיות": "שגרם לו ליהיות.text",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "אלומה, ערימת שחת_1", "loc": { "x": -69, "y": -254 }, "rect": { "width": 68, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "באיזה מקרה מדובר_2", "loc": { "x": 17, "y": -218 }, "rect": { "width": 125, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם נאמר_3", "loc": { "x": -112, "y": -218 }, "rect": { "width": 92, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהיתה מגיעה_4", "loc": { "x": 324, "y": -182 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהיתה מגיעה_5", "loc": { "x": 158, "y": -146 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נדירה_6", "loc": { "x": -70, "y": -146 }, "rect": { "width": 179, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכיחה, רגילה_7", "loc": { "x": 95, "y": -182 }, "rect": { "width": 77, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_8", "loc": { "x": -138, "y": -182 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שגרם לו ליהיות_9", "loc": { "x": -117, "y": -110 }, "rect": { "width": 80, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אלומה, ערימת שחת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלומה, ערימת שחת_1"],
			},
			{
				"type": "באיזה מקרה מדובר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "באיזה מקרה מדובר_2"],
			},
			{
				"type": "אם נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם נאמר_3"],
			},
			{
				"type": "שהיתה מגיעה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהיתה מגיעה_4", "שהיתה מגיעה_5"],
			},
			{
				"type": "שהיתה מגיעה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהיתה מגיעה_4", "שהיתה מגיעה_5"],
			},
			{
				"type": "נדירה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נדירה_6"],
			},
			{
				"type": "שכיחה, רגילה", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכיחה, רגילה_7"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_8"],
			},
			{
				"type": "שגרם לו ליהיות", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שגרם לו ליהיות_9"],
			},
		],
		"ex_name": "babakama_56a_hakofef_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}