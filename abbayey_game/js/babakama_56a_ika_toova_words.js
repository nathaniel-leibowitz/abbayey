var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_56a_middle2.png",
		"מיועד להריסה": "מיועד להריסה.text",
		"שתבוא": "שתבוא.text",
		"בשבילך": "בשבילך.text",
		"מעיד לו": "מעיד לו.text",
		"שמא, אולי": "שמא, אולי.text",
		"גם": "גם.text",
		"מה בסך הכל עשה": "מה בסך הכל עשה.text",
		"אני": "אני.text",
		"מהו שנאמר בטעות": "מהו שנאמר בטעות.text",
		"קא משמע לן": "קא משמע לן.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "מיועד להריסה_1", "loc": { "x": 90, "y": -283 }, "rect": { "width": 206, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שתבוא_2", "loc": { "x": 185, "y": -175 }, "rect": { "width": 86, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בשבילך_3", "loc": { "x": 148, "y": -66 }, "rect": { "width": 74, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מעיד לו_4", "loc": { "x": 284, "y": 151 }, "rect": { "width": 167, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, אולי_5", "loc": { "x": 36, "y": 151 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_6", "loc": { "x": 135, "y": -211 }, "rect": { "width": 41, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_7", "loc": { "x": 254, "y": -139 }, "rect": { "width": 41, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_8", "loc": { "x": -71, "y": -30 }, "rect": { "width": 41, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה בסך הכל עשה_9", "loc": { "x": -95, "y": -283 }, "rect": { "width": 125, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אני_10", "loc": { "x": -131, "y": -102 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מהו שנאמר בטעות_11", "loc": { "x": 31, "y": -211 }, "rect": { "width": 146, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מהו שנאמר בטעות_12", "loc": { "x": -17, "y": -102 }, "rect": { "width": 149, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_13", "loc": { "x": -31, "y": -247 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "קא משמע לן_14", "loc": { "x": 25, "y": -139 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מיועד להריסה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מיועד להריסה_1"],
			},
			{
				"type": "שתבוא", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שתבוא_2"],
			},
			{
				"type": "בשבילך", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בשבילך_3"],
			},
			{
				"type": "מעיד לו", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מעיד לו_4"],
			},
			{
				"type": "שמא, אולי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, אולי_5"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_6", "גם_7", "גם_8"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_6", "גם_7", "גם_8"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_6", "גם_7", "גם_8"],
			},
			{
				"type": "מה בסך הכל עשה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה בסך הכל עשה_9"],
			},
			{
				"type": "אני", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אני_10"],
			},
			{
				"type": "מהו שנאמר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהו שנאמר בטעות_11", "מהו שנאמר בטעות_12"],
			},
			{
				"type": "מהו שנאמר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהו שנאמר בטעות_11", "מהו שנאמר בטעות_12"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_13", "קא משמע לן_14"],
			},
			{
				"type": "קא משמע לן", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "קא משמע לן_13", "קא משמע לן_14"],
			},
		],
		"ex_name": "babakama_56a_ika_toova_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}