﻿

window.onload = ()=>{
};


var sh = [];

let pathBuilder = window.location.href;
let aUrl = new URL(pathBuilder);
let currentGroup = aUrl.searchParams.get("player_group");
let currentName = aUrl.searchParams.get("player_name");

colorSugiotOfPlayer(currentName);


function colorSugiotOfPlayer(currentName) {

    clearColors();
    let sugiotMap = new Map();

    var ref = firebase.database().ref("Score Board/" + currentGroup + "/" + currentName);

    ref.once("value", function (snapshot) {
        var studentNode = snapshot;
        studentNode.forEach(function (exerciseNode) {
            var ex = exerciseNode.key.slice(0, exerciseNode.key.lastIndexOf("_"));
            var type = exerciseNode.key.substring(exerciseNode.key.lastIndexOf("_") + 1);
            if (!sugiotMap.has(ex))
                sugiotMap.set(ex, { translate: false, syntax: false });
            exerciseNode.forEach(function (instance) {
                var data = instance.val();
                if (data.success && !sugiotMap.get(exerciseNode)) {
                    res = sugiotMap.get(ex);
                    if (type === "words")
                        res.translate = true;
                    if (type === "syntax")
                        res.syntax = true;
                    sugiotMap.set(ex, res);
                }
            })
        })

        sugiotMap.forEach(function (success, sugiya) {
            if (success.translate && success.syntax) {
                var el = document.getElementById(sugiya);
                if (el != null) {
                    if (el.className === "btn_amud_a")
                        el.className = "btn_amud_a_completed";
                    if (el.className === "btn_amud_b")
                        el.className = "btn_amud_b_completed";
                }
            }
        })
    });
}

addUsers(currentName);


function clearColors() {
    var sugiot = document.getElementsByTagName('*');

    for (i = 0; i < sugiot.length; i++) {
        if (sugiot[i].className === "btn_amud_a_completed")
            sugiot[i].className = "btn_amud_a";
        if (sugiot[i].className === "btn_amud_b_completed")
            sugiot[i].className = "btn_amud_b";
    };
}


function addUsers(currentName) {
    let usersRadio = document.getElementById("radContainer_1");
    let usersSelect = document.getElementById("selected_student");
    if (currentName !== "המורה") {
        usersElement.style.display = "none";
        return;
    }
    let names = new Set();
    var ref = firebase.database().ref("Score Board/" + currentGroup);
    ref.once("value", function (students) {
        students.forEach(function (name) {
            names.add(name.key);
        });
        names.forEach(name => addUser(usersRadio, usersSelect, name));
    });
}

function addUser(usersRadio, usersSelect, name) {
    addUserSelect(usersSelect, name);
    //addUserRadio(usersRadio, name);I prefet the radio. But it wont show well in small screen or phone, so using the select until radio positioning is improved
}

function addUserSelect(usersElement, name) {
    var opt = document.createElement('option');
    // create text node to add to option element (opt)
    opt.appendChild(document.createTextNode(name));
    // set value property of opt
    opt.value = name;
    // add opt to end of select box (sel)
    usersElement.appendChild(opt);
}

function addUserRadio(usersElement, name) {
    theInput = document.createElement("input");
    theInput.setAttribute('type',"radio");
    theInput.setAttribute('name',"student");
    theInput.setAttribute('id',name);
    theInput.onclick = function(element) {
        colorSugiotOfPlayer(element.currentTarget.id);
        };
    var lbl = document.createElement("label");  
    var txt = document.createTextNode(name);
    lbl.appendChild(txt);  

    usersElement.appendChild(theInput);
    usersElement.appendChild(lbl);
    var linebreak = document.createElement("br");
    usersElement.appendChild(linebreak);
}