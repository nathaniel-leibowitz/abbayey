var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/taanit_2a_top.png",
		"מכיוון": "מכיוון.text",
		"לבקש שירדו גשמים": "לבקש שירדו גשמים.text",
		"לציין ולהודיע": "לציין ולהודיע.text",
		"בזמן המתאים": "בזמן המתאים.text",
		"אם כן, אם כבר אז כבר": "אם כן, אם כבר אז כבר.text",
		"תמיד, בכל השנה": "תמיד, בכל השנה.text",
	},
	"parameters": {
		"numOfItems": 7,
		"placeholders": [
			{ "id": "מכיוון_1", "loc": { "x": 67, "y": -60 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לבקש שירדו גשמים_2", "loc": { "x": -60, "y": 5 }, "rect": { "width": 77, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לציין ולהודיע_3", "loc": { "x": -199, "y": 5 }, "rect": { "width": 77, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בזמן המתאים_4", "loc": { "x": -98, "y": 37 }, "rect": { "width": 77, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אם כן, אם כבר אז כבר_5", "loc": { "x": -267, "y": 37 }, "rect": { "width": 44, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תמיד, בכל השנה_6", "loc": { "x": -335, "y": 37 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לציין ולהודיע_7", "loc": { "x": 12, "y": 70 }, "rect": { "width": 62, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מכיוון", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מכיוון_1"],
			},
			{
				"type": "לבקש שירדו גשמים", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לבקש שירדו גשמים_2"],
			},
			{
				"type": "לציין ולהודיע", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לציין ולהודיע_3", "לציין ולהודיע_7"],
			},
			{
				"type": "בזמן המתאים", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בזמן המתאים_4"],
			},
			{
				"type": "אם כן, אם כבר אז כבר", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כן, אם כבר אז כבר_5"],
			},
			{
				"type": "תמיד, בכל השנה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תמיד, בכל השנה_6"],
			},
			{
				"type": "לציין ולהודיע", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לציין ולהודיע_3", "לציין ולהודיע_7"],
			},
		],
		"ex_name": "taanit_2a_amar_lo_rabi_yehoshua_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}