﻿
	window.onload = ()=>{
        let aUrl = new URL(document.location.href);
        let sugiyaHebrewName = aUrl.searchParams.get("sugiya_hebrew_name");
        document.getElementById("sugiya_hebrew_name").value = sugiyaHebrewName;
    };
	
	function  onClick(pExerciseType) {
		let pathBuilder = window.location.href;
		let aUrl = new URL(pathBuilder);
		let sugiya = aUrl.searchParams.get("sugiya_file_name");
		let sugiyaFullFileName = sugiya + "_" + pExerciseType;
		let student = aUrl.searchParams.get("player_name");
		let currentGroup = aUrl.searchParams.get("player_group");
		
		var aDate = new Date();
        var aDateStr = aDate.toLocaleDateString() + "    " + aDate.toLocaleTimeString();
		var msEpoch = aDate.getTime();
        var aDataJson = {
                    student_name: student,
                    ex_name: sugiyaFullFileName,
                    group_name: currentGroup,
                    date: aDateStr,
					msFromEpoch: msEpoch
                };
		firebase.database().ref("Entrance").child(aDate.getTime().toString()).set(aDataJson, function () {
                });
		
		pathBuilder = pathBuilder.replace("sugiya", "sugiya_dispatcher");
		pathBuilder = pathBuilder.replace(sugiya, sugiyaFullFileName);
		window.location.href = pathBuilder;
    }
	
	function  onEnter(pId) {
		var x = document.getElementById(pId);
		if (x != null)
			x.style.display = "block";
	}
	
	function  onLeave(pId) {
		var x = document.getElementById(pId);
		if (x != null)
			x.style.display = "none";
	}
	
	
	let pathBuilder = window.location.href;
	let aUrl = new URL(pathBuilder);
	let sugiyaWords = aUrl.searchParams.get("sugiya_file_name") + "_" + "words";
	let sugiyaSyntax = aUrl.searchParams.get("sugiya_file_name") + "_" + "syntax";
	let sugiyaHebrewWords = aUrl.searchParams.get("sugiya_hebrew_name") + "_" + "words";
	let sugiyaHebrewSyntax = aUrl.searchParams.get("sugiya_hebrew_name") + "_" + "syntax";
	let student = aUrl.searchParams.get("player_name");
	let currentGroup = aUrl.searchParams.get("player_group");
	var completersThisGroup = [];
	var completers = [];
	var wordsCompleters = [];
	var syntaxCompleters = [];
	var wordsCompletersThisGroup = [];
	var syntaxCompletersThisGroup = [];

	var ref = firebase.database().ref("Score Board/" + currentGroup + "/");
	ref.on("value", function (snapshot) {
		
	    var groupNode = snapshot;//.child("Score Board/");
	    groupNode.forEach(function(studentNode) {
	        studentNode.forEach(function(exerciseNode) {
	            exerciseNode.forEach(function(instance) {
	                var data = instance.val();
	                if (data.didComplete && data.success)
	                    completersThisGroup.push({ name: data.student_name, date: data.date, time: data.time, group: data.group_name, exercise: data.ex_name, ms: data.msFromEpoch });
	            })
	        })
	    })
	    completersThisGroup.forEach(function (item, index) {
	        if ((item.exercise == sugiyaWords) || (item.exercise == sugiyaHebrewWords)) {
	            wordsCompletersThisGroup.push({ name: item.name, date: item.date, time: item.time, ms: item.ms });
	        }
	        if ((item.exercise == sugiyaSyntax) || (item.exercise == sugiyaHebrewSyntax)) {
	            syntaxCompletersThisGroup.push({ name: item.name, date: item.date, time: item.time, ms: item.ms });
	        }
	    });

	    wordsCompletersThisGroup.sort(function (a, b) { return (b.ms - a.ms) });
	    var wordsCompletersThisGroupUniqe = [];
	    for (var i = 0, l = wordsCompletersThisGroup.length; i < l; i++)
	        if (!wordsCompletersThisGroupUniqe.find(x => x.name === wordsCompletersThisGroup[i].name))
	            wordsCompletersThisGroupUniqe.push(wordsCompletersThisGroup[i]);

	    syntaxCompletersThisGroup.sort(function (a, b) { return (b.ms - a.ms) });
	    var syntaxCompletersThisGroupUniqe = [];
	    for (var i = 0, l = syntaxCompletersThisGroup.length; i < l; i++)
	        if (!syntaxCompletersThisGroupUniqe.find(x => x.name === syntaxCompletersThisGroup[i].name))
				syntaxCompletersThisGroupUniqe.push(syntaxCompletersThisGroup[i]);
				
		var completersUnique = [];
		var maxSz = Math.max(wordsCompletersThisGroupUniqe.length, syntaxCompletersThisGroupUniqe.length);

		for (var i=0; i < maxSz; i++)
		{
			var entry = new Object();
			entry.words = "";
			entry.syntax = "";
			if (i < wordsCompletersThisGroupUniqe.length)
				entry.words = wordsCompletersThisGroupUniqe[i].name;
			if (i < syntaxCompletersThisGroupUniqe.length)
				entry.syntax = syntaxCompletersThisGroupUniqe[i].name;
			completersUnique.push(entry);
		}


		var htmlString;
	    htmlString = completersUnique.map(function (entry) {
	        return '<tr class=\"mytr\"><td class=\"mytd\">' + entry.syntax + '</td><td class=\"mytd\">' + entry.words + '</td></tr>';
	    }).join('');
	    var helperstr = "תלמידי " + "\"" + currentGroup + "\"" + " שהשלימו את הסוגיא";
	    htmlString = "<caption class=\"mycaption\">" + helperstr + "</caption><tr class=\"mytr\"><th class=\"myth\">לוגי</th><th class=\"myth\">מילולי</th></tr>" + htmlString;
	    document.getElementById("completions").innerHTML = htmlString;
		
		
		/*htmlString = "";
	    htmlString = syntaxCompletersThisGroupUniqe.map(function (completer) {
			return '<tr class=\"mytr\"></td><td class=\"mytd\">' + completer.name + '</td></tr>';

	    var htmlString;
	    htmlString = wordsCompletersThisGroupUniqe.map(function (completer) {
	        return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.name + '</td></tr>';
	    }).join('');
	    var helperstr = "תלמידי " + "\"" + currentGroup + "\"" + " שהשלימו תרגולים";
	    htmlString = "<caption class=\"mycaption\">" + helperstr + "</caption><tr class=\"mytr\"><th class=\"myth\">מילולי</th></tr>" + htmlString;
	    document.getElementById("wordCompletions").innerHTML = htmlString;

	    htmlString = "";
	    htmlString = syntaxCompletersThisGroupUniqe.map(function (completer) {
			return '<tr class=\"mytr\"></td><td class=\"mytd\">' + completer.name + '</td></tr>';
			

	    }).join('');
	    var helperstr = "תלמידי " + "\"" + currentGroup + "\"" + " שהשלימו תרגול לוגי";
	    htmlString = "<caption class=\"mycaption\">" + helperstr + "</caption><tr class=\"mytr\"><th class=\"myth\">תלמיד</th></tr>" + htmlString;
	    document.getElementById("syntaxCompletions").innerHTML = htmlString;*/
	    document.getElementById("syntax_button").disabled = false;
	    document.getElementById("words_button").disabled = false;

	}, function (error) {
	    console.log("Error: " + error.code);
	});


/*
	wordsCompletersThisGroup.length = 0;
	syntaxCompletersThisGroup.length = 0;


    var ref2 = firebase.database().ref("Score Board/");
	ref2.once("value", function (snapshot) {
		
		var scoreboard = snapshot;//.child("Score Board/");
		scoreboard.forEach(function(groupNode) {
			groupNode.forEach(function(studentNode) {
				studentNode.forEach(function(exerciseNode) {
					exerciseNode.forEach(function(instance) {
						var data = instance.val();
						if (data.didComplete && data.success)
							completers.push({name: data.student_name, date: data.date, time: data.time, group: data.group_name, exercise: data.ex_name, ms: data.msFromEpoch});
					})
				})
			})
		})
		
		completers.forEach(function (item, index) {
			if ((item.exercise == sugiyaWords) || (item.exercise == sugiyaHebrewWords)) {
			    wordsCompleters.push({ name: item.name, date: item.date, time: item.time, group: item.group, ms: item.ms});
				if (item.group == currentGroup)
				    wordsCompletersThisGroup.push({ name: item.name, date: item.date, time: item.time, ms: item.ms});
			}
			if ((item.exercise == sugiyaSyntax) || (item.exercise == sugiyaHebrewSyntax)) {
			    syntaxCompleters.push({ name: item.name, date: item.date, time: item.time, group: item.group, ms: item.ms});
				if (item.group == currentGroup)
				    syntaxCompletersThisGroup.push({ name: item.name, date: item.date, time: item.time, ms: item.ms});
			}
		});
		
		wordsCompletersThisGroup.sort(function (a, b) { return (b.ms - a.ms) });
		var wordsCompletersThisGroupUniqe = [];
		for (var i = 0, l = wordsCompletersThisGroup.length; i < l; i++)
		    if (!wordsCompletersThisGroupUniqe.find(x => x.name === wordsCompletersThisGroup[i].name))
		        wordsCompletersThisGroupUniqe.push(wordsCompletersThisGroup[i]);

		syntaxCompletersThisGroup.sort(function (a, b) { return (b.ms - a.ms) });
		var syntaxCompletersThisGroupUniqe = [];
		for (var i = 0, l = syntaxCompletersThisGroup.length; i < l; i++)
		    if (!syntaxCompletersThisGroupUniqe.find(x => x.name === syntaxCompletersThisGroup[i].name))
		        syntaxCompletersThisGroupUniqe.push(syntaxCompletersThisGroup[i]);
		//wordsCompletersThisGroup.length = 0;//disabling the feature for anonymity purposes
		//syntaxCompletersThisGroup.length = 0;
      
		var htmlString;
		htmlString = wordsCompletersThisGroupUniqe.map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
		var helperstr = "תלמידי קבוצת " + "\"" + currentGroup + "\"" + " שהשלימו תרגול מילולי";
		htmlString = "<caption class=\"mycaption\">" + helperstr + "</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("wordCompletions").innerHTML = htmlString;
		
		htmlString = "";
		htmlString = syntaxCompletersThisGroupUniqe.map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
		}).join('');
		var helperstr = "תלמידי קבוצת " + "\"" + currentGroup + "\"" + " שהשלימו תרגול לוגי";
		htmlString = "<caption class=\"mycaption\">" + helperstr + "</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("syntaxCompletions").innerHTML = htmlString;
		
		wordsCompleters.sort(function (a, b) { return a.time - b.time });
		var wordsCompletersUniqe = [];
		for (var i = 0, l = wordsCompleters.length; i < l; i++)
		    if (!wordsCompletersUniqe.find(x => x.name === wordsCompleters[i].name))
		        wordsCompletersUniqe.push(wordsCompleters[i]);
		syntaxCompleters.sort(function (a, b) { return a.time - b.time });
		var syntaxCompletersUniqe = [];
		for (var i = 0, l = syntaxCompleters.length; i < l; i++)
		    if (!syntaxCompletersUniqe.find(x => x.name === syntaxCompleters[i].name))
		        syntaxCompletersUniqe.push(syntaxCompleters[i]);
		
		htmlString = wordsCompletersUniqe.slice(0, 4).map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.time + '</td><td class=\"mytd\">' + completer.group + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
			htmlString = "<caption class=\"mycaption\">הזריזים מבין כל הקבוצות</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">שניות</th><th class=\"myth\">קבוצה</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
		document.getElementById("wordsFastestCompletions").innerHTML = htmlString;
		
		htmlString = syntaxCompletersUniqe.slice(0,4).map(function (completer) {
			return '<tr class=\"mytr\"><td class=\"mytd\">' + completer.date + '</td><td class=\"mytd\">' + completer.time + '</td><td class=\"mytd\">' + completer.group + '</td><td class=\"mytd\">' + completer.name + '</td></tr>';
			}).join('');
			htmlString = "<caption class=\"mycaption\">הזריזים מבין כל הקבוצות</caption><tr class=\"mytr\"><th class=\"myth\">תאריך</th><th class=\"myth\">שניות</th><th class=\"myth\">קבוצה</th><th class=\"myth\">תלמיד</th></tr>" + htmlString;
			document.getElementById("syntaxFastestCompletions").innerHTML = htmlString;
			document.getElementById("syntax_button").disabled = false;
			document.getElementById("words_button").disabled = false;
		
	}, function (error) {
		console.log("Error: " + error.code);
	});
    */