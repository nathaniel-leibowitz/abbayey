var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/hanisrafim_baniskalim.png",
		"אם כך": "אם כך.text",
		"ראה את הסוף": "ראה את הסוף.text",
		"שנה, לימד, ציטט": "שנה, לימד, ציטט.text",
		"כך": "כך.text",
		"מה קשור לכאן": "מה קשור לכאן.text",
		"תוציא את הדין מתוך זה ש": "תוציא את הדין מתוך זה ש.text",
		"בנו": "בנו.text",
		"תשנה, תלמד, תאמר": "תשנה, תלמד, תאמר.text",
		"שהרוב": "שהרוב.text",
		"הם, הינם": "הם, הינם.text",
		"שם": "שם.text",
	},
	"parameters": {
		"numOfItems": 18,
		"placeholders": [
			{ "id": "אם כך_1", "loc": { "x": 33, "y": -122 }, "rect": { "width": 104, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ראה את הסוף_2", "loc": { "x": -116, "y": -122 }, "rect": { "width": 170, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנה, לימד, ציטט_3", "loc": { "x": -57, "y": -383 }, "rect": { "width": 71, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_4", "loc": { "x": -299, "y": -253 }, "rect": { "width": 53, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה קשור לכאן_5", "loc": { "x": 223, "y": -209 }, "rect": { "width": 152, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תוציא את הדין מתוך זה ש_6", "loc": { "x": -159, "y": -209 }, "rect": { "width": 128, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_7", "loc": { "x": 177, "y": -340 }, "rect": { "width": 71, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בנו_8", "loc": { "x": 48, "y": -253 }, "rect": { "width": 77, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תשנה, תלמד, תאמר_9", "loc": { "x": -200, "y": -253 }, "rect": { "width": 110, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תשנה, תלמד, תאמר_10", "loc": { "x": -127, "y": -166 }, "rect": { "width": 113, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_11", "loc": { "x": -30, "y": -166 }, "rect": { "width": 53, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרוב_12", "loc": { "x": -278, "y": -209 }, "rect": { "width": 95, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הם, הינם_13", "loc": { "x": 145, "y": -166 }, "rect": { "width": 71, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה קשור לכאן_14", "loc": { "x": 224, "y": -36 }, "rect": { "width": 152, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהרוב_15", "loc": { "x": -279, "y": -36 }, "rect": { "width": 92, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הם, הינם_16", "loc": { "x": 141, "y": 8 }, "rect": { "width": 68, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שם_17", "loc": { "x": -14, "y": 8 }, "rect": { "width": 71, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תוציא את הדין מתוך זה ש_18", "loc": { "x": -160, "y": -36 }, "rect": { "width": 128, "height": 37 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "אם כך", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אם כך_1"],
			},
			{
				"type": "ראה את הסוף", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראה את הסוף_2"],
			},
			{
				"type": "שנה, לימד, ציטט", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנה, לימד, ציטט_3"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_4", "כך_11"],
			},
			{
				"type": "מה קשור לכאן", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה קשור לכאן_5", "מה קשור לכאן_14"],
			},
			{
				"type": "תוציא את הדין מתוך זה ש", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תוציא את הדין מתוך זה ש_6", "תוציא את הדין מתוך זה ש_18"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_7", "בנו_8"],
			},
			{
				"type": "בנו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בנו_7", "בנו_8"],
			},
			{
				"type": "תשנה, תלמד, תאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תשנה, תלמד, תאמר_9", "תשנה, תלמד, תאמר_10"],
			},
			{
				"type": "תשנה, תלמד, תאמר", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תשנה, תלמד, תאמר_9", "תשנה, תלמד, תאמר_10"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_4", "כך_11"],
			},
			{
				"type": "שהרוב", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרוב_12", "שהרוב_15"],
			},
			{
				"type": "הם, הינם", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הם, הינם_13", "הם, הינם_16"],
			},
			{
				"type": "מה קשור לכאן", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה קשור לכאן_5", "מה קשור לכאן_14"],
			},
			{
				"type": "שהרוב", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהרוב_12", "שהרוב_15"],
			},
			{
				"type": "הם, הינם", 
				"score": 1,
				"loc": { "x": -700, "y": -320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הם, הינם_13", "הם, הינם_16"],
			},
			{
				"type": "שם", 
				"score": 1,
				"loc": { "x": -700, "y": 320 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שם_17"],
			},
			{
				"type": "תוציא את הדין מתוך זה ש", 
				"score": 1,
				"loc": { "x": -700, "y": -360 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תוציא את הדין מתוך זה ש_6", "תוציא את הדין מתוך זה ש_18"],
			},
		],
		"ex_name": "kidushin_32a_hanisrafim_baniskalim_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}