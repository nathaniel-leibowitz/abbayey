function addBabametziaHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {
        case "babametzia_21a_elu_metziot_shelo_syntax":
            hints.push({ text: 'המשנה פותחת בשאלה, מקמו בקצה שלה סימן שאלה', symbol: "question" });
            hints.push({ text: 'המשנה מציגה שלוש דעות. הפרידו ביניהם בעזרת הנקודות', symbol: "period" });
            hints.push({ text: 'כל אחת מהדעות מנוסחת כמשפט תנאי. הפרידו בין חלקיהן בעזרת הקו המפריד', symbol: "hyphen" });
            hints.push({ text: 'אתגר: מקמו את הנקודותיים במקום הכי מתאים', symbol: "colon" });
            break;
        case "babametzia_21a_matza_perot_mefuzarin_syntax":
            hints.push({ text: 'סוגיא מעט קשה לקריאה, אך נפענח אותה בקלות לפי שלבים', symbol: "" });
            hints.push({ text: 'הסוגיא מצטטת מהמשנה. הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'הגמרא שואלת שאלה לגבי הציטוט. סמנו אותה בסימן השאלה', symbol: "question" });
            hints.push({ text: 'החלק המרכזי של הסוגיא היא קושייתו הארוכה של המקשן. זהו היכן הוא מסיים את התקפתו וסמנו', symbol: "qushia" });
            hints.push({ text: 'הקושיא מכילה בתוכה גם שאלה רגילה. אתרוה וסמנו', symbol: "question" });
            hints.push({ text: 'זהו היכן התרצן מסיים להשיב את תירוצו הארוך וסמנו', symbol: "resolution" });
            hints.push({ text: 'אתגר: גם הקושיא וגם התירוץ מורכבים ממשפטי תנאי. הפרידו בין חלקיהם בעזרת הקו', symbol: "hyphen" });
            hints.push({ text: 'אתגר: מקמו את הנקודותיים במקום הכי מתאים', symbol: "colon" });
            break;
        case "babametzia_21a_hatzi_kav_beshtey_amot_syntax":
            hints.push({ text: 'סוגיא מעט קשה לקריאה, אך נפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'רב ירמיה מתאר מקרה שאותו הוא רוצה לברר, מקמו אחריו את הנקודותיים', symbol: "colon" });
            hints.push({ text: 'בכדי לברר זאת הוא מצטט את רב יצחק. הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'רב ירמיה מעלה שתי אפשרויות להבין את הציטוט. מקמו סימן שאלה אחרי כל אחת מהאפשרויות', symbol: "question" });
            hints.push({ text: 'אתגר: שתי האפשרויות מורכבות ממשפטי תנאי. הפרידו בין חלקיהם בעזרת הקו', symbol: "hyphen" });
            break;

        case "babametzia_21a_kabayim_beshmone_amot_syntax":
            hints.push({ text: 'סוגיא מעט קשה לקריאה, אך נפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'רב ירמיה מתאר מקרה שאותו הוא רוצה לברר, מקמו אחריו את הנקודותיים', symbol: "colon" });
            hints.push({ text: 'בכדי לברר זאת הוא מצטט את רב יצחק. הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'רב ירמיה מעלה שתי אפשרויות להבין את הציטוט. מקמו סימן שאלה אחרי כל אחת מהאפשרויות', symbol: "question" });
            hints.push({ text: 'אתגר: שתי האפשרויות מורכבות ממשפטי תנאי. הפרידו בין חלקיהם בעזרת הקו', symbol: "hyphen" });
            break;

        case "babametzia_21a_kav_shumshumin_syntax":
            hints.push({ text: 'סוגיא מעט קשה לקריאה, אך נפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'רב ירמיה מתאר מקרה שאותו הוא רוצה לברר, מקמו אחריו את הנקודותיים', symbol: "colon" });
            hints.push({ text: 'בכדי לברר זאת הוא מצטט את רב יצחק. הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'רב ירמיה מעלה שתי אפשרויות להבין את הציטוט. מקמו סימן שאלה אחרי כל אחת מהאפשרויות', symbol: "question" });
            hints.push({ text: 'אתגר: שתי האפשרויות מורכבות ממשפטי תנאי. הפרידו בין חלקיהם בעזרת הקו', symbol: "hyphen" });
            break;

        case "babametzia_21a_kav_tamrey_syntax":
            hints.push({ text: 'סוגיא מעט קשה לקריאה, אך נפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'רב ירמיה מתאר מקרה שאותו הוא רוצה לברר, מקמו אחריו את הנקודותיים', symbol: "colon" });
            hints.push({ text: 'בכדי לברר זאת הוא מצטט את רב יצחק. הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'רב ירמיה מעלה שתי אפשרויות להבין את הציטוט. מקמו סימן שאלה אחרי כל אחת מהאפשרויות', symbol: "question" });
            hints.push({ text: 'אתגר: שתי האפשרויות מורכבות ממשפטי תנאי. הפרידו בין חלקיהם בעזרת הקו', symbol: "hyphen" });
            break;
    }

    addHint(div, hints);
}
