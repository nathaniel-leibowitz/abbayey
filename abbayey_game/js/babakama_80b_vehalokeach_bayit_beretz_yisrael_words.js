var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_80b_bottom.png",
		"הקונה": "הקונה.text",
		"שטר מכר, טאבו": "שטר מכר, טאבו.text",
		"יעלה על דעתך, האמנם": "יעלה על דעתך, האמנם.text",
		"הכא נמי, כך גם כאן": "הכא נמי, כך גם כאן.text",
		"שם, בסוגיא אחרת אך דומה": "שם, בסוגיא אחרת אך דומה.text",
		"ואף על גב": "ואף על גב.text",
		"איסור מדברי חכמים, לא מדאורייתא": "איסור מדברי חכמים, לא מדאורייתא.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "הקונה_1", "loc": { "x": -61, "y": 174 }, "rect": { "width": 89, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שטר מכר, טאבו_2", "loc": { "x": 390, "y": 210 }, "rect": { "width": 50, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יעלה על דעתך, האמנם_3", "loc": { "x": 2, "y": 210 }, "rect": { "width": 161, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הכא נמי, כך גם כאן_4", "loc": { "x": 221, "y": 246 }, "rect": { "width": 47, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שם, בסוגיא אחרת אך דומה_5", "loc": { "x": -361, "y": 210 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואף על גב_6", "loc": { "x": -105, "y": 246 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "איסור מדברי חכמים, לא מדאורייתא_7", "loc": { "x": -391, "y": 246 }, "rect": { "width": 68, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הקונה_8", "loc": { "x": 154, "y": 318 }, "rect": { "width": 80, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "הקונה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הקונה_1", "הקונה_8"],
			},
			{
				"type": "שטר מכר, טאבו", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שטר מכר, טאבו_2"],
			},
			{
				"type": "יעלה על דעתך, האמנם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יעלה על דעתך, האמנם_3"],
			},
			{
				"type": "הכא נמי, כך גם כאן", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הכא נמי, כך גם כאן_4"],
			},
			{
				"type": "שם, בסוגיא אחרת אך דומה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שם, בסוגיא אחרת אך דומה_5"],
			},
			{
				"type": "ואף על גב", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואף על גב_6"],
			},
			{
				"type": "איסור מדברי חכמים, לא מדאורייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "איסור מדברי חכמים, לא מדאורייתא_7"],
			},
			{
				"type": "הקונה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הקונה_1", "הקונה_8"],
			},
		],
		"ex_name": "babakama_80b_vehalokeach_bayit_beretz_yisrael_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}