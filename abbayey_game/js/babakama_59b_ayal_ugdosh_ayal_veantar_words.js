var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama59b_bottom.png",
		"נאמר": "נאמר.text",
		"הרי אמר": "הרי אמר.text",
		"כאן": "כאן.text",
		"בשומר": "בשומר.text",
		"ואשמור": "ואשמור.text",
		"היכנס": "היכנס.text",
		"ותערום את התבואה שלך": "ותערום את התבואה שלך.text",
		"בית התבואה, אסם": "בית התבואה, אסם.text",
		"אנו עוסקים": "אנו עוסקים.text",
		"שאמר לו": "שאמר לו.text",
		"המשנה": "המשנה.text",
		"שאם": "שאם.text",
		"החבר אחראי רק כשאמר זאת במפורש": "החבר אחראי רק כשאמר זאת במפורש.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "נאמר_1", "loc": { "x": -258, "y": 48 }, "rect": { "width": 62, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי אמר_2", "loc": { "x": 222, "y": 84 }, "rect": { "width": 74, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן_3", "loc": { "x": 387, "y": 120 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בשומר_4", "loc": { "x": 304, "y": 120 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ואשמור_5", "loc": { "x": -332, "y": 120 }, "rect": { "width": 74, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכנס_6", "loc": { "x": -107, "y": 120 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היכנס_7", "loc": { "x": -257, "y": 120 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ותערום את התבואה שלך_8", "loc": { "x": -181, "y": 120 }, "rect": { "width": 71, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית התבואה, אסם_9", "loc": { "x": 221, "y": 120 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אנו עוסקים_10", "loc": { "x": 125, "y": 120 }, "rect": { "width": 83, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאמר לו_11", "loc": { "x": -35, "y": 120 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "המשנה_12", "loc": { "x": -326, "y": 48 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאם_13", "loc": { "x": 391, "y": 84 }, "rect": { "width": 47, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "החבר אחראי רק כשאמר זאת במפורש_14", "loc": { "x": -61, "y": 84 }, "rect": { "width": 464, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "נאמר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נאמר_1"],
			},
			{
				"type": "הרי אמר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי אמר_2"],
			},
			{
				"type": "כאן", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן_3"],
			},
			{
				"type": "בשומר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בשומר_4"],
			},
			{
				"type": "ואשמור", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ואשמור_5"],
			},
			{
				"type": "היכנס", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכנס_6", "היכנס_7"],
			},
			{
				"type": "היכנס", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היכנס_6", "היכנס_7"],
			},
			{
				"type": "ותערום את התבואה שלך", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ותערום את התבואה שלך_8"],
			},
			{
				"type": "בית התבואה, אסם", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית התבואה, אסם_9"],
			},
			{
				"type": "אנו עוסקים", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אנו עוסקים_10"],
			},
			{
				"type": "שאמר לו", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאמר לו_11"],
			},
			{
				"type": "המשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "המשנה_12"],
			},
			{
				"type": "שאם", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאם_13"],
			},
			{
				"type": "החבר אחראי רק כשאמר זאת במפורש", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "החבר אחראי רק כשאמר זאת במפורש_14"],
			},
		],
		"ex_name": "babakama_59b_ayal_ugdosh_ayal_veantar_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}