var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_bottom.png",
		"היא, האשה": "היא, האשה.text",
		"מאיפה לנו, מנין לומדים זאת": "מאיפה לנו, מנין לומדים זאת.text",
		"מצאנו, ידוע לנו": "מצאנו, ידוע לנו.text",
		"שכתוב בפסוק": "שכתוב בפסוק.text",
		"שנה, למד": "שנה, למד.text",
		"בית המדרש של ר ישמעל": "בית המדרש של ר ישמעל.text",
		"חייבת למול את בנה": "חייבת למול את בנה.text",
	},
	"parameters": {
		"numOfItems": 10,
		"placeholders": [
			{ "id": "היא, האשה_1", "loc": { "x": -259, "y": -173 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_2", "loc": { "x": -326, "y": -173 }, "rect": { "width": 53, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצאנו, ידוע לנו_3", "loc": { "x": -315, "y": -136 }, "rect": { "width": 86, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מאיפה לנו, מנין לומדים זאת_4", "loc": { "x": 363, "y": -100 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_5", "loc": { "x": 287, "y": -64 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנה, למד_6", "loc": { "x": 300, "y": -100 }, "rect": { "width": 50, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בית המדרש של ר ישמעל_7", "loc": { "x": 170, "y": -100 }, "rect": { "width": 191, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "חייבת למול את בנה_8", "loc": { "x": 437, "y": -136 }, "rect": { "width": 98, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_9", "loc": { "x": 334, "y": -136 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שכתוב בפסוק_10", "loc": { "x": -331, "y": -64 }, "rect": { "width": 77, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "היא, האשה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היא, האשה_1"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_2", "מאיפה לנו, מנין לומדים זאת_4"],
			},
			{
				"type": "מצאנו, ידוע לנו", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצאנו, ידוע לנו_3"],
			},
			{
				"type": "מאיפה לנו, מנין לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מנין לומדים זאת_2", "מאיפה לנו, מנין לומדים זאת_4"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_5", "שכתוב בפסוק_9", "שכתוב בפסוק_10"],
			},
			{
				"type": "שנה, למד", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנה, למד_6"],
			},
			{
				"type": "בית המדרש של ר ישמעל", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בית המדרש של ר ישמעל_7"],
			},
			{
				"type": "חייבת למול את בנה", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "חייבת למול את בנה_8"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_5", "שכתוב בפסוק_9", "שכתוב בפסוק_10"],
			},
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_5", "שכתוב בפסוק_9", "שכתוב בפסוק_10"],
			},
		],
		"ex_name": "kidushin_29a_lemulu_ihi_minalan_delo_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}