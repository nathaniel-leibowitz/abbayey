var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_32atop.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
	},
	"parameters": {
		"numOfItems": 13,
		"placeholders": [
			{ "id": "biblequote_1", "loc": { "x": -203, "y": 278 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_2", "loc": { "x": -2, "y": 313 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_3", "loc": { "x": -26, "y": 313 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_4", "loc": { "x": -262, "y": 313 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_5", "loc": { "x": 11, "y": 347 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": -121, "y": 347 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_7", "loc": { "x": -140, "y": 347 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_8", "loc": { "x": 129, "y": 381 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_9", "loc": { "x": -185, "y": 381 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_10", "loc": { "x": 15, "y": 416 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_11", "loc": { "x": 16, "y": 209 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_12", "loc": { "x": -24, "y": 244 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_13", "loc": { "x": 100, "y": 278 }, "rect": { "width": 18, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_5", "biblequote_6"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_5", "biblequote_6"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_3", "qushia_7", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_4", "resolution_8", "resolution_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_5", "biblequote_6"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_1", "biblequote_2", "biblequote_5", "biblequote_6"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_3", "qushia_7", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_4", "resolution_8", "resolution_10"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_3", "qushia_7", "qushia_9"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -670, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_4", "resolution_8", "resolution_10"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_11"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_12"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_13"],
			},
		],
		"ex_name": "kidushin_32a_mishel_me_kara_shirayey_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}