var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/taanit_2ab.png",
		"שכתוב בפסוק": "שכתוב בפסוק.text",
		"וכולי": "וכולי.text",
		"מדוע, מה הסיבה": "מדוע, מה הסיבה.text",
		"לא החשיב, לא הזכיר": "לא החשיב, לא הזכיר.text",
		"לזה, למפתח הרביעי": "לזה, למפתח הרביעי.text",
		"שקול, אותו דבר": "שקול, אותו דבר.text",
		"בארץ ישראל": "בארץ ישראל.text",
		"הוסיפו ואמרו גם": "הוסיפו ואמרו גם.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "שכתוב בפסוק_1", "loc": { "x": -117, "y": 309 }, "rect": { "width": 80, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכולי_2", "loc": { "x": 141, "y": 345 }, "rect": { "width": 38, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מדוע, מה הסיבה_3", "loc": { "x": -63, "y": 345 }, "rect": { "width": 131, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לא החשיב, לא הזכיר_4", "loc": { "x": -218, "y": 345 }, "rect": { "width": 161, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לזה, למפתח הרביעי_5", "loc": { "x": -334, "y": 345 }, "rect": { "width": 53, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שקול, אותו דבר_6", "loc": { "x": -103, "y": 381 }, "rect": { "width": 56, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בארץ ישראל_7", "loc": { "x": -194, "y": 273 }, "rect": { "width": 101, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוסיפו ואמרו גם_8", "loc": { "x": -308, "y": 273 }, "rect": { "width": 107, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "שכתוב בפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שכתוב בפסוק_1"],
			},
			{
				"type": "וכולי", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכולי_2"],
			},
			{
				"type": "מדוע, מה הסיבה", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מדוע, מה הסיבה_3"],
			},
			{
				"type": "לא החשיב, לא הזכיר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לא החשיב, לא הזכיר_4"],
			},
			{
				"type": "לזה, למפתח הרביעי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לזה, למפתח הרביעי_5"],
			},
			{
				"type": "שקול, אותו דבר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שקול, אותו דבר_6"],
			},
			{
				"type": "בארץ ישראל", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בארץ ישראל_7"],
			},
			{
				"type": "הוסיפו ואמרו גם", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוסיפו ואמרו גם_8"],
			},
		],
		"ex_name": "taanit_2b_bemaarava_umrey_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}