var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"colon": "storage/miniGames/gemara/syntax/colon.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"wrongly": "storage/miniGames/gemara/syntax/wrongly.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
	},
	"parameters": {
		"numOfItems": 42,
		"placeholders": [
			{ "id": "qushia_1", "loc": { "x": 69, "y": -216 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_2", "loc": { "x": -4, "y": -216 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_3", "loc": { "x": -320, "y": -216 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_4", "loc": { "x": 145, "y": -179 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_5", "loc": { "x": -209, "y": -106 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_6", "loc": { "x": -326, "y": -106 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_7", "loc": { "x": 305, "y": -70 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_8", "loc": { "x": 157, "y": -70 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_9", "loc": { "x": 207, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_10", "loc": { "x": 93, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_11", "loc": { "x": 76, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_12", "loc": { "x": -94, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_13", "loc": { "x": -288, "y": -34 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_14", "loc": { "x": 460, "y": 3 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_15", "loc": { "x": -179, "y": 3 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_16", "loc": { "x": 456, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_17", "loc": { "x": -30, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_18", "loc": { "x": -191, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_19", "loc": { "x": -311, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_20", "loc": { "x": -328, "y": 39 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_21", "loc": { "x": 447, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_22", "loc": { "x": 422, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_23", "loc": { "x": 384, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_24", "loc": { "x": 166, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_25", "loc": { "x": -102, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_26", "loc": { "x": -258, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_27", "loc": { "x": -382, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_28", "loc": { "x": -407, "y": 76 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_29", "loc": { "x": 381, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_30", "loc": { "x": 113, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_31", "loc": { "x": -10, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_32", "loc": { "x": -79, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_33", "loc": { "x": -201, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_34", "loc": { "x": -219, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_35", "loc": { "x": -383, "y": 112 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_36", "loc": { "x": 77, "y": 148 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_37", "loc": { "x": -387, "y": 148 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "colon_38", "loc": { "x": 448, "y": 185 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_39", "loc": { "x": 168, "y": 185 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_40", "loc": { "x": 20, "y": 185 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "wrongly_41", "loc": { "x": -375, "y": 185 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 2, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_42", "loc": { "x": 454, "y": 221 }, "rect": { "width": 20, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 12, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -700, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2", "colon_38"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -700, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -730, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -670, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -670, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -760, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -760, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -640, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -670, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -670, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -760, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -760, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -640, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -640, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -790, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -640, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -790, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -790, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -610, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -610, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -820, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -580, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -850, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_18", "biblequote_19", "biblequote_22", "biblequote_23", "biblequote_26", "biblequote_27", "biblequote_30", "biblequote_31", "biblequote_32", "biblequote_33"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -610, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -820, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -820, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -580, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_1", "qushia_12", "qushia_15", "qushia_17", "qushia_21", "qushia_25", "qushia_29", "qushia_35", "qushia_37"],
			},
			{
				"type": "colon", 
				"score": 1,
				"loc": { "x": -730, "y": 100 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "colon_2", "colon_38"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -790, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_3", "hyphen_5", "hyphen_7", "hyphen_9", "hyphen_13", "hyphen_39"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -640, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "wrongly", 
				"score": 1,
				"loc": { "x": -790, "y": -350 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "wrongly_4", "wrongly_6", "wrongly_8", "wrongly_10", "wrongly_40", "wrongly_41"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -580, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_11", "resolution_14", "resolution_16", "resolution_20", "resolution_24", "resolution_28", "resolution_34", "resolution_36", "resolution_42"],
			},
		],
		"ex_name": "babakama_60a_kotzim_gadish_kama_vesade_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}