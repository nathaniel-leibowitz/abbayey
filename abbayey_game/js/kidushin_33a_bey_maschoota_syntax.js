var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33a_middle.png",
		"speakend": "storage/miniGames/gemara/syntax/speakend.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"speakbegin": "storage/miniGames/gemara/syntax/speakbegin.png",
		"period": "storage/miniGames/gemara/syntax/period.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"assist": "storage/miniGames/gemara/syntax/assist.png",
		"nonassist": "storage/miniGames/gemara/syntax/nonassist.png",
	},
	"parameters": {
		"numOfItems": 23,
		"placeholders": [
			{ "id": "speakend_1", "loc": { "x": -463, "y": -233 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_2", "loc": { "x": 391, "y": -451 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_3", "loc": { "x": -283, "y": -451 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_4", "loc": { "x": -302, "y": -451 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_5", "loc": { "x": -356, "y": -451 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_6", "loc": { "x": 59, "y": -364 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_7", "loc": { "x": 376, "y": -320 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "period_8", "loc": { "x": 406, "y": -277 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 26, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_9", "loc": { "x": 420, "y": -233 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakbegin_10", "loc": { "x": 485, "y": -190 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "speakend_11", "loc": { "x": 116, "y": -190 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_12", "loc": { "x": 10, "y": -190 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_13", "loc": { "x": -501, "y": -190 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_14", "loc": { "x": -557, "y": -190 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_15", "loc": { "x": 311, "y": -146 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_16", "loc": { "x": 83, "y": -146 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_17", "loc": { "x": -96, "y": -146 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_18", "loc": { "x": 185, "y": -103 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_19", "loc": { "x": 447, "y": -59 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": -7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "assist_20", "loc": { "x": 425, "y": -59 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 7, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "nonassist_21", "loc": { "x": 83, "y": -59 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_22", "loc": { "x": -310, "y": -407 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_23", "loc": { "x": -25, "y": -277 }, "rect": { "width": 22, "height": 39 }, "isReused": false, "offsetX": 0, "offsetY": 13, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -700, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_1", "speakend_7", "speakend_11"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_18"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_3"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_19"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_5"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -700, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_6", "speakbegin_9", "speakbegin_10"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -730, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_1", "speakend_7", "speakend_11"],
			},
			{
				"type": "period", 
				"score": 1,
				"loc": { "x": -700, "y": -50 },
				"scale": { "x": 0.06, "y": 0.06 },
				"placeholderIds": [ "period_8"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -730, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_6", "speakbegin_9", "speakbegin_10"],
			},
			{
				"type": "speakbegin", 
				"score": 1,
				"loc": { "x": -670, "y": -300 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakbegin_6", "speakbegin_9", "speakbegin_10"],
			},
			{
				"type": "speakend", 
				"score": 1,
				"loc": { "x": -670, "y": -250 },
				"scale": { "x": 0.07, "y": 0.07 },
				"placeholderIds": [ "speakend_1", "speakend_7", "speakend_11"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -730, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_14"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -670, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -760, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_17"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_2", "quotebegin_18"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_4", "quoteend_19"],
			},
			{
				"type": "assist", 
				"score": 1,
				"loc": { "x": -700, "y": -150 },
				"scale": { "x": 0.25, "y": 0.25 },
				"placeholderIds": [ "assist_20"],
			},
			{
				"type": "nonassist", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 0.2, "y": 0.2 },
				"placeholderIds": [ "nonassist_21"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -640, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -790, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_12", "hyphen_13", "hyphen_15", "hyphen_16", "hyphen_22", "hyphen_23"],
			},
		],
		"ex_name": "kidushin_33a_bey_maschoota_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}