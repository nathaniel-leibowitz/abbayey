var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_94b_middle.png",
		"מקשים מברייתא": "מקשים מברייתא.text",
		"הם עצמם, הילדים": "הם עצמם, הילדים.text",
		"הרי ש, מכאן ש": "הרי ש, מכאן ש.text",
		"גם כן": "גם כן.text",
		"וזה שכתוב": "וזה שכתוב.text",
		"בהם, בילדים": "בהם, בילדים.text",
		"שהיה צריך": "שהיה צריך.text",
		"לשנות, לכתוב": "לשנות, לכתוב.text",
		"בסוף הברייתא": "בסוף הברייתא.text",
		"שנה, כתב": "שנה, כתב.text",
		"דבר מיוחד, מובחן, מזוהה": "דבר מיוחד, מובחן, מזוהה.text",
		"בתחילת הברייתא": "בתחילת הברייתא.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "מקשים מברייתא_1", "loc": { "x": -110, "y": -111 }, "rect": { "width": 80, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הם עצמם, הילדים_2", "loc": { "x": 328, "y": -6 }, "rect": { "width": 68, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי ש, מכאן ש_3", "loc": { "x": 131, "y": -6 }, "rect": { "width": 38, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם כן_4", "loc": { "x": 99, "y": 28 }, "rect": { "width": 38, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וזה שכתוב_5", "loc": { "x": 295, "y": 63 }, "rect": { "width": 131, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בהם, בילדים_6", "loc": { "x": 180, "y": 63 }, "rect": { "width": 86, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שהיה צריך_7", "loc": { "x": -16, "y": 63 }, "rect": { "width": 104, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם כן_8", "loc": { "x": 68, "y": 168 }, "rect": { "width": 38, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בהם, בילדים_9", "loc": { "x": -18, "y": 168 }, "rect": { "width": 86, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לשנות, לכתוב_10", "loc": { "x": -113, "y": 63 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בסוף הברייתא_11", "loc": { "x": 329, "y": 98 }, "rect": { "width": 62, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שנה, כתב_12", "loc": { "x": 229, "y": 168 }, "rect": { "width": 50, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "דבר מיוחד, מובחן, מזוהה_13", "loc": { "x": 276, "y": 133 }, "rect": { "width": 170, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתחילת הברייתא_14", "loc": { "x": 147, "y": 168 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מקשים מברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מקשים מברייתא_1"],
			},
			{
				"type": "הם עצמם, הילדים", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הם עצמם, הילדים_2"],
			},
			{
				"type": "הרי ש, מכאן ש", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי ש, מכאן ש_3"],
			},
			{
				"type": "גם כן", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם כן_4", "גם כן_8"],
			},
			{
				"type": "וזה שכתוב", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וזה שכתוב_5"],
			},
			{
				"type": "בהם, בילדים", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בהם, בילדים_6", "בהם, בילדים_9"],
			},
			{
				"type": "שהיה צריך", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שהיה צריך_7"],
			},
			{
				"type": "גם כן", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם כן_4", "גם כן_8"],
			},
			{
				"type": "בהם, בילדים", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בהם, בילדים_6", "בהם, בילדים_9"],
			},
			{
				"type": "לשנות, לכתוב", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לשנות, לכתוב_10"],
			},
			{
				"type": "בסוף הברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בסוף הברייתא_11"],
			},
			{
				"type": "שנה, כתב", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שנה, כתב_12"],
			},
			{
				"type": "דבר מיוחד, מובחן, מזוהה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "דבר מיוחד, מובחן, מזוהה_13"],
			},
			{
				"type": "בתחילת הברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתחילת הברייתא_14"],
			},
		],
		"ex_name": "babakama_94b_hiniach_lahem_avihem_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}