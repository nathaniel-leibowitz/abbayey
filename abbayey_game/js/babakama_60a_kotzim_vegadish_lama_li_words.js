var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_60a_middle.png",
		"נומר": "נומר.text",
		"התורה, הפסוק": "התורה, הפסוק.text",
		"צריך, הכפילות נחוצה": "צריך, הכפילות נחוצה.text",
		"הוה אמינא, הייתי אומר בטעות": "הוה אמינא, הייתי אומר בטעות.text",
		"מצוי, רגיל": "מצוי, רגיל.text",
		"למה נחוצה לנו הכפילות": "למה נחוצה לנו הכפילות.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "נומר_1", "loc": { "x": -247, "y": -95 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נומר_2", "loc": { "x": 168, "y": -22 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הפסוק_3", "loc": { "x": -97, "y": -241 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "התורה, הפסוק_4", "loc": { "x": -111, "y": -168 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריך, הכפילות נחוצה_5", "loc": { "x": 30, "y": -204 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוה אמינא, הייתי אומר בטעות_6", "loc": { "x": -350, "y": -204 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הוה אמינא, הייתי אומר בטעות_7", "loc": { "x": 235, "y": -59 }, "rect": { "width": 134, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצוי, רגיל_8", "loc": { "x": -284, "y": -168 }, "rect": { "width": 83, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצוי, רגיל_9", "loc": { "x": -343, "y": -131 }, "rect": { "width": 65, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצוי, רגיל_10", "loc": { "x": 21, "y": -131 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למה נחוצה לנו הכפילות_11", "loc": { "x": 97, "y": -241 }, "rect": { "width": 95, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "נומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נומר_1", "נומר_2"],
			},
			{
				"type": "נומר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נומר_1", "נומר_2"],
			},
			{
				"type": "התורה, הפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הפסוק_3", "התורה, הפסוק_4"],
			},
			{
				"type": "התורה, הפסוק", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "התורה, הפסוק_3", "התורה, הפסוק_4"],
			},
			{
				"type": "צריך, הכפילות נחוצה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך, הכפילות נחוצה_5"],
			},
			{
				"type": "הוה אמינא, הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוה אמינא, הייתי אומר בטעות_6", "הוה אמינא, הייתי אומר בטעות_7"],
			},
			{
				"type": "הוה אמינא, הייתי אומר בטעות", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הוה אמינא, הייתי אומר בטעות_6", "הוה אמינא, הייתי אומר בטעות_7"],
			},
			{
				"type": "מצוי, רגיל", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצוי, רגיל_8", "מצוי, רגיל_9", "מצוי, רגיל_10"],
			},
			{
				"type": "מצוי, רגיל", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצוי, רגיל_8", "מצוי, רגיל_9", "מצוי, רגיל_10"],
			},
			{
				"type": "מצוי, רגיל", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצוי, רגיל_8", "מצוי, רגיל_9", "מצוי, רגיל_10"],
			},
			{
				"type": "למה נחוצה לנו הכפילות", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למה נחוצה לנו הכפילות_11"],
			},
		],
		"ex_name": "babakama_60a_kotzim_vegadish_lama_li_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}