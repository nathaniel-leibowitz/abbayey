var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama57a_top.png",
		"והרי": "והרי.text",
		"כאן": "כאן.text",
		"במה מדובר": "במה מדובר.text",
		"ושני ענינים כתב": "ושני ענינים כתב.text",
		"וכך כתב": "וכך כתב.text",
		"מצוי וסביר": "מצוי וסביר.text",
		"שיכנס": "שיכנס.text",
		"ויצא": "ויצא.text",
		"ראה": "ראה.text",
		"מה פירוש": "מה פירוש.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "והרי_1", "loc": { "x": -4, "y": -146 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כאן_2", "loc": { "x": 54, "y": -109 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "במה מדובר_3", "loc": { "x": -72, "y": -109 }, "rect": { "width": 167, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ושני ענינים כתב_4", "loc": { "x": 32, "y": -73 }, "rect": { "width": 137, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "וכך כתב_5", "loc": { "x": -97, "y": -73 }, "rect": { "width": 116, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצוי וסביר_6", "loc": { "x": -116, "y": -36 }, "rect": { "width": 77, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מצוי וסביר_7", "loc": { "x": 333, "y": 74 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיכנס_8", "loc": { "x": 333, "y": 1 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שיכנס_9", "loc": { "x": 246, "y": 74 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ויצא_10", "loc": { "x": 255, "y": 1 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ויצא_11", "loc": { "x": 161, "y": 74 }, "rect": { "width": 62, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ראה_12", "loc": { "x": 187, "y": 1 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ראה_13", "loc": { "x": 19, "y": 74 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה פירוש_14", "loc": { "x": -130, "y": -219 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "והרי", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי_1"],
			},
			{
				"type": "כאן", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כאן_2"],
			},
			{
				"type": "במה מדובר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "במה מדובר_3"],
			},
			{
				"type": "ושני ענינים כתב", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושני ענינים כתב_4"],
			},
			{
				"type": "וכך כתב", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכך כתב_5"],
			},
			{
				"type": "מצוי וסביר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצוי וסביר_6", "מצוי וסביר_7"],
			},
			{
				"type": "מצוי וסביר", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מצוי וסביר_6", "מצוי וסביר_7"],
			},
			{
				"type": "שיכנס", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיכנס_8", "שיכנס_9"],
			},
			{
				"type": "שיכנס", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שיכנס_8", "שיכנס_9"],
			},
			{
				"type": "ויצא", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ויצא_10", "ויצא_11"],
			},
			{
				"type": "ויצא", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ויצא_10", "ויצא_11"],
			},
			{
				"type": "ראה", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראה_12", "ראה_13"],
			},
			{
				"type": "ראה", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ראה_12", "ראה_13"],
			},
			{
				"type": "מה פירוש", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה פירוש_14"],
			},
		],
		"ex_name": "babakama_57a_siman_hechezira_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}