var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_34a_bottom.png",
		"מאיפה לנו, מהיכן לומדים זאת": "מאיפה לנו, מהיכן לומדים זאת.text",
		"יעלה על דעתך": "יעלה על דעתך.text",
		"צריך, רוצה, מבקש": "צריך, רוצה, מבקש.text",
		"הרי": "הרי.text",
		"כך": "כך.text",
		"יש צורך כיוון ש...": "יש צורך כיוון ש....text",
		"היה עולה בדעתך לומר": "היה עולה בדעתך לומר.text",
		"ונלמד בשיטת ההיקש": "ונלמד בשיטת ההיקש.text",
		"סמוך, נכתב בצמידות": "סמוך, נכתב בצמידות.text",
		"למד": "למד.text",
	},
	"parameters": {
		"numOfItems": 14,
		"placeholders": [
			{ "id": "מאיפה לנו, מהיכן לומדים זאת_1", "loc": { "x": -252, "y": -287 }, "rect": { "width": 68, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יעלה על דעתך_2", "loc": { "x": 209, "y": 70 }, "rect": { "width": 203, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריך, רוצה, מבקש_3", "loc": { "x": 193, "y": 110 }, "rect": { "width": 53, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "צריך, רוצה, מבקש_4", "loc": { "x": -44, "y": 110 }, "rect": { "width": 53, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הרי_5", "loc": { "x": -133, "y": 229 }, "rect": { "width": 44, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_6", "loc": { "x": -258, "y": 229 }, "rect": { "width": 53, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יש צורך כיוון ש..._7", "loc": { "x": -124, "y": 268 }, "rect": { "width": 128, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "היה עולה בדעתך לומר_8", "loc": { "x": 212, "y": 308 }, "rect": { "width": 197, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ונלמד בשיטת ההיקש_9", "loc": { "x": 254, "y": -89 }, "rect": { "width": 83, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ונלמד בשיטת ההיקש_10", "loc": { "x": 129, "y": 30 }, "rect": { "width": 83, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סמוך, נכתב בצמידות_11", "loc": { "x": 242, "y": -49 }, "rect": { "width": 113, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "סמוך, נכתב בצמידות_12", "loc": { "x": 242, "y": 30 }, "rect": { "width": 113, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למד_13", "loc": { "x": 265, "y": -248 }, "rect": { "width": 59, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "למד_14", "loc": { "x": 157, "y": -168 }, "rect": { "width": 59, "height": 35 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מאיפה לנו, מהיכן לומדים זאת", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מאיפה לנו, מהיכן לומדים זאת_1"],
			},
			{
				"type": "יעלה על דעתך", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יעלה על דעתך_2"],
			},
			{
				"type": "צריך, רוצה, מבקש", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך, רוצה, מבקש_3", "צריך, רוצה, מבקש_4"],
			},
			{
				"type": "צריך, רוצה, מבקש", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "צריך, רוצה, מבקש_3", "צריך, רוצה, מבקש_4"],
			},
			{
				"type": "הרי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הרי_5"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_6"],
			},
			{
				"type": "יש צורך כיוון ש...", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יש צורך כיוון ש..._7"],
			},
			{
				"type": "היה עולה בדעתך לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "היה עולה בדעתך לומר_8"],
			},
			{
				"type": "ונלמד בשיטת ההיקש", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ונלמד בשיטת ההיקש_9", "ונלמד בשיטת ההיקש_10"],
			},
			{
				"type": "ונלמד בשיטת ההיקש", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ונלמד בשיטת ההיקש_9", "ונלמד בשיטת ההיקש_10"],
			},
			{
				"type": "סמוך, נכתב בצמידות", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סמוך, נכתב בצמידות_11", "סמוך, נכתב בצמידות_12"],
			},
			{
				"type": "סמוך, נכתב בצמידות", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "סמוך, נכתב בצמידות_11", "סמוך, נכתב בצמידות_12"],
			},
			{
				"type": "למד", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למד_13", "למד_14"],
			},
			{
				"type": "למד", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "למד_13", "למד_14"],
			},
		],
		"ex_name": "kidushin_34a_gamar_metfilin_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}