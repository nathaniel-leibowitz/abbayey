function addHakonesHint(sugiya, div)
{
    var hints = new Array;
    switch (sugiya) {

        case "babakama_55b_hakones_syntax":
            hints.push({ text: 'משנה ארוכה שעוסקת במקרים רבים, אך מנוסחת בתבנית טיפוסית וקבועה', symbol: "" });
            hints.push({ text: 'תחילה הפרידו בין המקרים השונים בעזרת הנקודה', symbol: "period" });
            hints.push({ text: 'כל מקרה מנוסח כמשפט תנאי המורכב ממקרה ואז הדין שלו. הפרידו ביניהם בעזרת הקו המפריד', symbol: "hyphen" });
            break;

        case "babakama_55b_keytzad_meshalmemt_syntax":
            hints.push({ text: 'הסיפא של המשנה פותחת בשאלה, סמן אותה בעזרת סימן השאלה', symbol: "question" });
            hints.push({ text: 'גם הסיפא מורכבת ממשפטי תנאי, הפרד בין המקרים לדינים בעזרת הקו', symbol: "hyphen" });
            hints.push({ text: 'אתגר: רבי שמעון קובע כלל ואז מפרט את פרטיו. הפרידו ביניהם בעזרת הנקודותיים', symbol: "colon" });
            break;

        case "babakama_55b_patish_syntax":
            hints.push({ text: 'המקשן מוצא סתירה בין המשנה לבין שיטת רבי מאיר. מיצאו היכן המקשן מסיים את דבריו ומקמו שם', symbol: "qushia" });
            hints.push({ text: 'התירוץ ארוך מאוד, מקמו בסופו', symbol: "resolution" });
            hints.push({ text: 'התירוץ מורכב מציטוט מימרא של ר אלעזר, הקיפוהו', symbol: "quotebegin" });
            hints.push({ text: 'וכן פסוקים רבים, הקיפום', symbol: "biblequote" });
            hints.push({ text: 'במשפטי התנאי, השתמשו בקו להפריד בין המקרה לבין הדין', symbol: "hyphen" });
            hints.push({ text: 'ובסופי משפטים מקמו נקודות', symbol: "period" });
            break;

        case "babakama_56a_hakofef_syntax":
            hints.push({ text: 'הקיפו את הציטוט מתוך דברי רבי יהושע', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא מקשה על דבריו. מיצאו היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'אך שימו לב שהקושיא עצמה מכילה שאלה רגילה', symbol: "question" });
            hints.push({ text: 'וגם משפט תנאי. השתמשו בקו להפריד בין שני חלקי המשפט', symbol: "hyphen" });
            hints.push({ text: 'הסוגיא מציעה שני תירוצים. מיצאו היכן כל אחד מסתיים ומקמו', symbol: "resolution" });
            break;

        case "babakama_56a_hasocher_edey_sheker_syntax":
            hints.push({ text: 'הקיפו את הציטוט מתוך דברי רבי יהושע', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא מקשה על דבריו. מיצאו היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'אך שימו לב שהקושיא עצמה מכילה שאלה רגילה', symbol: "question" });
            hints.push({ text: 'וגם משפט תנאי. השתמשו בקו להפריד בין שני חלקי המשפט', symbol: "hyphen" });
            hints.push({ text: 'מיצאו היכן מסתיים התירוץ ומקמו', symbol: "resolution" });
            break;

        case "babakama_56a_hayodea_edut_lechavero_syntax":
            hints.push({ text: 'הקיפו את הציטוט מתוך דברי רבי יהושע', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא מצטטת פסוק שאותו הקיפו', symbol: "biblequote" });
            hints.push({ text: 'הסוגיא מקשה על דברי רבי יהושע. מיצאו היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'אך שימו לב שהקושיא עצמה מכילה שאלה רגילה', symbol: "question" });
            hints.push({ text: 'הקושיא גם מכילה משפט תנאי. השתמשו בקו להפריד בין שני חלקי המשפט', symbol: "hyphen" });
            hints.push({ text: 'אתגר: המקשן מסביר את קושייתו, השתמשו בנקודותיים היכן שמתחיל הביאור', symbol: "colon" });
            hints.push({ text: 'מיצאו היכן מסתיים התירוץ ומקמו', symbol: "resolution" });
            break;

        case "babakama_56a_vetoo_leika_syntax":
            hints.push({ text: 'הסוגיא מקשה על רבי יהושע. תחילה היא שואלת שאלה ריטורית, תמיהה, סמנו אותה ב', symbol: "interrobang" });
            hints.push({ text: 'המקשן ממשיך ומזכיר חמישה ציטוטים של מקרים דומים. הקיפום ב', symbol: "quotebegin" });
            hints.push({ text: 'מיצאו היכן המקשן מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'מיצאו היכן מסתיים התירוץ ומקמו', symbol: "resolution" });
            hints.push({ text: 'אתגר: התירוץ נעזר בטענת סרק מופרכת ושגויה, הקיפו אותה ב', symbol: "wrongly" });
            break;

        case "babakama_56a_ika_toova_syntax":
            hints.push({ text: 'הסוגיא חוזרת ומבארת את חמשת הציטוטים מהסוגיא הקודמת. הקיפום ב', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא מרבה להשתמש בשיטת טענת הסרק המופרכת והשגויה. מצאו את הטענות והקיפום', symbol: "wrongly" });
            hints.push({ text: 'בחלק מההסברים, מובא מעין דו שיח בין דמויות, מצאו את הדיאלוגים והקיפו ברמקול', symbol: "speakbegin" });
            break;

        case "babakama_56a_vehoo_shechatra_syntax":
            hints.push({ text: 'סוגיא מאתגרת ולכן מומלץ לחזור אליה רק בשלב מתקדם. היא מבררת את המימרא של רבא בעזרת 3 קושיות ותירוצים', symbol: "" });
            hints.push({ text: 'מקם בסוף כל קושיא', symbol: "qushia" });
            hints.push({ text: 'ומקם בסוף כל תירוץ', symbol: "resolution" });
            hints.push({ text: 'בסוף מובאת מעין טענת סרק מופרכת ושגויה, הקיפוה ב', symbol: "wrongly" });
            hints.push({ text: 'וכן מובאת מעין שיחה. הקיפו את הדיאלוג ברמקול', symbol: "speakbegin" });
            break;

        case "babakama_56b_dekamu_la_baapa_syntax":
            hints.push({ text: 'הסוגיא מקשה על דברי המשנה: הוציאוה ליסטים, לסטים חייבין', symbol: "" });
            hints.push({ text: 'תחילה מיצאו היכן מסיים המקשן את קושייתו וסמנו ב', symbol: "qushia" });
            hints.push({ text: 'המקשן מבאר את קושייתו. מקמו נקודותיים היכן שהוא מתחיל לבאר', symbol: "colon" });
            hints.push({ text: 'המקשן משתמש במשפט תנאי, הפרידו את חלקיו בעזרת הקו ', symbol: "hyphen" });
            hints.push({ text: 'סמנו היכן התרצן הראשון מסיים את תירוצו', symbol: "resolution" });
            hints.push({ text: 'התרצן מבאר את תירוצו. מקמו נקודותיים היכן שהוא מתחיל לבאר', symbol: "colon" });
            break;

        case "babakama_56b_ki_ha_deamar_raba_syntax":
            hints.push({ text: 'הגמרא מביאה דיון דומה לקודם. תחילה הקיפו את ציטוט דברי רב ב', symbol: "quotebegin" });
            hints.push({ text: 'הדיון הזה מאוד דומה לקודם, פסקו אותו בהתאם', symbol: "" });
            hints.push({ text: 'אתגר: שימו לב שהקושיא מכילה בתוכה שאלה ריטורית, תמיהה', symbol: "interrobang" });
            break;

        case "babakama_56b_hikisha_syntax":
            hints.push({ text: 'אביי מציע תירוץ שני לקושיא שראינו בסוגיא בראש העמוד. תחילה פסקו שוב את הקושיא שכבר ראינו בראש העמוד', symbol: "" });
            hints.push({ text: 'נדלג אל התירוץ השני שבו אביי מזכיר מילה שאמר לו רב יוסף, הקיפוה ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'וסמנו את סיום התירוץ השני ב', symbol: "resolution" });
            break;

        case "babakama_56b_barziley_syntax":
            hints.push({ text: 'בסוגיא מופיעים ציטוטים הן ממשנתינו והן של ברייתא אחרת, הקף אותם', symbol: "quotebegin" });
            hints.push({ text: 'הדיון בנוי משתי קושיות ושני תירוצים. תחילה מיצאו היכן המקשן מסיים את קושיותיו ומקמו', symbol: "qushia" });
            hints.push({ text: 'ומיצאו היכן התרצן מסיים את תירוציו ומקמו', symbol: "resolution" });
            hints.push({ text: 'אתגר: במשפטי התנאי, הפרד בין המקרה לדין בעזרת הקו', symbol: "hyphen" });
            hints.push({ text: 'אתגר: הבדילו בין השאלות הרגילות', symbol: "question" });
            hints.push({ text: 'לבין השאלות הרטוריות', symbol: "interrobang" });
            break;

        case "babakama_56b_barziley_icha_deamrey_syntax":
            hints.push({ text: 'גירסא זו לסוגיא מעט מאתגרת ולכן כדאי ללמוד אותה רק בשלב מאוחר. הפעם מדובר בניסיון לסייע לרבא ודחיית הסיוע', symbol: "" });
            hints.push({ text: 'תחילה מצאו היכן מסתיים הניסיון הארוך לסייע לרבא', symbol: "" });
            hints.push({ text: 'הוא מורכב מציטוטים הן ממשנתינו והן של ברייתא אחרת, הקף אותם', symbol: "quotebegin" });
            hints.push({ text: 'ואתרו את שני משפטי התנאי וחלקו אותם בעזרת', symbol: "hyphen" });
            hints.push({ text: 'עכשיו מצאו היכן מסתיימת דחיית הסיוע ושבצו בתוכה את הנקודותיים', symbol: "colon" });
            break;






        case "babakama_57a_siman_hechezira_part_1_syntax":
            hints.push({ text: 'הקיפו את הברייתא שעליה דנים', symbol: "quotebegin" });
            hints.push({ text: 'היא מתארת שתי סיטואציות שונות. הפרידו ביניהם בעזרת הנקודה', symbol: "period" });
            hints.push({ text: 'כל אחת מהסיטואציות בנויה כמשפט תנאי. הפרד בין המקרה והדין בעזרת הקו הצהוב', symbol: "hyphen" });
            hints.push({ text: 'המקשן מביא את הבייתא כקושיא, אתרו היכן הוא מסיים את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'עכשיו מצאו היכן מסיים התרצן את תירוצו הראשון', symbol: "resolution" });
            hints.push({ text: 'אתגר: המקשן מביע תמיהה ומנסח את קושייתו כשאלה רטורית, אתרו את תמיהתו ומקמו', symbol: "interrobang" });
            break;

        case "babakama_57a_siman_hechezira_syntax":
            hints.push({ text: 'המשך הסוגיא הקודמת. תחילה פסקו את הקושיא והתירוץ הראשונים כמו התרגיל הקודם', symbol: "" });
            hints.push({ text: 'רב יוסף מקשה על התירוץ. הוא מצטט מתוך הביירתא, אתרו את הציטוט והקיפוהו', symbol: "quotebegin" });
            hints.push({ text: 'אתרו היכן הוא מסיים להקשות ומקמו', symbol: "qushia" });
            hints.push({ text: 'רבה מתרץ ומסביר את הברייתא באריכות. אתרו את סוף דבריו וסמנו', symbol: "resolution" });
            hints.push({ text: 'בהסבר שלו מופיעים שני משפטי תנאי, הפרידו בין המקרה לדין בעזרת הקו הצהוב', symbol: "hyphen" });
            break;

        case "babakama_57a_siman_leolam_syntax":
            hints.push({ text: 'הקף את הברייתא שעליה דנים', symbol: "quotebegin" });
            hints.push({ text: 'מיצאו היכן מסיים המקשן, רב יוסף, את קושייתו ומקמו', symbol: "qushia" });
            hints.push({ text: 'שימו לב שבקושייתו, רב יוסף גם שואל שאלה רגילה', symbol: "question" });
            hints.push({ text: 'וגם שואל שאלה רטורית', symbol: "interrobang" });
            hints.push({ text: 'אתרו היכן רבה מסיים את תירוצו ומקמו', symbol: "resolution" });
            break;

        case "babakama_57a_siman_hashev_syntax":
            hints.push({ text: 'סוגיא קשה! כדאי לתרגל אותה רק אחרי שצוברים ניסיון', symbol: "" });
            hints.push({ text: 'רבה מנסח קושיא ארוכה. תחילה הוא מצטט ברייתא, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'בברייתא מובא מדרש הלכה. מצא בו ציטוטים מתוך פסוק והקיפום', symbol: "biblequote" });
            hints.push({ text: 'מיצאו היכן רבה מסיים לנסח את קושייתו הארוכה ומקמו ', symbol: "qushia" });
            hints.push({ text: 'שימו לב שהקושיא הזו עצמה מכילה בתוכה גם קושיא', symbol: "qushia" });
            hints.push({ text: 'וגם תירוץ', symbol: "resolution" });
            hints.push({ text: 'בנוסף, אתר בתוך הקושיא את טענת הסרק, המופרכת והשגויה, והקיפוה ב', symbol: "wrongly" });
            hints.push({ text: 'מצאו היכן רב יוסף מסיים את תירוצו', symbol: "resolution" });
            hints.push({ text: 'לבסוף, רב יוסף מביא סיוע מברייתא, הקיפוה ב', symbol: "quotebegin" });
            break;

        case "babakama_57a_siman_hiya_syntax":
            hints.push({ text: 'סוגיא קשה מתאימה רק למנוסים! היא מנוסחת כדו שיח בין אביי לרב יוסף. הקיפו את חלקי הדיאלוג ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'בדיאלוג, מצטטים את דבריו של רבי חייא, הקיפום ב', symbol: "quotebegin" });
            hints.push({ text: 'אתרו היכן אביי מסיים את קושייתו הראשונה ומקמו', symbol: "qushia" });
            hints.push({ text: 'אתרו היכן רבה מסיים את תירוצו הראשון ומקמו', symbol: "resolution" });
            hints.push({ text: 'אתרו וסמנו את הקושיא והתירוץ הנוספים', symbol: "qushia" });
            hints.push({ text: 'אביי משתמשת בשאלה ריטורית, תמיהה. סמנוהו ב', symbol: "interrobang" });
            break;

        case "babakama_57b_siman_amarta_syntax":
            hints.push({ text: 'שוב סוגייא קשה למנוסים בלבד! היא פותחת בציטוט חלקי של ברייתא, הקיפוהה ב', symbol: "quotebegin" });
            hints.push({ text: 'המקשן מקשה ממנה על קביעתו של רב יוסף שליסטים מזוין נקרא גנב. אתרו היכן הוא מסיים את קושייתו', symbol: "qushia" });
            hints.push({ text: 'אתגר: הקושיא הזו מכילה טענת סרק מופרכת ושגויה הקיפוה ב', symbol: "wrongly" });
            hints.push({ text: 'רב יוסף מתרץ על ידי ניסוח אחר לאותה ברייתא. הקיפו את הניסוח החדש ב', symbol: "quotebegin" });
            hints.push({ text: 'ואתרו את סיום התירוץ', symbol: "resolution" });
            break;

        case "babakama_57b_siman_venishbar_syntax":
            hints.push({ text: 'שוב סוגייא קשה למנוסים בלבד! היא פותחת בציטוט ברייתא ארוכה, הקיפוהה ב', symbol: "quotebegin" });
            hints.push({ text: 'הברייתא הוא מדרש הלכה על מילים מתוך פסוק, הקיפו את מילות הפסוק ב', symbol: "biblequote" });
            hints.push({ text: 'המדרש מכיל גם שאלה רגילה', symbol: "question" });
            hints.push({ text: 'וגם שאלה רטורית', symbol: "interrobang" });
            hints.push({ text: 'המקשן מקשה מברייתא זו על רב יוסף, אתרו היכן הוא מסיים את קושייתו', symbol: "qushia" });
            hints.push({ text: 'ואתרו היכן מסתיים התירוץ של רב יוסף', symbol: "resolution" });
            break;

        case "babakama_57b_siman_sachar_syntax":
            hints.push({ text: 'אתגרי! זו אחת הסוגיות הקשות לקריאה כדאי לפענח אותה בשלבים', symbol: "" });
            hints.push({ text: 'מטרת הסוגיא להביא סיוע לקביעה של רב יוסף שליסטים מזוין נקרא גנב', symbol: "" });
            hints.push({ text: 'תחילה מצא היכן מסתיים הניסיון הארוך לסייע לרב יוסף ומקמו שם', symbol: "assist" });
            hints.push({ text: 'הסיוע עצמו סבוך ומורכב מכמה מהלכים. תחילה מובאת ברייתא הקיפוה ב', symbol: "quotebegin" });
            hints.push({ text: 'ואז מתמקדים ומצטטים שני משפטים מהברייתא. הקיפו כל אחד מהם ב', symbol: "quotebegin" });
            hints.push({ text: 'הגמרא דוחה שלוש פעמים את ניסיון הסיוע. אתרו את כולם ומקמו בסוף כל דחייה  ', symbol: "noassist" });
            break;









        case "babakama_59b_hamagdish_syntax":
            hints.push({ text: 'במשנה שלושה מקרים שונים, הפרידו ביניהם בעזרת הנקודות', symbol: "period" });
            hints.push({ text: 'המשפטים הם משפטי תנאי המביאים מקרה ואז את הדין. הפרידו את חלקיהם בעזרת', symbol: "hyphen" });
            break;


        case "babakama_59b_ayal_ugdosh_ayal_veantar_syntax":
            hints.push({ text: 'הגמרא מצטטת את רבי, הקיפו את דבריו ב', symbol: "quotebegin" });
            hints.push({ text: 'הציטוט מובא כקושיא על המשנה. זהו היכן המקשה מסיים את קושייתו וסמנו', symbol: "qushia" });
            hints.push({ text: 'ומצאו היכן מסתיים התירוץ וסמנו', symbol: "resolution" });
            hints.push({ text: 'אתגר: התירוץ מכיל מעין דו-שיח, הקיפו את המשפט ברמקולים', symbol: "speakbegin" });
            break;

        case "babakama_60a_liba_velibta_harooach_syntax":
            hints.push({ text: 'הסוגיא פותחת בציטוט של ברייתא, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: ' היא עוסקת בשני מקרים, מקם ביניהם את הנקודה', symbol: "period" });
            hints.push({ text: ' היא בנוייה ממשפטי תנאי, הפרד בין המקרה לדין בעזרת', symbol: "hyphen" });
            hints.push({ text: ' סיים לנקד אותה בעזרת הנקודותיים', symbol: "colon" });
            hints.push({ text: 'הברייתא מובאת כקושיא הסותרת את המשנה. סמנו היכן המקשן מסיים את קושייתו', symbol: "qushia" });
            hints.push({ text: 'הסוגיא מציעה ארבעה תירוצים. סמנו את הסיום של כל אחד מהם', symbol: "resolution" });
            break;

        case "babakama_60a_hasholeah_et_habeera_syntax":
            hints.push({ text: 'הקף את הפסוק בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'והפרד במשפטי התנאי בין הדין למקרה בעזרת', symbol: "hyphen" });
            break;

        case "babakama_60a_kotzim_vegadish_lama_li_syntax":
            hints.push({ text: 'הסוגיא עוסקת בכפילויות שיש בפסוק. החלק הזה של הסוגיא מתמקד אך ורק בכפילות של קוצים וגדיש', symbol: "" });
            hints.push({ text: 'הסוגיא פותחת בקושיא על הכפילות, סמנו היכן מסיים המקשן את קושייתו', symbol: "qushia" });
            hints.push({ text: 'התירוץ מכיל בתוכו שתי טענות סרק מופרכות ושגויות, המכונות בגמרא הוה-אמינא. מצא אותם והקף כל אחד ב', symbol: "wrongly" });
            hints.push({ text: 'ומקם בסוף הסוגיא היכן שמסתיים התירוץ את הסימן', symbol: "resolution" });
            break;

        case "babakama_60a_kama_lama_li_syntax":
            hints.push({ text: 'סוגיא מבלבלת! פינג פונג של קושיות ותירוצים בין רבנן ורב יהודה עד שהכל מתישב', symbol: "" });
            hints.push({ text: 'הקף את חמשת הציטוטים מהפסוקים בספר תורה', symbol: "biblequote" });
            hints.push({ text: 'מצא את כל חמשת הקושיות ותירוצים. בסיום הקושיות מקמו', symbol: "qushia" });
            hints.push({ text: 'ובסיום התירוצים מקמו', symbol: "resolution" });
            hints.push({ text: 'אתגר: מיצאו את משפט התנא והפרידו בין חלקיו בעזרת', symbol: "hyphen" });
            break;

        case "babakama_60a_sadeh_lama_li_syntax":
            hints.push({ text: 'סוף סוף סיום לסוגיא הארוכה והמתישה', symbol: "" });
            hints.push({ text: 'תחילה מקשים מדוע דרושה המילה שדה. זהו היכן המקשה מסיים את קושייתו וסמנו', symbol: "qushia" });
            hints.push({ text: 'ומצאו היכן מסתיים התירוץ וסמנו', symbol: "resolution" });
            hints.push({ text: 'הקושיא השניה היא מדוע הפסוק לא מסתפק במילה שדה, סמנו אותה', symbol: "qushia" });
            hints.push({ text: 'התרוץ מכיל בתוכו טענת סרק שגויה ומופרכת המכונה בגמרא הוה-אמינא. הקיפוה', symbol: "wrongly" });
            hints.push({ text: 'לסיום חגיגי סמנו את סיום התירוץ שהוא גם סיום הסוגיא הארוכה ב', symbol: "resolution" });
            break;

        case "babakama_60b_dever_baeer_syntax":
            hints.push({ text: 'בברייתא הזו מובא מדרש הלכה. הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'המדרש מביא שלושה פסוקים הקיפו כל אחד ב', symbol: "biblequote" });
            hints.push({ text: 'אתגר: המדרש פותח במשפט תנאי, התשמשו בקו המפריד להפריד בין המקרה לבין הדין', symbol: "hyphen" });
            break;

        case "babakama_60b_dever_baeer_my_veomer_syntax":
            hints.push({ text: 'הסוגיא מקשה קושיא על הברייתא הקודמת. סמנו היכן מסיים המקשן להקשות את קושייתו הקצרה', symbol: "qushia" });
            hints.push({ text: 'הקושיא מכילה ציטוט של מילה אחת מתוך הברייתא הקיפו אותה', symbol: "quotebegin" });
            hints.push({ text: 'המתרץ מתרץ תירוץ ארוך, תחילה מיצאו את סופו ומקמו', symbol: "resolution" });
            hints.push({ text: 'מיצאו בתוך התירוץ את שני הפסוקים והקיפו', symbol: "biblequote" });
            hints.push({ text: 'אתגר: המתרץ משתמש פעמיים בטענות סרק שגויות ומופרכות. הקיפו כל אחת ב', symbol: "wrongly" });
            break;

        case "babakama_60b_raav_baeer_syntax":
            hints.push({ text: 'סוגיא דומה מאוד לקודמת, לכן מומלץ ללמוד קודם את סוגיות דבר בעיר. בברייתא הזו מובא מדרש הלכה. הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'המדרש מביא שני פסוקים הקיפו כל אחד ב', symbol: "biblequote" });
            hints.push({ text: 'אתגר: המדרש פותח במשפט תנאי, התשמשו בקו המפריד להפריד בין המקרה לבין הדין', symbol: "hyphen" });
            hints.push({ text: 'המתרץ מתרץ תירוץ ארוך, תחילה מיצאו את סופו ומקמו', symbol: "resolution" });
            hints.push({ text: 'מיצאו בתוך התירוץ את שני הפסוקים והקיפו', symbol: "biblequote" });
            hints.push({ text: 'אתגר: המתרץ משתמש בטענת סרק שגויה ומופרכת. הקיפוה ב', symbol: "wrongly" });
            break;

        case "babakama_61a_avra_gader_syntax":
            hints.push({ text: 'הפרידו במשפט התנאי בין המקרה לדין בעזרת', symbol: "hyphen" });
            break;

        case "babakama_61a_milemala_lemata_syntax":
            hints.push({ text: 'הקיפו את הברייתא הסותרת את המשנה', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסופה את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'התמקדו בתירוץ: מקמו בסופו', symbol: "resolution" });
            hints.push({ text: 'אתגר: שבצו בתירוץ נקודותיים', symbol: "colon" });
            hints.push({ text: 'והפרידו במשפטי התנאי בין המקרה לדין בעזרת', symbol: "hyphen" });
            break;

        case "babakama_61b_hakol_lefi_hadleka_syntax":
            hints.push({ text: 'המשנה מבררת שאלה. סמנו אותה ב', symbol: "question" });
            hints.push({ text: 'מובאים ארבע דעות לשאלה. מקמו ביניהם נקודות', symbol: "period" });
            hints.push({ text: 'אחת הדעות מצטטת חלק של פסוק, מצאוהו והקיפוהו', symbol: "biblequote" });
            break;

        case "babakama_61b_leit_lerash_shiura_bidleka_syntax":
            hints.push({ text: 'המקשן מצטט ברייתא ארוכה הסותרת את המשנה, הקיפוה', symbol: "quotebegin" });
            hints.push({ text: 'ומקמו בסופה את סימן הקושיא', symbol: "qushia" });
            hints.push({ text: 'אתרו את סיום התירוץ וסמנו', symbol: "resolution" });
            hints.push({ text: 'נחזור ונתמקד בברייתא. היא דנה במספר מקרים ודעות. הפרד ביניהם בעזרת נקודות', symbol: "period" });
            hints.push({ text: 'אחת הדעות מנוסחת כמשפט תנאי: מקרה ואז הדין שלו. השתמשו בקו להפריד ביניהם', symbol: "hyphen" });
            hints.push({ text: 'אתגר: הקושיא פותחת בסימן תמיהה, מקמו בה את השאלה הרטורית', symbol: "interrobang" });
            break;

        case "babakama_61b_amar_rava_betartey_pligey_syntax":
            hints.push({ text: 'סוגייא לא קלה! תחילה סמנו היכן רבא מסיים להקשות את קושייתו', symbol: "qushia" });
            hints.push({ text: 'ואז מיצאו הרחק בסוף את סיום התירוץ שלו לקושייתו', symbol: "resolution" });
            hints.push({ text: 'נתמקד בקושיא: רבא מצטט מהמשנה, הקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'אתגר: בקושייתו רבא טוען שאם כבר, המשנה הייתה צריכה לומר אחרת. הקף את הניסוח ההיפותטי השגוי ב', symbol: "wrongly" });
            break;





        case "babakama_79b_ein_megadlin_syntax":
            hints.push({ text: 'המשנה עוסקת בחמישה נושאים שונים, הראשון שבהם הוא התגוננות מהנזקים של עיזים לשדות', symbol: "" });
            hints.push({ text: 'הפרידו בין חמשת הנושאים בעזרת הנקודות', symbol: "period" });
            break;

        case "babakama_79b_ein_megadlin_bryta_1_syntax":
            hints.push({ text: 'הגמרא מצטטת ברייתא הדומה למשנה, הקיפוה ב', symbol: "quotebegin" });
            hints.push({ text: 'והשתמש בנקודות בשביל לחלק אותה למקרים השונים', symbol: "period" });
            hints.push({ text: 'אתגר קשה: איפה לדעתכם מתאים למקם את הקו המפריד', symbol: "hyphen" });
            break;

        case "babakama_79b_ein_megadlin_bryta_2_syntax":
            hints.push({ text: 'הגמרא ממשיכה ומביאה ברייתא נוספת דומה וארוכה אך קוטעת אותה באמצע בשביל לדון בה', symbol: "" });
            hints.push({ text: 'היכן מסתיים החלק הראשון של הברייתא הנוספת? (רמז: הדיון מתחיל במעבר לארמית) הקיפו את החלק הראשון ב', symbol: "quotebegin" });
            hints.push({ text: 'והשתמשו בנקודות בשביל לחלק אותה למקרים השונים', symbol: "period" });
            hints.push({ text: 'אתגר קשה: היכן לדעתכם יש למקם נקודותיים', symbol: "colon" });
            hints.push({ text: 'אתגר קשה: היכן לדעתכם יש למקם קו מפריד', symbol: "hyphen" });
            break;

        case "babakama_80a_shaalu_talmidav_et_raban_gamliel_syntax":
            hints.push({ text: 'מיצאו היכן הסוגיא מצטטת מתוך משנתינו, והקיפו את הציטוט ב', symbol: "quotebegin" });
            hints.push({ text: 'הסוגיא מנוסחת כסיפור בו מתנהל דו שיח. הקיפו את המשפטים של הדמויות ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'הלב של הסוגיא הוא קושיא על הדו שיח. אתרו היכן המקשן מסיים את קושייתו וסמנו ב', symbol: "qushia" });
            hints.push({ text: 'ואת סיום התירוץ סמנו ב', symbol: "resolution" });
            break;

        case "babakama_80a_maase_behasid_syntax":
            hints.push({ text: 'הסוגיא היא בעצם ברייתא ארוכה מאוד הקיפוה ב', symbol: "quotebegin" });
            hints.push({ text: 'הברייתא מנוסחת כסיפור בו מתנהל דו שיח. הקיפו את המשפטים של הדמויות ברמקולים', symbol: "speakbegin" });
            hints.push({ text: 'והוסיפו מידי פעם בסופי המשפט נקודות', symbol: "period" });
            hints.push({ text: 'אתגר קשה: אחד המשפטים נאמר כתמיהה או שאלה ריטורית, סמנוה ב', symbol: "interrobang" });
            break;





        case "babakama_80b_vehalokeach_bayit_beretz_yisrael_syntax":
            hints.push({ text: 'הסוגיא פותחת בציטוט ברייתא הקיפוה ב', symbol: "quotebegin" });
            hints.push({ text: 'במהלך הסוגיא מצוטטים שתי מימרות נוספות, של רבא ושל רבי יונתן. הקיפום ב ', symbol: "quotebegin" });
            hints.push({ text: 'ליבה של הסוגיא היא קושיא על הברייתא. היא מנוסחת כשאלה ריטורית, סמנו אותה בסימן התמיהה', symbol: "interrobang" });
            hints.push({ text: 'ומקמו את סימן הקושיא אחריו', symbol: "qushia" });
            hints.push({ text: 'זהו היכן מסיים התרצן את תירוצו ומקמו', symbol: "resolution" });
            hints.push({ text: 'אתגר קשה: היכן לדעתכם יש למקם קו מפריד', symbol: "hyphen" });
            break;

        case "babakama_81b_haroeh_havero_toweh_syntax":
            hints.push({ text: 'סוגיא להרתפקנים! היא מלבנת ברייתא שאותה הקיפו ב', symbol: "quotebegin" });
            hints.push({ text: 'הקושיא הראשונה מתיחסת לכפילות בברייתא. מקמו את הסימן היכן שהמקשן מסיים את קושייתו הראשונה', symbol: "qushia" });
            hints.push({ text: 'התרצן משתמש בטענת סרק שגויה ומופרכת. הקיפוה ב', symbol: "wrongly" });
            hints.push({ text: 'הסוגיא ממשיכה להקשות על בסיס ברייתא נוספת. תחילה זהו ציטוט של מילה מפסוק, והקיפו את המילה ב', symbol: "biblequote" });
            hints.push({ text: 'עתה, הקיפו את כל הברייתא ב', symbol: "quotebegin" });
            hints.push({ text: 'זהו היכן המקשן מסיים את קושייתו השנייה ומקמו', symbol: "qushia" });
            hints.push({ text: 'זהו היכן התרצן מסיים את תירוצו השני ומקמו', symbol: "resolution" });
            break;
    }
    addHint(div, hints);
}
