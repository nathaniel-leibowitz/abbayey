var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_33b_middle.png",
		"מלפני": "מלפני.text",
		"שונה, אינו דומה": "שונה, אינו דומה.text",
		"גם": "גם.text",
		"עמד, קם": "עמד, קם.text",
		"מלפניו": "מלפניו.text",
		"כך": "כך.text",
		"לפעמים": "לפעמים.text",
		"שבא, שעובר": "שבא, שעובר.text",
		"לכבוד": "לכבוד.text",
		"שלי": "שלי.text",
	},
	"parameters": {
		"numOfItems": 11,
		"placeholders": [
			{ "id": "מלפני_1", "loc": { "x": -435, "y": -355 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שונה, אינו דומה_2", "loc": { "x": -13, "y": -315 }, "rect": { "width": 74, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_3", "loc": { "x": -191, "y": -275 }, "rect": { "width": 53, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "עמד, קם_4", "loc": { "x": -258, "y": -275 }, "rect": { "width": 77, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_5", "loc": { "x": -351, "y": -275 }, "rect": { "width": 110, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך_6", "loc": { "x": -176, "y": -235 }, "rect": { "width": 56, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לפעמים_7", "loc": { "x": -437, "y": -235 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שבא, שעובר_8", "loc": { "x": 69, "y": -195 }, "rect": { "width": 80, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מלפניו_9", "loc": { "x": -266, "y": -195 }, "rect": { "width": 110, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכבוד_10", "loc": { "x": 61, "y": -156 }, "rect": { "width": 89, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שלי_11", "loc": { "x": -20, "y": -156 }, "rect": { "width": 65, "height": 34 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מלפני", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפני_1"],
			},
			{
				"type": "שונה, אינו דומה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שונה, אינו דומה_2"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_3"],
			},
			{
				"type": "עמד, קם", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "עמד, קם_4"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_5", "מלפניו_9"],
			},
			{
				"type": "כך", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך_6"],
			},
			{
				"type": "לפעמים", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לפעמים_7"],
			},
			{
				"type": "שבא, שעובר", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שבא, שעובר_8"],
			},
			{
				"type": "מלפניו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מלפניו_5", "מלפניו_9"],
			},
			{
				"type": "לכבוד", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכבוד_10"],
			},
			{
				"type": "שלי", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שלי_11"],
			},
		],
		"ex_name": "kidushin_33b_bno_vehu_rabo_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}