var mGameComponent;

var mGameData1 = {
    "id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
    "assets": {
		"home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
        "arrow": "storage/miniGames/gemara/syntax/arrow6.png",
		"arrowFocus": "storage/miniGames/gemara/syntax/arrowFocus.png",
		
		
		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)syntax_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_29a_middle.png",
		"quotebegin": "storage/miniGames/gemara/syntax/quotebegin.png",
		"quoteend": "storage/miniGames/gemara/syntax/quoteend.png",
		"question": "storage/miniGames/gemara/syntax/question.png",
		"interrobang": "storage/miniGames/gemara/syntax/interrobang.png",
		"biblequote": "storage/miniGames/gemara/syntax/biblequote.gif",
		"qushia": "storage/miniGames/gemara/syntax/qushia.png",
		"resolution": "storage/miniGames/gemara/syntax/resolution.png",
		"assist": "storage/miniGames/gemara/syntax/assist.png",
		"hyphen": "storage/miniGames/gemara/syntax/hyphen.png",
	},
	"parameters": {
		"numOfItems": 22,
		"placeholders": [
			{ "id": "quotebegin_1", "loc": { "x": -237, "y": -93 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_2", "loc": { "x": -14, "y": -57 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_3", "loc": { "x": -35, "y": -57 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_4", "loc": { "x": -275, "y": -21 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_5", "loc": { "x": 139, "y": 15 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_6", "loc": { "x": 122, "y": 15 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_7", "loc": { "x": 64, "y": 15 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "question_8", "loc": { "x": -277, "y": 15 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_9", "loc": { "x": 76, "y": 52 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "biblequote_10", "loc": { "x": -215, "y": 52 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_11", "loc": { "x": 141, "y": 88 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_12", "loc": { "x": 127, "y": 88 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_13", "loc": { "x": -112, "y": 88 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_14", "loc": { "x": 89, "y": 160 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_15", "loc": { "x": 73, "y": 160 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quotebegin_16", "loc": { "x": -150, "y": 160 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "quoteend_17", "loc": { "x": -32, "y": 305 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": -5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "assist_18", "loc": { "x": -47, "y": 305 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 5, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "interrobang_19", "loc": { "x": -200, "y": 305 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "qushia_20", "loc": { "x": -216, "y": 305 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "resolution_21", "loc": { "x": -32, "y": 341 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
			{ "id": "hyphen_22", "loc": { "x": -105, "y": -21 }, "rect": { "width": 17, "height": 30 }, "isReused": false, "offsetX": 0, "offsetY": 10, "url": "storage/miniGames/gemara/syntax/arrow6.png" },
		],
		"items": [
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -700, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_13", "quotebegin_16"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_11", "quoteend_14", "quoteend_17"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_3", "question_8"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -700, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_4", "interrobang_19"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -730, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_13", "quotebegin_16"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -700, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -730, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "question", 
				"score": 1,
				"loc": { "x": -730, "y": 200 },
				"scale": { "x": 0.08, "y": 0.08 },
				"placeholderIds": [ "question_3", "question_8"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -670, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "biblequote", 
				"score": 1,
				"loc": { "x": -760, "y": 300 },
				"scale": { "x": 0.18, "y": 0.18 },
				"placeholderIds": [ "biblequote_6", "biblequote_7", "biblequote_9", "biblequote_10"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -730, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_11", "quoteend_14", "quoteend_17"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -700, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_12", "qushia_20"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -670, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_13", "quotebegin_16"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -670, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_11", "quoteend_14", "quoteend_17"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -700, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_15", "resolution_21"],
			},
			{
				"type": "quotebegin", 
				"score": 1,
				"loc": { "x": -760, "y": 50 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quotebegin_1", "quotebegin_5", "quotebegin_13", "quotebegin_16"],
			},
			{
				"type": "quoteend", 
				"score": 1,
				"loc": { "x": -760, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "quoteend_2", "quoteend_11", "quoteend_14", "quoteend_17"],
			},
			{
				"type": "assist", 
				"score": 1,
				"loc": { "x": -700, "y": -150 },
				"scale": { "x": 0.25, "y": 0.25 },
				"placeholderIds": [ "assist_18"],
			},
			{
				"type": "interrobang", 
				"score": 1,
				"loc": { "x": -730, "y": 150 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "interrobang_4", "interrobang_19"],
			},
			{
				"type": "qushia", 
				"score": 1,
				"loc": { "x": -730, "y": 350 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "qushia_12", "qushia_20"],
			},
			{
				"type": "resolution", 
				"score": 1,
				"loc": { "x": -730, "y": 250 },
				"scale": { "x": 0.7, "y": 0.7 },
				"placeholderIds": [ "resolution_15", "resolution_21"],
			},
			{
				"type": "hyphen", 
				"score": 1,
				"loc": { "x": -700, "y": -100 },
				"scale": { "x": 0.4, "y": 0.4 },
				"placeholderIds": [ "hyphen_22"],
			},
		],
		"ex_name": "kidushin_29a_muy_kol_mitzvot_haben_ul_haav_syntax",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
				
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "show_ex_name": false,
        "debugMode": false,
		"showMousePos": false,
        "isPlaceItem": true,
        "itemHoverScaleFactor": 1.4, // sets scale when hovering on items in left panel
        "showHitFeedback": false, // sets rectangle color to green
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            //"shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.2, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    }
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}