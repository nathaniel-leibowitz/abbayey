var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babametzia_21a_bottom.png",
		"מהי הסיבה": "מהי הסיבה.text",
		"זניח, לא נחשב": "זניח, לא נחשב.text",
		"שמא, אולי": "שמא, אולי.text",
		"מרובה": "מרובה.text",
		"נשארת השאלה בלי פתרון": "נשארת השאלה בלי פתרון.text",
		"גם": "גם.text",
		"מה התשובה לכל השאלות הללו": "מה התשובה לכל השאלות הללו.text",
	},
	"parameters": {
		"numOfItems": 9,
		"placeholders": [
			{ "id": "מהי הסיבה_1", "loc": { "x": -19, "y": 294 }, "rect": { "width": 140, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "זניח, לא נחשב_2", "loc": { "x": -259, "y": 294 }, "rect": { "width": 143, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "זניח, לא נחשב_3", "loc": { "x": -225, "y": 331 }, "rect": { "width": 143, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, אולי_4", "loc": { "x": 392, "y": 367 }, "rect": { "width": 68, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מרובה_5", "loc": { "x": 217, "y": 367 }, "rect": { "width": 92, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מרובה_6", "loc": { "x": 194, "y": 403 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "נשארת השאלה בלי פתרון_7", "loc": { "x": -244, "y": 403 }, "rect": { "width": 56, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "גם_8", "loc": { "x": -63, "y": 331 }, "rect": { "width": 38, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "מה התשובה לכל השאלות הללו_9", "loc": { "x": -171, "y": 403 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "מהי הסיבה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מהי הסיבה_1"],
			},
			{
				"type": "זניח, לא נחשב", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זניח, לא נחשב_2", "זניח, לא נחשב_3"],
			},
			{
				"type": "זניח, לא נחשב", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "זניח, לא נחשב_2", "זניח, לא נחשב_3"],
			},
			{
				"type": "שמא, אולי", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, אולי_4"],
			},
			{
				"type": "מרובה", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מרובה_5", "מרובה_6"],
			},
			{
				"type": "מרובה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מרובה_5", "מרובה_6"],
			},
			{
				"type": "נשארת השאלה בלי פתרון", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "נשארת השאלה בלי פתרון_7"],
			},
			{
				"type": "גם", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "גם_8"],
			},
			{
				"type": "מה התשובה לכל השאלות הללו", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "מה התשובה לכל השאלות הללו_9"],
			},
		],
		"ex_name": "babametzia_21a_kav_tamrey_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}