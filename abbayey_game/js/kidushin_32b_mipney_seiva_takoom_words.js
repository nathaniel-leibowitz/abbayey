var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_32b_bottom_web.png",
		"תלמוד לומר": "תלמוד לומר.text",
		"הפסד כספי": "הפסד כספי.text",
		"שירותים": "שירותים.text",
		"שמא, היתכן": "שמא, היתכן.text",
		"יעמיד פנים": "יעמיד פנים.text",
		"יגרום לציבור להדר אותו": "יגרום לציבור להדר אותו.text",
		"תנו רבנן, ברייתא": "תנו רבנן, ברייתא.text",
	},
	"parameters": {
		"numOfItems": 15,
		"placeholders": [
			{ "id": "תלמוד לומר_1", "loc": { "x": 133, "y": -209 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_2", "loc": { "x": -18, "y": -63 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_3", "loc": { "x": 56, "y": 10 }, "rect": { "width": 47, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_4", "loc": { "x": 613, "y": 155 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_5", "loc": { "x": 195, "y": 192 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תלמוד לומר_6", "loc": { "x": -75, "y": 83 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "הפסד כספי_7", "loc": { "x": 61, "y": 46 }, "rect": { "width": 134, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שירותים_8", "loc": { "x": 215, "y": 83 }, "rect": { "width": 149, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, היתכן_9", "loc": { "x": -154, "y": -99 }, "rect": { "width": 53, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, היתכן_10", "loc": { "x": -240, "y": -27 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, היתכן_11", "loc": { "x": 478, "y": 83 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שמא, היתכן_12", "loc": { "x": 94, "y": 119 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יעמיד פנים_13", "loc": { "x": -6, "y": 119 }, "rect": { "width": 137, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "יגרום לציבור להדר אותו_14", "loc": { "x": 264, "y": 192 }, "rect": { "width": 71, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "תנו רבנן, ברייתא_15", "loc": { "x": -240, "y": -281 }, "rect": { "width": 50, "height": 32 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "תלמוד לומר", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תלמוד לומר_1", "תלמוד לומר_2", "תלמוד לומר_3", "תלמוד לומר_4", "תלמוד לומר_5", "תלמוד לומר_6"],
			},
			{
				"type": "הפסד כספי", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "הפסד כספי_7"],
			},
			{
				"type": "שירותים", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שירותים_8"],
			},
			{
				"type": "שמא, היתכן", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, היתכן_9", "שמא, היתכן_10", "שמא, היתכן_11", "שמא, היתכן_12"],
			},
			{
				"type": "שמא, היתכן", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, היתכן_9", "שמא, היתכן_10", "שמא, היתכן_11", "שמא, היתכן_12"],
			},
			{
				"type": "שמא, היתכן", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, היתכן_9", "שמא, היתכן_10", "שמא, היתכן_11", "שמא, היתכן_12"],
			},
			{
				"type": "שמא, היתכן", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שמא, היתכן_9", "שמא, היתכן_10", "שמא, היתכן_11", "שמא, היתכן_12"],
			},
			{
				"type": "יעמיד פנים", 
				"score": 1,
				"loc": { "x": -700, "y": 240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יעמיד פנים_13"],
			},
			{
				"type": "יגרום לציבור להדר אותו", 
				"score": 1,
				"loc": { "x": -700, "y": -280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "יגרום לציבור להדר אותו_14"],
			},
			{
				"type": "תנו רבנן, ברייתא", 
				"score": 1,
				"loc": { "x": -700, "y": 280 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "תנו רבנן, ברייתא_15"],
			},
		],
		"ex_name": "kidushin_32b_mipney_seiva_takoom_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}