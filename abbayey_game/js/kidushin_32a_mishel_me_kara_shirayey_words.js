var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/kidushin_32atop.png",
		"וכמו זה": "וכמו זה.text",
		"אלך": "אלך.text",
		"בפני, בנוכחות": "בפני, בנוכחות.text",
		"אראה": "אראה.text",
		"והרי": "והרי.text",
		"שעשה": "שעשה.text",
		"ושמא": "ושמא.text",
		"לכבוד שלו": "לכבוד שלו.text",
		"בתפר של הבגד": "בתפר של הבגד.text",
		"בגד משי יקר": "בגד משי יקר.text",
	},
	"parameters": {
		"numOfItems": 12,
		"placeholders": [
			{ "id": "וכמו זה_1", "loc": { "x": -205, "y": 186 }, "rect": { "width": 89, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אלך_2", "loc": { "x": -49, "y": 254 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בפני, בנוכחות_3", "loc": { "x": 214, "y": 254 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "אראה_4", "loc": { "x": -118, "y": 254 }, "rect": { "width": 65, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי_5", "loc": { "x": 206, "y": 358 }, "rect": { "width": 50, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שעשה_6", "loc": { "x": -217, "y": 392 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ושמא_7", "loc": { "x": 53, "y": 289 }, "rect": { "width": 83, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לכבוד שלו_8", "loc": { "x": -208, "y": 323 }, "rect": { "width": 89, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שעשה_9", "loc": { "x": -163, "y": 358 }, "rect": { "width": 71, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ושמא_10", "loc": { "x": 92, "y": 392 }, "rect": { "width": 92, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בתפר של הבגד_11", "loc": { "x": 188, "y": 392 }, "rect": { "width": 110, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "בגד משי יקר_12", "loc": { "x": -216, "y": 220 }, "rect": { "width": 74, "height": 29 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "וכמו זה", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "וכמו זה_1"],
			},
			{
				"type": "אלך", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אלך_2"],
			},
			{
				"type": "בפני, בנוכחות", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בפני, בנוכחות_3"],
			},
			{
				"type": "אראה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "אראה_4"],
			},
			{
				"type": "והרי", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי_5"],
			},
			{
				"type": "שעשה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שעשה_6", "שעשה_9"],
			},
			{
				"type": "ושמא", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושמא_7", "ושמא_10"],
			},
			{
				"type": "לכבוד שלו", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לכבוד שלו_8"],
			},
			{
				"type": "שעשה", 
				"score": 1,
				"loc": { "x": -700, "y": 160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שעשה_6", "שעשה_9"],
			},
			{
				"type": "ושמא", 
				"score": 1,
				"loc": { "x": -700, "y": -200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ושמא_7", "ושמא_10"],
			},
			{
				"type": "בתפר של הבגד", 
				"score": 1,
				"loc": { "x": -700, "y": 200 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בתפר של הבגד_11"],
			},
			{
				"type": "בגד משי יקר", 
				"score": 1,
				"loc": { "x": -700, "y": -240 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "בגד משי יקר_12"],
			},
		],
		"ex_name": "kidushin_32a_mishel_me_kara_shirayey_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}