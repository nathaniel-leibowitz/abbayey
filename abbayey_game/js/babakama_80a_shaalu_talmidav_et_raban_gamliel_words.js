var mGameComponent;

var mGameData1 = {

    "assets": {
        "home_btn": "storage/miniGames/gemara/buttons/home_btn.png",
        "restart_btn": "storage/miniGames/gemara/buttons/restart_btn.png",
        "oc_logo": "storage/miniGames/gemara/buttons/onecode_logo.png",
		

		//---------------------------------------replace the following from output of the sugiya generator (untill the similar comment below)translate_prefix.js
		"backGround": "storage/miniGames/gemara/page_sections/babakama_80a_top.png",
		"רבן גמליאל": "רבן גמליאל.text",
		"והרי למדנו במשנה": "והרי למדנו במשנה.text",
		"כך, בלשון הזו": "כך, בלשון הזו.text",
		"שאלו, בקשו תשובה": "שאלו, בקשו תשובה.text",
		"ממנו, מרבן גמליאל": "ממנו, מרבן גמליאל.text",
		"לגדל בהמה": "לגדל בהמה.text",
		"להחזיק בהמה באופן זמני": "להחזיק בהמה באופן זמני.text",
		"לרגליים, למוטות": "לרגליים, למוטות.text",
	},
	"parameters": {
		"numOfItems": 8,
		"placeholders": [
			{ "id": "רבן גמליאל_1", "loc": { "x": -20, "y": -184 }, "rect": { "width": 41, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "והרי למדנו במשנה_2", "loc": { "x": 124, "y": -148 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "כך, בלשון הזו_3", "loc": { "x": -155, "y": -148 }, "rect": { "width": 44, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "שאלו, בקשו תשובה_4", "loc": { "x": -237, "y": -148 }, "rect": { "width": 95, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "ממנו, מרבן גמליאל_5", "loc": { "x": -330, "y": -148 }, "rect": { "width": 65, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לגדל בהמה_6", "loc": { "x": -140, "y": -184 }, "rect": { "width": 59, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "להחזיק בהמה באופן זמני_7", "loc": { "x": 52, "y": -112 }, "rect": { "width": 86, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
			{ "id": "לרגליים, למוטות_8", "loc": { "x": -327, "y": -76 }, "rect": { "width": 74, "height": 31 }, "isReused": false, "offsetX": 0, "offsetY": 0, "url": "" },
		],
		"items": [
			{
				"type": "רבן גמליאל", 
				"score": 1,
				"loc": { "x": -700, "y": 0 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "רבן גמליאל_1"],
			},
			{
				"type": "והרי למדנו במשנה", 
				"score": 1,
				"loc": { "x": -700, "y": -40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "והרי למדנו במשנה_2"],
			},
			{
				"type": "כך, בלשון הזו", 
				"score": 1,
				"loc": { "x": -700, "y": 40 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "כך, בלשון הזו_3"],
			},
			{
				"type": "שאלו, בקשו תשובה", 
				"score": 1,
				"loc": { "x": -700, "y": -80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "שאלו, בקשו תשובה_4"],
			},
			{
				"type": "ממנו, מרבן גמליאל", 
				"score": 1,
				"loc": { "x": -700, "y": 80 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "ממנו, מרבן גמליאל_5"],
			},
			{
				"type": "לגדל בהמה", 
				"score": 1,
				"loc": { "x": -700, "y": -120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לגדל בהמה_6"],
			},
			{
				"type": "להחזיק בהמה באופן זמני", 
				"score": 1,
				"loc": { "x": -700, "y": 120 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "להחזיק בהמה באופן זמני_7"],
			},
			{
				"type": "לרגליים, למוטות", 
				"score": 1,
				"loc": { "x": -700, "y": -160 },
				"scale": { "x": 1, "y": 1 },
				"placeholderIds": [ "לרגליים, למוטות_8"],
			},
		],
		"ex_name": "babakama_80a_shaalu_talmidav_et_raban_gamliel_words",
		//---------------------------------------replace the above from output of the sugiya generator (begin from the similar comment above)
		
		
		
		
		"compatability": {
			"placeholders_move": {
				"x": 0,
				"y": 0,
			},
			"symbols_scale": 1,
			"snap_offset_from_placeholder_move": {
				"x": 0,
				"y": 0,
			},
		},
        "showExName": false,
        "debugMode": true,
		"showMousePos": false,
        "itemHoverScaleFactor": 1.2, // sets scale when hovering on items in left panel
        "showHitFeedback": true, // sets rectangle color to green
        "isPlaceItem": false,
        "itemTextSize": 32,
        "showVersion": true,
        "offsetX": 100,
        "offsetY": 0,
        "showScore": true,
        "hitAction": {
            "isToRemove": false,
            "isStickToPlace": true,
            "shrink": [{ "time": 0.2, "delay": 0, "transition": "easeOutBack", "toValue": 0.9 }, { "time": 0.3, "delay": 0.2, "transition": "easeOutBounce", "toValue": 1 }],
            "snapToPoint": [{ "time": 0.8, "delay": 0, "transition": "easeOutBounce" }]
        },
        "missAction": {
            "minDistanc": 45,
            "isToRemove": false,
            "backToStartPoint": [{ "time": 0.9, "delay": 0, "transition": "easeOutBack" }]
        },
    },
	
	"id": "DragEngine",
    "variation": "Presents-Preset1",
    "type": "game",
    "isDrawRectsAroundPlaceholder": true,
    "resources": [
        "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/4.5.3/pixi.min.js",
        "storage/miniGames/DragEngine/js/DragEngine.js",
        "storage/miniGames/common/tweener.js"
    ],
};
//__________________________________________________________

window.onload = function () {
    onecode.Main.loadPageAssets(mGameData1);
    startGame();
};

function startGame() {
    mGameComponent = new ocBase.CoComponentBase(mGameData1, document.getElementById("TapGame"));
    ocBase.ResourcesManager.isResourcesLoaded(mGameData1.resources, onAssetsLoaded);
}
//__________________________________________________________

function onAssetsLoaded() {
    window[mGameData1.id].init(mGameComponent);
}