//@ sourceURL=DragEngine.js
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var DataManager = /** @class */ (function () {
            function DataManager() {
            }
            DataManager.getPlaceholderById = function (pPlaceholderId) {
                for (var i = 0; i < DataManager.data.placeholders.length; i++) {
                    if (DataManager.data.placeholders[i].id == pPlaceholderId) {
                        return DataManager.data.placeholders[i];
                    }
                }
            };
            return DataManager;
        }());
        drag.DataManager = DataManager;
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../oc/drag/DataManager.ts" />
var firebase = firebase;
var oc;
(function (oc) {
    var firebase_utils;
    (function (firebase_utils) {
        var DataManager = oc.drag.DataManager;
        var FirebaseManager = /** @class */ (function () {
            //______________________________________________________________________________________________________________
            function FirebaseManager() {
                this.mDatabaseRef = firebase.database();
            }
            //______________________________________________________________________________________________________________
            FirebaseManager.instance = function () {
                if (this.mInstance == null) {
                    this.mInstance = new FirebaseManager();
                }
                return this.mInstance;
            };
            //______________________________________________________________________________________________________________
            FirebaseManager.prototype.uploadScore = function (pPlayerName, pScore, pMisses, pPlayerGroup, pTimeInSeconds, pTrials, pSuccess, pFinishCallback) {
                var aDate = new Date();
                var aDateStr = aDate.toLocaleDateString() + "    " + aDate.toLocaleTimeString();
				var msEpoch = aDate.getTime();
                var aDataJson = {
                    student_name: pPlayerName,
                    ex_name: DataManager.data.ex_name,
                    group_name: pPlayerGroup,
					items: DataManager.data.numOfItems,
					didComplete: (DataManager.data.numOfItems === pScore),
					success: pSuccess,
                    score: pScore,
                    misses: pMisses,
					missPerc: Math.round(pMisses*100/DataManager.data.numOfItems),
                    date: aDateStr,
					msFromEpoch: msEpoch,
					time: pTimeInSeconds,
                    trials: pTrials
                };
                this.mDatabaseRef.ref("Score Board").child(pPlayerGroup + "/" + pPlayerName + "/" + DataManager.data.ex_name + "/" + aDate.getTime().toString()).set(aDataJson, function () {
                    if (pFinishCallback)
                        pFinishCallback();
                });
            };
            return FirebaseManager;
        }());
        firebase_utils.FirebaseManager = FirebaseManager;
    })(firebase_utils = oc.firebase_utils || (oc.firebase_utils = {}));
})(oc || (oc = {}));
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var Container = /** @class */ (function () {
            function Container(pItemParams, pGameParams) {
                this.mDragging = false;
                this.mTargetPOW2Raduse = -1;
                this.mState = drag.Item.STATE_ACTIVE;
                this.mGameParams = pGameParams;
                this.mItemParams = pItemParams;
                var aImagePath = oc.drag.pixiExtention.MiniGameBase.data.assets[pItemParams.type];
                var aJsonPath = oc.drag.pixiExtention.MiniGameBase.data.assets[pItemParams.type + "_json"];
                this.mItem = new oc.drag.pixiExtention.OcPIXIAnimation(aImagePath, aJsonPath, 0, "idle");
                if (pItemParams.loc == null) {
                    var aConnected = this.getConnectedPlaceholder(pItemParams.placeholderId);
                    if (aConnected != null) {
                        pItemParams.loc = aConnected.loc;
                    }
                    else {
                        pItemParams.loc = { x: 0, y: 0 };
                    }
                }
                this.mItem.x = pItemParams.loc.x;
                this.mItem.y = pItemParams.loc.y;
                this.mItem.interactive = true;
                this.mItem.setAnimation(null, "idle", false);
                this.placeholderId = pItemParams.placeholderId;
            }
            //______________________________________________________
            Container.prototype.getConnectedPlaceholder = function (pPlaceholderId) {
                for (var i = 0; i < this.mGameParams.placeholders.length; i++) {
                    if (this.mGameParams.placeholders[i].id == pPlaceholderId) {
                        return this.mGameParams.placeholders[i];
                    }
                }
                return null;
            };
            //______________________________________________________
            Container.prototype.playGood = function () {
                var _this = this;
                this.mItem.setAnimation(null, "hit", false, function () { return _this.onAnimationEnd(); });
            };
            //______________________________________________________
            Container.prototype.playMiss = function () {
                this.mItem.setAnimation(null, "miss", false);
            };
            //____________________________________________________________________
            Container.prototype.onAnimationEnd = function () {
                if (this.mItemParams.removeOnHit) {
                    if (this.mItem.parent != null) {
                        this.mItem.parent.removeChild(this.mItem);
                    }
                }
            };
            Object.defineProperty(Container.prototype, "state", {
                //____________________________________________________________________
                get: function () {
                    return this.mState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Container.prototype, "display", {
                //_____________________________________________________
                get: function () {
                    return this.mItem;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Container.prototype, "item", {
                //______________________________________________________
                get: function () {
                    return this.mItem;
                },
                enumerable: true,
                configurable: true
            });
            //______________________________________________________
            Container.prototype.tick = function (delta) {
                this.mItem.update();
            };
            //______________________________________________________
            Container.prototype.destract = function () {
                this.mItem.destroy();
            };
            return Container;
        }());
        drag.Container = Container;
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../../../packages/pixi.js.d.ts" />
/// <reference path="../../../packages/screemowrapper.d.ts" />
/// <reference path="../../firebase_utils/FirebaseManager.ts" />
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var pixiExtention;
        (function (pixiExtention) {
            var FirebaseManager = oc.firebase_utils.FirebaseManager;
            var MiniGameBase = /** @class */ (function () {
                function MiniGameBase() {
                }
                //____________________________________________________________
                MiniGameBase.prototype.init = function (pPage) {
                    var _this = this;
                    ocBase.events.EventManager.addEventListener(onecode.Events.EVENT_MINI_GAME_FINISH, function (pData) { return _this.onGameFinished(pData.mData); }, this);
                    ocBase.events.EventManager.addEventListener(onecode.Events.EVENT_MINI_GAME_START_HARDENING, function () { return _this.onGameStartHardening(); }, this);
                    this.mIsActive = true;
                    MiniGameBase.data = pPage.data;
                    MiniGameBase.isDrawRectsAroundPlaceholder = (MiniGameBase.data.isDrawRectsAroundPlaceholder);
                    if (this.mPIXIapp != null) {
                        this.mPIXIapp.destroy();
                    }
                    this.mPIXIapp = new PIXI.Application(768, 1024, { backgroundColor: 0x556688 }, true);
                    pPage.contentWrapper.appendChild(this.mPIXIapp.view);
                    this.mPIXIapp.view.style.position = "fixed";
                    this.mPIXIapp.view.style.top = "0px";
                    this.mPIXIapp.view.style.left = "0px";
                    this.mGamePanel = new PIXI.Container();
                    this.mPIXIapp.stage.addChild(this.mGamePanel);
                    this.resizeScreen(); // Call to resize screen
                    this.startGame();
                    this.mTickFunction = function (delta) { return _this.tick(delta); };
                    this.mPIXIapp.ticker.add(this.mTickFunction);
                    window.addEventListener('resize', function () { return _this.resizeScreen(); }, false);
                    drag.Dashboard.instance.resizeScreen(); // Call to Dashboard resizeScreen;
                };
                //____________________________________________________________
                /// Resize Screen
                MiniGameBase.prototype.resizeScreen = function () {
                    var _this = this;
                    var w = window.innerWidth;
                    var h = window.innerHeight;
                    /*var clientH = document.documentElement.clientHeight;
                    var screenH = window.screen.height;
                    var screenW = window.screen.width;
                    var r = window.devicePixelRatio;

                    alert("w=" + w + " h=" + h + " r=" + r + " wr=" + w*r + " hr=" + h*r + " clientH=" + clientH + " screenH=" + screenH + " screenW=" + screenW);
                    */
                    //unfortunately in mobile when chaning orientation to landscape, the innerWidth and height are bogus. Didnt find a solution yet (tried the commented attributes).
                    //An alternative is to use the metadata content="width=device-width in the sugiya_dispatcher, but the price is that it treats the screen size smaller by a factor of devicePixelRatio
                    this.mPIXIapp.view.width = w;
                    this.mPIXIapp.view.height = h;
                    

                    var yScale = h / MiniGameBase.BASE_HEIGHT;
                    if (yScale > 1)
                        yScale = 1;
                    //There are 3 approaches:
                    //1. Only scale down. Scaling always blurs so do it only when really in need (hence cap aScale=1 if its larger)
                    //2. Always scale.
                    //3. Never scalse since it blurs, and then if window is too small, picture will be cut when overflowing.

                    var minWidthRequirement = 1660;//estimated by the average sugiya width (1020) and space for 12 dragable items at its left side with the 13 and 14 items a bit cut but dragable
                    var xScale = w / minWidthRequirement;
                    if (xScale > 1)
                        xScale = 1;

                    var aScale = Math.min(yScale, xScale);


                    this.mGamePanel.scale.x = this.mGamePanel.scale.y = aScale;
                    var aX = (w - MiniGameBase.BASE_HEIGHT * 2 * aScale) / 2;
                    this.mGamePanel.y = h / 2;
                    this.mGamePanel.x = w / 2;

                    MiniGameBase.SCREEN_RECT = { top: 0, left: 0, bottom: 0, right: 0 };
                    MiniGameBase.SCREEN_RECT.top = -MiniGameBase.BASE_HEIGHT / 2;
                    MiniGameBase.SCREEN_RECT.left = -w / aScale / 2;
                    MiniGameBase.SCREEN_RECT.bottom = MiniGameBase.BASE_HEIGHT / 2;
                    MiniGameBase.SCREEN_RECT.right = w / aScale / 2;

                    if (drag.Dashboard.instance != null) {
                        drag.Dashboard.instance.resizeScreen();
                    }
                    if (w > h) {
                        setTimeout(function () { return _this.resizeScreen(); }, 500);
                    }
                };

                //___________________________________________________________________
                MiniGameBase.prototype.startGame = function () { };
                //________________________________________________
                MiniGameBase.prototype.tick = function (delta) {
                    if (this.mIsActive) {
                        this.gameLoop(delta);
                    }
                };
                //________________________________________________
                MiniGameBase.prototype.gameLoop = function (delta) { };
                //_________________________________________________
                MiniGameBase.prototype.finishGame = function (pPlayerName, pScore, pMisses, pPlayerGroup, pTimeInSeconds, pFailMsg, pTrials, pSuccess) {
                    //this.mPIXIapp.ticker.remove(this.mTickFunction);
                    if (pScore === void 0) { pScore = 0; }
                    if (pMisses === void 0) { pMisses = 0; }
                    if (this.mIsActive) {
                        ocBase.events.EventManager.dispatchEvent(onecode.Events.EVENT_MINI_GAME_FINISH, this, { "playerName": pPlayerName, "score": pScore, "misses": pMisses,
                            "playerGroup": pPlayerGroup, "timeInSeconds": pTimeInSeconds, "failMsg": pFailMsg, "trials": pTrials, "success": pSuccess });
                    }
                    //this.mIsActive = false;
                };
                //_________________________________________________
                MiniGameBase.prototype.failed = function (pScore) {
                    if (pScore === void 0) { pScore = 0; }
                    this.mPIXIapp.ticker.remove(this.mTickFunction);
                    if (this.mIsActive) {
                        ocBase.events.EventManager.dispatchEvent(onecode.Events.EVENT_MINI_GAME_FINISH, this, { "succsess": false, "score": pScore });
                    }
                    this.mIsActive = false;
                };
                //_________________________________________________
                MiniGameBase.prototype.onGameFinished = function (pData) {
                    // TODO - upload score to firebase
                    FirebaseManager.instance().uploadScore(pData.playerName, pData.score, pData.misses, pData.playerGroup, pData.timeInSeconds, pData.trials, pData.success);
                    
                    if (pData.success) {
                        var txt = document.getElementById("modal_success_text");
                        txt.innerText = pData.failMsg;
                        $('#ModalSuccess').modal('show');
                    }
                    else {
                        var txt = document.getElementById("modal_failure_text");
                        txt.innerText = pData.failMsg;
                        $('#ModalFailure').modal('show');
                    }
                    this.mIsActive = false;
                    this.destruct();
                };
                //_________________________________________________
                MiniGameBase.prototype.onGameStartHardening = function () {
                    var txt = document.getElementById("modal_hardening_text");
                    txt.innerText = "רגע! עוד מאמץ קטן\n" + "לסיום התרגיל ורישום בטבלא קראו את הסוגיא\n" + "והקליקו      לפי הסדר      על הסימנים שגררתם\n" + "עד שכולם יהפכו לדלוקים\n";
                    $('#ModalHardening').modal('show');
                    this.destruct();
                };
                //_________________________________________________
                MiniGameBase.prototype.destruct = function () {
                };
                MiniGameBase.BASE_HEIGHT = 900;
                return MiniGameBase;
            }());
            pixiExtention.MiniGameBase = MiniGameBase;
        })(pixiExtention = drag.pixiExtention || (drag.pixiExtention = {}));
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../pixiextention/minigamebase.ts" />
/// <reference path="../../../packages/pixi.js.d.ts" />
/// <reference path="../../../packages/screemowrapper.d.ts" />
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var MiniGameBase = oc.drag.pixiExtention.MiniGameBase;
        var FirebaseManager = oc.firebase_utils.FirebaseManager;
        var Dashboard = /** @class */ (function (_super) {
            __extends(Dashboard, _super);
            function Dashboard() {
                var _this = _super.call(this) || this;
                _this.mStartTime = -1;
                _this.mNumOfHits = 0;
                _this.mNumOfMisses = 0;
                _this.mCurrentDisplayItems = 0;
                _this.mTimerPosX = 8;
                _this.mTimerPosY = -350;
                _this.mDate = new Date().getTime();
                _this.mGamePanelItems = new Array();
                if (!_this.hideTimer()) {
                    _this.mTimer = new PIXI.Text(Dashboard.TIMER_TEXT + 0, Dashboard.GAME_PANEL_TEXT_STYLE);
                    _this.mTimer.anchor.set(1, 0.5);
                    _this.addChild(_this.mTimer);
                }
                Dashboard.mInstance = _this;
                _this.mGameTime = drag.DataManager.data.time * 1000;
                _this.mNumOfHits = 0;
                _this.mNumOfMisses = 0;
                _this.mTrials = [];
                _this.setGamePanel();
                _this.showHomeBtn();
                _this.setTimer();
                _this.setStartScreen();
                _this.showHeader();
                _this.showVersion();
                _this.showOCLogo();
                _this.resizeScreen();
                _this.mPlayerGroup = _this.getUrlParameters("player_group");
                return _this;
            };

            Dashboard.prototype.hideTimer = function () {
                //recent survey indicated that students dont want time displayed
                return true;
            };

            Dashboard.prototype.hideHitCount = function () {
                //Lets hide for now. It confuses uer and does not add info
                return true;
            };

            //______________________________________________________________________________________________________________
            Dashboard.prototype.setGamePanel = function () {
                this.showPlayerName();
                this.showExName();
                if (!this.hideHitCount())
                    this.showHitCountText();
                this.showMissCountText();
                this.updateTimer();
                if (!this.hideTimer())
                    this.mGamePanelItems.push(this.mTimer);
                //this.addFrameToGamePanel();
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showPlayerName = function () {
                this.mPlayerName = this.getUrlParameters("player_name");
                return;//Screen space is scarce and we want it for phone displays. Lets skip this for now
                this.mUserNameText = new PIXI.Text(this.mPlayerName, { fontWeight: "bold",
                    fontFamily: 'Arial', fontSize: 12,
                    fill: 0xffffff,
                    align: 'center',
                    stroke: "0x000000",
                    strokeThickness: 2 });//Nathaniel: decreased font sizes and the like
                this.mUserNameText.text.fontsize(1);
                this.mUserNameText.anchor.set(1, 0.5);
                this.mGamePanelItems.push(this.mUserNameText);
                this.addChild(this.mUserNameText);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showExName = function () {
                if (drag.DataManager.data.showExName) {
                    this.mExName = drag.DataManager.data.ex_name;
                    this.mExNameText = new PIXI.Text(this.mExName, Dashboard.GAME_PANEL_TEXT_STYLE);
                    this.mExNameText.anchor.set(1, 0.5);
                    this.addChild(this.mExNameText);
                    this.mGamePanelItems.push(this.mExNameText);
                }
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showHitCountText = function () {
                this.mHitCountText = new PIXI.Text(Dashboard.HIT_COUNT_TEXT + this.mNumOfHits, Dashboard.GAME_PANEL_TEXT_STYLE);
                this.mHitCountText.anchor.set(1, 0.5);
                this.mGamePanelItems.push(this.mHitCountText);
                this.addChild(this.mHitCountText);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showMissCountText = function () {
                this.mMissCount = new PIXI.Text(Dashboard.MISS_COUNT_TEXT + this.mNumOfMisses, Dashboard.GAME_PANEL_TEXT_STYLE);
                this.mMissCount.anchor.set(1, 0.5);
                this.mGamePanelItems.push(this.mMissCount);
                this.addChild(this.mMissCount);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.addFrameToGamePanel = function () {
                if (this.mGamePanelItems == null || this.mGamePanelItems.length <= 0) {
                    return;
                }
                var aPixiCircle = new PIXI.Graphics();
                aPixiCircle.lineStyle(3, 0x000000);
                aPixiCircle.drawRect(MiniGameBase.SCREEN_RECT.right - Dashboard.GAME_PANEL_X_OFFSET - Dashboard.GAME_PANEL_WIDTH + 15, MiniGameBase.SCREEN_RECT.top + Dashboard.GAME_PANEL_Y_OFFSET - 30, Dashboard.GAME_PANEL_WIDTH, this.mGamePanelItems.length * 72);
                aPixiCircle.endFill();
                this.addChild(aPixiCircle);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showHomeBtn = function () {
                var _this = this;
                this.mHomeBtn = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["home_btn"]]);
                this.mHomeBtn.anchor.set(1, 0.5);
                this.mHomeBtn.x = this.mMissCount.x;
                this.mHomeBtn.y = MiniGameBase.SCREEN_RECT.bottom + 50;
                this.mHomeBtn.scale.set(0.4, 0.4);
                this.mHomeBtn.interactive = true;
                this.mHomeBtn.on("pointerover", function () {
                    document.body.style.cursor = "pointer";
                });
                this.mHomeBtn.on("pointerout", function () {
                    document.body.style.cursor = "auto";
                });
                this.mHomeBtn.on("mouseup", function () { return _this.onHomeBtnClick(); });
                this.mHomeBtn.on("touchend", function () { return _this.onHomeBtnClick(); });
                //this.addChild(this.mHomeBtn);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showOCLogo = function () {
                return;//Screen space is scarce and we want it for phone displays. Lets skip this for now
                this.mOCLogo = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["oc_logo"]]);
                this.mOCLogo.anchor.set(1, 11.5);
                this.mOCLogo.interactive = true;
                this.mOCLogo.on("mousedown", function () {
                    window.open("https://onecode.co.il/");
                });
                this.mOCLogo.on("pointerover", function () {
                    document.body.style.cursor = "pointer";
                });
                this.mOCLogo.on("pointerout", function () {
                    document.body.style.cursor = "auto";
                });
                this.addChild(this.mOCLogo);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.updateTimer = function () {
                var _this = this;
                if (this.mIsGameOver)
                    return;
                this.mTimeInSeconds = Math.floor((new Date().getTime() - this.mDate) / 1000);
                if (!_this.hideTimer())
                    this.mTimer.text = Dashboard.TIMER_TEXT + this.mTimeInSeconds.toString();
                setTimeout(function () { return _this.updateTimer(); }, 1000);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showVersion = function () {
                return;//Screen space is scarce and we want it for phone displays. Lets skip this for now
                if (drag.DataManager.data.showVersion) {
                    this.mVersionText = new PIXI.Text(Dashboard.VERSION_TEXT, {
                        fontFamily: 'Arial',
                        fontSize: 18,
                        fill: 0xffffff,
                        align: 'center'
                    });//Nathaniel: decreased font sizes and the like
                    this.mVersionText.anchor.set(1, 10);
                    this.addChild(this.mVersionText);
                }
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.showHeader = function () {
                if (MiniGameBase.data.assets["header"] != null) {
                    this.mHeader = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["header"]]);
                    this.mHeader.x = 0;
                    this.mHeader.anchor.set(0.5, 0);
                    this.addChild(this.mHeader);
                }
            };
            //_______ _______________________________________________________________________________________________________
            Dashboard.prototype.getUrlParameters = function (pParam) {
                var aUrl = new URL(document.location.href);
                return aUrl.searchParams.get(pParam);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.resizeScreen = function () {
                if (this.mHeader != null) {
                    this.mHeader.y = MiniGameBase.SCREEN_RECT.top;
                }
                if (this.mCounter != null) {
                    this.mCounter.y = MiniGameBase.SCREEN_RECT.bottom - 75;
                    this.mCounter.x = MiniGameBase.SCREEN_RECT.right - 65;
                }//Nathaniel: in the next few lines modified the positioning of panels
                for (var i = 0; i < this.mGamePanelItems.length; i++) {
                    this.mGamePanelItems[i]
                        .position.set(MiniGameBase.SCREEN_RECT.right - 25, MiniGameBase.SCREEN_RECT.top + Dashboard.GAME_PANEL_Y_OFFSET + (i * 60));
                }
                if (this.mHomeBtn != null) {
                    this.mHomeBtn.position.
                        set(MiniGameBase.SCREEN_RECT.right - 25, MiniGameBase.SCREEN_RECT.bottom - 200);
                }
                if (this.mOCLogo != null) {
                    this.mOCLogo.scale.set(0.75, 0.75);
                    this.mOCLogo.position.
                        set(MiniGameBase.SCREEN_RECT.right - 25, MiniGameBase.SCREEN_RECT.bottom - 25);
                }
                var aTimerTopY = (drag.DataManager.data.timerYPosition ? drag.DataManager.data.timerYPosition : 17) / 100;
                this.mTimerPosY = MiniGameBase.SCREEN_RECT.top + MiniGameBase.SCREEN_RECT.bottom * aTimerTopY * 2;
                this.mTime.y = this.mTimerPosY;
                this.mTimerFrame.y = this.mTimerPosY;
                this.mtimeIcon.y = this.mTimerPosY;
                this.mTimeMask.y = this.mTimerPosY - 25;
                if (this.mVersionText != null) {
                    this.mVersionText.x = MiniGameBase.SCREEN_RECT.right - 25;
                    this.mVersionText.y = MiniGameBase.SCREEN_RECT.bottom - 85;
                }
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.setStartScreen = function () {
                var _this = this;
                if (ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["start_screen"]] == null) {
                    this.startGame();
                    return;
                }
                this.mStartPage = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["start_screen"]]);
                this.mStartPage.x = 0;
                this.mStartPage.y = 0;
                this.addChild(this.mStartPage);
                this.mStartPage.anchor.set(0.5, 0.5);
                this.mStartButton = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["start_btn"]]);
                this.mStartButton.x = drag.DataManager.data.startButtonLocation ? drag.DataManager.data.startButtonLocation.x : 0;
                this.mStartButton.y = drag.DataManager.data.startButtonLocation ? drag.DataManager.data.startButtonLocation.y : 150;
                this.addChild(this.mStartButton);
                this.mStartButton.anchor.set(0.5, 0.5);
                this.mStartButton.interactive = true;
                this.mStartButton.on('mouseup', function (event) { return _this.onStartGame(); }).on('touchend', function (event) { return _this.onStartGame(); });
                clearTimeout(this.mAutoStartGameTimeout);
                clearTimeout(this.mStartGameTimeout);
                this.mAutoStartGameTimeout = setTimeout(function () { return _this.autoStartGame(); }, 3000);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.onStartGame = function () {
                var _this = this;
                Tweener.addTween(this.mStartButton.scale, { x: 0.95, y: 0.95, time: 0.1, delay: 0 });
                Tweener.addTween(this.mStartButton.scale, { x: 1.1, y: 1.1, time: 0.2, delay: 0.1, transition: "easeOutBounce" });
                clearTimeout(this.mAutoStartGameTimeout);
                clearTimeout(this.mStartGameTimeout);
                this.mStartGameTimeout = setTimeout(function () { return _this.startGame(); }, 350);
                ocBase.events.EventManager.dispatchEvent("EVENT_START_GAME_BUTTON_CLICK", this);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.autoStartGame = function () {
                this.startGame();
                ocBase.events.EventManager.dispatchEvent("EVENT_AUTO_START_GAME", this);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.startGame = function () {
                clearTimeout(this.mAutoStartGameTimeout);
                clearTimeout(this.mStartGameTimeout);
                this.removeChild(this.mStartButton);
                this.removeChild(this.mStartPage);
                this.startTime();
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.endGame = function () {
                this.destruct();
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.setTimer = function () {
                this.mTime = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["timer_bkg"]]);
                this.mTime.x = this.mTimerPosX;
                this.mTime.y = this.mTimerPosY;
                this.mTime.anchor.set(0.5, 0.5);
                this.addChild(this.mTime);
                this.mTimerFrame = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["timer_bar"]]);
                this.mTimerFrame.x = this.mTimerPosX - 10;
                this.mTimerFrame.y = this.mTimerPosY;
                this.mTimerFrame.anchor.set(0.5, 0.5);
                this.addChild(this.mTimerFrame);
                this.mtimeIcon = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["timer_icon"]]);
                this.mtimeIcon.x = this.mTimerPosX - 182;
                this.mtimeIcon.y = this.mTimerPosY;
                this.mtimeIcon.anchor.set(0.5, 0.5);
                this.addChild(this.mtimeIcon);
                this.mTimeMask = new PIXI.Graphics();
                this.mTimeMask.beginFill(0xff0000);
                this.mTimeMask.drawRect(0, 0, 700, 50);
                this.mTimeMask.x = this.mTimerPosX - 200;
                this.mTimeMask.y = this.mTimerPosY - 25;
                this.mTimerFrame.mask = this.mTimeMask;
                this.addChild(this.mTimeMask);
                if (drag.DataManager.data.showScore) {
                    if (ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["counter"]] != null) {
                        this.mCounter = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["counter"]]);
                        this.addChild(this.mCounter);
                        this.mCounter.anchor.set(0.5, 0.5);
                    }
                }
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.onTimerGraphic = function () {
                if (this.mStartTime < 0) {
                    return;
                }
                var aTimePassed = Date.now() - this.mStartTime;
                this.mTimerFrame.x = this.mTimerPosX - 30 - 395 * aTimePassed / this.mGameTime + 25;
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.startTime = function () {
                this.mStartTime = Date.now();
            };
            Object.defineProperty(Dashboard, "instance", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return Dashboard.mInstance;
                },
                enumerable: true,
                configurable: true
            });
            //______________________________________________________________________________________________________________
            // Add Score by scroll numbers // not in used in that version.
            Dashboard.prototype.onTimer = function () {
                if (this.mNumOfHits != this.mCurrentDisplayItems) {
                    var aScore = (this.mNumOfHits - this.mCurrentDisplayItems) / 5;
                    this.mCurrentDisplayItems += aScore;
                    if (Math.abs(aScore) < 0.5) {
                        this.mCurrentDisplayItems = this.mNumOfHits;
                    }
                    var aDisplayScore = Math.floor(this.mCurrentDisplayItems);
                }
            };
            Object.defineProperty(Dashboard.prototype, "numOfPlacedItems", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mNumOfHits;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dashboard.prototype, "playerName", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mPlayerName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dashboard.prototype, "numOfMisses", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mNumOfMisses;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dashboard.prototype, "playerGroup", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mPlayerGroup;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dashboard.prototype, "timeInSeconds", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mTimeInSeconds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dashboard.prototype, "isGameOver", {
                //______________________________________________________________________________________________________________
                get: function () {
                    return this.mIsGameOver;
                },
                //______________________________________________________________________________________________________________
                set: function (pVal) {
                    this.mIsGameOver = pVal;
                },
                enumerable: true,
                configurable: true
            });
            //______________________________________________________________________________________________________________
            Dashboard.prototype.gotAnItem = function (pIsHitGood, pTrial) {
                this.mTrials.push(pTrial);
                if (pIsHitGood) {
                    this.mNumOfHits++;
                    if (this.mHitCountText != null) {
                        this.mHitCountText.text = Dashboard.HIT_COUNT_TEXT + this.mNumOfHits;
                    }
                }
                else {
                    this.mNumOfMisses++;
                    if (this.mMissCount != null) {
                        this.mMissCount.text = Dashboard.MISS_COUNT_TEXT + this.mNumOfMisses;
                    }
                }
            };
            //______________________________________________________________________________________________________________
			Dashboard.prototype.onHomeBtnClick = function () {
                var _this = this;
                if (this.mIsGameOver) {
                    this.goToHomePage();
                }
                else {
                    FirebaseManager.instance().uploadScore(this.mPlayerName, this.mNumOfHits, this.mNumOfMisses, this.mPlayerGroup, this.mTimeInSeconds, this.mTrials, false, function () {
                        _this.goToHomePage();
                    });
                }
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.goToHomePage = function () {
                window.open("index.html?player_name=" + this.mPlayerName + "&player_group=" + this.mPlayerGroup, '_self');
				//window.open("index.html", '_self');
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.isFinished = function () {
                return (drag.DataManager.data.numOfItems == this.mNumOfHits);
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.isGameTimeout = function () {
                if (this.mStartTime < 0) {
                    return false;
                }
                var aLeftTime = this.mGameTime - (Date.now() - this.mStartTime);
                if ((aLeftTime > 500 && aLeftTime < 800) || (aLeftTime > 1500 && aLeftTime < 1800) || (aLeftTime > 2500 && aLeftTime < 2800)) {
                    this.mTime.visible = false;
                    this.mTimerFrame.visible = false;
                    this.mtimeIcon.visible = false;
                }
                else {
                    this.mTime.visible = true;
                    this.mTimerFrame.visible = true;
                    this.mtimeIcon.visible = true;
                }
                this.onTimerGraphic();
                return aLeftTime < 0;
            };
            //______________________________________________________________________________________________________________
            Dashboard.prototype.destruct = function () {
                clearInterval(this.mScoreTimerId);
                //this.onHomeBtnClick();
            };
            Dashboard.HIT_COUNT_TEXT = "ניקוד: ";
            Dashboard.MISS_COUNT_TEXT = "תיקונים: ";
            Dashboard.TIMER_TEXT = "זמן: ";
            Dashboard.GAME_PANEL_FONT_SIZE = 18;
            Dashboard.VERSION_TEXT = "v0.02";
            Dashboard.GAME_PANEL_X_OFFSET = 50;
            Dashboard.GAME_PANEL_Y_OFFSET = 70;
            Dashboard.GAME_PANEL_WIDTH = 400;
            Dashboard.GAME_PANEL_TEXT_STYLE = { fontWeight: "bold",
                fontFamily: 'Arial', fontSize: Dashboard.GAME_PANEL_FONT_SIZE,
                fill: 0xffffff,
                align: 'center',
                stroke: "0x000000",
                strokeThickness: 3 };
            return Dashboard;
        }(PIXI.Container));
        drag.Dashboard = Dashboard;
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../pixiextention/minigamebase.ts" />
/// <reference path="../../../packages/pixi.js.d.ts" />
/// <reference path="../../../packages/screemowrapper.d.ts" />
/// <reference path="container.ts" />
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var MiniGameBase = oc.drag.pixiExtention.MiniGameBase;
        var DragEngine = /** @class */ (function (_super) {
            __extends(DragEngine, _super);
            function DragEngine() {
                return _super.call(this) || this;
            }
            //________________________________________________
            /*override*/
            DragEngine.prototype.startGame = function () {
				this.readData();
				drag.DataManager.data = MiniGameBase.data.parameters;
				drag.DataManager.data.hitAction.alphaValWhenHit = 0.15;
                this.mIsUsed = new Array();
                var aBackGround = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["backGround"]]);
                aBackGround.anchor.set(0.5);
                var aBackgroundOffsetX = drag.DataManager.data.offsetX;
                var aBackgroundOffsetY = drag.DataManager.data.offsetY;
                aBackGround.position.x += aBackgroundOffsetX;
                aBackGround.position.y += aBackgroundOffsetY;
                this.mGamePanel.addChild(aBackGround);
                this.mItemsPanel = new PIXI.Container();
                this.mGamePanel.addChild(this.mItemsPanel);
                this.addItems();
                this.addContainers();
                if (!drag.DataManager.data.isPlaceholderOnTop) {
                    this.mGamePanel.addChild(this.mItemsPanel);
                }
                if (MiniGameBase.data.assets["topLayer"] != null) {
                    var aTopLayer = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["topLayer"]]);
                    aTopLayer.y = -MiniGameBase.BASE_HEIGHT / 2;
                    aTopLayer.x = 0;
                    this.mGamePanel.addChild(aTopLayer);
                }
                if (MiniGameBase.isDrawRectsAroundPlaceholder) {
                    this.drawwRectsAroundPlaceholder();
                }
                this.mDashboard = new drag.Dashboard();
                this.mGamePanel.addChild(this.mDashboard);
            };
            //______________________________________________________
            DragEngine.prototype.getSetPanel = function () {
                return this.mGamePanel;
            };
            //______________________________________________________
            DragEngine.prototype.addContainers = function () {
                this.mContainers = new Array();
                if (drag.DataManager.data.containers != null) {
                    for (var i = 0; i < drag.DataManager.data.containers.length; i++) {
                        var aContainers = new drag.Container(drag.DataManager.data.containers[i], drag.DataManager.data);
                        this.mGamePanel.addChild(aContainers.display);
                        this.mContainers.push(aContainers);
                    }
                }
            };
            //______________________________________________________
            DragEngine.prototype.isInTarget = function (pItem) {
                if (oc.drag.DragEngine.selectedPlaceholder == null) {
                    return null;
                }
                var aSelected = oc.drag.DragEngine.selectedPlaceholder.placeholder;
                if (aSelected == null) {
                    return null;
                }
                if (this.mIsUsed.indexOf(aSelected.id) > -1) {
                    return null;
                }
                // If item was inserted to the wrong placeholder
                if (pItem.placeholderIds.indexOf(aSelected.id) == -1) {
                    return { id: aSelected.id, x: aSelected.loc.x, y: aSelected.loc.y, hit: false };
                }
                var aPlaceholder = oc.drag.DragEngine.selectedPlaceholder;
                // If hit was correct, turn placeholder white
                if (!aSelected.isReused) {
                    this.mIsUsed.push(aSelected.id);
                    if (aSelected.arrow) {
						aSelected.arrowFocus.alpha = 0;//Nathaniel: support for changing colors of hotspot when hovering. Here we need to turn off the focus, do guard against the race of the onDragEnd
                        aSelected.arrow.alpha = 0;
                    }
                    aPlaceholder.clear();
                    if (drag.DataManager.data.showHitFeedback) {
                        aPlaceholder.lineStyle(3, 0x00ff00);
                        aPlaceholder.beginFill(0x00ff00, 0.6);
                        aPlaceholder.drawRect(aSelected.loc.x - (aSelected.rect.width / 2), aSelected.loc.y -
                            (aSelected.rect.height / 2), aSelected.rect.width, aSelected.rect.height);
                        aPlaceholder.endFill();
                        aPlaceholder.interactive = false;
                        this.mGamePanel.addChild(aPlaceholder);
                    }
                }
                var aX = 1000000000;
                var aY = 1000000000;
                if (drag.DataManager.data.isPlaceItem) {
                    aX = aSelected.loc.x + aSelected.offsetX;//Nathaniel: I think this is where the updated version handles that the collision is around the arrow and not the sticky location
                    aY = aSelected.loc.y + aSelected.offsetY;
                }
                return { id: aSelected.id, x: aX, y: aY, placeholderX: aSelected.loc.x, placeholderY: aSelected.loc.y, hit: true };
            };
			
			//______________________________________________________
            DragEngine.prototype.readData = function () {
                drag.DataManager.data = MiniGameBase.data.parameters;
				var compatability = drag.DataManager.data.compatability;
				//Nathaniel: new configurable parameters that will assist in translating coordinates generated by external tool, to these. 
				//If all systems are well coordinated, values will probably remain 0. But these params enable correcting small systematic misalignments, by displacing all hotspots by a fixed vector
				if (compatability == null)
					return;
				
				for (var i = 0; i < drag.DataManager.data.placeholders.length; i++) 
				{
					if ((compatability.placeholders_move != null) && (compatability.placeholders_move.x != null))
						drag.DataManager.data.placeholders[i].loc.x += compatability.placeholders_move.x;
					if ((compatability.placeholders_move != null) && (compatability.placeholders_move.y != null))
                        drag.DataManager.data.placeholders[i].loc.y += compatability.placeholders_move.y;
					
					if ((compatability.snap_offset_from_placeholder_move != null) && (compatability.snap_offset_from_placeholder_move.x != null))
						drag.DataManager.data.placeholders[i].offsetX += compatability.snap_offset_from_placeholder_move.x;
					if ((compatability.snap_offset_from_placeholder_move != null) && (compatability.snap_offset_from_placeholder_move.y != null))
                        drag.DataManager.data.placeholders[i].offsetY += compatability.snap_offset_from_placeholder_move.y;
				}
					

				for (var i = 0; i < drag.DataManager.data.items.length; i++) 
				{
					if (compatability.symbols_scale != null)
						drag.DataManager.data.items[i].scale.x *= compatability.symbols_scale;
					if (compatability.symbols_scale != null)
                        drag.DataManager.data.items[i].scale.y *= compatability.symbols_scale;
                        
                    drag.DataManager.data.items[i].loc.x += 50;//Making the dragable items a bit more adjacent to the sugiya
				}
            };
			
            //______________________________________________________
            DragEngine.prototype.addItems = function () {
                this.mItems = new Array();
                for (var i = 0; i < drag.DataManager.data.items.length; i++) {
                    var aItem = new drag.Item(drag.DataManager.data.items[i], drag.DataManager.data, this);
                    this.mItemsPanel.addChild(aItem.display);
                    this.mItems.push(aItem);
                }
            }
            //______________________________________________________
            DragEngine.prototype.getContainerById = function (pPlaceholderId) {
                for (var i = 0; i < this.mContainers.length; i++) {
                    if (this.mContainers[i].placeholderId == pPlaceholderId) {
                        return this.mContainers[i];
                    }
                }
                return null;
            };
            //______________________________________________________
            /*override*/
            DragEngine.prototype.gameLoop = function (delta) {
                for (var i = 0; i < this.mContainers.length; i++) {
                    this.mContainers[i].tick(delta);
                }
                for (var i = 0; i < this.mItems.length; i++) {
                    this.mItems[i].tick(delta);
                }
                this.checkGameEnd();
            };
            //_________________________________________________
            DragEngine.prototype.checkGameEnd = function () {
                successMsg = "התרגיל הסתיים בהצלחה\n" + "ונרשמתם בטבלא\n";
                if (this.mDashboard.isFinished() || this.mDashboard.isGameTimeout()) {
                    //this.mDashboard.endGame();
                    //this.mPIXIapp.ticker.remove(this.mTickFunction);
                    this.mDashboard.isGameOver = true;
                    var aFailMsg = void 0;
                    var success = false;
                    var allowedMissesAsPercent = 30.0;
                    var allowedMisses = Math.ceil(drag.DataManager.data.numOfItems * 0.3);
                    if (this.mDashboard.numOfMisses > allowedMisses) {
                        aFailMsg = "רגע! מידי הרבה תיקונים ולכן לא נרשמתם בטבלא\n" + "נא לתרגל שוב עד שתירשמו בטבלא\n";
                        success = false;
                        this.finishGame(this.mDashboard.playerName, this.mDashboard.numOfPlacedItems, this.mDashboard.numOfMisses, this.mDashboard.playerGroup, this.mDashboard.timeInSeconds, aFailMsg, this.mDashboard.mTrials, success);
                    }
                    else {
                        if (!drag.DataManager.data.isPlaceItem) {//translation phase, finish the game
                            success = true;
                            this.finishGame(this.mDashboard.playerName, this.mDashboard.numOfPlacedItems, this.mDashboard.numOfMisses, this.mDashboard.playerGroup, this.mDashboard.timeInSeconds, successMsg, this.mDashboard.mTrials, success);
                        }
                        else {//syntax phase, so we need to bother with the hardening phase
                            if (!drag.isHardening) {
                                drag.isHardening = true;
                                drag.finishedHardening = false;
                                //lets sort them according to their placeholder (arrow) location, 
                                //so that in the hardening phase, can track and enforce the order of what is hardened
                                this.mItems.sort(function (a, b) {
                                    if (a.mItem.placeholder.y == b.mItem.placeholder.y)
                                        return (b.mItem.placeholder.x - a.mItem.placeholder.x);
                                    else
                                        return (a.mItem.placeholder.y - b.mItem.placeholder.y);
                                });
                                ocBase.events.EventManager.dispatchEvent(onecode.Events.EVENT_MINI_GAME_START_HARDENING, this);
                            }
                            else {
                                if (drag.finishedHardening) {
                                    success = true;
                                    this.finishGame(this.mDashboard.playerName, this.mDashboard.numOfPlacedItems, this.mDashboard.numOfMisses, this.mDashboard.playerGroup, this.mDashboard.timeInSeconds, successMsg, this.mDashboard.mTrials, success);
                                }
                            }
                        }
                    }
                    
                }
            };
            //_________________________________________________
            DragEngine.prototype.drawwRectsAroundPlaceholder = function () {
                if (drag.DataManager.data.movementZone != null) {
                    var aPixiCircle = new PIXI.Graphics();
                    var aRect = drag.DataManager.data.movementZone;
                    aPixiCircle.lineStyle(3, 0xFF00FF);
                    aPixiCircle.beginFill(0xAA00AA, 0.5);
                    aPixiCircle.drawRect(aRect.left, aRect.top, aRect.width, aRect.height);
                    aPixiCircle.endFill();
                    this.mGamePanel.addChild(aPixiCircle);
                }
                var _loop_1 = function (i) {
                    var aArrow;
					var aArrowFocus;//Nathaniel: supporting changing  hotspot on hover by adding a duplicate hotspot with some difference (i.e. different color)
                    var aPlaceholder = drag.DataManager.data.placeholders[i];
                    aPlaceholder.loc.x += drag.DataManager.data.offsetX;
                    aPlaceholder.loc.y += drag.DataManager.data.offsetY;
					
					/* Nathaniel: Looks like unnecessary code that I added
					if (aPlaceholder.offsetX == null)
						aPlaceholder.offsetX = 0;
					if (aPlaceholder.offsetY == null)
						aPlaceholder.offsetY = 0;
					*/
					
                    var aPixiCircle = new PIXI.Graphics();
                    var aAlpha = void 0;
                    if (drag.DataManager.data.debugMode) {
                        aAlpha = 0.5;
                    }
                    else {
                        aAlpha = 0;
                    }
                    if (aPlaceholder.radius != null) {
                        aPixiCircle.lineStyle(3, 0xFF00FF, aAlpha);
                        aPixiCircle.beginFill(0xAA00AA, aAlpha);
                        aPixiCircle.drawCircle(aPlaceholder.loc.x, aPlaceholder.loc.y, aPlaceholder.radius);
						//aPixiCircle.drawCircle(aPlaceholder.loc.x + aPlaceholder.offsetX, aPlaceholder.loc.y + aPlaceholder.offsetY, aPlaceholder.radius);Nathaniel: an alternative calc of arrow position, currently not used
                        aPixiCircle.endFill();
                    }
                    if (aPlaceholder.url) {
                        aArrow = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["arrow"]]);
                        //   aArrow = PIXI.Sprite.from(aPlaceholder.url);
						aArrow.position.set(aPlaceholder.loc.x - aArrow.width / 2, aPlaceholder.loc.y - aArrow.height / 2);
						//Nathaniel: two alternatives to how to calc the positioning of arrow 
						//aArrow.position.set(aPlaceholder.loc.x + aPlaceholder.offsetX, aPlaceholder.loc.y + aPlaceholder.offsetY);
						//aArrow.position.set(aPlaceholder.loc.x, aPlaceholder.loc.y);
                        aArrow.interactive = true;
                        this_1.mGamePanel.addChild(aArrow);
                        aPlaceholder.arrow = aArrow;
						//Nathaniel: supporting changing  hotspot on hover by adding a duplicate hotspot with some difference (i.e. different color)
						aArrowFocus = PIXI.Sprite.from(ocBase.ResourcesManager.recourcesHash[MiniGameBase.data.assets["arrowFocus"]]);
						aArrowFocus.position.set(aPlaceholder.loc.x - aArrow.width / 2, aPlaceholder.loc.y - aArrow.height / 2);
						//aArrowFocus.position.set(aPlaceholder.loc.x + aPlaceholder.offsetX, aPlaceholder.loc.y + aPlaceholder.offsetY);
						//aArrowFocus.position.set(aPlaceholder.loc.x, aPlaceholder.loc.y);
						aArrowFocus.alpha = 0;
						aArrowFocus.interactive = false;
						this_1.mGamePanel.addChild(aArrowFocus);
						aPlaceholder.arrowFocus = aArrowFocus;
                    }
                    if (aPlaceholder.rect != null) {
                        aPixiCircle.lineStyle(3, 0xFF0000, aAlpha);
                        aPixiCircle.beginFill(0xAA0000, aAlpha);
                        aPixiCircle.drawRect(aPlaceholder.loc.x - aPlaceholder.rect.width / 2, aPlaceholder.loc.y - aPlaceholder.rect.height / 2, aPlaceholder.rect.width, aPlaceholder.rect.height);
						//Nathaniel: two alternatives to how to calc the positioning of arrow 
						//aPixiCircle.drawRect(aPlaceholder.loc.x + aPlaceholder.offsetX - aPlaceholder.rect.width / 2, aPlaceholder.loc.y + aPlaceholder.offsetY - aPlaceholder.rect.height / 2, aPlaceholder.rect.width, aPlaceholder.rect.height);
						//aPixiCircle.drawRect(aPlaceholder.loc.x + aPlaceholder.offsetX, aPlaceholder.loc.y + aPlaceholder.offsetY, aPlaceholder.rect.width, aPlaceholder.rect.height);
                        aPixiCircle.endFill();
                    }
                    this_1.mGamePanel.addChild(aPixiCircle);
                    aPixiCircle.interactive = true;
                    aPixiCircle.placeholder = drag.DataManager.data.placeholders[i];
                    aPixiCircle.on('pointerout', function (mouseData) {
                        oc.drag.DragEngine.selectedPlaceholder = null;
                        this.alpha = 1;
						if (aArrow)
						{
							//aArrow.scale.set(1, 1);//Nathaniel: supporting changing  hotspot on hover by adding a duplicate hotspot with some difference (i.e. different color) instead of scaling
							if (aArrowFocus.alpha != 0) //handle a race condition that was turned off by hit, in this case lets leave it off 
								aArrow.alpha = 1;
						}
						if (aArrowFocus)
							aArrowFocus.alpha = 0;
                    });
                    aPixiCircle.on('pointerover', function (mouseData) {
                        oc.drag.DragEngine.selectedPlaceholder = this;
                        this.alpha = 0.3;
						if (aArrow)
						{
							//aArrow.scale.set(1.3, 1.3);//Nathaniel: supporting changing  hotspot on hover by adding a duplicate hotspot with some difference (i.e. different color) instead of scaling
                            aArrow.alpha = 0;
						}
						if (aArrowFocus)
						{
                            aArrowFocus.alpha = 1;
							aArrowFocus.scale.set(1.5, 1.5);
						}
                    });
                    aPixiCircle.on('touchend', function (mouseData) {
                        oc.drag.DragEngine.selectedPlaceholder = null;
                        this.alpha = 1;
                    });
                    aPixiCircle.on('touchmove', function (mouseData) {
                        oc.drag.DragEngine.selectedPlaceholder = this;
                        this.alpha = 0.3;
                    });
                };
                var this_1 = this;
                for (var i = 0; i < drag.DataManager.data.placeholders.length; i++) {
                    _loop_1(i);
                }
            };
            /*override*/
            DragEngine.prototype.destruct = function () {
                this.mDashboard.destruct();
                //this.mPIXIapp.destroy();
            };
            return DragEngine;
        }(oc.drag.pixiExtention.MiniGameBase));
        drag.DragEngine = DragEngine;
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
window.DragEngine = new oc.drag.DragEngine();
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var Item = /** @class */ (function () {
            function Item(pItemParams, pGameParams, pGameManeger) {
                var _this = this;
                this.mDragging = false;
                this.mState = Item.STATE_ACTIVE;
                this.mGameManeger = pGameManeger;
                this.mGameParams = pGameParams;
                this.mBoundRect = new ocBase.math.Rectangle();
                if (pGameParams.movementZone != null) {
                    pGameParams.movementZone.height;
                    this.mBoundRect.left = pGameParams.movementZone.left;
                    this.mBoundRect.right = pGameParams.movementZone.left + pGameParams.movementZone.width;
                    this.mBoundRect.top = pGameParams.movementZone.top;
                    this.mBoundRect.bottom = pGameParams.movementZone.top + pGameParams.movementZone.height;
                }
                this.mPlaceholderIds = pItemParams.placeholderIds;
                var aImagePath = oc.drag.pixiExtention.MiniGameBase.data.assets[pItemParams.type];
                var aJsonPath = oc.drag.pixiExtention.MiniGameBase.data.assets[pItemParams.type + "_json"];
                this.mScore = pItemParams.score;
                this.mItem = new oc.drag.pixiExtention.OcPIXIAnimation(aImagePath, aJsonPath, 0, "idle");
                this.mItem.x = pItemParams.loc.x;
                this.mItem.y = pItemParams.loc.y;
                this.mItem.type = pItemParams.type;
                if (pItemParams.scale) {
                    this.mItem.scale.set(pItemParams.scale.x, pItemParams.scale.y);
					this.mItem.origScale = pItemParams.scale;//Nathaniel: scaling will only occur while hovering, when starting to drag will revert to orig scale
                }
				else
					this.mItem.origScale = {x : 1, y : 1};
                
				this.mStartPoint = pItemParams.loc;
                this.mItem.interactive = true;
                this.mItem.on('mousedown', function (event) { return _this.onDragStart(event); })
                    .on('touchstart', function (event) { return _this.onDragStart(event); })
                    // events for drag end
                    .on('mouseup', function (event) { return _this.onDragEnd(event); })//Nathaniel: to enable printing to console the mouse location on various events
                    .on('mouseupoutside', function (event) { return _this.onDragEnd(event); })
                    .on('touchend', function (event) { return _this.onDragEnd(event); })
                    .on('touchendoutside', function (event) { return _this.onDragEnd(event); })
                    // events for drag move
                    .on('mousemove', function (event) { return _this.onDragMove(event); })
                    .on('touchmove', function (event) { return _this.onDragMove(event); })
                    .on('pointerover', function (event) { return _this.onMouseHover(); })
                    .on('pointerout', function (event) { return _this.onMouseOut(); })
                    .on('pointertap', function (event) { return _this.onMouseClick(); });
                this.mItem.setAnimation(1, "idle", false);
                setTimeout(function () { return _this.play(); }, Math.random() * 2000 + 1000);
            }
            //______________________________________________________
            Item.prototype.play = function () {
                var _this = this;
                if (this.mItem.currentAnimationName == "idle") {
                    this.mItem.replay();
                    setTimeout(function () { return _this.play(); }, Math.random() * 2000 + 1000);
                }
            };
            //______________________________________________________
            Item.prototype.onMouseHover = function () {
                if (this.mItem == null)
                    return;
				//Nathaniel: scaling will only occur while hovering, when starting to drag will revert to orig scale
                //this.mItem.scale.set(this.mItem.scale.x * drag.DataManager.data.itemHoverScaleFactor, this.mItem.scale.y * drag.DataManager.data.itemHoverScaleFactor);
                if ((this.mItem.isInPlace != null) && this.mItem.isInPlace) {
                    this.highlightItems(1, 1, drag.DataManager.data.itemHoverScaleFactor);
                }
                else
                    if (!this.mDragging)
                        this.mItem.scale.set(this.mItem.origScale.x * drag.DataManager.data.itemHoverScaleFactor, this.mItem.origScale.y * drag.DataManager.data.itemHoverScaleFactor);
            };

            Item.prototype.onMouseClick = function () {
                if (this.mItem == null)
                    return;
                if ((drag.isHardening == null) || !drag.isHardening)
                    return;
                if ((this.mItem.isInPlace == null) || !this.mItem.isInPlace)
                    return;
                var currentIndex = DragEngine.mItems.findIndex(function (item) {
                    return ((item.mItem.position.x == this.x) && (item.mItem.position.y == this.y));
                }, this.mItem.position);
                if ((currentIndex == 0) || DragEngine.mItems[currentIndex - 1].mItem.isHardened) {
                    this.mItem.alpha = 1;
                    this.mItem.isHardened = true;
                    document.getElementById("play").play();
                    if (currentIndex == (DragEngine.mItems.length - 1)) {
                        drag.finishedHardening = true;
                    }
                }
            };

            //______________________________________________________
            Item.prototype.onMouseOut = function () {
                if (this.mItem == null)
                    return;
				//Nathaniel: scaling will only occur while hovering, when starting to drag will revert to orig scale
                //this.mItem.scale.set(this.mItem.scale.x / drag.DataManager.data.itemHoverScaleFactor, this.mItem.scale.y / drag.DataManager.data.itemHoverScaleFactor);nathaniel
                if (this.mItem.isInPlace != null && this.mItem.isInPlace)
                    this.highlightItems(this.mGameParams.hitAction.alphaValWhenHit, 1, 1);
                else
                    this.mItem.scale.set(this.mItem.origScale.x, this.mItem.origScale.y);
				//this.mItem.parent.addChild(this.mItem);
            };
            //______________________________________________________
            Item.prototype.onDragStart = function (event) {
                if ((this.mItem != null) && (this.mItem.isInPlace != null) && this.mItem.isInPlace)
                    return;
                this.mEventData = event.data;
                if (this.mItem.parent != null) {
					//Nathaniel: scaling will only occur while hovering, when starting to drag will revert to orig scale
					//this.mItem.scale.set(this.mItem.scale.x / drag.DataManager.data.itemHoverScaleFactor, this.mItem.scale.y / drag.DataManager.data.itemHoverScaleFactor);
					this.mItem.scale.set(this.mItem.origScale.x, this.mItem.origScale.y);
                }
                this.mDragging = true;
            };
            //______________________________________________________
            Item.prototype.onDragEnd = function (event) {
                if ((this.mItem != null) && (this.mItem.isInPlace != null) && this.mItem.isInPlace)
                    return;
				if (event != null)//Nathaniel: printing to console the mouse location on various events. The numbers used should realy be taken from params, but currently this is quick and dirty, and not sure why need the 8
					console.log((event.data.originalEvent.clientX-960+50+8) + "," + (event.data.originalEvent.clientY-460+30));
                var _this = this;
                if (!this.mDragging) {
                    return;
                }
                this.mDragging = false;
                this.mEventData = null;
                this.mTargetLoc = this.mGameManeger.isInTarget(this);
                if (this.mTargetLoc != null) {
                    var trial = new Object();
                    trial.isCorrect = this.mTargetLoc.hit;
                    trial.response = this.mItem.type;
                    trial.question = { type: this.mTargetLoc.id, x: this.mTargetLoc.x, y: this.mTargetLoc.y }
                    if (this.mTargetLoc.hit == false) {
                        this.setMiss(trial);
                    }
                    else {
                        this.setHit(trial);
                        this.mGameManeger.getSetPanel().addChild(this.mItem);
                        this.mItem.interactive = true;
                        //this is where the arrow was located. Will be used in the hardening phase, in order to enforce the order of hardening
                        this.mItem.placeholder = { x: this.mTargetLoc.placeholderX, y: this.mTargetLoc.placeholderY }
                    }
                }
                else {
                    this.mItem.setAnimation(1, "miss", false, function () { return _this.removeFromScreenOnMiss(); });
                    this.setAction(this.mGameParams.missAction);
                }
            };
            //_____________________________________________________
            Item.prototype.highlightItems = function (pAlpha, pAlphaOfHardened, scaleFactor) {
                var items = this.mItem.parent.children;
                for (var i = 0; i < items.length; i++) {
                    if ((items[i].animName != null) && ((items[i].mAnimName == this.mItem.mAnimName) || arePairs(items[i].mAnimName, this.mItem.mAnimName))) {
                        items[i].alpha = items[i].isHardened ? pAlphaOfHardened : pAlpha;
                        items[i].scale.set(items[i].origScale.x * scaleFactor, items[i].origScale.y * scaleFactor);
                    }
                }
            };
            arePairs = function (pSymbol1, pSymbol2) {
                if (pSymbol1.includes("quotebegin")) return (pSymbol2.includes("quoteend"));
                if (pSymbol1.includes("quoteend")) return (pSymbol2.includes("quotebegin"));
                if (pSymbol1.includes("speakbegin")) return (pSymbol2.includes("speakend"));
                if (pSymbol1.includes("speakend")) return (pSymbol2.includes("speakbegin"));
                if (pSymbol1.includes("qushia")) return (pSymbol2.includes("resolution"));
                if (pSymbol1.includes("resolution")) return (pSymbol2.includes("qushia"));
                return false;
            }
            //_____________________________________________________
            Item.prototype.setAction = function (pAction) {
                var aSnap = pAction.snapToPoint;
                if (aSnap != null) {
                    for (var i = 0; i < aSnap.length; i++) {
                        Tweener.addTween(this.mItem, { x: this.mTargetLoc.x, y: this.mTargetLoc.y,
                            time: aSnap[i].time, delay: aSnap[i].delay, transition: aSnap[i].transition });
                    }
                }
                var aBackToStartPoint = pAction.backToStartPoint;
                if (aBackToStartPoint != null) {
                    for (var i = 0; i < aBackToStartPoint.length; i++) {
                        Tweener.addTween(this.mItem, { x: this.mStartPoint.x, y: this.mStartPoint.y,
                            time: aBackToStartPoint[i].time, delay: aBackToStartPoint[i].delay,
                            transition: aBackToStartPoint[i].transition });
                    }
                }
                var aShrink = pAction.shrink;
                if (aShrink != null) {
                    for (var i = 0; i < aShrink.length; i++) {
                        Tweener.addTween(this.mItem.scale, { x: aShrink[i].toValue, y: aShrink[i].toValue,
                            time: aShrink[i].time, delay: aShrink[i].delay, transition: aShrink[i].transition });
                    }
                }
                var aAlpha = pAction.alpha;
                if (aAlpha != null) {
                    for (var i = 0; i < aAlpha.length; i++) {
                        Tweener.addTween(this.mItem, { alpha: aAlpha[i].toValue,
                            time: aAlpha[i].time, delay: aAlpha[i].delay, transition: aAlpha[i].transition });
                    }
                }
                var aRotation = pAction.rotate;
                if (aRotation != null) {
                    for (var i = 0; i < aRotation.length; i++) {
                        var aToRotation = this.mItem.rotation + aRotation[i].toValue;
                        Tweener.addTween(this.mItem, { rotation: aToRotation, time: aRotation[i].time, delay: aRotation[i].delay, transition: aRotation[i].transition });
                    }
                }
            };
            //_____________________________________________________
            Item.prototype.setHit = function (pTrial) {
                var _this = this;
                /*if (this.mGameParams.hitAction.isStickToPlace) {
                    this.mItem.interactive = false;
                }*/
                this.mItem.isInPlace = true;
                this.mItem.isHardened = false;
                this.mItem.setAnimation(1, "hit", false, function () { return _this.removeFromScreenOnHit(); });
                drag.Dashboard.instance.gotAnItem(true, pTrial);
                this.setAction(this.mGameParams.hitAction);
            };
            //______________________________________________________
            Item.prototype.setMiss = function (pTrial) {
                var _this = this;
                this.mItem.setAnimation(1, "miss", false, function () { return _this.removeFromScreenOnMiss(); });
                this.setAction(this.mGameParams.missAction);
                drag.Dashboard.instance.gotAnItem(false, pTrial);
            };
            //_____________________________________________________
            Item.prototype.removeFromScreenOnMiss = function () {
                if (!this.mGameParams.missAction.isToRemove) {
                    return;
                }
                if (this.mItem.parent != null) {
                    this.mItem.parent.removeChild(this.mItem);
                }
                this.mState = Item.STATE_MISS;
            };
            //_____________________________________________________
            Item.prototype.removeFromScreenOnHit = function () {
                if (!this.mGameParams.hitAction.isToRemove) {
                    return;
                }
                if (this.mItem.parent != null) {
                    this.mItem.parent.removeChild(this.mItem);
                }
                this.mState = Item.STATE_HIT;
            };
            //_____________________________________________________
            Item.prototype.onDragMove = function (event) {
                if (drag.DataManager.data.showMousePos) {
                    var aEventData = event.data;
                    var aConsolPosPos = aEventData.getLocalPosition(this.mItem.parent);
                    console.log(aConsolPosPos.x - drag.DataManager.data.offsetX, aConsolPosPos.y - drag.DataManager.data.offsetY);
                }
                if (this.mDragging) {
                    var newPosition = this.mEventData.getLocalPosition(this.mItem.parent);
                    this.mItem.position.x = newPosition.x;
                    this.mItem.position.y = newPosition.y;
                    if (event.type == "touchmove")
                        this.mItem.position.y = newPosition.y - 200;
                    if (this.mGameParams.missAction.isToRemove) {
                        if ((this.mBoundRect != null) && (!this.mBoundRect.intersectsPoint(this.mItem.x, this.mItem.y))) {
                            if (this.mItem.y > this.mBoundRect.bottom) {
                                var aH = 10;
                                if (this.mItem.spriteSheet != null) {
                                    aH = this.mItem.spriteSheet.height / 4;
                                }
                                this.mItem.y = this.mBoundRect.bottom - aH;
                            }
                            this.onDragEnd();
                        }
                    }
                }
            };

            Object.defineProperty(Item.prototype, "item", {
                //______________________________________________________
                get: function () {
                    return this.mItem;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Item.prototype, "state", {
                //_____________________________________________________
                get: function () {
                    return this.mState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Item.prototype, "display", {
                //_____________________________________________________
                get: function () {
                    return this.mItem;
                },
                enumerable: true,
                configurable: true
            });
            //______________________________________________________
            Item.prototype.tick = function (delta) {
                this.mItem.update();
            };
            Object.defineProperty(Item.prototype, "placeholderIds", {
                //______________________________________________________
                get: function () {
                    return this.mPlaceholderIds;
                },
                enumerable: true,
                configurable: true
            });
            //______________________________________________________
            Item.prototype.destract = function () {
                this.mItem.destroy();
            };
            Item.STATE_ACTIVE = "ACTIVE_STATE";
            Item.STATE_MISS = "MISS_STATE";
            Item.STATE_HIT = "HIT_STATE";
            return Item;
        }());
        drag.Item = Item;
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../../ts_maps/jquery.d.ts" />
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var pixiExtention;
        (function (pixiExtention) {
            var OcPIXIAnimation = /** @class */ (function (_super) {
                __extends(OcPIXIAnimation, _super);
                function OcPIXIAnimation(pImageURL, pJsonURL, pCurrFrame, pAnimationName) {
                    var _this = _super.call(this) || this;
                    _this.isContinuePlaying = false;
                    _this.mAnimName = "";
                    _this.mIsToPlay = true;
                    _this.mAnimName = pImageURL;
                    if (pCurrFrame == null) {
                        pCurrFrame = 0;
                    }
                    var aType = pImageURL.substr(pImageURL.lastIndexOf('.') + 1);
                    if (aType == "text") {
                        var aTEXT = pImageURL.substr(0, pImageURL.lastIndexOf('.'));
                        var aTextSize = void 0;
                        if (drag.DataManager.data.itemTextSize) {
                            aTextSize = drag.DataManager.data.itemTextSize;
                        }
                        else {
                            aTextSize = 32;
                        }
                        var aText = new PIXI.Text(aTEXT, { fontFamily: 'Arial', fontSize: aTextSize, fill: 0xe1c699, align: 'center', stroke: "0x000000", strokeThickness: 3 });
                        aText.anchor.set(0.5, 0.5);
                        _this.addChild(aText);
                        return _this;
                    }
                    _this.mSpriteSheet = new pixiExtention.SpriteSheet(pImageURL, 12);
                    _this.mSpriteSheet.scale.set(0.9, 0.9);
                    _this.addChild(_this.mSpriteSheet);
                    _this.mSpriteSheet.data = ocBase.ResourcesManager.recourcesHash[pJsonURL];
                    if (pCurrFrame != 0) {
                        _this.mCurrFrame = pCurrFrame + 1;
                        _this.isContinuePlaying = true;
                    }
                    else {
                        _this.mCurrFrame = 0;
                        _this.isContinuePlaying = false;
                    }
                    return _this;
                }
                //_____________________________________________________
                OcPIXIAnimation.prototype.setAnimation = function (pCurrFrame, pAnimationName, pLoop, pCallback) {
                    if (pLoop === void 0) { pLoop = false; }
                    if (this.mSpriteSheet == null) {
                        return;
                    }
                    this.mSpriteSheet.setAnimation(pAnimationName, this.mCurrFrame, pLoop, pCallback);
                };
                Object.defineProperty(OcPIXIAnimation.prototype, "currentAnimationName", {
                    //_____________________________________________________
                    get: function () {
                        if (this.mSpriteSheet == null) {
                            return;
                        }
                        return this.mSpriteSheet.currentAnimationName;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OcPIXIAnimation.prototype, "play", {
                    get: function () {
                        return this.mIsToPlay;
                    },
                    //_____________________________________________________
                    set: function (pPlay) {
                        this.mIsToPlay = pPlay;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OcPIXIAnimation.prototype, "currFrame", {
                    get: function () {
                        return this.mCurrFrame;
                    },
                    //_____________________________________________________
                    set: function (pCurrFrame) {
                        this.mCurrFrame = pCurrFrame;
                    },
                    enumerable: true,
                    configurable: true
                });
                //______________________________________________________________________________
                OcPIXIAnimation.prototype.replay = function () {
                    if (this.mSpriteSheet == null) {
                        return;
                    }
                    this.mSpriteSheet.replay();
                };
                //_____________________________________________________
                OcPIXIAnimation.prototype.onRady = function (pSpriteSheet) { };
                //______________________________________________________________________________
                // update the animation sequence by the sprite movement on x and y axis
                OcPIXIAnimation.prototype.update = function () {
                    if (this.mSpriteSheet == null) {
                        return;
                    }
                    if (this.mIsToPlay) {
                        this.mSpriteSheet.update();
                    }
                };
                Object.defineProperty(OcPIXIAnimation.prototype, "spriteSheet", {
                    //____________________________________________________
                    // return the image SpriteSheet
                    get: function () {
                        return this.mSpriteSheet;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OcPIXIAnimation.prototype, "animName", {
                    //_____________________________________________________\
                    get: function () {
                        return this.mAnimName;
                    },
                    enumerable: true,
                    configurable: true
                });
                return OcPIXIAnimation;
            }(PIXI.Sprite));
            pixiExtention.OcPIXIAnimation = OcPIXIAnimation;
        })(pixiExtention = drag.pixiExtention || (drag.pixiExtention = {}));
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
/// <reference path="../../../packages/pixi.js.d.ts" />
var oc;
(function (oc) {
    var drag;
    (function (drag) {
        var pixiExtention;
        (function (pixiExtention) {
            var SpriteSheet = /** @class */ (function (_super) {
                __extends(SpriteSheet, _super);
                function SpriteSheet(pPath, pFrameRate) {
                    var _this = _super.call(this) || this;
                    _this.mCounter = 0;
                    _this.mFirstToPlay = 0;
                    _this.mLastFrameTime = -1;
                    SpriteSheet.mInstanceCounter++;
                    _this.mCounter = 0;
                    _this.mFrameRate = 1000 / pFrameRate;
                    _this.mPath = pPath;
                    return _this;
                }
                Object.defineProperty(SpriteSheet.prototype, "data", {
                    //______________________________________________________________________________
                    get: function () {
                        return this.mJSONObj;
                    },
                    //______________________________________________________________________________
                    set: function (pData) {
                        this.mJSONObj = pData;
                        var aImage = ocBase.ResourcesManager.recourcesHash[this.mPath];
                        var aBase = new PIXI.BaseTexture(aImage);
                        this.mTexture = new PIXI.Texture(aBase);
                        this.mSprite = new PIXI.Sprite(this.mTexture);
                        if (pData == undefined) {
                            this.mSprite.x = -aImage.width / 2;
                            this.mSprite.y = -aImage.height / 2;
                            this.addChild(this.mSprite);
                            this.mSprite.visible = true;
                            return;
                        }
                        this.mPrix = this.getPrix();
                        var aStrInJSON = this.mPrix + "000";
                        this.mDeltaX = -Number(this.mJSONObj.frames[aStrInJSON].sourceSize.w) / 2;
                        this.mDeltaY = -Number(this.mJSONObj.frames[aStrInJSON].sourceSize.h) / 2;
                        this.update();
                        this.addChild(this.mSprite);
                        this.mSprite.visible = true;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SpriteSheet.prototype, "currentFrame", {
                    //________________________________________________________________________
                    get: function () {
                        return this.mCurrentFrame;
                    },
                    enumerable: true,
                    configurable: true
                });
                //________________________________________________________________________
                SpriteSheet.prototype.onJsonLoad = function (pObject) {
                    this.mJSONObj = JSON.parse(pObject);
                    this.mPrix = this.getPrix();
                    this.mTotalFrames = 0;
                    for (var key in this.mJSONObj.frames) {
                        this.mTotalFrames++;
                    }
                    this.mLoadedCallback(this);
                    var aStrInJSON = this.mPrix + "000";
                    this.mDeltaX = Number(this.mJSONObj.frames[aStrInJSON].sourceSize.w);
                    this.mDeltaY = Number(this.mJSONObj.frames[aStrInJSON].sourceSize.h);
                    this.update();
                    this.addChild(this.mSprite);
                    this.mSprite.visible = true;
                };
                //____________________________________________________________________
                SpriteSheet.prototype.getPrix = function () {
                    for (var key in this.mJSONObj.frames) {
                        return (key.substring(0, key.length - 3));
                    }
                };
                //______________________________________________________________________________
                SpriteSheet.prototype.getTotalFrames = function () {
                    var aCount = 0;
                    for (var key in this.mJSONObj.frames) {
                        aCount++;
                    }
                    return aCount;
                };
                //
                //______________________________________________________________________________
                SpriteSheet.prototype.update = function () {
                    if (this.mJSONObj == undefined) {
                        return;
                    }
                    if (this.mFirstToPlay < 0) {
                        return;
                    }
                    var aTime = Date.now();
                    if (aTime - this.mLastFrameTime < this.mFrameRate) {
                        return;
                    }
                    this.mSprite.visible = true;
                    //this.mSprite.anchor.set(0.5, 0.5);
                    this.mLastFrameTime = aTime;
                    this.mCurrentFrame = Math.floor(this.mFirstToPlay + this.mCounter);
                    if ((this.mCurrentFrame == this.mLastToPlay) && (!(this.mIsInLoop))) {
                        if (this.mEndAnimationCallback != null) {
                            this.mEndAnimationCallback(this);
                            this.mEndAnimationCallback = null;
                        }
                        return;
                    }
                    var aStrInJSON = this.mPrix + String(Math.floor((this.mFirstToPlay + this.mCounter) / 100)) + String(Math.floor((this.mFirstToPlay + this.mCounter) / 10)) + String(Math.floor((this.mFirstToPlay + this.mCounter) % 10));
                    //console.log(aStrInJSON);
                    var aStartX = Number(this.mJSONObj.frames[aStrInJSON].frame.x);
                    var aStartY = Number(this.mJSONObj.frames[aStrInJSON].frame.y);
                    var aWidth = Number(this.mJSONObj.frames[aStrInJSON].frame.w);
                    var aHeight = Number(this.mJSONObj.frames[aStrInJSON].frame.h);
                    var aPosX = Number(this.mJSONObj.frames["" + aStrInJSON].spriteSourceSize.x);
                    var aPosY = Number(this.mJSONObj.frames["" + aStrInJSON].spriteSourceSize.y);
                    this.mTexture.frame = new PIXI.Rectangle(aStartX, aStartY, aWidth, aHeight);
                    this.mSprite.x = aPosX + this.mDeltaX;
                    this.mSprite.y = aPosY + this.mDeltaY;
                    this.mCounter = Math.floor((this.mCounter + 1) % (this.mLastToPlay - this.mFirstToPlay + Number(this.mFirstToPlay != 0)));
                };
                //______________________________________________________________________________
                SpriteSheet.prototype.replay = function () {
                    this.mCurrentFrame = this.mFirstToPlay;
                    this.mCounter = 0;
                };
                //______________________________________________________________________________
                SpriteSheet.prototype.setAnimation = function (pAnimation, pCurrFrame, pIsInLoop, pCallback) {
                    this.mEndAnimationCallback = pCallback;
                    if (this.mJSONObj == null) {
                        return false;
                    }
                    this.mCurrentAnimation = pAnimation;
                    if (this.mJSONObj.meta.animations == null) {
                        this.mFirstToPlay = 0;
                        this.mLastToPlay = this.mTotalFrames = this.getTotalFrames();
                        pIsInLoop = true;
                    }
                    else {
                        if (this.mJSONObj.meta.animations[pAnimation] == null) {
                            return;
                        }
                        this.mFirstToPlay = Number(this.mJSONObj.meta.animations[pAnimation][0]);
                        this.mLastToPlay = Number(this.mJSONObj.meta.animations[pAnimation][1]);
                    }
                    this.mCounter = pCurrFrame - this.mFirstToPlay;
                    if ((pCurrFrame == 0) || (pCurrFrame == undefined) || (pCurrFrame == this.mLastToPlay) || (this.mCounter < 0)) {
                        this.mCounter = 0;
                    }
                    this.mIsInLoop = pIsInLoop;
                    console.log(" setAnimation >> " + SpriteSheet.mInstanceCounter + " >> mFirstToPlay: " + this.mFirstToPlay + ", mLastToPlay: " + this.mLastToPlay + ", animation: " + pAnimation + ", pCurrFrame: " + pCurrFrame);
                    return true;
                };
                //______________________________________________________________________________
                SpriteSheet.prototype.displayAnim = function (pFirst, pLast) {
                    this.mCounter = 0;
                    this.mFirstToPlay = pFirst;
                    this.mLastToPlay = pLast;
                };
                Object.defineProperty(SpriteSheet.prototype, "currentAnimationName", {
                    //_________________________________________________________________
                    get: function () {
                        return this.mCurrentAnimation;
                    },
                    enumerable: true,
                    configurable: true
                });
                SpriteSheet.mInstanceCounter = 0;
                return SpriteSheet;
            }(PIXI.Container));
            pixiExtention.SpriteSheet = SpriteSheet;
        })(pixiExtention = drag.pixiExtention || (drag.pixiExtention = {}));
    })(drag = oc.drag || (oc.drag = {}));
})(oc || (oc = {}));
//# sourceMappingURL=DragEngine.js.map



$("#ModalSuccess").on("hidden.bs.modal", function () {
    window.history.back();
});

$("#ModalFailure").on("hidden.bs.modal", function () {
    location.reload();
});